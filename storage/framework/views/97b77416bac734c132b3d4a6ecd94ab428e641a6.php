<!DOCTYPE HTML>
<html lang="en" dir="ltr">

<head>
    <link rel="icon" type="image/x-icon" href="<?php echo e(_asset(Cache::get('favicon', 'images/favicon.ico'))); ?>" />
    <title>
        <?php echo e(((isset($data['title']) && trim($data['title']) != "") ? $data['title']." | " : ''). Cache::get('app_name', get('name'))); ?>

        <?php
            $title = $data['title'];
        ?>
    </title>
    <meta charset="utf-8">
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="cache-control" content="max-age=604800" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ValuePac">
    <meta name="copyright" content="">
    <meta name="keywords" content="<?php echo e(Cache::get('common_meta_keywords', '')); ?>">
    <meta name="description" content="<?php echo e(Cache::get('common_meta_description', '')); ?>">

    <link href="<?php echo e(theme('css/ui.css')); ?>" rel="stylesheet" type="text/css" />
    
    <link href="<?php echo e(theme('css/custom.css')); ?>" rel="stylesheet" type="text/css" />
    
    <link href="<?php echo e(theme('css/plugins.css')); ?>" rel="stylesheet" type="text/css" />
    
    <link href="<?php echo e(theme('css/responsive.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(theme('css/stepper.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(theme('css/calender.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(theme('css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(theme('css/jquery-ui.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(theme('css/intlTelInput.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(theme('css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(theme('css/swiper-bundle.min.css')); ?>" rel="stylesheet" type="text/css" />
    
    <link href="<?php echo e(theme('fonts/fontawesome/css/all.min.css')); ?>" rel="stylesheet" type="text/css" />
<link rel="preconnect" href="https://fonts.gstatic.com">
    

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"/>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css"/>

    <script src="<?php echo e(theme('js/jquery-3.5.1.min.js')); ?>"></script>
    <script src="<?php echo e(theme('js/popper.js')); ?>"></script>
    <script src="<?php echo e(theme('js/bootstrap.min.js')); ?>"></script>
    
    <!-- Bootbox -->
    <script src="<?php echo e(theme('js/bootbox.min.js')); ?>"></script>
    <script src="<?php echo e(theme('js/bootbox.locales.min.js')); ?>"></script>
    <!-- Bootbox end -->

    <script src="<?php echo e(theme('js/jquery-ui.min.js')); ?>"></script>
    <script src="<?php echo e(theme('js/intlTelInput.js')); ?>"></script>
    <script src="<?php echo e(theme('js/select2.min.js')); ?>"></script>
    <script src="<?php echo e(theme('js/swiper-bundle.min.js')); ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="https://raw.githack.com/peet86/Ratyli/master/jquery.ratyli.min.js"></script>
    
    <script>
        var home = "<?php echo e(get('home_url')); ?>";
    </script>
    <script src="<?php echo e(asset('js/script.js')); ?>"></script>

    <link href="<?php echo e(theme('css/cart.css')); ?>" rel="stylesheet" type="text/css" />
    <script>
        window.addEventListener('offline', (e) => {
            $("#internet_exists").css('display','none');
            $("#no-internet").css('display','block');
        });

        if (navigator.onLine) { 
            $("#internet_exists").css('display','block');
                $("#no-internet").css('display','none');
        }

            function nextInList(event) {
        switch (event.which) {
            case 40:
                $(".res_0").css("background-color","green");
                break;
            default:
                break;
        }
    }

    </script>
    
    <script>

        window.addEventListener("load", function () {
          $('#status').fadeOut(); // will first fade out the loading animation 
          $('#preloader').delay(350).fadeOut(); // will fade out the white DIV that covers the website.     
           document.getElementById('internet_exists').style.display = 'block'; 
        });

        $(document).ready(function(){
            $('.dropdown').mouseenter(function(){ 
              $('.dropdown-toggle', this).trigger('click'); 
            });
            $('.dropdown').mouseleave(function(){ 
              $('.dropdown-toggle', this).trigger('click'); 
            });
        });
    </script>
    
    <style>
        .reloadBTN{
            width: 17%;
            padding: 15px 14px;
            border: none;
            background: blue;
            color: white;
            font-size: 18px;
            text-transform: uppercase;
            cursor: pointer;            
        }
        
        .header-top-right{
            display:flex;
            justify-content: flex-end;
            align-items: center;
        }
        .header_whatsapp_icon{
            margin-right:60px;
        }
        
        .header_whatsapp_icon>p{
            color:#000;
        }
        
        .header_whatsapp_icon>p>i{
            font-size:25px;
            color:#00e676;
        }

        #preloader {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: white;
            z-index: 99999;
            height: 100%;
        }
        #status {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            /*margin-top: -149px;*/
            /*margin-left: -200px;    */
            /*padding: 0;*/
        }
        .selected {
    background: white;
}
    .checkbtn{
        text-transform: uppercase;
        font-weight: 700;
        padding: 4px 10px;
        color: #ec141e;
        border: 1px solid #ced4da;
    }
    </style>

</head>

<body>
    <div id="no-internet" style="display: none">
    <center>
        <br>
        <br>
        <h1>No Internet</h1>
    <script src="<?php echo e(theme('js/lottie.js')); ?>"></script>
<lottie-player src="<?php echo e(theme('js/lottie-icon.json')); ?>"  background="transparent"  speed="1"  style="width: 300px; height: 300px;"  loop  autoplay></lottie-player>
    <h3>You're Offline</h3>
    <p style="width: 25%">No internet or Slow Connection found.
Check your connection or Try again</p>
    <button class="reloadBTN" onclick="location.reload();">Reload</button>
    </center>
    </div>

    <div id="preloader">
   <div id='status'>
      <img alt='' src="<?php echo e(theme('images/preloader3.gif')); ?>"/>
   </div>    	
    </div>


    <div id="internet_exists" class="page-wrapper-main">


    <div class="mobile_overlay"></div>
    <div class="mobile_menu">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    
                    <div class="mobile_wrapper">
                        <div class="bar_close">
                            <a href="#"><i class="fas fa-times"></i></a>
                        </div>
                        <div class="freeshipping">
                            <p><?php echo e(__('msg.you_can_get_free_delivery_by_shopping_more_than')); ?> <?php echo e(get_price(Cache::get('min_amount'))); ?></p>
                        </div>

                        <?php if(Cache::has('social_media') && is_array(Cache::get('social_media')) && count(Cache::get('social_media'))): ?>
                        <div class="header_social_icon text-center">
                            <ul>
                                <?php $__currentLoopData = Cache::get('social_media'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li class="social-icon">
                                        <a target="_blank" href="<?php echo e($c->link); ?>"><em class="fab <?php echo e($c->icon); ?>"></em></a>
                                    </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                        <?php endif; ?>
                        <?php if(trim(Cache::get('support_number', '')) != ''): ?>
                        <div class="header_call-support">
                            <p><a href="#"><?php echo e(Cache::get('support_number')); ?></a> Customer Support</p>
                        </div>
                        <?php endif; ?>
                        <div id="menu" class="text-left ">
                            <ul class="header_main_menu">
                            <?php if(Cache::has('categories') && is_array(Cache::get('categories')) && count(Cache::get('categories'))): ?>
                            <?php $__currentLoopData = Cache::get('categories'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ca): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($ca->is_header == 1): ?>
                                <li class="header_submenu_item"><a href="<?php echo e(route('shop', ['category' => $ca->slug])); ?>"><?php echo e($ca->name); ?></a></li>
                            <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>

<!--                                 <li class="header_submenu_item active">
                                    <a href="<?php echo e(route('home')); ?>">Home </a>
                                </li>
 -->

<!--                                 <li class="header_submenu_item">
                                    <a href="<?php echo e(route('shop')); ?>"> Shop</a>
                                </li>
 -->
                            </ul>
                        </div>
                        <div class="offcanvas_footer">
                            <span><a href="#"><i class="fa fa-envelope"></i> <?php echo e(Cache::get('support_email')); ?></a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--offcanvas menu area end-->
    

    <header class="shadow-sm bg-white">
        <div class="main_header">
            <div class="header_top">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-md-6">

                            <div class="freeshipping">
                                <!--<p><?php echo e(__('msg.you_can_get_free_delivery_by_shopping_more_than')); ?> <?php echo e(get_price(Cache::get('min_amount'))); ?></p>-->
                                <p style="font-weight: 700; color: #ec1922; text-transform:capitalize;">Only best flat rates, No misleading offers.</p>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="header-top-right">
                                
                                <!--<div class="header_whatsapp_icon text-right">-->
                                <!--    <p><i class="fab fa-whatsapp"></i> Whatsapp :+91 9407322212</p>-->
                                <!--</div>-->
                                
                                
                                <?php if(trim(Cache::get('support_number', '')) != ''): ?>
                                    <div class="header_call-support">
                                        <p><a href="#"><span><i class="fab fa-whatsapp"></i></span>Whatsapp :+91 9407322212</a></p>
                                    </div>
                                    <div class="header_call-support">
                                        <p><a href="#"><i class="fas fa-phone-volume"></i><?php echo e(Cache::get('support_number')); ?></a></p>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
            
        </div>
    </header>
    
    
    <nav class="shadow-sm bg-white top-sticky-nav" style="">
        <div class="secondheader py-1">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-2 col-md-12 col-sm-12 col-12">
                            <div class="bar_open">
                                <a href="#"><i class="fas fa-bars"></i></a>
                            </div>
                            <div class="logo">
                                <a href="<?php echo e(route('home')); ?>"><img src="<?php echo e(_asset(Cache::get('web_logo'))); ?>" alt="logo"></a>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-12 col-sm-12 col-12">
                            <div class="header_right">
                                <div class="header_search mobile_screen_none">
                                    <form action="<?php echo e(route('psearch')); ?>" method="post" id="searchFormNew">
                                        <?php echo csrf_field(); ?>
                                        <?php
                                            $categories = Cache::get('categories', []);
                                        ?>
                                        <div class="header_hover_category">
                                            <select class="select_option" name="category" id="cat_select">
                                                <option selected value="">All Category</option>
                                                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i=>$c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($c->slug); ?>"><?php echo e($c->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>

                                        <div class="header_search_box">
                                            <input type="text" class="form-control top-search" name="keyword" placeholder="Search Product..." id="searchInput" autocomplete="off">
                                            <button type="button" id="searchButton"><i class="fas fa-search"></i></button>
                                        </div>
                                    </form>
                                    
                                    <div id="searchresult">
                                    	<ul id="ul_results"></ul>
                                    </div>
                                </div>
                                <div class="header_account_area">
                                    <div class="header_account_list register">

                                        <ul>
                                            <?php if(isLoggedIn()): ?>
                                            <li><a href="<?php echo e(route('my-account')); ?>"><span class="top-bar-text"><?php echo (isset(session()->get('user')['name'])) ? session()->get('user')['name'] : ''; ?>, </span><i class="far fa-user"></i></a></li>

                                            <?php else: ?>
                                                <li><a href="<?php echo e(route('login')); ?>"><span class="top-bar-text">Sign In / Register</span> <i class="far fa-user"></i></a></li>
                                            <?php endif; ?>
                                            
                                            <li>
                                                <div class="dropdown">
                                                      <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        More
                                                      </button>
                                                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                        
                                                        <?php if(isLoggedIn()): ?>
<!--                                                             <a class="dropdown-item" href="<?php echo e(route('favourite')); ?>"><i class="fas fa-heart fa-sm"></i> Wishlist</a>
 -->
                                                            <a class="dropdown-item" href="<?php echo e(route('favourite')); ?>">
<img src="<?php echo e(asset('new_icons/my_wishlist.png')); ?>" class="img-fluid" style="width: 20px" alt=""> 
                                                                 Wishlist</a>

                                                        <?php endif; ?>
                                                            <!-- <a class="dropdown-item" href="<?php echo e(route('store')); ?>"><i class="fas fa-store fa-sm"></i> Our Stores</a>                                                         -->
                                                            <a class="dropdown-item" href="<?php echo e(route('store')); ?>">
                                                               <img src="<?php echo e(asset('new_icons/store.png')); ?>" class="img-fluid" style="width: 20px" alt="">   Our Stores</a>                                                            
                                                      </div>
                                                </div>
                                                
                                            </li>
                                            <?php if(isLoggedIn()): ?>
                                            <li>

                                                        <a href="javascript:void(0)">
                                                            <button type="button" id="list_item_modal_show" class="btn btn-top-list p-0">
                                                            <img src="<?php echo e(asset('new_icons/kirana_list.png')); ?>" class="img-fluid" style="width: 20px" alt=""> 
                                                        </button>
                                                        <input type="hidden" id="list_item_url" value="<?php echo e(route('check-user-login')); ?>">
                                                        </a>

                                            </li>
                                            <?php endif; ?>
                                            <!--    <button type="button" id="list_item_modal_show" class="btn btn-top-list"><i class="fas fa-clipboard-list"></i></button>-->
                                            <!--    <input type="hidden" id="list_item_url" value="<?php echo e(route('check-user-login')); ?>">-->
                                            <!--</li>-->

                                        
                                        </ul>
                                    </div>
                                    <!--<div class="header_account_list header_wishlist">-->
                                    <!--    <?php if(isLoggedIn()): ?>-->
                                    <!--        <a href="<?php echo e(route('favourite')); ?>"><i class="fas fa-heart fa-sm"></i></a>-->
                                    <!--    <?php endif; ?>-->
                                    <!--</div>-->
                                    <div class="header_account_list">
                                        <a href="<?php echo e(route('cart')); ?>"><i class="fas fa-shopping-cart fa-sm"></i></a>
                                        <?php if(isset($data['cart']['cart']) && is_array($data['cart']['cart']) && count($data['cart']['cart'])): ?>
                                            <span class="item_count"><?php echo e(count($data['cart']['cart'])); ?></span>
                                        <?php endif; ?>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            
            <div class="header_bottom sticky-header" >
                <div class="container">
                    <div class="row align-items-center positionheader">
                        <div class="col-12 col-md-6 mobile_screen_block">
                            <div class="header_search">
                                <!--<form action="#">-->
                                    
                                    <form action="<?php echo e(route('psearch')); ?>" method="post" id="searchFormNewMobile">
                                        <?php echo csrf_field(); ?>
                                        <?php
                                            $categories = Cache::get('categories', []);
                                        ?>
                                        <div class="header_hover_category">
                                            <select class="select_option" name="category" id="cat_select_mobile">
                                                <option selected value="">All Category</option>
                                                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i=>$c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($c->slug); ?>"><?php echo e($c->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                        <div class="header_search_box">
                                            <input type="text" class="form-control top-search" name="keyword" placeholder="Search Product..." id="searchInputMobile" autocomplete="off">
                                            <button type="button" id="searchButtonMobile"><i class="fas fa-search"></i></button>
                                        </div>
                                    </form>
                                    
                                    <div id="searchresultMobile">
                                    	<ul id="ul_resultsMobile"></ul>
                                    </div>

                            </div>
                        </div>
                        <div class="col-lg-1" style="display:none">
<div style="display: flex;color: white;justify-content: space-between;width: 100%;cursor: pointer;" data-toggle="modal" data-target="#checkDeliveryModal">
                            <i class="fas fa-map-marker-alt" style="padding-top: 10px;"></i>
                            <p style="
    color: white;
    line-height: 18px;
    font-size: 14px;
">Deliver to<br>
<span style="font-weight: bold;">
<?php if(!isset(session()->get('user')['user_id'])): ?>
	462001
<?php else: ?>
    <?php if(isset(session()->get('user')['dpincode'])): ?>
	<?php echo e(session()->get('user')['dpincode']); ?>


    <?php else: ?>
    462001
    <?php endif; ?>
<?php endif; ?>
</span>
</p>                                
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="header_categories_menu">
                                <?php
                                    $categories = Cache::get('categories', []);
                                    $maxProductToShow = 10;
                                    $totalCategories = count($categories);
                                ?>

<!--                                 <div class="categories_title">
                                    <h2 class="category_toggle">All Categories</h2>
                                    <i class="fas fa-chevron-down fa-xs"></i>
                                </div>

 -->                            <div class="categories_title_new">
                                    <h2 class="category_toggle"><span><i class="fas fa-list"></i></span> All Departments</h2>
                                    <i class="fas fa-chevron-down fa-xs"></i>
                                </div>


                                <div class="header_categories_toggle">
                                    <ul>
                                        <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if(isset($c->childs) && count((array)$c->childs)): ?>
                                                <li class="header_menu_item <?php echo e($k >= $maxProductToShow ? 'hidden' : ''); ?>"><a><?php echo e($c->name); ?><i
                                                            class="fas fa-plus fa-xs"></i></a>
                                                    <ul class="header_categories_mega_menu">
                                                            <?php $__currentLoopData = $c->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                                    <li><a href="<?php echo e(route('shop', ['category' => $c->slug, 'sub-category' => $child->slug])); ?>"><?php echo e($child->name); ?></a></li>

                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </ul>
                                                </li>
                                            <?php else: ?>
                                            <li class="<?php echo e($k >= $maxProductToShow ? 'hidden' : ''); ?>"><a href="<?php echo e(route('category', $c->slug)); ?>"><?php echo e($c->name); ?></a></li>
                                            <?php endif; ?>
                                                <?php if(intval(--$maxProductToShow)): ?>
                                                <?php else: ?>
                                                    <?php if($maxProductToShow == 0): ?>
                                                        <li ><a href="#" id="more-btn"><i class="fa fa-plus" aria-hidden="true"></i> More
                                                        Categories</a>
                                                        </li>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <!--main menu start-->
                            <div class="main_menu header_menu_position">
                                <nav>
                                    <ul>
                                        <?php if(Cache::has('categories') && is_array(Cache::get('categories')) && count(Cache::get('categories'))): ?>

						                <?php $maxProductShow = 6; ?>


                                        <?php $__currentLoopData = Cache::get('categories'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ca): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        				
                        				<?php if((--$maxProductShow) > -1): ?>

                                        <?php if($ca->is_header == 1): ?>
                                            <li><a href="<?php echo e(route('shop', ['category' => $ca->slug])); ?>"><?php echo e($ca->name); ?></a></li>
                                        <?php endif; ?>

                                        <?php endif; ?>

                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                        <!-- <li><a class="active" href="<?php echo e(route('home')); ?>">Home</a> -->
                                            <!--<ul class="sub_menu">
                                                <li><a href="#">Home shop 1</a></li>
                                                <li><a href="#">Home shop 2</a></li>
                                                <li><a href="#">Home shop 3</a></li>
                                                <li><a href="#">Home shop 4</a></li>
                                                <li><a href="#">Home shop 5</a></li>
                                            </ul>-->
<!--                                         </li>
                                        <li><a href="<?php echo e(route('shop')); ?>"> Shop</a></li>
                                        <li><a href="<?php echo e(route('store')); ?>"> Our Stores</a></li>

                                        <li><a>More <em class="fas fa-chevron-down fa-xs"></em></a>
                                            <ul class="sub_menu">
                                                <li><a href="<?php echo e(route('page', 'about')); ?>">About Us</a></li>
                                                <li><a href="<?php echo e(route('page', 'faq')); ?>">FAQ</a></li>
                                                <?php if(isLoggedIn()): ?>
                                                    <li><a href="<?php echo e(route('my-account')); ?>">My Account</a></li>
                                                <?php endif; ?>
                                            </ul>
                                        </li>

                                        <li><a href="<?php echo e(route('contact')); ?>"> Contact Us</a></li>
 -->                                    </ul>
                                </nav>
                            </div>
                            <!--main menu end-->
                        </div>
<!--                         <div class="col-lg-3">
                            <?php if(trim(Cache::get('support_number', '')) != ''): ?>
                            <div class="header_call-support">
                                <p><a href="#"><?php echo e(Cache::get('support_number')); ?></a> Customer Support</p>
                            </div>
                            <?php endif; ?>
                        </div>
 -->                    </div>
                </div>
            </div>

            <!-- Delivery Address Mobile -->
            <div style="text-align: center;font-size: 15px;font-weight: 700;color: #1d86d6" data-toggle="modal" data-target="#checkDeliveryModal">
                <i class="fas fa-map-marker-alt"></i> &nbsp;Deliver to <span>
<?php if(!isset(session()->get('user')['user_id'])): ?>
    462001
<?php else: ?>
    <?php if(isset(session()->get('user')['dpincode'])): ?>
    <?php echo e(session()->get('user')['dpincode']); ?>


    <?php else: ?>
    462001
    <?php endif; ?>
<?php endif; ?>
                </span>
            </div>            
            </nav>
    <div>
        <?php echo $__env->make("themes.".get('theme').".parts.breadcrumb", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php echo $__env->make("themes.".get('theme').".common.msg", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>

<!-- The Modal -->
<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">List Your Kirana Items</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <form action="<?php echo e(route('kiranasubmit')); ?>" method="post" enctype="multipart/form-data" id="kiranaform">      
        <?php echo csrf_field(); ?>
      <!-- Modal body -->
      <div class="modal-body">
        <p class="alert alert-danger" id="fail_msg" style="display: none"><i class="fas fa-times"></i> Invalid Extension. Please try again!</p>

        <div class="form-group">
            <input type="hidden" name="user_id" value="<?php echo (isset(session()->get('user')['user_id'])) ? session()->get('user')['user_id'] : ''; ?>">
            <label for="">Items</label>
            <textarea name="request_list" id="request_area" cols="5" rows="5" class="form-control"></textarea>
        </div>
        
        <div class="form-group">
            <label>Upload List File : <span style="color:red;font-size: 12px">( Only .jpg , .png, and .pdf extensions allowed. )</span></label>
            <input type="file" name="kiranaFile" id="kiranaFile" class="form-control" accept=".pdf,.jpg,.png"/>
        </div>
        
       </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button class="btn btn-success" type="button" id="requestbtn">Submit</button>
        <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
      </div>
    </form>
    
    </div>
  </div>
</div>

<!-- The Modal -->
<div class="modal fade" id="checkDeliveryModal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <!-- Modal body -->
      <div class="modal-body">

      	<?php if(!isset(session()->get('user')['user_id'])): ?>
      	<a href="" class="btn btn-primary btn-block">Sign in to see all addresses</a>

      	<?php else: ?>
      	<h4>User Addresses</h4>
      	<p>
        <?php if(isset(session()->get('user')['darea']) && !empty(session()->get('user')['darea']) && isset(session()->get('user')['dcity']) && !empty(session()->get('user')['dcity']) && isset(session()->get('user')['dstate']) && !empty(session()->get('user')['dstate']) ): ?>
      		<?php echo e(session()->get('user')['darea'] . " " . session()->get('user')['dcity'] . " " . session()->get('user')['dstate']); ?>

         <?php endif; ?>
      	</p>

      	<?php if(!empty(session()->get('user')['other_add_array'])): ?>

      	<?php $__currentLoopData = session()->get('user')['other_add_array']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $oarr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

      	<p><?php echo e($oarr->area . " " . $oarr->city . " " . $oarr->state); ?></p>

      	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

      	<?php endif; ?>

      	<?php endif; ?>
        <hr>

                        <form action="<?php echo e(route('pincheck')); ?>" method="post" id="pinForm">
                        <div class="row">
                            <div class="col-md-12">
                                <span style="margin-bottom: 5px !important; margin-top: 20px; display:block;"><strong>Delivery Availablity</strong></span>
                            </div>
                            
                            <div class="col-lg-12">
                                <input type="text" class="form-control form-control-pincode" placeholder="Enter Pincode" id="pincode_input" name="pincode_input">
                                <?php echo csrf_field(); ?>
                                <div id="pinstatus"></div>
                            </div>
                            <div class="col-lg-12" style="display: flex;justify-content: center;margin-top: 3%">
                                <button class="btn checkbtn" type="submit" id="checkPin">Check</button>
                            </div>
                            
                        </div>
                        </form>        
        
       </div>
    
    </div>
  </div>
</div>
<script src="<?php echo e(theme('js/plugins.js')); ?>"></script>
<script src="<?php echo e(theme('js/home.js')); ?>"></script><?php /**PATH /home/batrasus/domains/batrasmart.com/public_html/resources/views/themes/ekart/common/header.blade.php ENDPATH**/ ?>
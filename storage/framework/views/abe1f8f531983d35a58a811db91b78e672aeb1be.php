<section class="footerfix section-content padding-bottom">
    <a href="#" id="scroll"><span></span></a>
    <div class="container">
        <nav class="row row-eq-height">
		<?php if(isset($data['dept_prods']) && count($data['dept_prods']['product']) > 0): ?>				
			<?php $__currentLoopData = $data['dept_prods']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php $__currentLoopData = $dp; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<?php if(!is_array($c)): ?>			
								<div class="col-md-3 mt-2">
                        <a href="<?php echo e(route('product-single', ['slug'=>$c->pslug])); ?>">									
										<div class="card card-category eq-height-element">
											<div class="img-wrap">
												<?php
												$url = "adminon/".$c->image;
												?>
			                                    <img src="<?php echo e(url($url)); ?>" alt="<?php echo e($c->name ?? ''); ?>">
											</div>
											<div class="card-body">
			                                    <h4 class="card-title"><?php echo e($c->name); ?></h4>
			                                    <?php echo e($c->subtitle); ?>

											</div>
										</div>
									</a>
								</div>
					<?php endif; ?> 
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			<?php else: ?>
			<div class="jumbotron jumbotron-fluid" style="width: 100%">
				<h1 class="text-center">No Products Found for this Department!<br/> Please try again later!</h1>
			</div>
		<?php endif; ?>
        </nav>
    </div>
</section><?php /**PATH /home/batrasmart/public_html/resources/views/themes/ekart/dept_products.blade.php ENDPATH**/ ?>
<style>
.top-search:focus{
    outline:none !important;
    box-shadow: none !important;
    background-color:transparent;
}
	#searchresult{
        width: 40.3%;
        height: auto;
        background: #e4e4e4;
        margin-left: 1%;
        position: absolute;
        z-index: 42;
        top: 49px;
        overflow-y: auto;
        display: none;
        border-radius: 2px;		
	}
	#ul_results{
	padding: 13px 17px 8px 13px;
    margin-bottom: 0px !important;
	}
	#ul_results a{
		display: block;
		padding: 9px;
		color: black
		/*border-bottom:1px solid #F2F2F2;*/
	}
	#ul_results a:hover{
		background: #F2F2F2
	}
	.ricon{
		padding-right: 10px
	}
	
	/*For Mobile*/
	#searchresultMobile{
        width: 91.3%;
        height: auto;
        background: #e4e4e4;
        margin-left: 1%;
        position: absolute;
        z-index: 42;
        top: 37px;
        overflow-y: auto;
        display: none;
        border-radius: 2px;	
	}
	#ul_resultsMobile{
	padding: 13px 17px 8px 13px;
    margin-bottom: 0px !important;
	}
	#ul_resultsMobile a{
		display: block;
		padding: 9px;
		color: black
		/*border-bottom:1px solid #F2F2F2;*/
	}
	#ul_resultsMobile a:hover{
		background: #F2F2F2
	}
	.ricon{
		padding-right: 10px
	}
</style>
<?php /**PATH /home/batrasmart/public_html/resources/views/themes/ekart/common/search-result.blade.php ENDPATH**/ ?>
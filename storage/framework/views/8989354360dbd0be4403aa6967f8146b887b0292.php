<style>
    .mobile_more_wrapper {
        position: absolute;
        top: 0;
        width: 80%;
        height: 100vh;
        display: none;
        z-index: 999;
        right: 0;
        background: #fff;
        padding: 15px;box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px;
    }

    .mobile_more_wrapper>ul {
        margin-top: 20px;
    }

    .mobile_more_wrapper>ul>li {
        padding: 10px;
      	box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;
      	margin-bottom: 8px;
    }
  
  	.btn-top-list-text{
      font-size:15px !important;
      font-weight: 700 !important;
      color: #007bff !important;
  	}
  .btn_mobile_more_close{    
    padding: 5px;
    background-color: #d61313;
    color: #fff;
    display: inline-block;
    line-height: 3px;
    height: 26px;
    width: 26px;
    border-radius: 22px;
  }
  
  .mobile_more_head{display:flex; justify-content:space-between;border-bottom: 1px solid #f1f1f1; padding-bottom: 10px;}

    @media(max-width:575px) {
        .mobile_more_wrapper {
            /* display: block; */
        }
      
      .header_right{display:none;}
      .cart_mobile{display:block;}
      .top-sticky-nav{width:100%;}
    }
</style>

<script>
    $(document).ready(function () {
        $(document).on("click", "#mobile_more_close", function (e) {
            e.preventDefault();
            $(".mobile_more_wrapper").slideUp();
        });

        $(document).on("click", "#mobile_more_open", function (e) {
            e.preventDefault();
            $(".mobile_more_wrapper").slideDown();
        });
    })
</script>

<div class="mobile_more_wrapper">
    <div class="mobile_more_head">
      <h4>Batras Mart</h4>
      <button type="button" class="btn btn_mobile_more_close" id="mobile_more_close"><i class="fas fa-times"></i></button>
  	</div>
    <ul>
        <?php if(isLoggedIn()): ?>
        <li><a href="<?php echo e(route('my-account')); ?>"><i class="far fa-user"></i> <span class="top-bar-text"><?php echo (isset(session()->get('user')['name'])) ? session()->get('user')['name'] : ''; ?>, </span></a></li>
        <?php else: ?>
        <li><a href="<?php echo e(route('login')); ?>"><i class="far fa-user"></i> <span class="top-bar-text">Sign In / Register</span></a></li>
        <?php endif; ?>
        <?php if(isLoggedIn()): ?>
		<li><a class="top-bar-text" href="<?php echo e(route('favourite')); ?>"><img src="<?php echo e(asset('new_icons/my_wishlist.png')); ?>" class="img-fluid" style="width: 20px" alt=""> Wishlist</a></li>
        <?php endif; ?>
        <li><a class="top-bar-text" href="<?php echo e(route('store')); ?>"><img src="<?php echo e(asset('new_icons/store.png')); ?>" class="img-fluid" style="width: 20px" alt=""> Our Stores</a> </li>
        <?php if(isLoggedIn()): ?>
        <li>
          <a href="javascript:void(0)">
            <button type="button" id="list_item_modal_show" class="btn btn-top-list p-0 btn-top-list-text">
              <img src="<?php echo e(asset('new_icons/kirana_list.png')); ?>" class="img-fluid" style="width: 20px" alt=""> Add Kirana List
            </button>
            <input type="hidden" id="list_item_url" value="<?php echo e(route('check-user-login')); ?>">
          </a>
        </li>
        <?php endif; ?>
        
    </ul>
</div><?php /**PATH /home/batrasmart/public_html/resources/views/themes/ekart/common/mobile_more.blade.php ENDPATH**/ ?>
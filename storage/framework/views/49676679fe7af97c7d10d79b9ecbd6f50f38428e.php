<style>
@media    only screen and (max-width: 600px) {
    #icon_categories {
        display: block
    }
}        
@media    only screen and (min-width: 600px) {
    #icon_categories {
        display: none
    }
}  	
.icon-cat-box{
	display: flex;flex-direction: column;background: white;width: 25%;padding-bottom: 12px
}
.icon-cat-name{
	font-size: 12px;text-align: center;font-weight: bold;padding: 0px 5px;line-height: 12px;
}
.icon-cat-name a{
	color: #363940;
}
</style>
<?php if(Cache::has('categories') && is_array(Cache::get('categories')) && count(Cache::get('categories'))): ?>
	<!--section icon categories popular categories transparent image-->
    <section class="section-content popular-categories" id="icon_categories">
		<div class="container">
			<div class="">
				<div class="row p-0">

                    <?php $maxCatShow = 8; ?>

					<?php $__currentLoopData = Cache::get('categories'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <?php if($c->is_home !=0 && (--$maxCatShow) > -1): ?>
                        		<div class="icon-cat-box">
                                <img class="category-item rounded-circle img-fluid" src="<?php echo e($c->image); ?>" alt="<?php echo e($c->name ?? 'Category'); ?>" style="">
                                <span class="icon-cat-name"><a href="<?php echo e(route('category', $i)); ?>"><?php echo e($c->name); ?></a></span>                        			
                        		</div>
                        <?php endif; ?>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
			</div>
		</div>
    </section>
    <!--end section icon categories-->
<?php endif; ?><?php /**PATH /home/batrasmart/public_html/resources/views/themes/ekart/parts/icon_categories.blade.php ENDPATH**/ ?>
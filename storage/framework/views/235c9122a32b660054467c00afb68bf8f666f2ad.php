<style>

/* Extra small devices (phones, 600px and down) */
@media  only screen and (max-width: 600px) {
    #webfilter{
        display: none !important;
    }    
    #mobfilter{
        display: block !important;
    }        
}

/* Small devices (portrait tablets and large phones, 600px and up) */
@media  only screen and (min-width: 600px) {
    #webfilter{
        display: none !important;
    }
    #mobfilter{
        display: block !important;
    }        
}

/* Medium devices (landscape tablets, 768px and up) */
@media  only screen and (min-width: 768px) {
    #webfilter{
        display: block !important;
    }    
    #mobfilter{
        display: none !important;
    }    
}

/* Large devices (laptops/desktops, 992px and up) */
@media  only screen and (min-width: 992px) {
    #webfilter{
        display: block !important;
    }    
    #mobfilter{
        display: none !important;
    }    
}

.mobile-fil-btn{
        width: 44%;
    height: 26px;
    background-color: green;
    position: fixed;
    z-index: 100000;
    color: white;
    text-align: center;
    left: 28%;
    bottom: 42px;
    border: none;
    outline: none;    
}
.mob-filters{
    position: fixed;
    overflow: scroll;
    width: 81%;
    height: 100%;
    top: 0;
    background-color: white;
    z-index: 200000;
    left: 0;
    box-shadow: rgb(0 0 0 / 16%) 0px 3px 6px, rgb(0 0 0 / 23%) 0px 3px 6px;
 animation: bounce; /* referring directly to the animation's @keyframe  declaration */
  animation-duration: 2s; /* don't forget to set a duration! */    
}
.filter-header{
        height: 8%;
     background-color: #1d86d6;
    padding: 11px;
    color: white;
    font-weight: bold;
    font-size: 20px;
}
.filter-body .card-link{
    font-size: 20px;
}
.showE{
    display: block !important;
}
.hideE{
    display: none !important;
}
</style>
<script>
    function showFilterArea() {
        // $(".mob-filters").css('display' , 'block');
const element = document.querySelector('.mob-filters');
element.classList.add('animate__slideInLeft','showE');
element.classList.remove('animate__slideOutLeft','hideE');        

    }
    function hideFilterArea(ty) {
            const element = document.querySelector('.mob-filters');
        if(ty==1){        
        // $(".mob-filters").css('display' , 'none');
            if(element.classList.contains('animate__slideInLeft')){
                element.classList.remove('animate__slideInLeft');        
                element.classList.add('animate__slideOutLeft');                       
            }        
        }
        if(ty==2){
                    // $(".mob-filters").css('display' , 'none');
            element.classList.remove('animate__slideInLeft');        
            element.classList.add('animate__slideOutLeft');                   
        }

    }
    function toggleStatus(div) {
        if($(`#collapse${div}`).hasClass("show")){
            $(`#collapse${div}Status`).html("+");
        }
        if(!$(`#collapse${div}`).hasClass("show")){
            $(`#collapse${div}Status`).html("-");
        }
    }

</script>
  <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
<!--shop page -->
<section class="section-content padding-bottom mt-5">
    <a href="#" id="scroll"><span></span></a>
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-4 col-xl-3 col-12 filter mb-3">
<div id="mobfilter">
    <br>
     <form action='#' method="GET" id='filter'>
                        <input type="hidden" name="s" value="<?php echo e(isset($_GET['s']) ? trim($_GET['s']) : ''); ?>">
                        <input type="hidden" name="section" value="<?php echo e(isset($_GET['section']) ? trim($_GET['section']) : ''); ?>">
                        <input type="hidden" name="category" value="<?php echo e(isset($_GET['category']) ? trim($_GET['category']) : ''); ?>">
                        <input type="hidden" name="sub-category" value="<?php echo e(isset($_GET['sub-category']) ? trim($_GET['sub-category']) : ''); ?>">
                        <input type="hidden" name="sort" value="<?php echo e(isset($_GET['sort']) ? trim($_GET['sort']) : ''); ?>">
<div class="row d-flex justify-content-center">
    <div style="width:50%">
                    <?php if(isset($data['categories']) && is_array($data['categories']) && count($data['categories'])): ?>
     <select name="cats_options" class="form-control" id="cats_options">
                            <?php $__currentLoopData = $data['categories']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if(isset($c->name) && trim($c->name) != ""): ?>
         <option value="<?php echo e($c->slug); ?>"  <?php echo e((isset($data['selectedCategory']) && is_array($data['selectedCategory']) && in_array($c->slug, $data['selectedCategory'])) ? 'selected' : ''); ?>><?php echo e($c->name); ?></option>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
     </select>           
    </div>


</div>
    </form>

<center>
    <span class="mobile-fil-btn" onclick="showFilterArea()">
        Filter Products        
    </span>
</center>

<div class="mob-filters animate__animated" style="display:none">
    <div class="filter-header">
        <div class="d-flex justify-content-between">
            <span>Filter</span>
            <span onclick="hideFilterArea(2)">X</span>
        </div>
    </div>
    <div class="filter-body">
<div class="container">
  <div id="accordion">
    <div class="mt-3 mb-3">
      <div>
        <a class="card-link" data-toggle="collapse" href="#collapseOne" onclick="toggleStatus('One')">
          <span id="collapseOneStatus">-</span> Price
        </a>
      </div>
      <div id="collapseOne" class="collapse show" data-parent="#accordion">
        <div class="card-body">

                    <form action='#' method="GET" id='filter'>
                        <input type="hidden" name="s" value="<?php echo e(isset($_GET['s']) ? trim($_GET['s']) : ''); ?>">
                        <input type="hidden" name="section" value="<?php echo e(isset($_GET['section']) ? trim($_GET['section']) : ''); ?>">
                        <input type="hidden" name="category" value="<?php echo e(isset($_GET['category']) ? trim($_GET['category']) : ''); ?>">
                        <input type="hidden" name="sub-category" value="<?php echo e(isset($_GET['sub-category']) ? trim($_GET['sub-category']) : ''); ?>">
                        <input type="hidden" name="sort" value="<?php echo e(isset($_GET['sort']) ? trim($_GET['sort']) : ''); ?>">
                        <div>
                            <div class="row">
                                <div class="col">
                                    <div id="slider-range" data-min="<?php echo e(intval($data['min_price'])); ?>" data-max="<?php echo e(intval($data['max_price']) + 1); ?>" data-selected-min="<?php echo e(intval($data['selectedMinPrice'])); ?>" data-selected-max="<?php echo e(intval($data['selectedMaxPrice'])+1); ?>"></div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col">
                                    <input type="number" name="min_price" value="<?php echo e(intval($data['selectedMinPrice'])); ?>" class="form-control">
                                </div>
                                <div class="col">
                                    <input type="number" name="max_price" value="<?php echo e(intval($data['selectedMaxPrice'])+1); ?>" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <br>
                                    <button type="submit" name="submit" class="btn btn-primary btn-block"><?php echo e(__('msg.filter')); ?></button>
                                </div>
                            </div>
                        </div>
                        <br>
                    </form>      


        </div>
      </div>
    </div>
    <div class="mt-3 mb-3">
      <div onclick="toggleStatus('Two')">
        <a class="card-link" data-toggle="collapse" href="#collapseTwo" onclick="toggleStatus('Two')">
        <span id="collapseTwoStatus">-</span> Categories
      </a>
      </div>
      <div id="collapseTwo" class="collapse show" data-parent="#accordion">
        <div class="card-body">


                    <?php if(isset($data['categories']) && is_array($data['categories']) && count($data['categories'])): ?>
                    <div>
                        <div class="text ml-4 ">
                            <?php $__currentLoopData = $data['categories']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if(isset($c->name) && trim($c->name) != ""): ?>
                                    <div class="custom-control custom-checkbox pb-2">
                                        <input type="checkbox" class="custom-control-input cats" id="cat-<?php echo e($c->id); ?>" value="<?php echo e($c->slug); ?>" <?php echo e((isset($data['selectedCategory']) && is_array($data['selectedCategory']) && in_array($c->slug, $data['selectedCategory'])) ? 'checked' : ''); ?>>
                                        <label class="custom-control-label" for="cat-<?php echo e($c->id); ?>"><?php echo e($c->name); ?></label>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if(isset($data['selectedCategory']) && is_array($data['selectedCategory'])): ?>
                        <?php $__currentLoopData = $data['selectedCategory']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(isset(Cache::get('categories',[])[$cat]) && isset(Cache::get('categories',[])[$cat]->childs)): ?>
                                <div>
                                    <h5 class="mb-3 name"><?php echo e(Cache::get('categories',[])[$cat]->name); ?></h5>
                                    <div class="text ml-4">
                                        <?php $__currentLoopData = Cache::get('categories',[])[$cat]->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="custom-control custom-checkbox pb-2">
                                                <input type="checkbox" class="custom-control-input subs" id="sub-<?php echo e($c->id); ?>" value="<?php echo e($c->slug); ?>" <?php echo e((isset($data['selectedSubCategory']) && is_array($data['selectedSubCategory']) && in_array($c->slug, $data['selectedSubCategory'])) ? 'checked' : ''); ?>>
                                                <label class="custom-control-label" for="sub-<?php echo e($c->id); ?>"><?php echo e($c->name); ?></label>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>

        </div>
      </div>
    </div>
  </div>
</div>
        
    </div>
</div>
</div>

<div id="webfilter">
                <div class="card">

                        <div class="pt-4">
                        <legend class="mb-1 p-0 title-sec"><?php echo e(__('msg.filter_by')); ?></legend>
                        <hr class="line mb-0 pb-0">
                    </div>

                    <form action='#' method="GET" id='filter'>
                        <input type="hidden" name="s" value="<?php echo e(isset($_GET['s']) ? trim($_GET['s']) : ''); ?>">
                        <input type="hidden" name="section" value="<?php echo e(isset($_GET['section']) ? trim($_GET['section']) : ''); ?>">
                        <input type="hidden" name="category" value="<?php echo e(isset($_GET['category']) ? trim($_GET['category']) : ''); ?>">
                        <input type="hidden" name="sub-category" value="<?php echo e(isset($_GET['sub-category']) ? trim($_GET['sub-category']) : ''); ?>">
                        <input type="hidden" name="sort" value="<?php echo e(isset($_GET['sort']) ? trim($_GET['sort']) : ''); ?>">
                        <div>
                            <h5 class="name title-sec mt-3"><?php echo e(__('msg.price')); ?></h5>
                            <div class="row">
                                <div class="col">
                                    <div id="slider-range" data-min="<?php echo e(intval($data['min_price'])); ?>" data-max="<?php echo e(intval($data['max_price']) + 1); ?>" data-selected-min="<?php echo e(intval($data['selectedMinPrice'])); ?>" data-selected-max="<?php echo e(intval($data['selectedMaxPrice'])+1); ?>"></div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col">
                                    <input type="number" name="min_price" value="<?php echo e(intval($data['selectedMinPrice'])); ?>" class="form-control">
                                </div>
                                <div class="col">
                                    <input type="number" name="max_price" value="<?php echo e(intval($data['selectedMaxPrice'])+1); ?>" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <br>
                                    <button type="submit" name="submit" class="btn btn-primary btn-block"><?php echo e(__('msg.filter')); ?></button>
                                </div>
                            </div>
                        </div>
                        <br>
                    </form>      
                    <?php if(isset($data['categories']) && is_array($data['categories']) && count($data['categories'])): ?>
                    <div>
                        <h5 class="mb-3 name title-sec"><?php echo e(__('msg.category')); ?></h5>
                        <div class="text ml-4 ">
                            <?php $__currentLoopData = $data['categories']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if(isset($c->name) && trim($c->name) != ""): ?>
                                    <div class="custom-control custom-checkbox pb-2">
                                        <input type="checkbox" class="custom-control-input cats" id="cat-<?php echo e($c->id); ?>" value="<?php echo e($c->slug); ?>" <?php echo e((isset($data['selectedCategory']) && is_array($data['selectedCategory']) && in_array($c->slug, $data['selectedCategory'])) ? 'checked' : ''); ?>>
                                        <label class="custom-control-label" for="cat-<?php echo e($c->id); ?>"><?php echo e($c->name); ?></label>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if(isset($data['selectedCategory']) && is_array($data['selectedCategory'])): ?>
                        <?php $__currentLoopData = $data['selectedCategory']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(isset(Cache::get('categories',[])[$cat]) && isset(Cache::get('categories',[])[$cat]->childs)): ?>
                                <div>
                                    <h5 class="mb-3 name"><?php echo e(Cache::get('categories',[])[$cat]->name); ?></h5>
                                    <div class="text ml-4">
                                        <?php $__currentLoopData = Cache::get('categories',[])[$cat]->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="custom-control custom-checkbox pb-2">
                                                <input type="checkbox" class="custom-control-input subs" id="sub-<?php echo e($c->id); ?>" value="<?php echo e($c->slug); ?>" <?php echo e((isset($data['selectedSubCategory']) && is_array($data['selectedSubCategory']) && in_array($c->slug, $data['selectedSubCategory'])) ? 'checked' : ''); ?>>
                                                <label class="custom-control-label" for="sub-<?php echo e($c->id); ?>"><?php echo e($c->name); ?></label>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
</div>                    

    
                    <div class="card">
                        <button type="button" class="btn btn-success btn-block" id="av_form_button">Availability Form</button>                        
                    </div>

                
                </div>

            </div>
            <div class="col-md-8 col-xl-9 col-lg-7 col-12 shopdetails" onclick="hideFilterArea(1)">
                <nav class="navbar navbar-md navbar-light bg-white row gridviewdiv">
                    <div class="col-md-6 col-xs-3 col-6">
                        <div class="row">
                            <div id="list">
                                <em class= "fa fa-list fa-lg" data-view ="list-view"></em>
                            </div>
                            <div id="grid">
                                <em class="selected fa fa-th fa-lg" data-view ="grid-view"></em>
                            </div>
                            <div class="letter">
                                <small><?php echo e((isset($data['total']) && intval($data['total'])) ? $data['total'].' Items' : ''); ?></small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-6">
                        <div class="gridviewselect">
                            <div class="row">
                                <div class="select"> <?php echo e(__('msg.sort_by')); ?>:  </div>
                                <div class="select1">
                                    <select class="form-control innerselect1" id="sort">
                                        <option value=""> <?php echo e(__('msg.relevent')); ?> </option>
                                        <option value="new" <?php echo e((isset($_GET['sort']) && $_GET['sort'] == 'new') ? 'selected' : ''); ?>><?php echo e(__('msg.new')); ?></option>
                                        <option value="old" <?php echo e((isset($_GET['sort']) && $_GET['sort'] == 'old') ? 'selected' : ''); ?>><?php echo e(__('msg.old')); ?></option>
                                        <option value="low" <?php echo e((isset($_GET['sort']) && $_GET['sort'] == 'low') ? 'selected' : ''); ?>><?php echo e(__('msg.low_to_high')); ?></option>
                                        <option value="high" <?php echo e((isset($_GET['sort']) && $_GET['sort'] == 'high') ? 'selected' : ''); ?>><?php echo e(__('msg.high_to_low')); ?></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
                <?php if(isset($data['list']) && isset($data['list']['data']) && is_array($data['list']['data']) && count($data['list']['data'])): ?>
                    <div id="products" class="row view-group">
                        <?php $__currentLoopData = $data['list']['data']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <?php if(count(getInStockVarients($p))): ?>

                            <div class="item1 col-sm-12 col-lg-6 col-md-4">
                                <div class="add-to-fav add-to-fav-1 text-center">
                                    <button type="button" class="d-block btn <?php echo e((isset($p->is_favorite) && intval($p->is_favorite)) ? 'saved' : 'save'); ?>" data-id='<?php echo e($p->id); ?>'></button>
                                    <?php if($p->indicator == 1): ?>
                                        <img style="width:15px;" class="tooltip_new"  src="<?php echo e(asset('images/vag.svg')); ?>" data-toggle="tooltip" data-placement="top" title="Vegetarian">
                                        
                                    <?php elseif($p->indicator == 2): ?>
                                        <img style="width:15px;" class="tooltip_new"  src="<?php echo e(asset('images/nonvag.svg')); ?>" data-toggle="tooltip" data-placement="top" title="Non Vegetarian">
                                    <?php endif; ?>
                                </div>
                                <a href="<?php echo e(route('product-single', $p->slug ?? '-')); ?>">
                                    <div class="thumbnail card-sm">
                                        <a href="<?php echo e(route('product-single', $p->slug ?? '-')); ?>">
                                            <div class="img-event">
                                                <img class="group list-group-image img-fluid" src="<?php echo e($p->image); ?>" alt="<?php echo $p->id; //in_array($p->id, $data['section-prods']); ?>">
                                            </div>
                                        </a>
                                        <div class="caption card-body">
                                            <div class="text-wrap text-left">
                                                <a href="<?php echo e(route('product-single', $p->slug ?? '-')); ?>" class="title product-name"><?php echo e($p->name); ?><span class="product_variant_val" id="variant_val_<?php echo e($p->id); ?>"> : 
                                                
                                                <?php if(count(getInStockVarients($p))): ?>
                                                    <?php
                                                        print_r( get_varient_name($p->variants[0]))
                                                    ?>
                                                <?php endif; ?>
                                            </span></a>
                                                <span class="d-none text-muted description1"><?php if(strlen(strip_tags($p->description)) > 18): ?> <?php echo substr(strip_tags($p->description), 0,18) ."..."; ?> <?php else: ?> <?php echo substr(strip_tags($p->description), 0,18); ?> <?php endif; ?></span>
                                                <div class="price mt-1 ">
                                                    
                                                    <div class="row">
                                                        <div class="col-8">
                                                            <div class="row">
                                                                <div class="col-6 pr-1">
                                                                    <p class="product-price">MRP</p>
                                                                    <p class="product-price" id="mrp_<?php echo e($p->id); ?>"><?php echo print_mrp($p); ?></p>
                                                                </div>
                                                                
                                                                <div class="col-6 pl-1">
                                                                    <p class="product-price-batras">Batras</p>
                                                                    <p class="product-price-batras product-price-batras-spl" id="price_<?php echo e($p->id); ?>"><?php echo print_price($p); ?></p>
                                                                </div>
                                                                <div class="col-12">
                                                                    <p class="product-price" style="font-size: 12px;">(Inclusive of all taxes)</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-4">
                                                            <div class="product-savings">
                                                                <p class="ps1 mb-0">Save</p>
                                                                <p class="ps2" id="save_<?php echo e($p->id); ?>">₹ <?php echo e($p->variants[0]->price - $p->variants[0]->discounted_price); ?> </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="add_cart_wrapper mt-2">
                                            <?php if(count(getInStockVarients($p))): ?>
                                            <div class="add_cart_style_1">
                                                <form action='<?php echo e(route('cart-add-single-varient')); ?>' method="POST">
                                                    <button type="submit" class="btn cart-1 fa fa-shopping-cart"><span>&nbsp;&nbsp;<?php echo e(__('msg.add_to_cart')); ?></span></button>
                                                    
                                                    <div class="style_1_select">
                                                        <input type="hidden" name="id" value="<?php echo e($p->id); ?>">
                                                        <select class="form-control" name="varient" data-id="<?php echo e($p->id); ?>">
                                                            <?php $__currentLoopData = getInStockVarients($p); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <option value="<?php echo e($v->id); ?>"  data-price='<?php echo e(get_price(get_price_varients($v))); ?>' data-mrp='<?php echo e(get_price(get_mrp_varients($v))); ?>' data-savings='<?php echo e(get_savings_varients($v)); ?>' data-save='<?php echo e(get_save_varients($v, false)); ?>'><?php echo e(get_varient_name($v)); ?></option>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </select>
                                                    </div>
                                                </form>
                                            </div>
                                            <?php else: ?>
                                            <div class="add_cart_style_1">
                                                <button type="button" disabled="true" class="btn cart-1 fa fa-shopping-cart"><span>&nbsp;&nbsp;<?php echo e(__('msg.add_to_cart')); ?></span></button>
                                            </div>
                                                <span class="sold-out"><?php echo e(__('msg.sold_out')); ?></span>
                                            <?php endif; ?>
                                            
                                            
                                        </div>
                                    </div>
                                </a>
                                
                                <?php if(!count(getInStockVarients($p)) < 1): ?>
                                    <div class="disc-price-tag disc-price-tag-1">
                                        <div class="tag_badge tag-badge-1">
                                            <img class="tag-img img-fluid" src="<?php echo e(asset('images/offerimg.png')); ?>">
                                            <p class="tag-text tag-text-1" id="savings_<?php echo e($p->id); ?>"> <?php echo e(get_savings_varients($p->variants[0])); ?> </p>
                                        </div>
                                        
                                    </div>
                                <?php endif; ?>
                            </div>

                            <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                        </div>
                        <div class="row">
                            <div class="col"><br></div>
                        </div>
                         <div class="row">
                           <!-- <div class="col">
                                <?php if(isset($data['last']) && $data['last'] != ""): ?>
                                    <a href="<?php echo e($data['last']); ?>" class="btn btn-primary pull-left text-white"><em class="fa fa-arrow-left"></em> <?php echo e(__('msg.previous')); ?></a>
                                <?php endif; ?>
                            </div>
                            <div class="col text-right">
                                <?php if(isset($data['next']) && $data['next'] != ""): ?>
                                    <a href="<?php echo e($data['next']); ?>" class="btn btn-primary pull-right text-white"><?php echo e(__('msg.next')); ?> <em class="fa fa-arrow-right"></em></a>
                                <?php endif; ?>
                            </div>-->
                           <div class="col text-right">
                            <?php
                                $number_of_pages = $data['number_of_pages'] + 1;
                                $currentpage = '0';
                            $currentpage = request()->input('page');
                            ?>
                            <?php for($page = max(1, $currentpage - 2); $page <= min($currentpage + 4, $number_of_pages); $page++): ?>
                            
                            <?php $pageprevious = $page-1;
                            ?>
                                <?php if(request()->query('min_price')!== NULL): ?>
                                <a href="<?php echo e(Request::fullUrl()); ?>&page=<?php echo e($pageprevious); ?>" <?php if($currentpage == $pageprevious ): ?> class="active" <?php else: ?> class="btn btn-primary pull-right text-white" <?php endif; ?>> <?php echo e($page); ?> </a>
                                <?php else: ?>
                                <a href="shop?page=<?php echo e($pageprevious); ?>" <?php if($currentpage == $pageprevious ): ?> class="active" <?php else: ?> class="btn btn-primary pull-right text-white" <?php endif; ?>><?php echo e($page); ?>  </a>
                                <?php endif; ?>
                            <?php endfor; ?>
                            
                        </div>
                          
                    <?php else: ?>
                        <div class="row">
                            <div class="col">
                                <br><br>
                                <h1 class="text-center"><?php echo e(__('msg.no_product_found')); ?></h1>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                    
            </div>
        </div>
    </div>
<!-- The Modal -->
<div class="modal fade" id="avModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title" style="font-family: Rubik, sans-serif !important">Request Product Form</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
        <form action="<?php echo e(route('product-request')); ?>" method="post" id="reqForm">
            <?php echo csrf_field(); ?>
      <!-- Modal body -->
      <div class="modal-body">
        <p class="alert alert-success" id="success_alert" style="display: none">Request Submitted Successfully</p>
        <div class="form-group">
            <label for="cname">Name</label>
            <input type="text" name="cname" class="form-control" id="cname" onblur="validatePRequest('cname',this.value,'Name')" required />
            <p style="color:red;" id="error_cname"></p>
        </div>
        <div class="form-group">
            <label for="cemail">Email</label>
            <input type="text" name="cemail" class="form-control" id="cemail" onblur="validatePRequest('cemail',this.value,'Email')" required/>
            <p style="color:red;" id="error_cemail"></p>
        </div>
        <div class="form-group">
            <label for="cphone">Phone</label>
            <input type="text" name="cphone" class="form-control" id="cphone" onblur="validatePRequest('cphone',this.value,'Phone')" required/>
            <p style="color:red;" id="error_cphone" ></p>
        </div>
        <div class="form-group">
            <label for="product_name">Product Name</label>
            <input type="text" name="product_name" class="form-control" id="product_name" onblur="validatePRequest('product_name',this.value,'Product Name')" required/>
            <p style="color:red;" id="error_product_name"></p>
        </div> 
        <div class="form-group">
            <label for="message">Message</label>
            <textarea name="message" id="message" cols="2" rows="2" class="form-control"></textarea>
        </div>       
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="submit" class="btn btn-success" id="reqBtn">Submit Request</button>        
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
        </form>
    </div>
  </div>
</div>    
</section>
<!-- End shop page --><?php /**PATH /home/batrasmart/public_html/resources/views/themes/ekart/shop.blade.php ENDPATH**/ ?>
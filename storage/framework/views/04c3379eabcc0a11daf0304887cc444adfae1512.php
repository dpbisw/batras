<!-------------------------------->

<?php if(isset($s->products) && is_array($s->products) && count($s->products) && count($s->products) >= 4): ?>
    <!--section recently added and new on ekart -->
    <section class="section-content padding-bottom mt-3 sellproc">
        <div class="container">
            <?php if(isset($s->title) && $s->title != ""): ?>
                <h4 class="title-section title-sec font-weight-bold"><?php echo e($s->title); ?> <small class="text-muted short-desc"> <?php echo e($s->short_description); ?></small></h4>
                <?php if(isset($s->slug) && $s->slug != ""): ?>
                    <a href="<?php echo e(route('section',$s->slug)); ?>" class="view title-section viewall"><?php echo e(__('msg.view_all')); ?></a>
                <?php endif; ?>
                <hr class="line">
            <?php endif; ?>
            
            <div class="slider-container-box">
                <div class="swiper-container swiper-style-1-slider-container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        <?php $__currentLoopData = $s->products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            
                            <div class="swiper-slide">
                                <figure class="card card-sm card-product-grid card-style-1">
                                    <aside class="add-to-fav add-to-fav-1">
                                        <button type="button" class="d-block btn <?php echo e((isset($p->is_favorite) && intval($p->is_favorite)) ? 'saved' : 'save'); ?>" data-id='<?php echo e($p->id); ?>' ></button>
                                        <?php if($p->indicator == 1): ?>
                                            <img style="width:15px;" class="tooltip_new"  src="<?php echo e(asset('images/vag.svg')); ?>" data-toggle="tooltip" data-placement="top" title="Vegetarian">
                                            
                                        <?php elseif($p->indicator == 2): ?>
                                            <img style="width:15px;" class="tooltip_new"  src="<?php echo e(asset('images/nonvag.svg')); ?>" data-toggle="tooltip" data-placement="top" title="Non Vegetarian">
                                        <?php endif; ?>
                                    </aside>
                                    
                                    <a href="<?php echo e(route('product-single', $p->slug)); ?>" class="img-wrap"> <img src="<?php echo e($p->image); ?>" alt="<?php echo e($p->name ?? 'Product Image'); ?>"> </a>
                                    <figcaption class="info-wrap">
                                        <div class="text-wrap p-2 text-left">
                                            <a href="<?php echo e(route('product-single', $p->slug)); ?>" class="title-product product-name"><?php echo e($p->name); ?><span class="product_variant_val" id="variant_val_<?php echo e($p->id); ?>"> : 
                                                
                                                <?php if(count(getInStockVarients($p))): ?>
                                                    <?php
                                                        print_r( get_varient_name($p->variants[0]))
                                                    ?>
                                                <?php endif; ?>
                                            </span></a>
                            
                                            <span class="text-muted style-desc d-none">
                                                <?php if(strlen(strip_tags($p->description)) > 20): ?> <?php echo substr(strip_tags($p->description), 0,20)."..."; ?> <?php else: ?> <?php echo substr(strip_tags($p->description), 0,20); ?> <?php endif; ?>
                                            </span>
                                            <div class="price mt-1 mb-2">
                                                <div class="row">
                                                    <div class="col-8">
                                                        <div class="row">
                                                            <div class="col-6 pr-1">
                                                                <p class="product-price">MRP</p>
                                                                <p class="product-price" id="mrp_<?php echo e($p->id); ?>"><?php echo print_mrp($p); ?></p>
                                                            </div>
                                                            
                                                            <div class="col-6 pl-1">
                                                                <p class="product-price-batras">Batras</p>
                                                                <p class="product-price-batras product-price-batras-spl" id="price_<?php echo e($p->id); ?>"><?php echo print_price($p); ?></p>
                                                            </div>
                                                            <div class="col-12">
                                                                <p class="product-price" style="font-size: 12px;">(Inclusive of all taxes)</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-4">
                                                        <div class="product-savings">
                                                            <p class="ps1">Save</p>
                                                            <p class="ps2" id="save_<?php echo e($p->id); ?>">₹ <?php echo e($p->variants[0]->price - $p->variants[0]->discounted_price); ?> </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </figcaption>
                                    
                                    
                                    
                                    <?php if(count(getInStockVarients($p))): ?>
                                    <div class="add_cart_style_1">
                                        <form action='<?php echo e(route('cart-add-single-varient')); ?>' method="POST">
                                            <button type="submit" class="btn cart-1 fa fa-shopping-cart"><span>&nbsp;&nbsp;<?php echo e(__('msg.add_to_cart')); ?></span></button>
                                            
                                            <div class="style_1_select">
                                                <input type="hidden" name="id" value="<?php echo e($p->id); ?>">
                                                <select class="form-control" name="varient" data-id="<?php echo e($p->id); ?>">
                                                    <?php $__currentLoopData = getInStockVarients($p); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($v->id); ?>"  data-price='<?php echo e(get_price(get_price_varients($v))); ?>' data-mrp='<?php echo e(get_price(get_mrp_varients($v))); ?>' data-savings='<?php echo e(get_savings_varients($v)); ?>' data-save='<?php echo e(get_save_varients($v, false)); ?>'><?php echo e(get_varient_name($v)); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                    <?php else: ?>
                                    <div class="add_cart_style_1">
                                        <button type="button" disabled="true" class="btn cart-1 fa fa-shopping-cart"><span>&nbsp;&nbsp;<?php echo e(__('msg.add_to_cart')); ?></span></button>
                                    </div>
                                        <span class="sold-out"><?php echo e(__('msg.sold_out')); ?></span>
                                    <?php endif; ?>
                                    
                                    <?php if(!count(getInStockVarients($p)) < 1): ?>
                                    <div class="disc-price-tag disc-price-tag-1">
                                        <div class="tag_badge tag-badge-1">
                                            <img class="tag-img img-fluid" src="<?php echo e(asset('images/offerimg.png')); ?>">
                                            <p class="tag-text tag-text-1" id="savings_<?php echo e($p->id); ?>"> <?php echo e(get_savings_varients($p->variants[0])); ?> </p>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    
                                    
                                </figure>
                            </div>
                            
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <!-- If we need pagination -->
                    <div class="swiper-pagination"></div>
        
                </div>
                
                <!-- If we need navigation buttons -->
                    <div class="swiper-button-prev sites-slider__prev"></div>
                    <div class="swiper-button-next sites-slider__next"></div>
            </div>
        </div>
    </section>
    <!--section end recently added and new on ekart -->
<?php endif; ?>



    <?php /**PATH /home/batrasus/domains/batrasmart.com/public_html/resources/views/themes/ekart/parts/style_1.blade.php ENDPATH**/ ?>
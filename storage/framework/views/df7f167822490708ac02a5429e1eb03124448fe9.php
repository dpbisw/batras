
<section class="section-content padding-bottom mt-4 spacingrm">
    <a href="#" id="scroll"><span></span></a>
    
    
    <!--Swiper Slider Start-->
    <style>
        .swiper-home-slider {
            width: 100%;
            height: auto;
        }
@media  only screen and (max-width: 600px) {
    .web-swipe-img{
        display: none
    }
    .mobile-img {
        display: block
    }
}        
@media  only screen and (min-width: 600px) {
    .web-swipe-img{
        display: block
    }
    .mobile-img {
        display: none
    }
    
    html, body { overscroll-behavior: none; }
}        

    </style>
   
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="swiper-container swiper-home-slider">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper">
                            <!-- Slides -->
                            <?php $__currentLoopData = Cache::get('sliders'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $s): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                
                                    <?php if($s->type_id == 0 && $s->link == ""): ?>
                                    <div class="swiper-slide" data-swiper-autoplay="<?php echo e($s->slider_delay); ?>">
                                        <img src="<?php echo e($s->image); ?>" alt="<?php echo e($s->name); ?>" class="img-fluid web-swipe-img">
                                        <img src="<?php echo e($s->mobile_image); ?>" alt="<?php echo e($s->name); ?>" class="img-fluid mobile-img">
                                    </div>    
                                    <?php elseif($s->type_id == 0 && $s->link != ""): ?>
                                     <div class="swiper-slide" data-swiper-autoplay="<?php echo e($s->slider_delay); ?>">
                                        <a href="<?php echo e($s->link); ?>">
                                            <img src="<?php echo e($s->image); ?>" alt="<?php echo e($s->name); ?>" class="img-fluid web-swipe-img">
                                            <img src="<?php echo e($s->mobile_image); ?>" alt="<?php echo e($s->name); ?>" class="img-fluid mobile-img">
                                        </a>
                                    </div>    
                                    <?php elseif($s->type == 'sub-category'): ?>
                                    <div class="swiper-slide" data-swiper-autoplay="<?php echo e($s->slider_delay); ?>">
                                        <a href="shop?category=<?php echo e($s->slug1); ?>&sub-category=<?php echo e($s->slug2); ?>" >
                                            <img src="<?php echo e($s->image); ?>" alt="<?php echo e($s->name); ?>" class="img-fluid web-swipe-img">
                                            <img src="<?php echo e($s->mobile_image); ?>" alt="<?php echo e($s->name); ?>" class="img-fluid mobile-img">
                                        </a>
                                    </div>
                                    <?php elseif($s->type == 'category'): ?>
                                    <div class="swiper-slide" data-swiper-autoplay="<?php echo e($s->slider_delay); ?>">
                                        <a href="<?php echo e($s->type); ?>/<?php echo e(slugify($s->name)); ?>">
                                            <img src="<?php echo e($s->image); ?>" alt="<?php echo e($s->name); ?>" class="img-fluid web-swipe-img">
                                            <img src="<?php echo e($s->mobile_image); ?>" alt="<?php echo e($s->name); ?>" class="img-fluid mobile-img">
                                        </a>

                                    </div>
                                    <?php elseif($s->type == 'product'): ?>
                                    <div class="swiper-slide" data-swiper-autoplay="<?php echo e($s->slider_delay); ?>">
                                        <a href="<?php echo e($s->type); ?>/<?php echo e(slugify($s->name)); ?>">
                                            <img src="<?php echo e($s->image); ?>" alt="<?php echo e($s->name); ?>" class="img-fluid web-swipe-img">
                                            <img src="<?php echo e($s->mobile_image); ?>" alt="<?php echo e($s->name); ?>" class="img-fluid mobile-img">

                                        </a>
                                    </div>
                                    <?php endif; ?>
                                    
                                    <!--<img src="<?php echo e(theme('images/test-slider-1.jpg')); ?>" alt="<?php echo e($s->name); ?>" class="img-fluid">-->
                                    
                                    <!--for link-->
                                    <!--<a href="#"><img src="<?php echo e(theme('images/test-slider-1.jpg')); ?>" alt="<?php echo e($s->name); ?>" class="img-fluid"></a>-->
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <!-- If we need pagination -->
                        <div class="swiper-pagination swiper-pagination-custom"></div>
            
                        <!-- If we need navigation buttons -->
                        <div class="swiper-button-prev swiper-button-custom"></div>
                        <div class="swiper-button-next swiper-button-custom"></div>
            
                        <!-- If we need scrollbar -->
                        <!--<div class="swiper-scrollbar"></div>-->
                    </div>
                </div>
            </div>
        </div>
    
    
    
    <!--Swiper Slider END-->

</section>


<?php /**PATH /home/batrasmart/public_html/resources/views/themes/ekart/home.blade.php ENDPATH**/ ?>
<section class="mt-5 padding-bottom section-contact footerfix">
	<div class="container">
		<div class="card">
            <h2 class="text-center pt-3 mb-5 title-contact">Our Stores</h2>
            <div class="container">
            <table class="table">
            	<thead>
            		<tr>
                              <th>Image</th>
            			<th>Name</th>
            			<th>Address</th>
            			<th>City</th>
            			<th>State</th>
            			<th>Phone</th>
            			<th>Timings</th>
            		</tr>
            	</thead>
            	<tbody>
            		<?php if(!empty($data['store'])): ?>
            			<?php $__currentLoopData = $data['store']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $st): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            			<tr>
                                 <td><img src="<?php echo e($st->image); ?>" style="width: 75px" /></td>
            			   <td><?php echo e($st->name); ?></td>
					   <td><?php echo e($st->address); ?></td>
					   <td><?php echo e($st->city); ?></td>
					   <td><?php echo e($st->state); ?></td>
					   <td><?php echo e($st->phone); ?></td>
					   <td><?php echo e($st->timings); ?></td>
					</tr>
            			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            		<?php endif; ?>
            	</tbody>
            </table>			
            </div>
		</div>
	</div>
</section><?php /**PATH /home/batrasus/domains/batrasmart.com/public_html/resources/views/themes/ekart/store.blade.php ENDPATH**/ ?>
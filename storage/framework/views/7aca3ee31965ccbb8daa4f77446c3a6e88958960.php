<style>
	#mega-menu{
		width: 100%;
    background-color: white;
    height: 90vh;	
    overflow-y: auto;
    position: sticky;
    top: 66px;
    z-index: 100;
	}
	.mcontent{
		padding: 12px 28px;
	}

	.mob-cate-list-heading{
    margin-top: 15px;
    padding-left: 11px;
    font-weight: bold;	
    font-size: 15px
}

	.mob-cat-heading-div{
		padding-left: 10px
	}
	.mob-cat-heading-div a{
		color: grey
	}

	.mob-cat-heading{
		text-transform: uppercase;
    	font-weight: bold;
    	font-size: 13px;
    	margin-top: 23px
	}

	.mob-cate-list{
		padding:0px 13px;
		border-bottom: 1px solid grey
	}

	.mob-flex-box{
		display: flex;
		justify-content: space-between;		
	}

	.mob-cate-list button{
		padding: 0px 14px;
	    font-size: 24px;
	    border: none;
	    background: none;
	}

	.mob-cate-list-content{
		display: flex;
	}

	.mob-cate-list-content img{
		width: 60px
	}

	.cate-heading{
		margin-top: 9px;
		font-size: 16px !important
	}
	.cate-heading a{
		color:black;
		text-transform: uppercase;
	}
	.subcat-div a{
		color: black
	}
	.cate-div img{
		width: 100px
	}
	#mobile-content{
		display: none
	}
	.sub-cats-list{
		display: none
	}
	/*Tab responsive*/
		@media  only screen and (min-width: 768px) and (max-width: 991px) {
	#mega-menu  {
	position: absolute;
    top: 0;
    height: 100vh;		
}
	#closebtn{
		margin-right: 26px !important
	}
	#mobile-content{
		display: none
	}
	}
	/* Mobile responsive */
@media  only screen and (max-width: 767px) {
	#mega-menu  {
	position: absolute;
    top: 0;
    height: 100vh;		
}
	#closebtn{
		margin-right: 26px !important;
		padding: 0px 10px !important;
    margin-top: 8px !important;
	}
	#desk-content{
		display: none
	}
	#mobile-content{
		display: block
	}
	.categories_title_new{
		display: none
	}
}
	#closebtn{
float: right;
    margin-right: 32px;
    padding: 10px 20px;
    background: none;
    border: none;
    font-size: 24px;
    border-radius: 50%;
	}
	#closebtn:hover{
		background-color: #F3F3F3
	}
	.cate-div{
		width: 100%;
		height: 100%
	}
	body::-webkit-scrollbar {
  width: 12px;               /* width of the entire scrollbar */
  z-index: 1000 !important
}

body::-webkit-scrollbar-track {
  background: #D5D5D5;        /* color of the tracking area */
  z-index: 1000 !important

}

body::-webkit-scrollbar-thumb {
  background-color: blue;    /* color of the scroll thumb */
  border-radius: 20px;       /* roundness of the scroll thumb */
  border: 3px solid blue;  /* creates padding around scroll thumb */
  z-index: 1000 !important

}

	#mega-menu::-webkit-scrollbar {
  width: 12px;               /* width of the entire scrollbar */
}

#mega-menu::-webkit-scrollbar-track {
  background: #D5D5D5;        /* color of the tracking area */
}

#mega-menu::-webkit-scrollbar-thumb {
  background-color: blue;    /* color of the scroll thumb */
  border-radius: 20px;       /* roundness of the scroll thumb */
  border: 3px solid blue;  /* creates padding around scroll thumb */
}
.msubs{
	display: none
}
</style>
<script>
	function closeMega(){
		$("#mega-menu").hide();
	}
	function toggleSubCats(cat_id,elem) {
		//Mobile

		if(elem.innerHTML == '+'){	elem.innerHTML = "-"; }
		else if(elem.innerHTML == '-'){ elem.innerHTML = "+"; }
		$(`#sub-cats-list-${cat_id}`).toggle();
	}

	function toggleSubCat(cat_id,elem,flag) {
		$(".subcats").hide();
		$(".div_subcat").css("border-top","none");

		if(flag==1){
		elem.style.borderTop="1px solid #ccc";
		$(`#subcat_${cat_id}`).show();
		$(`#subcat_${cat_id}`).css("padding",'0px 12px');
		$(`#subcat_${cat_id}`).css("box-shadow",'0px 1px 1px 1px #ccc');
		}
	}

	function mtoggleSubCat(cat_id) {
		$(".msubs").hide();

  var x = document.getElementById(`msubcat_${cat_id}`);
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }

		// $(`#msubcat_${cat_id}`).show();
	}

	function toggleSubcategories(cat_id,flag) {
		$(".subcats").hide();
		if(flag==1){
		$(`#subcat_${cat_id}`).show();
		}
		else{
		$(".subcats").hide();
		}
	}

	function hideSubcats(flag) {
		if(flag==0)
		{
		$(".subcats").hide();
		}
	}
</script>
<div id="mega-menu" style="display: none">
		<button id="closebtn" onclick="closeMega()">&times;</button>
	<div class="container mcontent" id="desk-content">
		<div>
<!-- 		<div class="row">
			<?php $__currentLoopData = Cache::get('categories'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<?php if($cat->is_home != 0): ?>
			<div class="col-md-3" style="margin-top: 10px">
		<div class="cate-div">
			<img src="<?php echo e($cat->image); ?>" alt="<?php echo e($cat->name); ?> image" class="img-fluid">
			<h5 class="cate-heading">
				<b>
					<a href="<?php echo e(route('shop', ['category' => $cat->slug])); ?>"><?php echo e($cat->name); ?></a>
				</b>
			</h5>
			<ul class="subcat-div">
				<?php $__currentLoopData = $cat->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subcat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<li><a href="<?php echo e(route('shop', ['category' => $cat->slug, 'sub-category' => $subcat->slug])); ?>"><?php echo e($subcat->name); ?></a></li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</ul>
		</div>
			</div>
			<?php endif; ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</div>
		<br>
		<br>
 -->		<div class="row">
			<?php $__currentLoopData = Cache::get('sub_menu_depts'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<?php if($cat->is_home != 0): ?>
			<div class="col-md-3" style="margin-top: 10px">
		<div class="cate-div">
			<img src="<?php echo e($cat->image); ?>" alt="<?php echo e($cat->name); ?> image" class="img-fluid">
			<h5 class="cate-heading">
				<b>
					<a href="department/<?php echo e($cat->slug); ?>"><?php echo e($cat->name); ?></a>
				</b>
			</h5>
			<ul class="subcat-div">
				<?php $subcats2=[]; ?>
				<?php $__currentLoopData = $cat->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subcat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php $__currentLoopData = Cache::get('categories'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<?php if($cats->id == $subcat->id): ?>
						<?php if(!empty($cats->childs)): ?>
							<?php $flag=1; $subcats2 = $cats->childs; ?>
						<?php else: ?>
							<?php $flag=0; ?>
						<?php endif; ?>
					<?php endif; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<li>
					<div>
						<div class="div_subcat" onmouseout="hideSubcats('<?php echo e($flag); ?>')">
					<a href="<?php echo e(route('shop', ['category' => $subcat->slug])); ?>" onmouseover="toggleSubcategories('<?php echo e($subcat->id); ?>','<?php echo e($flag); ?>')"><?php echo e($subcat->name); ?> </a>
					<?php if($flag==1): ?>
						&nbsp;<i style="margin-top: 4px" class="fas fa-chevron-right"></i>
					<?php endif; ?>							
						<div class="subcats" style="margin-left: 30px;display: none" id="subcat_<?php echo e($subcat->id); ?>">
							<ul>
										<?php if(!empty($subcats2)): ?>
											<?php $__currentLoopData = $subcats2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<li>
													<a href="<?php echo e(route('shop', ['category' =>$cat->slug, 'sub-category' => $c->slug])); ?>"><?php echo e($c->name); ?></a>
												</li>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										<?php endif; ?>	
							</ul>
						</div>
					</div>
				</li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</ul>
		</div>
			</div>
			<?php endif; ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

		</div>
		</div>
	</div>
		<!-- Mobile Content -->
		<div id="mobile-content">
			<h5 class="mob-cate-list-heading">All Departments</h5>
			<hr>
<!-- 			<?php $__currentLoopData = Cache::get('categories'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<?php if($cat->is_home != 0): ?>
			<div class="mob-cate-list">
				<div class="mob-flex-box">
				<div class="mob-cate-list-content">
				<div>
					<img src="<?php echo e($cat->image); ?>" alt="" class="img-fluid">
				</div>
				<div class="mob-cat-heading-div">
					<h5 class="mob-cat-heading"><?php echo e($cat->name); ?></h5>
					<div id="sub-cats-list-<?php echo e($cat->id); ?>" class="sub-cats-list">
						<ul>
				<?php $__currentLoopData = $cat->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subcat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<li><a href="<?php echo e(route('shop', ['category' => $cat->slug, 'sub-category' => $subcat->slug])); ?>"><?php echo e($subcat->name); ?></a></li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>							
						</ul>
					</div>
				</div>
				</div>
				<div>
				<button onclick="toggleSubCats('<?php echo e($cat->id); ?>',this)">+</button>
				</div>
				</div>
			</div>
			<?php endif; ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
 -->


			<?php $__currentLoopData = Cache::get('sub_menu_depts'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<?php if($cat->is_home != 0): ?>
			<div class="mob-cate-list">
				<div class="mob-flex-box">
				<div class="mob-cate-list-content">
				<div>
					<img src="<?php echo e($cat->image); ?>" alt="" class="img-fluid">
				</div>
				<div class="mob-cat-heading-div">
					<h5 class="mob-cat-heading"><?php echo e($cat->name); ?></h5>
					<div id="sub-cats-list-<?php echo e($cat->id); ?>" class="sub-cats-list" style="width: 273px">
						<ul>

				<?php $subcats2=[]; ?>
				<?php $__currentLoopData = $cat->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subcat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php $__currentLoopData = Cache::get('categories'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<?php if($cats->id == $subcat->id): ?>
						<?php if(!empty($cats->childs)): ?>
							<?php $flag=1; $subcats2 = $cats->childs; ?>
						<?php else: ?>
							<?php $flag=0; ?>
						<?php endif; ?>					
					<li style="position: relative;">
						<a href="<?php echo e(route('shop', ['category' =>$cats->slug])); ?>"><span><?php echo e($cats->name); ?></span></a>
						<?php if($flag==1): ?>
						<a style="position: absolute;left: 165px" onclick="mtoggleSubCat('<?php echo e($cats->id); ?>')">+</a>
						<ul class="msubs" style="margin-left: 40px;" id="msubcat_<?php echo e($cats->id); ?>">
							<?php $__currentLoopData = $subcats2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<li>
									<a href="<?php echo e(route('shop', ['category' =>$cats->slug, 'sub-category' => $c->slug])); ?>"><?php echo e($c->name); ?></a>
								</li>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</ul>
						<?php endif; ?>
					</li>
					<?php endif; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<!-- 				<?php $__currentLoopData = $cat->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subcat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<li>
					<a href="<?php echo e(route('shop', ['category' => $cat->slug, 'sub-category' => $subcat->slug])); ?>"><?php echo e($subcat->name); ?></a>
				</li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>							
 -->						</ul>
					</div>
				</div>
				</div>
				<div>
				<button onclick="toggleSubCats('<?php echo e($cat->id); ?>',this)">+</button>
				</div>
				</div>
			</div>
			<?php endif; ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

		</div>
	
</div><?php /**PATH /home/batrasus/domains/batrasmart.com/public_html/resources/views/themes/ekart/common/mega-menu.blade.php ENDPATH**/ ?>
<section class="section-content padding-bottom mt-5">
    <!--user address-->
    <a href="#" id="scroll"><span></span></a>
    <nav aria-label="breadcrumb"> 
        <ol class="breadcrumb">
            <li class=" item-1"></li>
            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>"><?php echo e(__('msg.home')); ?></a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php echo e(__('msg.my_account')); ?></li>
        </ol>   
    </nav>
    <div class="container">
        <div class="row">
            <?php echo $__env->make("themes.$theme.user.sidebar", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <main class="col-md-9">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg">
                                        <form action="<?php echo e(route('change_avatar')); ?>" id="avatar_image_form" enctype="multipart/form-data" method="post">
                                    <div>
                                        <?php echo csrf_field(); ?>
                                        <div>
                                        <?php 
                                            $image = 'https://apps.smsolutions.in/BATRAS/adminon/upload/avatars/'. $data['profile']['image'];
                                        ?>
                                        <img src="<?php echo e(asset($image)); ?>" alt="" class="img-fluid img-thumbnail" style="width: 164px">                                            
                                        </div>
                                        
                                        <input type="file" name="avatar_image" id="avatar_image" style="display: none">
                                       <input type="hidden" name="user_id" value="<?php echo (isset(session()->get('user')['user_id'])) ? session()->get('user')['user_id'] : ''; ?>">
									<div>
                                        <button class="btn btn-success" type="button" id="avatar_change_btn" style="margin-top: 9px">Upload Profile Image</button>
                                        <br>
                                        <small class="text-center" style="color: red;font-size: 14px;font-weight: bold;" id="err_avatar_image"></small>
									</div>                            
                                            
                                    </div>
                                        </form>
                                    <br>
                                <form method='POST' enctype="multipart/form-data">
                                    <?php echo csrf_field(); ?>
                                    <div class="form-row">
                                        <div class="col form-group">
                                            <label><?php echo e(__('msg.name')); ?></label>
                                            <input type="text" name="name" class="form-control" value="<?php echo e($data['profile']['name']); ?>" required>
											<!-- <small class="text-danger"><?php echo e($errors->first('name')); ?></small>                                             -->
                                        </div>
                                        <div class="col form-group">
                                            <label><?php echo e(__('msg.email')); ?></label>
                                            <input type="email" name="email" value="<?php echo e($data['profile']['email']); ?>" class="form-control">                                    
                                            <!-- <small class="text-danger"><?php echo e($errors->first('email')); ?></small>         -->
                                        </div>                                       
                                        <div class="col form-group">
                                            <label><?php echo e(__('msg.mobile')); ?></label>
                                            <input type="text" value="<?php echo e($data['profile']['mobile']); ?>" class="form-control" disabled="disabled">
                                            <!-- <small class="text-danger"><?php echo e($errors->first('mobile')); ?></small> -->
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col form-group">
                                            <label>Gender</label>
                                            <select name="gender" id="gender" class="form-control">
                                                <option value="male" <?php if($data['profile']['gender'] == 'male'){ ?> selected="selected" <?php } ?>>Male</option>
                                                <option value="female" <?php if($data['profile']['gender'] == 'female'){ ?> selected="selected" <?php } ?>>Female</option>
                                                <option value="other" <?php if($data['profile']['gender'] == 'other'){ ?> selected="selected" <?php } ?>>Others</option>
                                            </select>                                            
                                        </div>
                                        <div class="col form-group">
                                            <label>Aadhar No.</label>
                                            <input type="text" name="aadhar_no" class="form-control" value="<?php echo e($data['profile']['aadhar_no']); ?>">
                                            <!-- <small class="text-danger"><?php echo e($errors->first('aadhar_no')); ?></small> -->
                                        </div>
                                        <div class="col form-group">
                                            <label>GST No.</label>
                                            <input type="text" name="gst_no" class="form-control" value="<?php echo e($data['profile']['gst_no']); ?>">
                                            <!-- <small class="text-danger"><?php echo e($errors->first('gst_no')); ?></small> -->

                                        </div>
                                        <div class="col form-group">
                                            <label>Business Name</label>
                                            <input type="text" name="business_name" class="form-control" value="<?php echo e($data['profile']['business_name']); ?>">
                                            <!-- <small class="text-danger"><?php echo e($errors->first('business_name')); ?></small> -->

                                        </div>
                                    </div>                                   
                                    <div class="form-group">
                                        <button type="submit" name="submit" value="submit" class="btn btn-primary mt-4"><?php echo e(__('msg.update')); ?> </button>
                                    </div>         
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </main>   
        </div>   
    </div>
    <!--end user address-->
</section><?php /**PATH /home/batrasus/domains/batrasmart.com/public_html/resources/views/themes/ekart/user/account.blade.php ENDPATH**/ ?>
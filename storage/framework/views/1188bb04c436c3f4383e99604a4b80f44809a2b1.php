<style>
    .offer_container, .banner_container{
        margin:20px 0px;
    }
    
    .ad_offer_box_wrapper{
        position:relative;
        margin-bottom:20px;
        background-color:#fff;
    }
    
    .ad_offer_box_wrapper>a{
        height:240px;
        width:100%;
        display:inline-block;
    }
    
    .ad_offer_box_wrapper>a>.offer_cat_img{
        width:150px;
        margin: 30px auto;
    }
    
    .ad_offer_badge{
        position:absolute;
        top: 2%;
        left: 2%;
        z-index: 2;
        width: 70px;
    }
    
    .ad_offer_text{
position: absolute;
    top: 6%;
    left: 3%;
    line-height: 17px;
    z-index: 3;
    color: #fff;
    font-size: 16px;
    width: 62px;
    font-weight: 700;
    }
    
    .ad_offer_text_dyn{
        font-weight:600px !important;
    }
    
    .offer_cat_name{
        position: absolute;
        top: 85%;
        color: #ffffff;
        font-size: 16px;
        z-index: 5;
        background: #1d86d6;
        width: 100%;
        text-align: center;
        font-weight: 700;
        box-shadow: 0px 2px 2px 0px #00000082;
    }
</style>

<section class="section-content spacingrm container">

<?php if(Cache::has('ad_groups') && is_array(Cache::get('ad_groups')) && count(Cache::get('ad_groups'))): ?>
    
<?php $__currentLoopData = Cache::get('ad_groups'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="banner_container">
        <div class="container">
        <?php if(isset($ag->ad_group_type) && $ag->ad_group_type == 1): ?>
    
            <div class="row">
        <?php if(Cache::has('ads') && is_array(Cache::get('ads')) && count(Cache::get('ads'))): ?>
            <?php $__currentLoopData = Cache::get('ads'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $o): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if(isset($o->image) && trim($o->image) !== "" && $o->ad_group_id == $ag->ad_group_id): ?>
                <div class="col-md-6 div_banner_style" style="padding-left: 3px !important;padding-right: 3px !important;padding-top: 7px">
                    <a href="<?php echo e($o->ad_link); ?>">
                        <img src="<?php echo e($o->image); ?>" alt="<?php echo e($o->ad_link); ?>" class="img-fluid image_banner_style" style="display: block;margin: 0 auto;width: 100%;">
                    </a>
                </div>
            <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    
        <?php endif; ?>
            </div>
        <?php endif; ?>
        </div>
    </div>
    
    <?php if(isset($ag->ad_group_type) && $ag->ad_group_type == 2): ?>
    <br>
        <div class="offer_container">
            <div class="container">
            <h4 class="title-section title-sec font-weight-bold"><?php echo e($ag->group_name); ?></h4>
            <hr class="line">
            
                <div class="row">
                    <?php if(Cache::has('ads') && is_array(Cache::get('ads')) && count(Cache::get('ads'))): ?>
                        <?php $__currentLoopData = Cache::get('ads'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $o): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if(isset($o->image) && trim($o->image) !== "" && $o->ad_group_id == $ag->ad_group_id): ?>
                            <div class="<?php echo e(getClassByCount($ag->image_per_row)); ?>" style="">
                                
                                <div class="ad_offer_box_wrapper" style="background-color: <?php echo e($ag->back_color); ?>">
                                <?php if($ag->is_third_party == 1): ?>
                                    <a href="<?php echo e($o->ad_link); ?>" target="_blank">
                                        <div class="offer_cat_img">
                                        <img src="<?php echo e($o->image); ?>" alt="<?php echo e($o->image_caption); ?>" class="img-fluid" style="<?php echo e(getStyleByCount($ag->image_per_row)); ?>">
                                        </div>
                                    </a>
                                <?php else: ?>
                                    <?php if(empty($o->subcategory_slug)): ?>
                                        <a href="<?php echo e(route('category',['slug'=>$o->category_slug2])); ?>">
                                            <div class="offer_cat_img">
                                                <img src="<?php echo e($o->image); ?>" alt="<?php echo e($o->image_caption); ?>" class="img-fluid" style=" <?php echo e(getStyleByCount($ag->image_per_row)); ?>">
                                            </div>
                                            <p class="offer_cat_name"><?php echo e($o->cat_name); ?></p>
                                        </a>
                                    <?php else: ?>
                                        <a href="<?php echo e(route('shop',['category'=>$o->category_slug2,'sub-category'=>$o->subcategory_slug2])); ?>">
                                            <div class="offer_cat_img">
                                                <img src="<?php echo e($o->image); ?>" alt="<?php echo e($o->image_caption); ?>" class="img-fluid" style="<?php echo e(getStyleByCount($ag->image_per_row)); ?>">
                                            </div>
                                            <p class="offer_cat_name"><?php echo e($o->scat_name); ?></p>
                                        </a>
                                    <?php endif; ?>
                                <?php endif; ?>
                                
                                <img class="ad_offer_badge" src="<?php echo e(asset('images/offer-badge.png')); ?>">
                                <p class="ad_offer_text" style="text-align: center;"><small>Upto</small><br/><span class="ad_offer_text_dyn"><?php echo e($o->discount_text); ?> % </span><br/><small>off</small></p>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<?php endif; ?>


</section>
<?php /**PATH /home/batrasmart/public_html/resources/views/themes/ekart/parts/ads.blade.php ENDPATH**/ ?>
<?php if(Cache::has('departments') && is_array(Cache::get('departments')) && count(Cache::get('departments'))): ?>
	<!--section categories popular categories transparent image-->
    <section class="section-content padding-bottom popular-categories mb-4 mt-4">
		<div class="container">
			<h4 class="title-section font-weight-bold"><?php echo e(__('msg.popular_departments')); ?></h4>
			<hr class="line">
			
			<style>
    			.owl-dept-item-wrapper{
    			    background-color:#fff;
    			    border-radius:3px;
    			    box-shadow: 1px 1px 5px #e6e6e6;
                    margin: 5px 0px;
    			}
    			
    			.owl-dept-item-wrapper a{
    			    color:#363940;
    			    font-weight: 700;
                    letter-spacing: 0.1px;
    			}
			    .owl-dept-item-wrapper>.owl-dept-img>a>img{
			        padding: 0px 20px;
			    }
			    
			    .owl-carousel-departments{
			        position:relative;
			    }
			    
			    .owl-carousel-departments>.owl-nav{
			        position:relative;
			    }
			    
			    .owl-dept-text{
			        min-height:50px;
			    }
			    
			    @media(max-width:480px){
			        .owl-dept-item-wrapper a{
			            font-size:12px;
			            line-height:13px;
			        }
			    }
			</style>
			
			<div class="owl-container">
			    <div class="owl-carousel owl-carousel-departments" id="departments-slider-hm">                    
                    <?php $__currentLoopData = Cache::get('departments'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dept): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($dept->is_home !=0): ?>
                        <?php if(isset($dept->image) && trim($dept->image) !== ""): ?>
                              <div class="owl-dept-item-wrapper">
                                  <div class="owl-dept-img">
                                      <a href="department/<?php echo e($dept->slug); ?>"><img src="<?php echo e($dept->image); ?>" alt="<?php echo e($dept->name); ?>" ></a>
                                  </div>
                                  
                                  <div class="owl-dept-text">
                                      <p class="text-center"><a href="departments/<?php echo e($dept->slug); ?>"><?php echo e($dept->name); ?></a></p>
                                  </div>
                              </div>          
                        <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
			</div>
			
			<script>
                $(document).ready(function(){
                    $("#departments-slider-hm").owlCarousel({
                        loop:true,
                        margin:20,
                        autoplay:true,
                        autoplayTimeout:1800,
                        autoplayHoverPause:true,
                        responsive:{
                            0:{
                                items:2
                            },
                            576:{
                                items:3
                            },
                            991:{
                                items:5,
                                // nav:true,
                            },
                            1199:{
                                items:6,
                                // nav:true,
                            },
                            1920:{
                                items:7,
                                // nav:true,
                            }
                        }
                    });
                });
            </script>
			
			<div class="d-none">
				<div class="row p-0">
					<?php $__currentLoopData = Cache::get('categories'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div class="col-lg-4 col-md-6 col-12">
                        <?php if($c->web_image !== ''): ?>
							<div class="item popular web">
                                <img class="category-item" src="<?php echo e($c->web_image); ?>" alt="<?php echo e($c->name ?? 'Category'); ?>">
                                <span class="overlay-text">
                                    <p class="text-dark title font-weight-bold name mb-0"><?php echo e($c->name); ?></p>
                                    <small class="text-muted subtitle"><?php echo e($c->subtitle); ?></small>
                                    <p class="m-0">
                                        <a href="<?php echo e(route('category', $i)); ?>" class="shop-now"><?php echo e(__('msg.shop_now')); ?> <i class="fa fa-chevron-right"></i></a>
                                    </p>
                                </span>
							</div>
                        <?php else: ?>
                            <div class="item category-item-card rounded">
                                <img class="category-item" src="<?php echo e($c->image); ?>" alt="<?php echo e($c->name ?? 'Category'); ?>">
                                <span class="overlay-text">
                                    <p class="text-dark title font-weight-bold name mb-0"><?php echo e($c->name); ?></p>
                                    <small class="text-muted subtitle"><?php echo e($c->subtitle); ?></small>
                                    <p class="m-0">
                                        <a href="<?php echo e(route('category', $i)); ?>" class="shop-now"><?php echo e(__('msg.shop_now')); ?> <em class="fa fa-chevron-right"></em></a>
                                    </p>
                                </span>
                            </div>
                        <?php endif; ?>

						</div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
			</div>
		</div>
    </section>
    <!--end section categories-->
<?php endif; ?><?php /**PATH /home/batrasus/domains/batrasmart.com/public_html/resources/views/themes/ekart/parts/departments.blade.php ENDPATH**/ ?>
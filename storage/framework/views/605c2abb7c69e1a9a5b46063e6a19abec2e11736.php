<?php if(Cache::has('offers') && is_array(Cache::get('offers')) && count(Cache::get('offers'))): ?>
             <section class="section-content banneradvertise spacingrm">
                <div class="container">
                    <div id="offer-slider-hm">                    
    <?php $__currentLoopData = Cache::get('offers'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $o): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if(isset($o->image) && trim($o->image) !== ""): ?>
                        <img src="<?php echo e($o->image); ?>" alt="offer" style="width:100%">
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </section>
<?php endif; ?>
<br>
<section class="section-content banneradvertise spacingrm">
<div class="container">
<!-- Promo Codes -->
<?php if(Cache::has('get-promo-codes')): ?>
    <?php $__currentLoopData = Cache::get('get-promo-codes'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if(isset($pr->home_image) && $pr->home_image!=''): ?>    
        <img src="adminon/<?php echo e($pr->home_image); ?>" alt="" style="width:100%;display: block">
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
<!-- Promo Codes end -->
</div>
</section>

<script>
    $(document).ready(function(){
        $("#offer-slider-hm").owlCarousel({
            items:1,
            loop:false,
            autoplay:false,
            autoplayTimeout:6000,
            autoplayHoverPause:true
        });
    });
</script>


<!---section advertise ---->


<!----section end advertise -->
<?php /**PATH /home/batrasmart/public_html/resources/views/themes/ekart/parts/offers.blade.php ENDPATH**/ ?>
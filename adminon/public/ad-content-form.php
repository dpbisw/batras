<?php 
include_once('includes/functions.php');
include_once('includes/functions2.php');
$function = new functions;
$fnc2 = new functions2;
// error_reporting(E_ALL);
// ini_set('display_errors', '1');
    if(isset($_POST['form_type']) && $_POST['form_type'] == 'banner')
    {
        $total_ads = $_POST['total_ads'];
        $success_flag = 0;
        if($total_ads > 0)
        {
            for($i = 1;$i<=$total_ads;$i++)
            {
                $ad_link = $_POST['ad_link_'.$i];
                $category_id = $_POST['add_category_'.$i];
                $category_slug_data = $fnc2->getSlugById($category_id,1);
                foreach($category_slug_data as $cat)
                {
                    $category_slug = $cat['slug'];
                }
                $subcategory_id = 0;
                $subcategory_slug = '';
                if(isset($_POST['chb_'.$i]))
                    {
                        $subcategory_id = $_POST['add_sub_category_'.$i];
                        $subcategory_slug_data = $fnc2->getSlugById($subcategory_id,0);
                        foreach ($subcategory_slug_data as $sc) {
                            $subcategory_slug = $sc['slug'];
                        }
                    }
        if(!empty($category_slug)){
        $extension = end(explode(".", $_FILES["banner_image_".$i]["name"]));
            // create random image file name
            $string = '0123456789';
            $file = preg_replace("/\s+/", "_", $_FILES['banner_image_'.$i]['name']);
            $image = $function->get_random_string($string, 4) . "-" . date("Y-m-d") . "." . $extension;
            // upload new image
            $upload = move_uploaded_file($_FILES['banner_image_'.$i]['tmp_name'], 'upload/advertisements/' . $image);
            $upload_image = 'upload/advertisements/' . $image;
        $sql = "INSERT INTO ads (`ad_image`,`category_id`,`subcategory_id`,`ad_group_id`) VALUES ('".$upload_image."','".$category_id."','".$subcategory_id."','".$_POST['ad_group_id']."')";      
            if ($db->sql($sql)) {
                $success_flag = 1;
            }
        }
        }
        if ($success_flag == 1) {
            ?>
            <p class='alert alert-success'>Saved Successfully!</p>
            <?php
        } else {
            ?>
            <p class='alert alert-danger'>Something went wrong please try again!</p>
            <?php
        }       
        }
            
    }
    if(isset($_POST['form_type']) && $_POST['form_type'] == 'offer')
    {
        // echo "<pre>";
        // print_r($_POST);
        // echo "</pre>";
        $total_offers = $_POST['total_offers'];
        $success_flag = 0;        
        if($total_offers > 0)
        {
            for($i = 1;$i<=$total_offers;$i++)
            {
                // $ad_link = $_POST['ad_link_'.$i];
                $image_text = $_POST['image_text_'.$i];
                $category_id = $_POST['add_category_'.$i];
                $category_slug_data = $fnc2->getSlugById($category_id,1);
                foreach($category_slug_data as $cat)
                {
                	$category_slug = $cat['slug'];
                }
                $subcategory_id = 0;
                $subcategory_slug = '';
                if(isset($_POST['chb_'.$i]))
                    {
                        $subcategory_id = $_POST['add_sub_category_'.$i];
                        $subcategory_slug_data = $fnc2->getSlugById($subcategory_id,0);
                        foreach ($subcategory_slug_data as $sc) {
                        	$subcategory_slug = $sc['slug'];
                        }
                    }
                if(!empty($category_slug)){
        $extension = end(explode(".", $_FILES["offer_image_".$i]["name"]));
            // create random image file name
            $string = '0123456789';
            $file = preg_replace("/\s+/", "_", $_FILES['offer_image_'.$i]['name']);
            $image = $function->get_random_string($string, 4) . "-" . date("Y-m-d") . "." . $extension;
            // upload new image
            $upload = move_uploaded_file($_FILES['offer_image_'.$i]['tmp_name'], 'upload/advertisements/' . $image);
            $upload_image = 'upload/advertisements/' . $image;
        $sql = "INSERT INTO ads (`ad_image`,`image_caption`,`ad_link`,`ad_group_id`,`category_id`,`subcategory_id`) VALUES ('".$upload_image."','".$image_text."','','".$_POST['ad_group_id']."','".$category_id."','".$subcategory_id."')";      
            if ($db->sql($sql)) {
                $success_flag = 1;
            }
        }
            }
        if ($success_flag == 1) {
            ?>
            <p class='alert alert-success'>Saved Successfully!</p>            
            <?php
        } else {
            ?>
            <p class='alert alert-danger'>Something went wrong please try again!</p>
            <?php
        }       
        }
    }
?>
<section class="content-header">
    <h1>Add Content</h1>
    <ol class="breadcrumb">
        <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
    </ol>
    <hr />
</section>
<?php 
if ($permissions['ads']['create'] == 1) {
?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
                    <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Group Content</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                	<div class="row">
                		<div class="col-md-3">
                	<div class="form-group">
                		<label for="">Select Group Type</label>
                		<select name="" id="" class="form-control" onchange="changeSection(this.value)">
                			<option value="1">Banner Type</option>
                			<option value="2">Offer Type</option>
                		</select>
                	</div>
                        </div>
                    </div>
                    <div id="banner_type_form">
                        <form action="" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="form_type" value="banner">
                    <div class="row">
                        <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Select Group</label>
                        <select name="ad_group_id" id="" class="form-control">
                            <?php 
                        $fn2 = new functions2();
                        $ad_groups = $fn2->getSelectedAdGroups(1);
                            foreach($ad_groups as $ag){ ?>
                                <option value="<?php echo $ag['ad_group_id']; ?>"><?php echo $ag['group_name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    </div>
                </div>
                <div id="banner_multi">
                <div class="row">
                    <div class="col-md-3">                                                      
                    <div class="form-group">
                        <label>Upload Banner Image <small>(Max size :100kb)</small></label>
                        <input type="file" name="banner_image_1" class="form-control banner_img" required onchange="checkImageSize(this,1)">
                        <p style="color:red" id="error_banner_image_1"></p>
                    </div>                
                    </div>
<!--                     <div class="col-md-5">                                      
                    <div class="form-group">
                        <label for="">Advertisement Link</label>
                        <input type="text" name="ad_link_1" class="form-control" required>
                    </div>                                                      
                    </div>
 -->
                                <div class="col-md-3">
                                <?php 
                                $cats = $fn2->getAllCategories();
                                ?>
                    <div class="form-group">
                        <label>Category</label>
                        <select name="add_category_1" id="ad_category_1" class="form-control" onchange="showSubcats(this.value,1)">
                            <?php foreach($cats as $c){
                                ?>
                                <option value="<?php echo $c['id']; ?>"><?php echo $c['name']; ?></option>
                                <?php
                            } ?>
                        </select>
                    </div>          
                            <input type="checkbox" name="chb_1" id="chbb_1" onclick="toggleSubcats2(this,1)"> Show Sub-categories                                            
                                </div>
                                <div class="col-md-2" id="subcat_1" style="display: none">
                    <div class="form-group">
                        <label>Sub Category</label>
                        <?php 
                            $first_subcat = $fn2->getFirstSubcategory();
                        ?>
                        <select name="add_sub_category_1" id="ad_sub_category_1" class="form-control">
                            <?php 
                                foreach($first_subcat as $fs)
                                {
                                    ?>
                                    <option value="<?php echo $fs['id']; ?>"><?php echo $fs['name']; ?></option>
                                    <?php
                                }
                            ?>
                        </select>
                    </div>                                                      
                                </div>
                </div>
                </div>
                        <input type="hidden" name="total_ads" value="1" id="total_ads">
                        <button type="button" class="btn btn-primary" onclick="addMoreImage()">Add More Image</button>
                        <br>
                        <br>
                            <button type="submit" class="btn btn-success" id="addBannerBtn">Add Banner</button>
                        </form>                        
                    </div>
                    <div id="offer_type_form" style="display: none">
                        <form action="" method="POST" enctype="multipart/form-data" id="ad_offer_form">
                        <input type="hidden" name="form_type" value="offer">                            
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                <label>Select Group Type</label>
                        <select name="ad_group_id" class="form-control" required>
                            <?php 
                        $ad_groups2 = $fn2->getSelectedAdGroups(2);
                            foreach($ad_groups2 as $ag){ ?>
                                <option value="<?php echo $ag['ad_group_id']; ?>"><?php echo $ag['group_name']; ?></option>
                            <?php } ?>
                        </select>
                                </div>
                            </div>
                        </div>
                        <div id="offer_multi">
                            <div class="row">
                                <div class="col-md-2">
                    <div class="form-group">
                        <label>Upload Section Image</label>
                        <input type="file" name="offer_image_1" class="form-control" required onchange="checkImageSize2(this,1)">
                        <small>Maximum size :100kb</small>
                        <small style="color:red;font-weight: bold" id="error_offer_image_1"></small>
                    </div>                                                                               
                                </div>
                                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Image Caption</label>
                        <input type="text" name="image_text_1" class="form-control" required>
                    </div>                                                                              
                                </div>
                                <div class="col-md-3">
                                <?php 
                                $cats = $fn2->getAllCategories();
                                ?>
                    <div class="form-group">
                        <label for="">Category</label>
                        <select name="add_category_1" id="add_category_1" class="form-control" onchange="showSubcats(this.value,1)">
                            <?php foreach($cats as $c){
                                ?>
                                <option value="<?php echo $c['id']; ?>"><?php echo $c['name']; ?></option>
                                <?php
                            } ?>
                        </select>
                    </div>          
                            <input type="checkbox" name="chb_1" id="chb_1" onclick="toggleSubcats(this,1)"> Show Sub-categories                                            
                                </div>
                                <div class="col-md-2" id="subcats_1" style="display: none">
                    <div class="form-group">
                        <label for="">Sub Category</label>
                        <?php 
                            $first_subcat = $fn2->getFirstSubcategory();
                        ?>
                        <select name="add_sub_category_1" id="add_sub_category_1" class="form-control">
                            <?php 
                                foreach($first_subcat as $fs)
                                {
                                    ?>
                                    <option value="<?php echo $fs['id']; ?>"><?php echo $fs['name']; ?></option>
                                    <?php
                                }
                            ?>
                        </select>
                    </div>                                                      
                                </div>
                            </div>                            
                        </div>
                        <input type="hidden" name="total_offers" value="1" id="total_offers">
                        <button type="button" class="btn btn-primary" onclick="addMoreImage2()">Add More Image</button>
                        <br>
                        <br>                        
                        <button type="submit" class="btn btn-success" id="addOfferBtn">Add Offer</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } else { ?>
    <div class="alert alert-danger topmargin-sm" style="margin-top: 20px;">You have no permission to create Ads.</div>
<?php } ?>
<script>
    function changeSection(sec_type) {
        if(sec_type == 1)
        {
            $("#offer_type_form").hide();
            $("#banner_type_form").show();
        }
        else if(sec_type == 2){
            $("#banner_type_form").hide();            
            $("#offer_type_form").show();
        }
    }
    // Function to add Banner row below the existing banner row
    var app_index = 1;
    function addMoreImage(){
        app_index += 1;
                    // <div class="col-md-5">                                      
                    // <div class="form-group">
                    //     <label for="">Advertisement Link</label>
                    //     <input type="text" name="ad_link_${app_index}" class="form-control" required>
                    // </div>                                                      
                    // </div>
        var html = `
                <div id="add_row_${app_index}">
                <div class="row">
                    <div class="col-md-3">                                                      
                    <div class="form-group">
                        <label for="">Upload Banner Image <small>(Max size :100kb)</small></label>
                        <input type="file" name="banner_image_${app_index}" class="form-control" required onchange="checkImageSize(this,${app_index})">
                        <p style="color:red" id="error_banner_image_${app_index}"></p>
                    </div>                
                    </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Category</label>
                        <select name="add_category_${app_index}" id="ad_category_${app_index}" class="form-control" onchange="showSubcats(this.value,${app_index})">
                            <?php foreach($cats as $c){
                                ?>
                                <option value="<?php echo $c['id']; ?>"><?php echo $c['name']; ?></option>
                                <?php
                            } ?>
                        </select>
                    </div>          
                            <input type="checkbox" name="chb_${app_index}" id="chbb_${app_index}" onclick="toggleSubcats2(this,${app_index})"> Show Sub-categories                                            
                    </div>
                                <div class="col-md-2" id="subcat_${app_index}" style="display: none">
                    <div class="form-group">
                        <?php 
                            $first_subcat = $fn2->getFirstSubcategory();
                        ?>                    
                        <label for="">Sub Category</label>
                        <select name="add_sub_category_${app_index}" id="add_sub_category_${app_index}" class="form-control">
                            <?php 
                                foreach($first_subcat as $fs)
                                {
                                    ?>
                                    <option value="<?php echo $fs['id']; ?>"><?php echo $fs['name']; ?></option>
                                    <?php
                                }
                            ?>                        
                        </select>
                    </div>                                                      
                                </div>
                    <div class="col-md-2">
                        <button class="btn btn-danger" style="margin-top: 25px" type="button" onclick="removeAddRow('${app_index}')">Remove</button>
                    </div>                    
                </div>
                </div>
        `;        
        $("#banner_multi").append(html);
        $("#total_ads").val(app_index);
    }
    //Function to append Offer row below the existing row
    var off_index=1;
    function addMoreImage2()
    {
        off_index += 1;
        var html = `
                            <div id="off_row_${off_index}">
                            <div class="row">
                                <div class="col-md-2">
                    <div class="form-group">
                        <label>Upload Section Image</label>
                        <input type="file" name="offer_image_${off_index}" class="form-control" required  onchange="checkImageSize2(this,${off_index})">
                        <small>Maximum size :100kb</small>
                        <small style="color:red;font-weight: bold" id="error_offer_image_${off_index}"></small>
                    </div>                                                                               
                                </div>
                                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Image Caption</label>
                        <input type="text" name="image_text_${off_index}" class="form-control" required>
                    </div>                                                                          
                                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Category</label>
                        <select name="add_category_${off_index}" id="add_category_${off_index}" class="form-control" onchange="showSubcats(this.value,${off_index})">
                            <?php foreach($cats as $c){
                                ?>
                                <option value="<?php echo $c['id']; ?>"><?php echo $c['name']; ?></option>
                                <?php
                            } ?>
                        </select>
                    </div>          
                            <input type="checkbox" name="chb_${off_index}" id="chb_${off_index}" onclick="toggleSubcats(this,${off_index})"> Show Sub-categories                                            
                                </div>
                                <div class="col-md-2" id="subcats_${off_index}" style="display: none">
                    <div class="form-group">
                        <?php 
                            $first_subcat = $fn2->getFirstSubcategory();
                        ?>                    
                        <label for="">Sub Category</label>
                        <select name="add_sub_category_${off_index}" id="ad_sub_category_${off_index}" class="form-control">
                            <?php 
                                foreach($first_subcat as $fs)
                                {
                                    ?>
                                    <option value="<?php echo $fs['id']; ?>"><?php echo $fs['name']; ?></option>
                                    <?php
                                }
                            ?>                        
                        </select>
                    </div>                                                      
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-danger" style="margin-top: 25px" onclick="removeOffRow('${off_index}')">Remove</button>
                                </div>                                
                            </div>
                            </div>                                    
        `;
        $("#offer_multi").append(html);
        $("#total_offers").val(off_index);
    }
    //Remove Ad row
    function removeAddRow(index) {
        $(`#add_row_${index}`).remove();
        // app_index -= 1;
        // $("#total_ads").val(app_index);
    }
    //Remove Offer row
    function removeOffRow(index) {
        $(`#off_row_${index}`).remove();
        // off_index -= 1;
        // $("#total_offers").val(off_index);
    }    
    function toggleSubcats(cb,index)
    {
        if(cb.checked)
        {
            $(`#subcats_${index}`).css('display','block');
        }
        else{
            $(`#subcats_${index}`).css('display','none');            
        }
    }
    function toggleSubcats2(cb,index)
    {
        if(cb.checked)
        {
            $(`#subcat_${index}`).css('display','block');
        }
        else{
            $(`#subcat_${index}`).css('display','none');            
        }
    }
    function showSubcats(cat_id,index) {
        // alert(cat_id);
            // var category_id = $("#edit_category").val();
            $.ajax({
                url : 'get-subcategorybyid.php',
                type: "post",
                dataType : 'json',
                data: 'accesskey=90336&category_id='+cat_id,
                success: function(result){
                    var html = '';
                    result.forEach(function(element){
                        html += `<option value='${element.id}'>${element.name}</option>`;
                    });
                    $(`#add_sub_category_${index}`).html(html);
                    // $("#edit-sub-category").html(html);
                    // alert(html);
                }
            });        
    }
function checkImageSize(elem,index) {
    if(elem.files[0].size > 100000){
        $(`#error_banner_image_${index}`).html("Please select a file less than 100kb size");
        $("#addBannerBtn").attr('disabled',true);
    }
    else{
        $(`#error_banner_image_${index}`).html("");
        $("#addBannerBtn").attr('disabled',false);        
    }
}
function checkImageSize2(elem,index) {
    if(elem.files[0].size > 100000){
        $(`#error_offer_image_${index}`).html("Please select a file less than 100kb size");
        $("#addOfferBtn").attr('disabled',true);
    }
    else{
        $(`#error_offer_image_${index}`).html("");
        $("#addOfferBtn").attr('disabled',false);        
    }
}
//                 //binds to onchange event of your input field
// $('#image').bind('change', function() {
//   //this.files[0].size gets the size of your file.
//   if(this.files[0].size > 100000)
//   {
//     $("#err_image").html("Please select a file less than 100kb size");
//     $("#btnAdd").attr('disabled',true);
 
//   }
//   else{
//     $("#err_image").html("");
//     $("#btnAdd").attr('disabled',false);
//   }
// });
</script>
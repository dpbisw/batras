<?php
include_once('includes/functions.php');
$function = new functions;
include_once('includes/custom-functions.php');
$fn = new custom_functions;
?>
<?php
if (isset($_POST['btnAdd'])) {
	if (ALLOW_MODIFICATION == 0 && !defined(ALLOW_MODIFICATION)) {
		echo '<label class="alert alert-danger">This operation is not allowed in demo panel!.</label>';
		return false;
	}
	if ($permissions['categories']['create'] == 1) {
        $department_id = $db->escapeString($fn->xss_clean($_POST['department_id']));
		$category_name = $db->escapeString($fn->xss_clean($_POST['category_name']));
		$slug = $db->escapeString($function->slugify($fn->xss_clean($_POST['category_name'])));
		$category_subtitle = $db->escapeString($fn->xss_clean($_POST['category_subtitle']));
		$is_home = $_POST['is_home'];
        $sql = "SELECT slug FROM category";
		$db->sql($sql);
		$res = $db->getResult();
		$i = 1;
		foreach ($res as $row) {
			if ($slug == $row['slug']) {
				$slug = $slug . '-' . $i;
				$i++;
			}
		}
		// get image info
		$menu_image = $db->escapeString($fn->xss_clean($_FILES['category_image']['name']));
		$image_error = $db->escapeString($fn->xss_clean($_FILES['category_image']['error']));
		$image_type = $db->escapeString($fn->xss_clean($_FILES['category_image']['type']));
		// get image info
		$menu_image2 = $db->escapeString($fn->xss_clean($_FILES['category_image2']['name']));
		$image_error2 = $db->escapeString($fn->xss_clean($_FILES['category_image2']['error']));
		$image_type2 = $db->escapeString($fn->xss_clean($_FILES['category_image2']['type']));
		// create array variable to handle error
		$error = array();
		if (empty($department_id)) {
			$error['category_name'] = " <span class='label label-danger'>Required!</span>";
		}
		if (empty($category_name)) {
			$error['category_name'] = " <span class='label label-danger'>Required!</span>";
		}
		if (empty($category_subtitle)) {
			$error['category_subtitle'] = " <span class='label label-danger'>Required!</span>";
		}
		// common image file extensions
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		// get image file extension
		error_reporting(E_ERROR | E_PARSE);
		$extension = end(explode(".", $_FILES["category_image"]["name"]));
		$extension2 = end(explode(".", $_FILES["category_image2"]["name"]));
		if ($image_error > 0) {
			$error['category_image'] = " <span class='label label-danger'>Not Uploaded!!</span>";
		} else {
			$mimetype = mime_content_type($_FILES["category_image"]["tmp_name"]);
			if (!in_array($mimetype, array('image/jpg', 'image/jpeg', 'image/gif', 'image/png'))) {
				$error['category_image'] = " <span class='label label-danger'>Image type must jpg, jpeg, gif, or png!</span>";
			}
		}
		if ($image_error > 0) {
			$error['category_image2'] = " <span class='label label-danger'>Not Uploaded!!</span>";
		} else {
			$mimetype2 = mime_content_type($_FILES["category_image2"]["tmp_name"]);
			if (!in_array($mimetype2, array('image/jpg', 'image/jpeg', 'image/gif', 'image/png'))) {
				$error['category_image2'] = " <span class='label label-danger'>Image type must jpg, jpeg, gif, or png!</span>";
			}
		}
		if (!empty($department_id) && !empty($category_name) && !empty($category_subtitle) && empty($error['category_image']) && empty($error['category_image2'])) {
			// create random image file name
			$string = '0123456789';
			$file = preg_replace("/\s+/", "_", $_FILES['category_image']['name']);
			$menu_image = $function->get_random_string($string, 4) . "-" . date("Y-m-d") . "." . $extension;
			// upload new image
			$upload = move_uploaded_file($_FILES['category_image']['tmp_name'], 'upload/images/' . $menu_image);
			// insert new data to menu table
			$upload_image = 'upload/images/' . $menu_image;
			$file2 = preg_replace("/\s+/", "_", $_FILES['category_image2']['name']);
			$menu_image2 = $function->get_random_string($string, 4) . "-" . date("Y-m-d") . "." . $extension2;
			// upload new image
			$upload2 = move_uploaded_file($_FILES['category_image2']['tmp_name'], 'upload/images/' . $menu_image2);
			// insert new data to menu table
			$upload_image2 = 'upload/images/' . $menu_image2;
			$sql_query = "INSERT INTO category (department_id,name,slug,subtitle, image, image2,web_image,is_home)VALUES('$department_id', '$category_name', '$slug', '$category_subtitle', '$upload_image','$upload_image2','','$is_home')";
			$db->sql($sql_query);
			$result = $db->getResult();
			if (!empty($result)) {
				$result = 0;
			} else {
				$result = 1;
			}
			if ($result == 1) {
				$error['add_category'] = " <section class='content-header'><span class='label label-success'>Category Added Successfully</span></section>";
			} else {
				$error['add_category'] = " <span class='label label-danger'>Failed add category</span>";
			}
		}
	} else {
		$error['check_permission'] = " <section class='content-header'><span class='label label-danger'>You have no permission to create category</span></section>";
	}
}
?>
<section class="content-header">
    <h1>Add Category <small><a href='categories.php'> <i class='fa fa-angle-double-left'></i>&nbsp;&nbsp;&nbsp;Back to Categories</a></small></h1>
    <?php echo isset($error['add_category']) ? $error['add_category'] : ''; ?>
    <ol class="breadcrumb">
        <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
    </ol>
    <hr />
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <?php if ($permissions['categories']['create'] == 0) { ?>
                <div class="alert alert-danger">You have no permission to create category.</div>
            <?php } ?>
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Category</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group">
							<label for="department_id">Department</label><?php echo isset($error['department_id']) ? $error['department_id'] : ''; ?>
							<select class="form-control" id="department_id" name="department_id" required>
								<option value="">--Select Department--</option>
								<?php
								if ($permissions['categories']['read'] == 1) {
    								$sql = "SELECT * FROM departments";
    								$db->sql($sql);
									$res = $db->getResult();
									foreach ($res as $department) {
										echo "<option value='" . $department['id'] . "'>" . $department['name'] . "</option>";
									}
								}
								?>
							</select>
						</div>
                        <div class="form-group">
                            <label for="category_name">Category Name</label><?php echo isset($error['category_name']) ? $error['category_name'] : ''; ?>
                            <input type="text" class="form-control" name="category_name" required>
                        </div>
                        <div class="form-group">
                            <label for="category_subtitle">Category Subtitle</label><?php echo isset($error['category_subtitle']) ? $error['category_subtitle'] : ''; ?>
                            <input type="text" class="form-control" name="category_subtitle" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Image&nbsp;&nbsp;&nbsp;*Please choose square image of larger than 350px*350px & smaller than 550px*550px.</label><?php echo isset($error['category_image']) ? $error['category_image'] : ''; ?>
                            <input type="file" name="category_image" id="category_image" required />
                            <p style="color:red" id="err_category_image"></p>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Image 2&nbsp;&nbsp;&nbsp;*Minimal Icon</label><?php echo isset($error['category_image2']) ? $error['category_image2'] : ''; ?>
                            <input type="file" name="category_image2" id="category_image2" required />
                            <p style="color:red" id="err_category_image2"></p>
                        </div>
                        <div class="form-group">
                        	<label for="">Visible in Home page</label><br>
                        	<input type="checkbox" id="is_home_button" class="js-switch" checked>
                        	<input type="hidden" name="is_home" value="1" id="is_home">
                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary" name="btnAdd" id="btnAdd">Add</button>
                        <input type="reset" class="btn-warning btn" value="Clear" />
                    </div>
                </form>
            </div><!-- /.box -->
            <?php echo isset($error['check_permission']) ? $error['check_permission'] : ''; ?>
        </div>
    </div>
</section>
<div class="separator"> </div>
<script>
	    var changeCheckbox = document.querySelector('#is_home_button');
    var init = new Switchery(changeCheckbox);
    changeCheckbox.onchange = function() {
        if ($(this).is(':checked')) {
            $('#is_home').val(1);
        } else {
            $('#is_home').val(0);
        }
    };
        //binds to onchange event of your input field
$('#category_image').bind('change', function() {
  //this.files[0].size gets the size of your file.
  if(this.files[0].size > 50000)
  {
    $("#err_category_image").html("Please select a file less than 50kb size");
    $("#btnAdd").attr('disabled',true);
 
  }
  else{
    $("#err_category_image").html("");
    $("#btnAdd").attr('disabled',false);
  }
});
        //binds to onchange event of your input field
$('#category_image2').bind('change', function() {
  //this.files[0].size gets the size of your file.
  if(this.files[0].size > 50000)
  {
    $("#err_category_image2").html("Please select a file less than 50kb size");
    $("#btnAdd").attr('disabled',true);
 
  }
  else{
    $("#err_category_image2").html("");
    $("#btnAdd").attr('disabled',false);
  }
});
</script>
<?php $db->disconnect(); ?>

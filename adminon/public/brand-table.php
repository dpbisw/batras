<?php
include_once('includes/functions.php');
include_once('includes/functions2.php');

?>
<section class="content-header">
    <h1>Brand /<small><a href="home.php"><i class="fa fa-home"></i> Home</a></small></h1>
</section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-xs-12">
                <div class="box container">
                    <div class="row">
                        <div class="col-md-4" style="border-right: 1px solid #F1F1F1">
                    <div>
                        <h3 class="box-title">Add Brand</h3>
                    </div>
                    <p id="status_cate" style="display: none"></p>
                            <form id="add_form" action="public/db-operation2.php" method="POST" enctype="multipart/form-data">
                            <input type="hidden" id="add_brand" name="add_brand" required="" value="1" aria-required="true">
                                <div class="box-body">
                                    <p id="add_group_status"></p>
                                    <div class="form-group" id="col1">
                                        <label for="brand_name">Brand Name</label>
                                        <input type="text" class="form-control" id="brand_name" name="brand_name">
                                        <p id="error_brand_name" style="color:red"></p>
                                    </div>
                                    <div class="form-group" id="col1">
                                        <label for="is_visible">Select Status</label>
                                        <select id="is_visible" name="is_visible" class="form-control" required>
                                            <option value="1">Visible</option>
                                            <option value="0">Hidden</option>
                                        </select>
                                    </div>
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary" id="submit_btn" name="btnAdd">Add</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        	<div class="col-md-8 container">
                    <div style="padding:5px 0px">
                        <h3 class="box-title">Brand Table</h3>
                    </div>
                        <table class="table table-hover" id="brand_table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Status</th>
                                    <th>View Products</th>
									<th>Action</th>
                                </tr>
                            </thead>
                        <?php 
                        $fn2 = new functions2();
                        $brands = $fn2->getBrands();
                        ?>
                            <tbody>
                                <?php foreach($brands as $b){ ?>
                                <tr>
                                    <td><?php echo $b['brand_id']; ?></td>
                                    <td><?php echo ($b['brand_name']); ?></td>
                                    <td><?php echo ($b['brand_slug']); ?></td>
                                    <td><?php echo ($b['is_visible'] == 1) ? 'Visible' : 'Hidden'; ?></td>
                                    <td>
                                        <?php $id = $b['brand_id']; ?>
                                        <a href="view-brand-products.php?id=<?php echo $id; ?>" class="btn btn-success">View Products</a>
                                    </td>
									<td style="display: flex">
										<button class="btn btn-primary" onclick="showEditModal('<?php echo $b['brand_id'] ?>')"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>&nbsp;
										<button onclick="deleteConfirm('<?php echo $b['brand_id']; ?>')" data-confirm="Do you really want to do this?" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>                        		
                        	</div>                        
                    </div>
                </div>
            </div>
            <div class="separator"> </div>
        </div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Brand</h4>
        <p id="edit_status"></p>
      </div>
        <form id="edit_form" action="public/db-operation2.php" method="POST" enctype="multipart/form-data">
        <input type="hidden" id="update_brand" name="update_brand" required="" value="1" aria-required="true">
      <div class="modal-body">
            <input type="hidden" name="edit_brand_id" id="edit_brand_id">
            <div class="form-group">
                <label for="edit_brand_name">Brand Name</label>
                <input type="text" class="form-control" id="edit_brand_name" name="edit_brand_name"  required>
            </div>            
            <div class="form-group">
                <label for="edit_is_visible">Status</label>
                <select id="edit_is_visible" name="edit_is_visible" class="form-control" required>
                  <option value="1">Visible</option>
                  <option value="0">Hidden</option>
                </select>
            </div>
      </div>
      <div class="modal-footer">
        <button type="submit" id="edit_btn" class="btn btn-success">Update</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
        </form>
    </div>

  </div>
</div>

    </section>
<script>
	$(function(){
		$("#brand_table").DataTable();
	});

    $("#edit_form").on('submit',function(e){
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            method : 'POST',
            url: $(this).attr('action'),
            data: formData,
            beforeSend: function() {
                $('#edit_btn').html('Please wait..');
            },
            cache: false,
            contentType: false,
            processData: false,
            success: function(result) {
                if(result == 1)
                {
                    $("#edit_status").removeClass("alert-danger");                    
                    $("#edit_status").addClass("alert alert-success");
                    $("#edit_status").html("Record Updated Successfully!");
                    location.reload();
                }

            	if(result == 0 || result == 2){
                    $("#edit_status").removeClass("alert-success");                    
                    $("#edit_status").addClass("alert alert-danger");

            		if(result == 0) msg = 'Something Went Wrong. Please Try Again!';
            		if(result == 2) msg = 'This Category name already exists. Please try Again!';
            		$("#edit_status").html(`<i class="fa fa-exclamation-circle" aria-hidden="true"></i> ${msg}`);
            		$('#edit_btn').html('Edit');
                }
            }
        });

    });
    
    $('#add_form').on('submit', function(e) {
        e.preventDefault();
        if($(`#brand_name`).val()=='')
        {
        	$("#error_brand_name").html(`<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Brand Name is Required`);
        }
        else{
        	$("#error_brand_name").html("");        	
        var formData = new FormData(this);
        $.ajax({
            method: 'POST',
            url: $(this).attr('action'),
            data: formData,
            beforeSend: function() {
                $('#submit_btn').html('Please wait..');
                $('#submit_btn').attr('disabled',true);
            },
            cache: false,
            contentType: false,
            processData: false,
            success: function(result) {
            	if(result == 1)
            	{
            		$("#status_cate").css("display",'block');
            		$("#status_cate").removeClass("alert-danger");
            		$("#status_cate").addClass("alert alert-success");
            		$("#status_cate").html(`<i class="fa fa-check-circle" aria-hidden="true"></i> Record Added Successfully!`);
            		location.reload();
            	}
            	if(result == 0 || result == 2){
            		$("#status_cate").css("display",'block');
            		$("#status_cate").removeClass("alert-success");
            		$("#status_cate").addClass("alert alert-danger");
            		var msg = '';
            		if(result == 0) msg = 'Something Went Wrong. Please Try Again!';
            		if(result == 2) msg = 'This Brand name already exists. Please try Again!';
            		$("#status_cate").html(`<i class="fa fa-exclamation-circle" aria-hidden="true"></i> ${msg}`);
            		$('#submit_btn').html('Add');
            	}
                $('#submit_btn').attr('disabled',false);

            }
        });

        }
    });

    function deleteGroup(group_id,elem) {
        var c = confirm('Do you want to delete this Group? All the Related Data will be deleted');
        if(c)
        {
        $.ajax({
            type: 'POST',
            url: "public/db-operation2.php",
            data: { group_delete_id : group_id },
            beforeSend : function(){
                elem.innerHTML="Deleting";
            },
            success: function(result) {
                if(result == 1)
                {
                    $("#group_delete_status").removeClass('alert-danger');
                    $("#group_delete_status").addClass('alert alert-success');
                    $("#group_delete_status").html("Record Deleted Successfully!");
                    elem.innerHTML = 'Deleted';

                    location.reload();
                }
                if(result == 0){
                    $("#group_delete_status").removeClass('alert-success');
                    $("#group_delete_status").addClass('alert alert-danger');    
                    $("#group_delete_status").html(`Something went wrong..Please try again!`);  
                    elem.innerHTML = 'Delete';
                }

            }
        });            
        }
    }

    function showEditModal(brand_edit_id) {
        $.ajax({
            type: 'POST',
            url: "public/db-operation2.php",
            data: { brand_edit_id : brand_edit_id },
            success: function(result) {
                var jObj = JSON.parse(result);
                jObj.forEach(function(item,index){
                	$("#edit_brand_id").val(item.brand_id);
                	$("#edit_brand_name").val(item.brand_name);
                	$("#edit_is_visible").val(item.is_visible);
                });
            }
        });            
        $("#myModal").modal('show');
    }

    function deleteConfirm(brand_id)
    {
		var c = confirm("Do you want to Delete this Brand?");
		if(c)
		{
        $.ajax({
            type: 'POST',
            url: "public/db-operation2.php",
            data: { brand_delete_id : brand_id },
            success: function(result) {
            	if(result == 1)
            	{
            		alert("Deleted");
            		location.reload();
            	}
            	else{
            		alert("Error");
            	}
            }
        });            			
		}    	
    }
</script>
<?php
include_once('includes/functions.php');
include_once('includes/functions2.php');
include_once('includes/custom-functions.php');
$fn = new custom_functions;
// include_once('includes/crud.php');
$function = new Functions;
$function2 = new Functions2;
// $db = new Database();
if (isset($_GET['id'])) {
    $ID = $db->escapeString($fn->xss_clean($_GET['id']));
} else {
    // $ID = "";
    return false;
    exit(0);
}
// create array variable to store category data
$category_data = array();
$product_status = "";
// $sql = "select id,name from category order by id asc";
// $db->sql($sql);
// $category_data = $db->getResult();
// $sql = "select * from subcategory";
// $db->sql($sql);
// $subcategory = $db->getResult();
$sql = "SELECT image, other_images FROM products WHERE id =" . $ID;
$db->sql($sql);
$res = $db->getResult();
foreach ($res as $row) {
    $previous_menu_image = $row['image'];
    $other_images = $row['other_images'];
}
if (isset($_POST['btnEdit'])) {

    // echo "<pre>";
    // print_r ($_POST);
    // echo "</pre>";
	
	$sqla = "SELECT attributes FROM products WHERE id =" . $ID;
	$db->sql($sqla);
	$resa = $db->getResult();
    $attrs = '';
	foreach ($resa as $value) {
		$attrs = unserialize($value['attributes']);
	}
    $attributes = $attrs['attribute'];
    $attribute_values = $attrs['attribute_value'];

    $av_attributes = explode(',',$_POST['attrs_av']);
    $av_attribute_values = explode(',',$_POST['attrs_value_av']);

    $av_attributes_search = [];
    foreach ($av_attributes as $av) {

        $sql = "SELECT attribute_name FROM attributes where attribute_id='$av'";
        $db->sql($sql);
        $res = $db->getResult();
        foreach($res as $r){
            array_push($av_attributes_search, $r['attribute_name']);
        }
    }

    // $av_attributes_values = [];
    foreach ($av_attribute_values as $av) {

        $sql = "SELECT attribute_value FROM attribute_values where attr_value_id='$av'";
        $db->sql($sql);
        $res = $db->getResult();
        foreach($res as $r){
            array_push($av_attributes_search, $r['attribute_value']);
        }
    }

    // echo "<pre>";
    // print_r ($av_attributes_search);
    // echo "</pre>";

    $user_attrs = $_POST['attributes1'];
    $flag_arr = [];
    foreach ($user_attrs as $ua) {
        if(substr_count($ua, ':')==1){
            $arr = explode(':', $ua);
            foreach ($arr as $value) {
                if(in_array($value, $av_attributes_search))
                {
                    array_push($flag_arr, '1');
                }
                else{
                    array_push($flag_arr, '0');                    
                }
            }
        }
        else{
            $whole_arr = [];
            $outer_arr = explode(' ', $ua);
            foreach ($outer_arr as $o){
                $inner_arr = explode(':', $o);
                foreach ($inner_arr as $i) {
                    array_push($whole_arr, $i);
                }
            }
            foreach ($whole_arr as $w) {
                if(in_array($w,$av_attributes_search)){
                    array_push($flag_arr,'1');
                }
                else{
                    array_push($flag_arr,'0');
                }
            }
        }
    }


    // echo "<pre>";
    // print_r ($_POST['attributes1']);
    // echo "</pre>";

  //           $attrsd='';
		// 	$attr_select = $_POST['attr_select'];
		// 	$attr_value_select = $_POST['attr_value_select'];
  //           if(!empty($attr_select) && !empty($attr_value_select)){
		// 	$attrs = [
		// 		'attribute'=>$attr_select,
		// 		'attribute_value'=>$attr_value_select
		// 	];
		// 	$attrsd = serialize($attrs);
		// 	}

		// var_dump($attrsd);

    if (ALLOW_MODIFICATION == 0 && !defined(ALLOW_MODIFICATION)) {
        echo '<label class="alert alert-danger">This operation is not allowed in demo panel!.</label>';
        return false;
    }
    if ($permissions['products']['update'] == 1) {
        $name = $db->escapeString($fn->xss_clean($_POST['name']));
        // if (strpos($name, '-') !== false) {
        //     $temp = (explode("-", $name)[1]);
        // } else {
        //     $temp = $name;
        // }

        $slug = $function->slugify($name);
        // $slug= $temp;
        $id = $db->escapeString($fn->xss_clean($_GET['id']));
        // $sql = "SELECT slug FROM products where id!=" . $id;
        // $db->sql($sql);
        // $res = $db->getResult();
        // $i = 1;
        // foreach ($res as $row) {
        //     if ($slug == $row['slug']) {
        //         $slug = $slug . '-' . $i;
        //         $i++;
        //     }
        // }

        $subcategory_id = (isset($_POST['subcategory_id']) && $_POST['subcategory_id'] != '') ? $db->escapeString($fn->xss_clean($_POST['subcategory_id'])) : 0;
        $category_id = $db->escapeString($fn->xss_clean($_POST['category_id']));
        $department_id = $db->escapeString($fn->xss_clean($_POST['department_id']));
        $brand_id = $db->escapeString($fn->xss_clean($_POST['brand_id']));
        $serve_for = $db->escapeString($fn->xss_clean($_POST['serve_for']));
        
        $description = $db->escapeString($fn->xss_clean($_POST['description']));
        $feature_details = $db->escapeString($fn->xss_clean($_POST['feature_details']));
        $disclaimer = $db->escapeString($fn->xss_clean($_POST['disclaimer']));
        $product_description = $db->escapeString($fn->xss_clean($_POST['product_description']));
        
        $pr_status = $db->escapeString($fn->xss_clean($_POST['pr_status']));
        $manufacturer = (isset($_POST['manufacturer']) && $_POST['manufacturer'] != '') ? $db->escapeString($fn->xss_clean($_POST['manufacturer'])) : '';
        $made_in = (isset($_POST['made_in']) && $_POST['made_in'] != '') ? $db->escapeString($fn->xss_clean($_POST['made_in'])) : '';
        $indicator = (isset($_POST['indicator']) && $_POST['indicator'] != '') ? $db->escapeString($fn->xss_clean($_POST['indicator'])) : '0';
        $return_status = (isset($_POST['return_status']) && $_POST['return_status'] != '') ? $db->escapeString($fn->xss_clean($_POST['return_status'])) : '0';
        $cancelable_status = (isset($_POST['cancelable_status']) && $_POST['cancelable_status'] != '') ? $db->escapeString($fn->xss_clean($_POST['cancelable_status'])) : '0';
        $till_status = (isset($_POST['till_status']) && $_POST['till_status'] != '') ? $db->escapeString($fn->xss_clean($_POST['till_status'])) : '';

        $dont_forget_ids = '';
        if(isset($_POST['product_ids']))
        {
            $product_ids = $fn->xss_clean_array($_POST['product_ids']);
            $dont_forget_ids = implode(',', $product_ids);            
        }

        $tax_id = (isset($_POST['tax_id']) && $_POST['tax_id'] != '') ? $db->escapeString($fn->xss_clean($_POST['tax_id'])) : 0;

        // get image info
        $image = $db->escapeString($fn->xss_clean($_FILES['image']['name']));
        $image_error = $db->escapeString($fn->xss_clean($_FILES['image']['error']));
        $image_type = $db->escapeString($fn->xss_clean($_FILES['image']['type']));
        $error = array();
        
        

        if (empty($name)) {
            $error['name'] = " <span class='label label-danger'>Required!</span>";
        }
        if ($cancelable_status == 1 && $till_status == '') {
            $error['cancelable'] = " <span class='label label-danger'>Required!</span>";
        }
        
        if (empty($department_id)) {
            $error['department_id'] = " <span class='label label-danger'>Required!</span>";
        }

        if (empty($category_id)) {
            $error['category_id'] = " <span class='label label-danger'>Required!</span>";
        }
        if (empty($serve_for)) {
            $error['serve_for'] = " <span class='label label-danger'>Not choosen</span>";
        }

        if (empty($description)) {
            $error['description'] = " <span class='label label-danger'>Required!</span>";
        }

        // common image file extensions
        $allowedExts = array("gif", "jpeg", "jpg", "png");

        // get image file extension
        error_reporting(E_ERROR | E_PARSE);
        $extension = end(explode(".", $_FILES["image"]["name"]));

        if (!empty($image)) {
            $mimetype = mime_content_type($_FILES["image"]["tmp_name"]);
            if (!in_array($mimetype, array('image/jpg', 'image/jpeg', 'image/gif', 'image/png'))) {
                $error['image'] = " <span class='label label-danger'>Image type must jpg, jpeg, gif, or png!</span>";
            }
        }
        /*updating other_images if any*/

        if (isset($_FILES['other_images']) && ($_FILES['other_images']['size'][0] > 0)) {
            $file_data = array();
            $target_path = 'upload/other_images/';
            for ($i = 0; $i < count($_FILES["other_images"]["name"]); $i++) {
                if ($_FILES["other_images"]["error"][$i] > 0) {
                    $error['other_images'] = " <span class='label label-danger'>Images not uploaded!</span>";
                } else {
                    $mimetype = mime_content_type($_FILES["other_images"]["tmp_name"][$i]);
                    if (!in_array($mimetype, array('image/jpg', 'image/jpeg', 'image/gif', 'image/png'))) {
                        $error['other_images'] = " <span class='label label-danger'>Images type must jpg, jpeg, gif, or png!</span>";
                    }
                }
                $filename = $_FILES["other_images"]["name"][$i];
                $temp = explode('.', $filename);
                $filename = microtime(true) . '-' . rand(100, 999) . '.' . end($temp);
                $file_data[] = $target_path . '' . $filename;
                if (!move_uploaded_file($_FILES["other_images"]["tmp_name"][$i], $target_path . '' . $filename))
                    echo "{$_FILES['image']['name'][$i]} not uploaded<br/>";
            }
            if (!empty($other_images)) {
                $arr_old_images = json_decode($other_images);
                $all_images = array_merge($arr_old_images, $file_data);
                $all_images = json_encode(array_values($all_images));
            } else {
                $all_images = json_encode($file_data);
            }
            if (empty($error)) {
                $sql = "update `products` set `other_images`='" . $all_images . "' where `id`=" . $ID;
                $db->sql($sql);
            }
        }
        if (!empty($name) && !empty($category_id) &&  !empty($serve_for) && !empty($description) && empty($error['cancelable']) && empty($error)) {
            if (strpos($name, "'") !== false) {
                $name = str_replace("'", "''", "$name");
                if (strpos($description, "'") !== false)
                    $description = str_replace("'", "''", "$description");
            }
            if (!empty($image)) {
                // create random image file name
                $string = '0123456789';
                $file = preg_replace("/\s+/", "_", $_FILES['image']['name']);
                $function = new functions;
                $image = $function->get_random_string($string, 4) . "-" . date("Y-m-d") . "." . $extension;
                // delete previous image
                $delete = unlink("$previous_menu_image");
                // upload new image
                $upload = move_uploaded_file($_FILES['image']['tmp_name'], 'upload/images/' . $image);

                $upload_image = 'upload/images/' . $image;
                $sql_query = "UPDATE products SET name = '$name' ,tax_id = '$tax_id' ,slug = '$slug' ,department_id = '$department_id' , category_id = '$category_id' , subcategory_id = '$subcategory_id',brand_id = '$brand_id', image = '$upload_image', description = '$description', feature_details = '$feature_details',  disclaimer = '$disclaimer', product_description = '$product_description' , dont_forget_at_add_products = '$dont_forget_ids' , indicator = '$indicator', manufacturer = '$manufacturer', made_in = '$made_in', return_status = '$return_status', cancelable_status = '$cancelable_status', till_status = '$till_status',`status` = $pr_status WHERE id = $ID";
                $db->sql($sql_query);
            } else {
                $sql_query = "UPDATE products SET name = '$name' ,tax_id = '$tax_id' ,slug = '$slug' , department_id = '$department_id' , category_id = '$category_id' ,subcategory_id = '$subcategory_id' ,brand_id='$brand_id',description = '$description', feature_details = '$feature_details',  disclaimer = '$disclaimer', product_description = '$product_description' , dont_forget_at_add_products = '$dont_forget_ids' ,  indicator = '$indicator', manufacturer = '$manufacturer', made_in = '$made_in', return_status = '$return_status', cancelable_status = '$cancelable_status', till_status = '$till_status' ,`status` = $pr_status WHERE id = $ID";
                $db->sql($sql_query);
            }
            // echo $sql_query;
            $res = $db->getResult();
            $product_variant_id = $db->escapeString($fn->xss_clean($_POST['product_variant_id']));

            $count = count($_POST['attrib_measurement']);

            // for ($i = 0; $i < $count; $i++) {
            //     if ($_POST['type'] == "packet") {
            //         $stock = $db->escapeString($fn->xss_clean($_POST['packate_stock'][$i]));
            //         $serve_for = ($stock == 0 || $stock <= 0)?'Sold Out':$db->escapeString($fn->xss_clean($_POST['packate_serve_for'][$i]));
            //             $batras_rec = 0;
            //             if(isset($_POST['batras_rec1'][$i])){ $batras_rec = 1; }

            //         $data = array(
            //             'type' => $db->escapeString($fn->xss_clean($_POST['type'])),
            //             'measurement' => $db->escapeString($fn->xss_clean($_POST['packate_measurement'][$i])),
            //             'measurement_unit_id' => $db->escapeString($fn->xss_clean($_POST['packate_measurement_unit_id'][$i])),
            //             'price' => $db->escapeString($fn->xss_clean($_POST['packate_price'][$i])),
            //             'discounted_price' => $db->escapeString($fn->xss_clean($_POST['packate_discounted_price'][$i])),
            //             'stock' => $stock,
            //             'stock_unit_id' => $db->escapeString($fn->xss_clean($_POST['packate_stock_unit_id'][$i])),
            //             'serve_for' => $serve_for,
            //             'product_unique_id' => $_POST['product_unique_id1'][$i],
            //             'max_order_quantity' => $_POST['max_order_quantity1'][$i],
            //             'product_threshold' => $_POST['product_threshold1'][$i],
            //             'product_unique_id' => $_POST['product_unique_id1'][$i],
            //             'batras_recommendation'=>$batras_rec
            //         );

            //         $db->update('product_variant', $data, 'id=' . $fn->xss_clean($_POST['product_variant_id'][$i]));
            //         $res = $db->getResult();
            //     } else if ($_POST['type'] == "loose") {
            //         $stock = $db->escapeString($fn->xss_clean($_POST['loose_stock']));
            //         $serve_for = ($stock == 0 || $stock <= 0)?'Sold Out' : $db->escapeString($fn->xss_clean($_POST['serve_for']));
            //             $batras_rec = 0;
            //             if(isset($_POST['batras_rec0'][$i])){ $batras_rec = 1; }

            //         $data = array(
            //             'type' => $db->escapeString($fn->xss_clean($_POST['type'])),
            //             'measurement' => $db->escapeString($fn->xss_clean($_POST['loose_measurement'][$i])),
            //             'measurement_unit_id' => $db->escapeString($fn->xss_clean($_POST['loose_measurement_unit_id'][$i])),
            //             'price' => $db->escapeString($fn->xss_clean($_POST['loose_price'][$i])),
            //             'discounted_price' => $db->escapeString($fn->xss_clean($_POST['loose_discounted_price'][$i])),
            //             'stock' => $stock,
            //             'stock_unit_id' => $db->escapeString($fn->xss_clean($_POST['loose_stock_unit_id'])),
            //             'serve_for' => $serve_for,
            //             'product_unique_id' => $_POST['product_unique_id0'][$i],
            //             'max_order_quantity' => $_POST['max_order_quantity0'][$i],
            //             'product_threshold' => $_POST['product_threshold0'][$i],
            //             'product_unique_id' => $_POST['product_unique_id0'][$i],
            //             'batras_recommendation'=>$batras_rec
            //         );
            //         $db->update('product_variant', $data, 'id=' . $fn->xss_clean($_POST['product_variant_id'][$i]));
            //         $res = $db->getResult();
            //     }
            // }

            if($_POST['type'] == "attribute")
            {
            	$count=count($_POST['attrib_measurement']);
            	for($i=0;$i<$count;$i++){
                    $stock = $db->escapeString($fn->xss_clean($_POST['attrib_stock']));
                    $serve_for = ($stock == 0 || $stock <= 0)?'Sold Out' : $db->escapeString($fn->xss_clean($_POST['serve_for']));
                        $batras_rec = 0;
                        if(isset($_POST['attrib_batras_rec1'][$i])){ $batras_rec = 1; }

                    $data = array(
                        'type' => $db->escapeString($fn->xss_clean($_POST['type'])),
                        'measurement' => $db->escapeString($fn->xss_clean($_POST['attrib_measurement'][$i])),
                        'measurement_unit_id' => $db->escapeString($fn->xss_clean($_POST['attrib_measurement_unit_id'][$i])),
                        'price' => $db->escapeString($fn->xss_clean($_POST['attrib_price'][$i])),
                        'discounted_price' => $db->escapeString($fn->xss_clean($_POST['attrib_discounted_price'][$i])),
                        'stock' => $stock,
                        'stock_unit_id' => $db->escapeString($fn->xss_clean($_POST['attrib_stock_unit_id'])),
                        'serve_for' => $serve_for,
                        'product_unique_id' => $_POST['attrib_product_unique_id1'][$i],
                        'max_order_quantity' => $_POST['attrib_max_order_quantity1'][$i],
                        'product_threshold' => $_POST['attrib_product_threshold1'][$i],
                        'product_unique_id' => $_POST['attrib_product_unique_id1'][$i],
                        'batras_recommendation'=>$batras_rec
                    );
                    $db->update('product_variant', $data, 'id=' . $fn->xss_clean($_POST['product_variant_id'][$i]));
                    $res = $db->getResult();
                }
            }



            if (
                isset($_POST['insert_packate_measurement']) && isset($_POST['insert_packate_measurement_unit_id'])
                && isset($_POST['insert_packate_price']) && isset($_POST['insert_packate_discounted_price'])
                && isset($_POST['insert_packate_stock']) && isset($_POST['insert_packate_stock_unit_id'])
            ) {
                $insert_packate_measurement = $db->escapeString($fn->xss_clean($_POST['insert_packate_measurement']));
                for ($i = 0; $i < count($insert_packate_measurement); $i++) {
                    $stock = $db->escapeString($fn->xss_clean($_POST['insert_packate_stock'][$i]));
                    $serve_for = ($stock == 0 || $stock <= 0)?'Sold Out':$db->escapeString($fn->xss_clean($_POST['insert_packate_serve_for'][$i]));
                        $batras_rec = 0;
                        if(isset($_POST['batras_rec1'][$i])){ $batras_rec = 1; }

                    $data = array(
                        "product_id" => $db->escapeString($ID),
                        "type" => $db->escapeString($fn->xss_clean($_POST['type'])),
                        "measurement" => $db->escapeString($fn->xss_clean($_POST['insert_packate_measurement'][$i])),
                        "measurement_unit_id" => $db->escapeString($fn->xss_clean($_POST['insert_packate_measurement_unit_id'][$i])),
                        "price" => $db->escapeString($fn->xss_clean($_POST['insert_packate_price'][$i])),
                        "discounted_price" => $db->escapeString($fn->xss_clean($_POST['insert_packate_discounted_price'][$i])),
                        "stock" => $stock,
                        "stock_unit_id" => $db->escapeString($fn->xss_clean($_POST['insert_packate_stock_unit_id'][$i])),
                        "serve_for" => $serve_for,
                        'product_unique_id' => $_POST['product_unique_id1'][$i],
                        'max_order_quantity' => $_POST['max_order_quantity1'][$i],
                        'product_threshold' => $_POST['product_threshold1'][$i],
                        'product_unique_id' => $_POST['product_unique_id1'][$i],
                        'batras_recommendation'=>$batras_rec
                    );
                    $db->insert('product_variant', $data);
                    $res = $db->getResult();
                }
            }

            if (
                isset($_POST['insert_loose_measurement']) && isset($_POST['insert_loose_measurement_unit_id'])
                && isset($_POST['insert_loose_price']) && isset($_POST['insert_loose_discounted_price'])
            ) {
                $insert_loose_measurement = $db->escapeString($fn->xss_clean($_POST['insert_loose_measurement']));
                        $batras_rec = 0;
                        if(isset($_POST['batras_rec0'][$i])){ $batras_rec = 1; }
                for ($i = 0; $i < count($insert_loose_measurement); $i++) {
                    $data = array(
                        "product_id" => $db->escapeString($ID),
                        "type" => $db->escapeString($fn->xss_clean($_POST['type'])),
                        "measurement" => $db->escapeString($fn->xss_clean($_POST['insert_loose_measurement'][$i])),
                        "measurement_unit_id" => $db->escapeString($fn->xss_clean($_POST['insert_loose_measurement_unit_id'][$i])),
                        "price" => $db->escapeString($fn->xss_clean($_POST['insert_loose_price'][$i])),
                        "discounted_price" => $db->escapeString($fn->xss_clean($_POST['insert_loose_discounted_price'][$i])),
                        "stock" => $db->escapeString($fn->xss_clean($_POST['loose_stock'])),
                        "stock_unit_id" => $db->escapeString($fn->xss_clean($_POST['loose_stock_unit_id'])),
                        "serve_for" => $db->escapeString($fn->xss_clean($_POST['serve_for'])),
                        'product_unique_id' => $_POST['product_unique_id0'][$i],
                        'max_order_quantity' => $_POST['max_order_quantity0'][$i],
                        'product_threshold' => $_POST['product_threshold0'][$i],
                        'product_unique_id' => $_POST['product_unique_id0'][$i],
                        'batras_recommendation'=>$batras_rec

                    );
                    $db->insert('product_variant', $data);
                    $res = $db->getResult();
                }
            }
            $error['update_data'] = "<span class='label label-success'>Product updated Successfully</span>";
        }
    } else {
        $error['check_permission'] = " <section class='content-header'><span class='alert alert-danger'>You have no permission to update product</span></section>";
    }
}
// create array variable to store previous data
$data = array();
$sql_query = "SELECT v.*,p.*,v.id as product_variant_id FROM product_variant v JOIN products p ON p.id=v.product_id WHERE p.id=" . $ID;
$db->sql($sql_query);
$res = $db->getResult();
$product_status = $res[0]['status'];
foreach ($res as $row)
    $data = $row;
function isJSON($string)
{
    return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
}
?>
<section class="content-header">
    <h1>Edit Product <small><a href='products.php'> <i class='fa fa-angle-double-left'></i>&nbsp;&nbsp;&nbsp;Back to Products</a></small></h1>
    <small><?php echo isset($error['update_data']) ? $error['update_data'] : ''; ?></small>
    <ol class="breadcrumb">
        <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
    </ol>
    <br>
</section>
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">
            <?php if ($permissions['products']['update'] == 0) { ?>
                <div class="alert alert-danger topmargin-sm">You have no permission to update product.</div>
            <?php } ?>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Product</h3>
                </div>
                <div class="box-header">
                    <?php echo isset($error['cancelable']) ? '<span class="label label-danger">Till status is required.</span>' : ''; ?>
                </div>
                <!-- form start -->
                <form id='edit_product_form' method="post" enctype="multipart/form-data">
                    <?php
                    $db->select('unit', '*');
                    $unit_data = $db->getResult();
                    ?>
                    <div class="box-body">
                        <div class="form-group">
                            <div class='col-md-6'>
                                <label for="exampleInputEmail1">Product Name</label> <i class="text-danger asterik">*</i> <?php echo isset($error['name']) ? $error['name'] : ''; ?>
                                <input type="text" name="name" class="form-control" value="<?php echo $data['name']; ?>" />
                            </div>
                            <?php $db->sql("SET NAMES 'utf8'");
                            $sql = "SELECT * FROM `taxes` ORDER BY id DESC";
                            $db->sql($sql);
                            $taxes = $db->getResult();
                            ?>
                            <div class='col-md-6'>
                                <label class="control-label " for="taxes">Tax</label>
                                <select id='tax_id' name="tax_id" class='form-control'>
                                    <option value="">Select Tax</option>
                                    <?php foreach ($taxes as $tax) { ?>
                                        <option value='<?= $tax['id'] ?>' <?= ($data['tax_id'] == $tax['id']) ? 'selected' : ''; ?>><?= $tax['title'] . " - " . $tax['percentage'] . " %" ?></option>
                                    <?php } ?>
                                </select><br>
                            </div>
                        </div>
                        <label for="type">Type</label><?php echo isset($error['type']) ? $error['type'] : ''; ?>
                        <div class="form-group">
                            <label class="radio-inline" style="display: none">
                            	<input type="radio" name="type" id="packate" value="packet" <?= ($res[0]['type'] == "packet") ? "checked" : ""; ?>>Packet</label>
                            <label class="radio-inline" style="display: none;">
                            	<input type="radio" name="type" id="loose" value="loose" <?= ($res[0]['type'] == "loose") ? "checked" : ""; ?>>Loose</label>
                            <label class="radio-inline"><input type="radio" name="type" id="attribute" value="attribute" <?= ($res[0]['type'] == "attribute") ? "checked" : ""; ?>>Attribute</label>
                        </div>
                        <hr>
                        <div id="variations">
                            <?php 
                            if($res[0]['type'] != "attribute"){
                            ?>
                            <h5>Product Variations</h5>
                            <hr>
                        <?php } ?>
                            <?php
                            if (isJSON($data['price'])) {
                                $price = json_decode($data['price'], 1);
                                $measurement = json_decode($data['measurement'], 1);
                                $discounted_price = json_decode($data['discounted_price'], 1);
                            } else {
                                $price = array('0' => $data['price']);
                                $measurement = array('0' => $data['measurement']);
                                $discounted_price = array('0' => $data['discounted_price']);
                            }
                            $i = 0;
                            if ($res[0]['type'] == "packet") {
                                foreach ($res as $row) {
                            ?>
                                    <div class="row packate_div" style="margin-top: 10px">
                                        <input type="hidden" class="form-control" name="product_variant_id[]" id="product_variant_id" value='<?= $row['product_variant_id']; ?>' />
                                        <div class="col-md-2">
                                            <div class="form-group packate_div">
                                                <label for="exampleInputEmail1">Measurement</label> <i class="text-danger asterik">*</i> <input type="number" step="any" min="0" class="form-control" name="packate_measurement[]" value='<?= $row['measurement']; ?>' required />
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group packate_div">
                                                <label for="unit">Unit:</label>
                                                <select class="form-control" name="packate_measurement_unit_id[]">
                                                    <?php
                                                    foreach ($unit_data as  $unit) {
                                                        echo "<option";
                                                        if ($unit['id'] == $row['measurement_unit_id']) {
                                                            echo " selected ";
                                                        }
                                                        echo " value='" . $unit['id'] . "'>" . $unit['short_code'] . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group packate_div">
                                                <label for="price">M.R.P (<?= $settings['currency'] ?>):</label> <i class="text-danger asterik">*</i> <input type="number" step="any" min="0" class="form-control" name="packate_price[]" id="packate_price" value='<?= $row['price']; ?>' required />
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group packate_div">
                                                <label for="discounted_price">Price(<?= $settings['currency'] ?>):</label>
                                                <input type="number" step="any" min="0" class="form-control" name="packate_discounted_price[]" id="discounted_price" value='<?= $row['discounted_price']; ?>' />
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group packate_div">
                                                <label for="qty">Stock:</label> <i class="text-danger asterik">*</i>
                                                <input type="number" step="any" min="0" class="form-control" name="packate_stock[]" required value='<?= $row['stock']; ?>' />
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group packate_div">
                                                <label for="unit">Unit:</label>
                                                <select class="form-control" name="packate_stock_unit_id[]">
                                                    <?php
                                                    foreach ($unit_data as  $unit) {
                                                        echo "<option";
                                                        if ($unit['id'] == $row['stock_unit_id']) {
                                                            echo " selected ";
                                                        }
                                                        echo " value='" . $unit['id'] . "'>" . $unit['short_code'] . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group packate_div">

                                                <label for="qty">Status:</label>
                                                <select name="packate_serve_for[]" class="form-control">
                                                    <option value="Available" <?php if (strtolower($row['serve_for']) == "availabel") {
                                                                                    echo "selected";
                                                                                } ?>>Available</option>
                                                    <option value="Sold Out" <?php if (strtolower($row['serve_for']) == "sold out") {
                                                                                    echo "selected";
                                                                                } ?>>Sold Out</option>
                                                </select>
                                            </div>
                                        </div>
                                <div class="col-md-3">
                                    <label>Product Unique ID</label>
                                    <input type="text" name="product_unique_id1[]" class="form-control" value="<?php echo $row['product_unique_id'] ?>">
                                </div>
                                <div class="col-md-2">
                                    <label>Max. Order Quantity</label>
                                    <input type="text" name="max_order_quantity1[]" class="form-control" value="<?php echo $row['max_order_quantity'] ?>">
                                </div>
                                <div class="col-md-2">
                                    <label>Stock Threshold</label>
                                    <input type="text" name="product_threshold1[]" class="form-control" value="<?php echo $row['product_threshold'] ?>">
                                </div>
                                <div class="col-md-3">
                                    <label>BATRAS Recommendation</label><br/>
                                    <input type="checkbox" name="batras_rec1[]" <?php echo ($row['batras_recommendation'] == 1) ? 'checked':''; ?>> 
                                </div>                                

                                        <?php if ($i == 0) { ?>
                                            <div class='col-md-1'>
                                                <label>Variation</label>
                                                <a id='add_packate_variation' title='Add variation of product' style='cursor: pointer;'><i class="fa fa-plus-square-o fa-2x"></i></a>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-md-1" style="display: grid;">
                                                <label>Remove</label>
                                                <a class="remove_variation text-danger" data-id="data_delete" title="Remove variation of product" style="cursor: pointer;"><i class="fa fa-times fa-2x"></i></a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <hr>
                                <?php $i++;
                                }
                            } else {
                                $db->select('unit', '*');
                                $resedit = $db->getResult();
                                ?>
                                <div id="packate_div" style="display:none">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group packate_div">
                                                <label for="exampleInputEmail1">Measurement</label> <i class="text-danger asterik">*</i> <input type="number" step="any" min="0" class="form-control" name="packate_measurement[]" required />
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group packate_div">
                                                <label for="unit">Unit:</label>
                                                <select class="form-control" name="packate_measurement_unit_id[]">
                                                    <?php
                                                    foreach ($resedit as  $row) {
                                                        echo "<option value='" . $row['id'] . "'>" . $row['short_code'] . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group packate_div">
                                                <label for="price">Price (INR):</label> <i class="text-danger asterik">*</i> <input type="number" step="any" min="0" class="form-control" name="packate_price[]" id="packate_price" required />
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group packate_div">
                                                <label for="discounted_price">Discount:</label>
                                                <input type="number" step="any" min="0" class="form-control" name="packate_discounted_price[]" id="discounted_price" />
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group packate_div">
                                                <label for="qty">Stock:</label> <i class="text-danger asterik">*</i>
                                                <input type="number" step="any" min="0" class="form-control" name="packate_stock[]" />
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group packate_div">
                                                <label for="unit">Unit:</label>
                                                <select class="form-control" name="packate_stock_unit_id[]">
                                                    <?php
                                                    foreach ($resedit as  $row) {
                                                        echo "<option value='" . $row['id'] . "'>" . $row['short_code'] . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group packate_div">
                                                <label for="qty">Status:</label>
                                                <select name="packate_serve_for[]" class="form-control" required>
                                                    <option value="Available">Available</option>
                                                    <option value="Sold Out">Sold Out</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <label>Variation</label>
                                            <a id="add_packate_variation" title="Add variation of product" style="cursor: pointer;"><i class="fa fa-plus-square-o fa-2x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div id="packate_variations"></div>
                            <?php
                            $i = 0;
                            if ($res[0]['type'] == "loose") {
                                foreach ($res as $row) {
                            ?>
                                    <div class="row loose_div">
                                        <input type="hidden" class="form-control" name="product_variant_id[]" id="product_variant_id" value='<?= $row['product_variant_id']; ?>' />
                                        <div class="col-md-4">
                                            <div class="form-group loose_div">
                                                <label for="exampleInputEmail1">Measurement</label> <i class="text-danger asterik">*</i>
                                                <input type="number" step="any" min="0" class="form-control" name="loose_measurement[]" required="" value='<?= $row['measurement']; ?>'>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group loose_div">
                                                <label for="unit">Unit:</label>
                                                <select class="form-control" name="loose_measurement_unit_id[]">
                                                    <?php
                                                    foreach ($unit_data as  $unit) {
                                                        echo "<option";
                                                        if ($unit['id'] == $row['measurement_unit_id']) {
                                                            echo " selected ";
                                                        }
                                                        echo " value='" . $unit['id'] . "'>" . $unit['short_code'] . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group loose_div">
                                                <label for="price">M.R.P (<?= $settings['currency'] ?>):</label> <i class="text-danger asterik">*</i>
                                                <input type="number" step="any"  min="0" class="form-control" name="loose_price[]" id="loose_price" required="" value='<?= $row['price']; ?>'>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group loose_div">
                                                <label for="discounted_price">Price(<?= $settings['currency'] ?>):</label>
                                                <input type="number" step="any" min="0" class="form-control" name="loose_discounted_price[]" id="discounted_price" value='<?= $row['discounted_price']; ?>' />
                                            </div>
                                        </div>
                                <div class="col-md-3">
                                    <label>Product Unique ID</label>
                                    <input type="text" name="product_unique_id0[]" class="form-control" value="<?php echo $row['product_unique_id'] ?>">
                                </div>
                                <div class="col-md-2">
                                    <label>Max. Order Quantity</label>
                                    <input type="text" name="max_order_quantity0[]" class="form-control" value="<?php echo $row['max_order_quantity'] ?>">
                                </div>
                                <div class="col-md-2">
                                    <label>Stock Threshold</label>
                                    <input type="text" name="product_threshold0[]" class="form-control" value="<?php echo $row['product_threshold'] ?>">
                                </div>
                                <div class="col-md-3">
                                    <label>BATRAS Recommendation</label><br/>
                                    <input type="checkbox" name="batras_rec0[]" <?php echo ($row['batras_recommendation'] == 1) ? 'checked':''; ?>> 
                                </div>                                

                                        <?php if ($i == 0) { ?>
                                            <div class='col-md-1'>
                                                <label>Variation</label>
                                                <a id='add_loose_variation' title='Add variation of product' style='cursor: pointer;'><i class="fa fa-plus-square-o fa-2x"></i></a>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-md-1" style="display: grid;">
                                                <label>Remove</label>
                                                <a class="remove_variation text-danger" data-id="data_delete" title="Remove variation of product" style="cursor: pointer;"><i class="fa fa-times fa-2x"></i></a>
                                            </div>
                                        <?php }
                                        $i++; ?>
                                    </div>
                                <?php } ?>
                                <div id="loose_variations"></div>

                                <hr>
                                <div class="form-group" id="loose_stock_div" style="display:block;">
                                    <label for="quantity">Stock :</label> <i class="text-danger asterik">*</i> <?php echo isset($error['quantity']) ? $error['quantity'] : ''; ?>
                                    <input type="number" step="any" min="0" class="form-control" name="loose_stock" required value='<?= $row['stock']; ?>'>
                                </div>
                                <div class="form-group">
                                    <label for="stock_unit">Unit :</label><?php echo isset($error['stock_unit']) ? $error['stock_unit'] : ''; ?>
                                    <select class="form-control" name="loose_stock_unit_id" id="loose_stock_unit_id">
                                        <?php
                                        foreach ($unit_data as  $unit) {
                                            echo "<option";
                                            if ($unit['id'] == $row['stock_unit_id']) {
                                                echo " selected ";
                                            }
                                            echo " value='" . $unit['id'] . "'>" . $unit['short_code'] . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            <?php } else {
                                $db->select('unit', '*');
                                $resedit = $db->getResult();
                            ?>
                                <div id="loose_div" style="display:none;">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group loose_div">
                                                <label for="exampleInputEmail1">Measurement</label> <i class="text-danger asterik">*</i>
                                                <input type="number" step="any" min="0"  class="form-control" name="loose_measurement[]" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group loose_div">
                                                <label for="unit">Unit:</label>
                                                <select class="form-control" name="loose_measurement_unit_id[]">
                                                    <?php
                                                    foreach ($resedit as  $row) {
                                                        echo "<option value='" . $row['id'] . "'>" . $row['short_code'] . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group loose_div">
                                                <label for="price">Price (INR):</label> <i class="text-danger asterik">*</i>
                                                <input type="number" step="any" min="0" class="form-control" name="loose_price[]" id="loose_price" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group loose_div">
                                                <label for="discounted_price">Discounted Price:</label>
                                                <input type="number" step="any" min="0" class="form-control" name="loose_discounted_price[]" id="discounted_price" />
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <label>Variation</label>
                                            <a id="add_loose_variation" title="Add variation of product" style="cursor: pointer;"><i class="fa fa-plus-square-o fa-2x"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div id="variations">
                                </div>
                                
                                <div class="form-group" id="loose_stock_div" style="display:none;">
                                    <label for="quantity">Stock :</label> <i class="text-danger asterik">*</i> <?php echo isset($error['quantity']) ? $error['quantity'] : ''; ?>
                                    <input type="number" step="any" min="0" class="form-control" name="loose_stock" required>

                                    <label for="stock_unit">Unit :</label><?php echo isset($error['stock_unit']) ? $error['stock_unit'] : ''; ?>
                                    <select class="form-control" name="loose_stock_unit_id" id="loose_stock_unit_id">
                                        <?php
                                        foreach ($resedit as $row) {
                                            echo "<option value='" . $row['id'] . "'>" . $row['short_code'] . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            <?php } ?>
                            
                            <div id="attribute_div">

                            <?php 
                            $attributes = unserialize($res[0]['attributes']);
                            $attrs_av = implode(',',$attributes['attribute']);
                            $attr_values_av = implode(',',$attributes['attribute_value']);
                            ?>
                            <input type="hidden" name="attrs_av" value="<?php echo $attrs_av; ?>">
                            <input type="hidden" name="attrs_value_av" value="<?php echo $attr_values_av; ?>">

                           <div class="row" style="display: none">
                                <div class="col-md-3" >
                        <div class="form-group">
                            <label>Select Attribute</label><br>
                            <select name="attr_select[]" id="attr_select" class="form-control" multiple="multiple" style="width:100%">
                                <?php 
                                $sql = 'select attribute_id,attribute_name from `attributes` where `is_visible` = 1 order by attribute_id desc';
                                        $db->sql($sql);

                                        $result = $db->getResult();
                                        foreach($result as $value){
                                            ?>
                                            <option value='<?=$value['attribute_id']?>'><?=$value['attribute_name']?></option>
                                        <?php }?>                               
                            </select>
                        </div>                                  
                                </div>                              
                                <div class="col-md-3">
                        <div class="form-group">
                            <label>Select Attribute Values</label>
                            <select name="attr_value_select[]" id="attr_value_select" class="form-control" multiple="multiple" style="width: 100%">
                            </select>
                        </div>                                  
                                </div>
                                <div class="col-md-3">
                                    <button class="btn btn-primary" style="margin-top: 24px" type="button" id="vargeneratebtn">Generate Variations</button>
                                </div>

                            </div>

                           <div id="attribute_variations"></div>

                           <h4 style="font-weight: bold">Available Variations</h4>
                           <?php 
                    $sql = "SELECT * FROM unit";
                    $db->sql($sql);
                    $res_unit = $db->getResult();

                    $i=1;
                    foreach($res as $r){
                           ?>
        <div class="row" style="margin-top:10px;box-shadow: 1px 1px 1px 2px #ccc;margin: 0px 19px;padding: 10px;" id="custom_attr_row_<?php echo $i ?>">
        <div class="col-md-12">
        <?php 
            $pvi=$function2->getVariantAttribute($r['product_variant_id']); 
            echo $pvi[0]['attributes'];
        ?>
        </div>        
        <div class="col-md-2"><div class="form-group"><label for="measurement">Measurement</label> <i class="text-danger asterik">*</i>
            <input type="number" class="form-control" name="attrib_measurement[]" required="" step="any" min="0" value="<?php echo $r['measurement']; ?>" onblur="updateVariant(this.value,'<?php echo $r['product_variant_id'] ?>','measurement')"></div>
        </div>
            <div class="col-md-1"><div class="form-group">
            <label for="measurement_unit">Unit</label>
            <select class="form-control" name="attrib_measurement_unit_id[]" onblur="updateVariant(this.value,'<?php echo $r['product_variant_id'] ?>','measurement_unit')">
            <?php
                foreach ($res_unit as $row) {
                    $sel= ($row['id']==$r['measurement_unit_id']) ? 'selected':'';
                    echo "<option value=" . $row['id'] . " ".$sel.">" . $row['short_code'] . "</option>";
                }
                ?>
            </select></div>
        </div>
            <div class="col-md-2">
                <div class="form-group">
                <label for="price">Price(<?= $settings['currency'] ?>):</label> <i class="text-danger asterik">*</i>
                <input type="number" step="any" min="0" class="form-control" name="attrib_price[]" required="" value="<?php echo $r['price']; ?>"  onblur="updateVariant(this.value,'<?php echo $r['product_variant_id'] ?>','price')">
                </div>
            </div>
            <div class="col-md-2"><div class="form-group"><label for="discounted_price">Discounted Price(<?= $settings['currency'] ?>):</label>
            <input type="number" step="any" min="0" class="form-control" name="attrib_discounted_price[]" value="<?php echo $r['discounted_price']; ?>"  onblur="updateVariant(this.value,'<?php echo $r['product_variant_id'] ?>','discounted_price')" /></div></div>
            <div class="col-md-1"><div class="form-group"><label for="stock">Stock:</label> <i class="text-danger asterik">*</i>
            <input type="number" step="any" min="0" class="form-control" name="attrib_stock[]" value="<?php echo $r['stock']; ?>"  onblur="updateVariant(this.value,'<?php echo $r['product_variant_id'] ?>','stock')" /></div></div>
            <div class="col-md-2"><div class="form-group"><label for="unit">Unit:</label>
            <select class="form-control" name="attrib_stock_unit_id[]" onblur="updateVariant(this.value,'<?php echo $r['product_variant_id'] ?>','unit')">
            <?php
                foreach ($res_unit as  $row) {
                    $sel= ($row['id']==$r['stock_unit_id']) ? 'selected':'';
                    echo "<option value=" . $row['id'] . " ".$sel.">" . $row['short_code'] . "</option>";
                }
                ?>
            </select>
            </div></div>
            <div class="col-md-2">
                <div class="form-group packate_div">
                    <label for="qty">Status:</label>
                    <select name="attrib_serve_for[]" class="form-control" required onblur="updateVariant(this.value,'<?php echo $r['product_variant_id'] ?>','serve_for')">
                        <option value="Available" <?php echo ($r['serve_for']=='Available') ? 'selected':''; ?>>Available</option>
                        <option value="Sold Out" <?php echo ($r['serve_for']=='Sold Out') ? 'selected':''; ?>>Sold Out</option>
                    </select>
                </div>
            </div>                    
                                <div class="col-md-2">
                                    <label>Product Unique ID</label>
                                    <input type="text" name="attrib_product_unique_id1[]" class="form-control" value="<?php echo $r['product_unique_id']; ?>" onblur="updateVariant(this.value,'<?php echo $r['product_variant_id'] ?>','product_unique_id')">
                                </div>
                                <div class="col-md-2">
                                    <label>Max. Order Quantity</label>
                                    <input type="text" name="attrib_max_order_quantity1[]" class="form-control" value="<?php echo $r['max_order_quantity']; ?>" onblur="updateVariant(this.value,'<?php echo $r['product_variant_id'] ?>','max_order_quantity')">
                                </div>
                                <div class="col-md-2">
                                    <label>Product Threshold</label>
                                    <input type="text" name="attrib_product_threshold1[]" class="form-control" value="<?php echo $r['product_threshold']; ?>" onblur="updateVariant(this.value,'<?php echo $r['product_variant_id'] ?>','product_threshold')">
                                </div>
                                <div class="col-md-2">
                                    <label>BATRAS Recommend</label><br/>
                                    <input type="checkbox" name="attrib_batras_rec1[]" value="<?php echo $r['batras_recommendation'] ?>"  onblur="updateVariant(this.value,'<?php echo $r['product_variant_id'] ?>','batras_recommendation')"> 
                                </div>
                                <div class="col-md-2" style="display:none">
                                    <label for="">Variant Image</label>
                                    <input type="file" name="attrib_variant_image1[]" class="form-control"  onblur="updateVariant(this.value,'<?php echo $r['product_variant_id'] ?>','variant_image')">
                                </div>
                                <div class="col-md-2">
                                    <label>&nbsp;</label>
                                    <input type="hidden" name="attributes1[]" value="<?php echo $pvi[0]['attributes']; ?>" class="form-control" />
                                    <button type="button" class="btn btn-danger" onclick="deleteCustomAttribute('<?php echo $i ?>','<?php echo $r['product_variant_id']; ?>','<?php echo $res[0]['id'] ?>')">Delete</button>
                                </div>

                                </div>
							<br>                                

                            <?php 
                            $i++;
                        } ?>
                        </div><hr/>


                            </div>

                            <div class="form-group">
                                <div class="form-group" id="status_div" <?php if ($res[0]['type'] == "packet") {
                                                                            echo "style='display:none'";
                                                                        } ?>>
                                    <label for="exampleInputEmail1">Status :</label><?php echo isset($error['serve_for']) ? $error['serve_for'] : ''; ?>
                                    <select name="serve_for" class="form-control">
                                        <option value="Available" <?php if (strtolower($res[0]['serve_for']) == "available") {
                                                                        echo "selected";
                                                                    } ?>>Available</option>
                                        <option value="Sold Out" <?php if (strtolower($res[0]['serve_for']) == "sold out") {
                                                                        echo "selected";
                                                                    } ?>>Sold Out</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    
                                    <div class="form-group">
            							<label for="department_id">Department</label> <i class="text-danger asterik">*</i><?php echo isset($error['department_id']) ? $error['department_id'] : ''; ?>
            							<?php
            							$db->select("departments", 'id,name');
            							$res_dept = $db->getResult();
            							?>
            							<select class="form-control" id="department_id" name="department_id" >
            								<?php foreach ($res_dept as $row) {
            									echo "<option value=" . $row['id'];
            									if ($row['id'] == $data['department_id']) {
            										echo " selected";
            									}
            									echo ">" . $row['name'] . "</option>";
            								} ?>
            							</select>
            
            						</div>
                                    
                                   
                                    <div class="form-group">
                                        <label for="category_id">Category :</label> <i class="text-danger asterik">*</i> <?php echo isset($error['category_id']) ? $error['category_id'] : ''; ?>
                                        <?php
                    							$db->select("category", 'id,name' , null, 'department_id=' . $data['department_id']);
                    							$category_data = $db->getResult();
                    					?>
                                        <select name="category_id" id="category_id" class="form-control">
                                            
                                            <?php
                                            if ($permissions['categories']['read'] == 1) {
                                                foreach ($category_data as $row) { ?>
                                                    <option value="<?php echo $row['id']; ?>" <?= ($row['id'] == $data['category_id']) ? "selected" : ""; ?>><?php echo $row['name']; ?></option>
                                                <?php }
                                            } else { ?>
                                                <option value="">---Select Category---</option>
                                                <?php } ?>?>
                                        </select>
                                    </div>
                                    
                                   
                                    <div class="form-group">
                                        <label for="subcategory_id">Sub Category :</label>
                                        <?php
                                                $sql = "select * from subcategory WHERE department_id = {$data['department_id']} AND category_id = {$data['category_id']}";
                                                $db->sql($sql);
                                                $subcategory = $db->getResult();
                    			
                    						?>
                                        <select name="subcategory_id" id="subcategory_id" class="form-control">
                                            
                                            

                                            <?php
                                            if ($permissions['subcategories']['read'] == 1) { ?>
                                                <option value="">---Select Subcategory---</option>
                                                <?php foreach ($subcategory as $subcategories) { ?>

                                                    <option value="<?= $subcategories['id']; ?>" <?= $data['subcategory_id'] == $subcategories['id'] ? 'selected' : '' ?>><?= $subcategories['name']; ?></option>
                                                <?php }
                                            } else { ?>
                                                <option value="">---Select Subcategory---</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="brand_id">Brand</label> <?php echo isset($error['brand_id']) ? $error['brand_id'] : ''; ?>
                                        <?php
                                        $db->select("brands", 'brand_id,brand_name');
                                        $res_brand = $db->getResult();
                                        ?>
                                        <select class="form-control" id="brand_id" name="brand_id" >
                                           <option value="0" <?php ($data['brand_id']==0)? "selected":""; ?>>----Select Brand----</option>
                                             <?php foreach ($res_brand as $row) {?>
                                                <option value="<?php echo $row['brand_id'] ?>" <?php echo ($row['brand_id'] == $data['brand_id']) ? "selected" : ""; ?>><?php echo $row['brand_name'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
            
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="">Product Type :</label>
                                        <select name="indicator" id="indicator" class="form-control">
                                            <option value="">--Select Type--</option>
                                            <option value="1" <?php if ($res[0]['indicator'] == 1) {
                                                                    echo 'selected';
                                                                } ?>>Veg</option>
                                            <option value="2" <?php if ($res[0]['indicator'] == 2) {
                                                                    echo 'selected';
                                                                } ?>>Non Veg</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Manufacturer :</label>
                                        <input type="text" name="manufacturer" value="<?= $res[0]['manufacturer'] ?>" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Made In :</label>
                                        <input type="text" name="made_in" value="<?= $res[0]['made_in'] ?>" class="form-control">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Is Returnable? :</label><br>
                                                <input type="checkbox" id="return_status_button" class="js-switch" <?= $res[0]['return_status'] == 1 ? 'checked' : '' ?>>
                                                <input type="hidden" id="return_status" name="return_status" value="<?= $res[0]['return_status'] == 1 ? 1 : 0 ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Is cancel-able? :</label><br>
                                                <input type="checkbox" id="cancelable_button" class="js-switch" <?= $res[0]['cancelable_status'] == 1 ? 'checked' : '' ?>>
                                                <input type="hidden" id="cancelable_status" name="cancelable_status" value="<?= $res[0]['cancelable_status'] == 1 ? 1 : 0 ?>">
                                            </div>
                                        </div>
                                        <?php
                                        $style = $res[0]['cancelable_status'] == 1 ? "" : "display:none;";
                                        ?>
                                        <div class="col-md-3" id="till-status" style="<?= $style; ?>">
                                            <div class="form-group">
                                                <label for="">Till which status? :</label> <i class="text-danger asterik">*</i> <?php echo isset($error['cancelable']) ? $error['cancelable'] : ''; ?><br>
                                                <select id="till_status" name="till_status" class="form-control">
                                                    <option value="">Select</option>
                                                    <option value="received" <?= $res[0]['till_status'] == 'received' ? 'selected' : '' ?>>Received</option>
                                                    <option value="processed" <?= $res[0]['till_status'] == 'processed' ? 'selected' : '' ?>>Processed</option>
                                                    <option value="shipped" <?= $res[0]['till_status'] == 'shipped' ? 'selected' : '' ?>>Shipped</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputFile">Image <i class="text-danger asterik">*</i> &nbsp;&nbsp;&nbsp;*Please choose square image of larger than 350px*350px & smaller than 550px*550px. (Maximum Upload Size: 100kb)</label><?php echo isset($error['image']) ? $error['image'] : ''; ?>
                                        <input type="file" name="image" id="image" title="Please choose square image of larger than 350px*350px & smaller than 550px*550px." />
                                        <p id="err_image" style="color:red"></p>
                                        <br />
                                        <img src="<?php echo $data['image']; ?>" width="210" height="160" />
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputFile">Other Images *Please choose square image of larger than 350px*350px & smaller than 550px*550px.</label><?php echo isset($error['other_images']) ? $error['other_images'] : ''; ?>
                                        <input type="file" name="other_images[]" id="other_images" multiple title="Please choose square image of larger than 350px*350px & smaller than 550px*550px." /><br />
                                        <?php
                                        if (!empty($data['other_images'])) {
                                            $other_images = json_decode($data['other_images']);

                                            for ($i = 0; $i < count($other_images); $i++) { ?>
                                                <img src="<?= $other_images[$i]; ?>" height="160" />
                                                <a class='btn btn-xs btn-danger delete-image' data-i='<?= $i; ?>' data-pid='<?= $_GET['id']; ?>'>Delete</a>
                                        <?php }
                                        } ?>
                                    </div>

                                <div class="form-group">    

                                    <input type="hidden" id="dnf_ids" value="<?php echo $data['dont_forget_at_add_products']; ?>">

                                    <label for='product_ids'>Don't Forget to Add Products<small>( Ex : 100,205, 360 <comma separated>)</small></label>
                                    <select name='product_ids[]' id='product_ids' class='form-control' placeholder='Enter the product IDs you want to display specially on home screen of the APP in CSV formate' multiple="multiple">
                                    <?php $sql = 'select id,name from `products` where `status` = 1 order by id desc';
                                        $db->sql($sql);

                                        $result = $db->getResult();
                                        foreach($result as $value){
                                            ?>
                                            <option value='<?=$value['id']?>'><?=$value['name']?></option>
                                        <?php }?>
                                        
                                    </select>
                                </div>


                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Description :</label> <i class="text-danger asterik">*</i> <?php echo isset($error['description']) ? $error['description'] : ''; ?>
                                        <textarea name="description" id="description" class="form-control" rows="16"><?php echo $data['description']; ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="feature_details">Features and Details :</label> <i class="text-danger asterik">*</i><?php echo isset($error['feature_details']) ? $error['feature_details'] : ''; ?>
                                        <textarea name="feature_details" id="feature_details" class="form-control" rows="8"><?php echo $data['feature_details']; ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="product_description">Product Information :</label> <i class="text-danger asterik">*</i><?php echo isset($error['product_description']) ? $error['product_description'] : ''; ?>
                                        <textarea name="product_description" id="product_description" class="form-control" rows="8"><?php echo $data['product_description']; ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="disclaimer">Disclaimer :</label> <i class="text-danger asterik">*</i><?php echo isset($error['disclaimer']) ? $error['disclaimer'] : ''; ?>
                                        <textarea name="disclaimer" id="disclaimer" class="form-control" rows="8"><?php echo $data['disclaimer']; ?></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label ">Status :</label>
                                        <div id="product_status" class="btn-group">
                                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                <input type="radio" name="pr_status" value="0"> Deactive
                                            </label>
                                            <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                <input type="radio" name="pr_status" value="1"> Active
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <input type="submit" class="btn-primary btn" value="Update" id="btnEdit" name="btnEdit" />
                            </div>
                </form>
            </div><!-- /.box -->
        </div>
    </div>
</section>
<div class="separator"> </div>

<script type="text/javascript" src="css/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('description');
    CKEDITOR.replace('feature_details');
    CKEDITOR.replace('disclaimer');
    CKEDITOR.replace('product_description');

</script>

<script>

    $(document).ready(function(){
        var dnf_ids = $("#dnf_ids").val();
        var dnf_array = dnf_ids.split(',');
        $('#product_ids').select2().val(dnf_array).trigger('change');
        // $("#product_ids").val(dnf_ids);
    });

    $(document).on('click', '.delete-image', function() {
        var pid = $(this).data('pid');
        var i = $(this).data('i');
        if (confirm('Are you sure want to delete the image?')) {
            $.ajax({
                type: 'POST',
                url: 'public/delete-other-images.php',
                data: 'i=' + i + '&pid=' + pid,
                success: function(result) {
                    if (result == '1') {
                        alert('Image deleted successfully');
                        window.location.replace("view-product-variants.php?id=" + pid);
                    } else
                        alert('Image could not be deleted!');

                }
            });
        }
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script>
    var changeCheckbox = document.querySelector('#return_status_button');
    var init = new Switchery(changeCheckbox);
    changeCheckbox.onchange = function() {
        if ($(this).is(':checked')) {
            $('#return_status').val(1);
        } else {
            $('#return_status').val(0);
        }
    };
</script>
<script>
    var changeCheckbox = document.querySelector('#cancelable_button');
    var init = new Switchery(changeCheckbox);
    changeCheckbox.onchange = function() {
        if ($(this).is(':checked')) {
            $('#cancelable_status').val(1);
            $('#till-status').show();

        } else {
            $('#cancelable_status').val(0);
            $('#till-status').hide();
            $('#till_status').val('');
        }
    };
</script>
<script>
    $.validator.addMethod('lessThanEqual', function(value, element, param) {
        return this.optional(element) || parseInt(value) < parseInt($(param).val());
    }, "Discounted Price should be lesser than Price");

    $('#edit_product_form').validate({
        rules: {
            name: "required",
            measurement: "required",
            price: "required",
            quantity: "required",
            discounted_price: "required",
            stock: "required",
            discounted_price: {
                lessThanEqual: "#price"
            },
            description: {
                required: function(textarea) {
                    CKEDITOR.instances[textarea.id].updateElement();
                    var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                    return editorcontent.length === 0;
                }
            }
        }
    });
</script>
<script>

</script>
<script>
    $('#add_loose_variation').on('click', function() {
        html = '<div class="row" style="margin-top:10px"><div class="col-md-4"><div class="form-group loose_div">' +
            '<label for="exampleInputEmail1">Measurement</label> <i class="text-danger asterik">*</i> <input type="number" step="any" min="0" class="form-control" name="insert_loose_measurement[]" required="">' +
            '</div></div>' +
            '<div class="col-md-2"><div class="form-group loose_div">' +
            '<label for="unit">Unit:</label>' +
            '<select class="form-control" name="insert_loose_measurement_unit_id[]">' +
            '<?php foreach ($unit_data as  $unit) {
                    echo "<option value=" . $unit['id'] . ">" . $unit['short_code'] . "</option>";
                } ?>' +
            '</select></div></div>' +
            '<div class="col-md-3"><div class="form-group loose_div">' +
            '<label for="price">M.R.P (<?= $settings['currency'] ?>):</label> <i class="text-danger asterik">*</i> ' +
            '<input type="number" step="any" min="0" class="form-control" name="insert_loose_price[]" id="loose_price" required="">' +
            '</div></div>' +
            '<div class="col-md-2"><div class="form-group loose_div">' +
            '<label for="discounted_price">Price(<?= $settings['currency'] ?>):</label>' +
            '<input type="number" step="any" min="0" class="form-control" name="insert_loose_discounted_price[]" id="discounted_price"/>' +
            '</div></div>' + 
            `                   <div class="col-md-3">
                                    <label>Product Unique ID</label>
                                    <input type="text" name="product_unique_id0[]" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <label>Max. Order Quantity</label>
                                    <input type="text" name="max_order_quantity0[]" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <label>Stock Threshold</label>
                                    <input type="text" name="product_threshold0[]" class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <label>BATRAS Recommendation</label><br/>
                                    <input type="checkbox" name="batras_rec0[]"> 
                                </div>                                
`
            +
            '<div class="col-md-1" style="display: grid;">' +
            '<label>Remove</label><a class="remove_variation text-danger" data-id="remove" title="Remove variation of product" style="cursor: pointer;"><i class="fa fa-times fa-2x"></i></a>' +
            '</div></div><hr>';
        $('#loose_variations').append(html);
    });

    $('#add_packate_variation').on('click', function() {
        html = '<div class="row"><div class="col-md-2"><div class="form-group packate_div">' +
            '<label for="exampleInputEmail1">Measurement</label> <i class="text-danger asterik">*</i> <input type="number" step="any" min="0" class="form-control" name="insert_packate_measurement[]" required />' +
            '</div></div>' +
            '<div class="col-md-1"><div class="form-group packate_div">' +
            '<label for="unit">Unit:</label>' +
            '<select class="form-control" name="insert_packate_measurement_unit_id[]">' +
            '<?php foreach ($unit_data as  $unit) {
                    echo "<option value=" . $unit['id'] . ">" . $unit['short_code'] . "</option>";
                } ?>' +
            '</select></div></div>' +
            '<div class="col-md-2"><div class="form-group packate_div">' +
            '<label for="price">M.R.P (<?= $settings['currency'] ?>):</label> <i class="text-danger asterik">*</i> <input type="number" step="any" min="0" class="form-control" name="insert_packate_price[]" id="packate_price" required />' +
            '</div></div>' +
            '<div class="col-md-2"><div class="form-group packate_div">' +
            '<label for="discounted_price">Price(<?= $settings['currency'] ?>):</label>' +
            '<input type="number" step="any" min="0" class="form-control" name="insert_packate_discounted_price[]" id="discounted_price"/>' +
            '</div></div>' +
            '<div class="col-md-1"><div class="form-group packate_div">' +
            '<label for="qty">Stock:</label> <i class="text-danger asterik">*</i> ' +
            '<input type="number" step="any" min="0" class="form-control" name="insert_packate_stock[]"/>' +
            '</div></div>' +
            '<div class="col-md-1"><div class="form-group packate_div">' +
            '<label for="unit">Unit:</label><select class="form-control" name="insert_packate_stock_unit_id[]">' +
            '<?php foreach ($unit_data as  $unit) {
                    echo "<option value=" . $unit['id'] . ">" . $unit['short_code'] . "</option>";
                } ?>' +
            '</select></div></div>' +
            '<div class="col-md-2"><div class="form-group packate_div"><label for="insert_packate_serve_for">Status:</label>'+
            '<select name="insert_packate_serve_for[]" class="form-control valid" required="" aria-invalid="false"><option value="Available">Available</option><option value="Sold Out">Sold Out</option></select></div></div>'+
            `                                <div class="col-md-3">
                                    <label>Product Unique ID</label>
                                    <input type="text" name="product_unique_id1[]" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <label>Max. Order Quantity</label>
                                    <input type="text" name="max_order_quantity1[]" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <label>Stock Threshold</label>
                                    <input type="text" name="product_threshold1[]" class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <label>BATRAS Recommendation</label><br/>
                                    <input type="checkbox" name="batras_rec1[]"> 
                                </div>                                
`
            +
            '<div class="col-md-1" style="display: grid;">' +
            '<label>Remove</label><a class="remove_variation text-danger" data-id="remove" title="Remove variation of product" style="cursor: pointer;"><i class="fa fa-times fa-2x"></i></a>' +
            '</div></div><hr>';
        $('#packate_variations').append(html);
    });
</script>
<script>
 
    $(document).on('click', '.remove_variation', function() {
        if ($(this).data('id') == 'data_delete') {
            if (confirm('Are you sure? Want to delete this row')) {
                var id = $(this).closest('div.row').find("input[id='product_variant_id']").val();
                $.ajax({
                    url: 'public/db-operation.php',
                    type: "post",
                    data: 'id=' + id + '&delete_variant=1',
                    success: function(result) {
                        location.reload();
                    }
                });
            }
        } else {
            $(this).closest('.row').remove();
        }
    });

    // $(document).on('change', '#category_id', function() {
    //     $.ajax({
    //         url: 'public/db-operation.php',
    //         method: 'POST',
    //         data: 'category_id=' + $('#category_id').val() + '&find_subcategory=1',
    //         success: function(data) {
    //             $('#subcategory_id').html("<option value=''>---Select Subcategory---</option>" + data);
    //         }
    //     });
    // });
    
    $(document).on('change', '#department_id', function() {
        $.ajax({
            url: "public/db-operation.php",
            data: "department_id=" + $('#department_id').val() + "&change_department=1",
            method: "POST",
            success: function(data) {
                $('#subcategory_id').html("<option value=''>--Select Sub Category--</option>");
                $('#category_id').html("<option value=''>---Select Category---</option>" + data);
            }
        });
    });


    $(document).on('change', '#category_id', function() {
        $.ajax({
            url: "public/db-operation.php",
            data: "category_id=" + $('#category_id').val() + "&change_category=1",
            method: "POST",
            success: function(data) {
                $('#subcategory_id').html("<option value=''>---Select Subcategory---</option>" + data);
            }
        });
    });
    
    
    $(document).on('change', '#packate', function() {
        $('#packate_div').show();
        $('.packate_div').show();
        $('#loose_div').hide();
        $('.loose_div').hide();
        $('#status_div').hide();
        $('#loose_stock_div').hide();
    });
    $(document).on('change', '#loose', function() {
        $('#loose_div').show();
        $('.loose_div').show();
        $('#loose_stock_div').show();
        $('#status_div').show();
        $('#packate_div').hide();
        $('.packate_div').hide();
    });
    $(document).ready(function() {
        var product_status = '<?= $product_status ?>';
        $("input[name=pr_status][value=1]").prop('checked', true);
        if (product_status == 0)
            $("input[name=pr_status][value=0]").prop('checked', true);

    $("#attr_value_select").select2({
        // width: 'element',
        placeholder: 'type in Attribute value to search',       
    });

    $("#attr_select").select2({
        // width: 'element',
        placeholder: 'type in Attribute name to search',        
    });

    $("#attr_select").change(function(){
        var seles = $(this).select2('data');
        var selectedAttrs = '';
        for(var i=0;i<seles.length;i++)
        {
            var id = (seles[i].id);
            selectedAttrs += `${id},`;
        }
        selectedAttrs = selectedAttrs.slice(0,-1);

            $.ajax({
            url: "public/db-operation2.php",
            data:  `attrs=${selectedAttrs}&get_attr_values=1`,
            method: "POST",
            success: function(data) {
                var jobj = JSON.parse(data);
                var ahtml='';
                jobj.forEach( function(element, index) {
                    ahtml += `<option data-attr="${element.attribute_id}" data-attrname="${element.attribute_name}" value="${element.attr_value_id}">${element.attribute_value}</option>`;
                });
                $("#attr_value_select").html(ahtml);
            }
        });        
    });

function getCombn(arr, pre) {
    pre = pre || '';
    if (!arr.length) {
        return pre;
    }
    var ans = arr[0].reduce(function(ans, value) {
        return ans.concat(getCombn(arr.slice(1), pre +" "+ value));
    }, []);
    return ans;
}
function getReqCombn(arr, pre) {
    pre = pre || '';
    if (!arr.length) {
        return pre;
    }
    var ans = arr[0].reduce(function(ans, value) {
        return ans.concat(getCombn(arr.slice(1), pre +"|"+ value));
    }, []);
    return ans;
}
function generateCombinations(comb_array,req_com_arr){
    var ahtml = '';
    for(var i=0;i<req_com_arr.length;i++)
    {
    	var final_arry = [];
            	if(req_com_arr[i].includes(' '))
            	{
            		//Multiple vars
            		var arr = req_com_arr[i].split(' ');
            		var idarr = [];
            		for(var j=0;j<arr.length;j++)
            		{
            			if(arr[j]!=''){
	            			var iarr = arr[j].split(":");
								var obj_a = {};
								iarr.forEach(function(val, i) {
								    if (i % 2 === 1) return; // Skip all even elements (= odd indexes)
								    obj_a[val] = iarr[i + 1];   // Assign the next element as a value of the object,
								                             // using the current value as key
								});	            
								idarr.push(obj_a);
	        			}
            		}
            		final_arry.push(idarr);
            	}
            	else{
            		var sarr = [];
            		var arr = req_com_arr[i].split(':');
            		var idarr = [];
            		for(var j=0;j<arr.length;j++)
            		{
            			if(arr[j]!=''){
            			idarr.push(arr[j]);
            			}
            		}
					
					var obj_a = {};
					idarr.forEach(function(val, i) {
					    if (i % 2 === 1) return; // Skip all even elements (= odd indexes)
					    obj_a[val] = idarr[i + 1];   // Assign the next element as a value of the object,
					                             // using the current value as key
					});
            		final_arry.push(obj_a);
            	}

            $.ajax({
            url: "public/db-operation2.php",
            data:  `removeCustomAttr=1&attrib=${ req_com_arr[i] }`,
            method: "POST",
            success: function(data) {
            	console.log(data);
            }
        });        


        ahtml += `
        <div class="row" style="margin-top:10px;box-shadow: 1px 1px 1px 2px #ccc;margin: 0px 19px;padding: 10px;" id="custom_attr_row_${i}">
        <div class="col-md-12">${ comb_array[i] }</div>
        <div class="col-md-2"><div class="form-group"><label for="measurement">Measurement</label> <i class="text-danger asterik">*</i>
            <input type="number" class="form-control" name="attrib_measurement[]" ="" step="any" min="0"></div></div>
            <div class="col-md-1"><div class="form-group">
            <label for="measurement_unit">Unit</label><select class="form-control" name="attrib_measurement_unit_id[]">
            <?php
                foreach ($res_unit as $row) {
                    echo "<option value=" . $row['id'] . ">" . $row['short_code'] . "</option>";
                }
                ?>
            </select></div></div>
            <div class="col-md-2">
                <div class="form-group">
                <label for="price">Price(<?= $settings['currency'] ?>):</label> <i class="text-danger asterik">*</i>
                <input type="number" step="any" min="0" class="form-control" name="attrib_price[]" ="">
                </div>
            </div>
            <div class="col-md-2"><div class="form-group"><label for="discounted_price">Discounted Price(<?= $settings['currency'] ?>):</label>
            <input type="number" step="any" min="0" class="form-control" name="attrib_discounted_price[]" /></div></div>
            <div class="col-md-1"><div class="form-group"><label for="stock">Stock:</label> <i class="text-danger asterik">*</i>
            <input type="number" step="any" min="0" class="form-control" name="attrib_stock[]" /></div></div>
            <div class="col-md-2"><div class="form-group"><label for="unit">Unit:</label>
            <select class="form-control" name="attrib_stock_unit_id[]">
            <?php
                foreach ($res_unit as  $row) {
                    echo "<option value=" . $row['id'] . ">" . $row['short_code'] . "</option>";
                }
                ?>
            </select>
            </div></div>
            <div class="col-md-2"><div class="form-group packate_div"><label for="qty">Status:</label><select name="packate_serve_for[]" class="form-control" ><option value="Available">Available</option><option value="Sold Out">Sold Out</option></select></div></div>                    
                                <div class="col-md-2">
                                    <label>Product Unique ID</label>
                                    <input type="text" name="product_unique_id1[]" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <label>Max. Order Quantity</label>
                                    <input type="text" name="max_order_quantity1[]" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <label>Product Threshold</label>
                                    <input type="text" name="product_threshold1[]" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <label>BATRAS Recommend</label><br/>
                                    <input type="checkbox" name="batras_rec1[]"> 
                                </div>
                                <div class="col-md-2">
                                    <label for="">Variant Image</label>
                                    <input type="file" name="variant_image1[]" class="form-control">
                                </div>
                                <div class="col-md-2">
                                	<label>&nbsp;</label>
                                    <input type="hidden" name="attributes1[]" value="${ req_com_arr[i] }" class="form-control" />
                                    <button type="button" class="btn btn-danger" onclick="deleteCustomAttribute('${i}')">Delete</button>
                                </div>
                                </div>
                        </div><hr/>
        `;
    }
    // console.log(ahtml)
    // $("#attribute_variations").html(ahtml);
}
    $("#vargeneratebtn").on("click",function(){
        var seles = $("#attr_select").select2('data');
        var values = $("#attr_value_select").select2('data');
        var seles_arr = [];
        var selesreq_arr = [];
        // console.log(seles);
        // console.log(values);
        if(seles.length==1)
        {
            var attrtext = seles[0].text;
            var gcarry=[];
            for(var i=0;i<values.length;i++)
            {
                gcarry.push(`${attrtext} : ${values[i].text}`);
            }
            generateCombinations(gcarry,gcarry);
        }
        else{
        for(var i=0;i<seles.length;i++)
        {
            var val_arr = [];
            var req_arr = [];
            for(var j=0;j<values.length;j++)
            {
                if(values[j].element.dataset.attr == seles[i].id){
                    val_arr.push(`${values[j].element.dataset.attrname} : ${values[j].text}`);
                    req_arr.push(`${values[j].element.dataset.attrname}:${values[j].text}`);
                }
            }
            seles_arr.push(val_arr);
            selesreq_arr.push(req_arr);
        }
        	var comb_arr = getCombn(seles_arr);
        	var req_com_arr = getCombn(selesreq_arr);
            var gcarry=[];
            for(var i=0;i<values.length;i++)
            {
                comb_arr.push(`${values[i].element.dataset.attrname} : ${values[i].text}`);
                req_com_arr.push(`${values[i].element.dataset.attrname}:${values[i].text}`);
            }
            generateCombinations(comb_arr,req_com_arr);
        }
    });



    $('#product_ids').select2({
        width: 'element',
        placeholder: 'type in product name to search',
        // minimumInputLength: 3,
        /* ajax: {
            url: 'api/get-bootstrap-table-data.php',
            dataType: 'json',
            type: "GET",
            quietMillis: 1,
            data:function(params){
                return{
                    products_list: 1,
                    name:params.term,
                };
            },
            processResults:function(data) {
                // alert(JSON.stringify(data));
                return {
                    results: data
                };
            },
        } */
    });


    });

                //binds to onchange event of your input field
$('#image').bind('change', function() {

  //this.files[0].size gets the size of your file.
  if(this.files[0].size > 100000)
  {
    $("#err_image").html("Please select a file less than 100kb size");
    $("#btnEdit").attr('disabled',true);
 
  }
  else{
    $("#err_image").html("");
    $("#btnEdit").attr('disabled',false);
  }

});

    function deleteCustomAttribute(index,product_variant_id,product_id) {
        $(`#custom_attr_row_${index}`).remove();
            $.ajax({
            url: "public/db-operation2.php",
            data:  `removeCustomAttr=1&product_variant_id=${product_variant_id}`,
            method: "POST",
            success: function(data) {
            	console.log(data);
            }
        });        

    }

    function updateVariant(value,product_variant_id,field){
        console.log(value+" "+product_variant_id+" "+field);
            $.ajax({
            url: "public/db-operation2.php",
            data:  `updateCustomAttr=1&product_variant_id=${product_variant_id}&field=${field}&value=${value}`,
            method: "POST",
            success: function(data) {
            }
        });        

    }
</script>
<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once('includes/functions.php');
include_once('includes/functions2.php');
$fn2 = new functions2;
?>
<section class="content-header">
    <h1>Product Requests /<small><a href="home.php"><i class="fa fa-home"></i> Home</a></small></h1>
</section>
    <!-- Main content -->
    <section class="content">
        <p class="alert alert-success" id="successalert" style="display: none">Success</p>
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-xs-12">
                <div class="box">
                	<div class="box-header with-border">
                		<h3 class="box-title">Product Requests</h3>
                	</div>
                	<div class="box-body">
                		<?php ?>	

                		<div class="table-responsive">
                			<table class="table table-bordered table-hover">
                				<thead>
                					<tr>
                						<th>Sl. No.</th>
                						<th>Product Name</th>
                						<th>Name</th>
                						<th>Email</th>
                						<th>Phone</th>
                						<th>Requested On</th>
                						<th>Action</th>
                					</tr>
                				</thead>
                				<tbody>
                					<?php 
                						$reqs = $fn2->getAllProductRequests(); 
                						$c=0;
                                        if(!empty($reqs)){
                						foreach($reqs as $r)
                						{
                							?>
                							<tr>
                								<td><?php echo ++$c; ?></td>
                								<td><?php echo $r['product_name']; ?></td>
                								<td><?php echo $r['name']; ?></td>
                								<td><?php echo $r['email']; ?></td>
                								<td><?php echo $r['phone']; ?></td>
                								<td>
                									<?php echo date("d-m-Y", strtotime($r['created_at'])); ?></td>
                								<td>
                									<button onclick="confDelete('<?php echo $r['product_request_id']; ?>')" class="btn btn-danger">Delete</button>
                								</td>
                							</tr>
                							<?php
                						}
                                    }
                                    else{
                                        ?>
                                        <tr>
                                            <td colspan="7">No Requests Found</td>
                                        </tr>
                                        <?php
                                    }
                						?>	
                                    
                				</tbody>
                			</table>
                		</div>

                	</div>
                </div>
            </div>
        </div>
    </section>
    <script>
        function confDelete(product_request_id) {
            var c = confirm("Do you want to delete?");
            if(c){
            $.ajax({
                url : 'public/db-operation2.php',
                type: "post",
                dataType : 'json',
                data: 'request_delete_id='+product_request_id,
                success: function(result){
                        $("#successalert").css('display','block');
                        location.reload();
                }
            });   
            }                 
        }
    </script>
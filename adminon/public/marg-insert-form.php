<?php
include_once('includes/crud.php');
$db = new Database();
$db->connect();
$db->sql("SET NAMES 'utf8'");
include('includes/variables.php');
include_once('includes/custom-functions.php');
include_once('includes/functions2.php');
$fn2 = new functions2;
$fn = new custom_functions;
$config = $fn->get_configurations();
?>
<section class="content-header">
  <h1>Marg Insert /<small><a href="products.php"><i class="fa fa-cubes"></i> Products</a></small></h1>
</section>
<!-- Main content -->
<section class="content">
  <!-- Main row -->
  <div class="row">
    <div class="col-md-4">
      <!-- general form elements -->
      <div class="box box-primary">
        <!-- form start -->
          <div class="box-footer" style="display: flex;justify-content: space-between;">
        <form method="get" action="marg-operation.php" onsubmit="changeText('upload_btn')">
          <input type="hidden" id="insert-marg" name="insert-marg" required="" value="1">
            <button type="submit" class="btn btn-primary" id="upload_btn">Upload</button>
        </form>
        <form method="get" action="marg-operation.php" onsubmit="changeText('upload_new_btn')">
          <input type="hidden" id="insert-marg-new" name="insert-marg-new" required="" value="1">
            <button type="submit" class="btn btn-primary" id="upload_new_btn">New products Upload</button>
        </form>
        <form method="get" action="marg-operation.php" onsubmit="changeText('update_btn')">
          <input type="hidden" id="update-marg" name="update-marg" required="" value="1">
        <button class="btn btn-success" id="update_btn" type="submit">Update data</button>
        </form>
        </div>
      </div><!-- /.box -->
    </div>
    <div class="col-md-8">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <p>Upload Logs</p>
        </div><!-- /.box-header -->
        <!-- form start -->
        <?php 
          $logs = $fn2->getMargUpdateLogs();
        ?>
      <table class="table table-bordered" id="marg-table">
        <thead>
          <tr>
            <th>Sl. No.</th>
            <th>User</th>
            <th>Updated At</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          if(!empty($logs)){
            $i=0;
            foreach($logs as $l){
            ?>
            <tr>
              <td><?php echo ++$i; ?></td>
              <td><?php echo $l['name']; ?></td>
              <td><?php echo date('d-m-Y h:i:sa',strtotime($l['updated_at'])); ?></td>
              <td><?php echo ($l['status'] == 2) ? 'Uploaded': 'Updated'; ?></td>
            </tr>
            <?php
          }
          }
          ?>
        </tbody>
      </table>
    </div>
    </div>
    <div class="separator"> </div>
  </div>
</section>

<script>
  $(function(){
    $("#marg-table").DataTable({
            "order": [[0,'asc']]
        });
  });

  function changeText(btn){
    $(`#${btn}`).html(`Please wait  <i class="fa fa-refresh fa-spin fa-fw"></i>`);
  }
</script>
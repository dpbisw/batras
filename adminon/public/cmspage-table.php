<?php

include_once('includes/functions.php');

include_once('includes/functions2.php');



if(isset($_POST['edit_group_name']))

{

    $is_third_party = 0;

    if(!empty($_POST['edit_is_third_party'])) $is_third_party = 1;



    $sql = "Update ad_groups set `group_name`='" . $_POST['edit_group_name'] . "', ad_group_type='" . $_POST['edit_ad_group_type'] . "', g_order='".$_POST['edit_group_order']."', image_per_row='".$_POST['edit_image_per_row']."', is_third_party='".$_POST['edit_is_third_party']."', is_visible = '".$_POST['edit_status']."' where `ad_group_id`=" . $_POST['edit_group_id'];



    $fn2 = new functions2();

    $sql_edit = $fn2->editGroup($sql);

    if($sql_edit == 1)

    {

        header("Refresh:1");

    }

}

?>

<section class="content-header">

    <h1>CMS Pages /<small><a href="home.php"><i class="fa fa-home"></i> Home</a></small></h1>
<?php 
if ($permissions['cmspages']['create'] == 1) {
?>

    <ol class="breadcrumb">

        <a class="btn btn-block btn-default" href="cmspage-form.php"><i class="fa fa-plus-square"></i> Add New Page</a>

    </ol>
<?php } ?>
</section>

<?php 
if ($permissions['cmspages']['read'] == 1) {

?>

    <!-- Main content -->

    <section class="content">

        <!-- Main row -->

        <div class="row" style="display: none">

            <!-- Left col -->

            <div class="col-xs-12">

                <div class="box">

                    <div class="box-header with-border">

                        <form method="POST" id="filter_form" name="filter_form">

                            <div class="form-group col-md-3">

                            </div>

                        </form>

                    </div>

                    <div class="box-header">

                        <h3 class="box-title">CMS Pages</h3>

                    </div>

                    <div class="box-body table-responsive">

                        <?php 

                        $fn2 = new functions2();

                        $cmspages = $fn2->getCmspages();

                        ?>

                        <table class="table table-hover">

                            <thead>

                                <tr>

                                    <th>Sl No.</th>

                                    <th>Title</th>

                                    <th>Slug</th>

                                    <th>Footer Column</th>

                                    <th>Order</th>

                                    <th>Status</th>

									<th>Action</th>

                                </tr>

                            </thead>

                            <tbody>

                            	<?php 

                            	if(count($cmspages) >0){

                            		$i = 0;

                            		foreach ($cmspages as $c) {

                            			?>

                            			<tr>

                            				<td><?php echo ++$i; ?></td>

                            				<td><?php echo $c['title']; ?></td>

                            				<td><?php echo $c['slug']; ?></td>

                            				<td><?php echo $c['footer_column']; ?></td>

                            				<td><?php echo $c['c_order']; ?></td>

                                            <td><?php echo ($c['status'] == 1) ? 'Visible' : 'Hidden'; ?></td>

                            				<td>                           					
                                                <?php if($c['slug']!='about-us'){ ?>
                            					<a class="btn btn-success" href="cmspage_edit.php?cid=<?php echo base64_encode($c['page_id']); ?>">Edit</a>

                            					<button class="btn btn-danger" onclick="deletePage('<?php echo $c['page_id']; ?>')">Delete</button>
                                            <?php } ?>

                            				</td>

                            			</tr>

                            			<?php

                            		}

                            	}

                            	?>

                            </tbody>

                        </table>

						

                    </div>

                </div>

            </div>

            <div class="separator"> </div>

        </div>



        <div id="view_content_div">

        	

        </div>

        <br>



<?php 

    $fn2 = new functions2();

    $cmspages = $fn2->getCmsFooterColumns();

    foreach ($cmspages as $c) {

        ?>

<div class="box box-default collapsed-box">

        <div class="box-header with-border">

          <h3 class="box-title">Footer column <?php echo $c['footer_column'] ?></h3>

          <div class="box-tools pull-right">

            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>

          </div><!-- /.box-tools -->

        </div><!-- /.box-header -->

        <div class="box-body">

                        <?php 

                        $cmspages = $fn2->getCmspagesByFooter($c['footer_column']);

                        ?>

                        <table class="table table-hover">

                            <thead>

                                <tr>

                                    <th>Sl No.</th>

                                    <th>Title</th>

                                    <th>Slug</th>

                                    <th>Order</th>

                                    <th>Status</th>

                                    <th>Action</th>

                                </tr>

                            </thead>

                            <tbody style="cursor: move;">

                                <?php 

                                if(count($cmspages) >0){

                                    $i = 0;

                                    foreach ($cmspages as $c) {

                                        ?>

                                        <tr class="ui-state-default" id="<?php echo $c['page_id'] ?>">

                                            <td><?php echo ++$i; ?></td>

                                            <td><?php echo $c['title']; ?></td>

                                            <td><?php echo $c['slug']; ?></td>

                                            <td><?php echo $c['row_order']; ?></td>

                                            <td><?php echo ($c['status'] == 1) ? 'Visible' : 'Hidden'; ?></td>

                                            <td>   
                                                <?php if($c['slug']!='about-us' && $c['slug']!='contact' && $c['slug']!='faq'){ ?>
                                                <a class="btn btn-success" href="cmspage_edit.php?cid=<?php echo base64_encode($c['page_id']); ?>">Edit</a>
                                            <?php } ?>
                                                <?php if($c['slug']=='contact'){ ?>
                                                <a class="btn btn-success" href="contact-us.php">Edit</a>
                                            <?php } ?>
                                                <?php if($c['slug']=='faq'){ ?>
                                                <a class="btn btn-success" href="faq.php">Edit</a>
                                            <?php } ?>
                                                <button class="btn btn-danger" onclick="deletePage('<?php echo $c['page_id']; ?>')">Delete</button>
                                            </td>

                                        </tr>

                                        <?php

                                    }

                                }

                                ?>

                            </tbody>

                        </table>

                        <center>



                        <form action="public/db-operation2.php" method="post" class="orderForm">

                        <input type="hidden" name="order_pages" value="1">

                        <input type="hidden" name="footer_column" value="<?php echo $c['footer_column']; ?>">

                        <input type="hidden" class="pageOrder" name="pageOrder" value="">

                        <button class="btn btn-primary" type="submit">Save Order</button>

                        </form>

                        </center>

        </div><!-- /.box-body -->

      </div><!-- /.box -->

        <?php        

    }

?>



    </section>
<?php } else { ?>

    <div class="alert alert-danger topmargin-sm" style="margin-top: 20px;">You have no permission to view cms pages.</div>

<?php } ?>

<script>



    function deletePage(page_id) {

        var c = confirm('Do you want to delete this Page? All the Related Data will be deleted');

        if(c)

        {

        $.ajax({

            type: 'POST',

            url: "public/db-operation2.php",

            data: { cms_delete_id : page_id },

            success: function(result) {

                if(result.includes("deleted"))

                {

                    location.reload();

                }

            }

        });            

        }

    }





  $( function() {

    $( "tbody" ).sortable({

        placeholder: "ui-state-highlight",

        update : function(event,ui){

               var cmsOrder = $(this).sortable('toArray').toString();  

               $(".pageOrder").val(cmsOrder);

        }        

    });

    $( "tbody" ).disableSelection();

  } );



        $('.orderForm').on('submit',function(e){

        e.preventDefault();

        var formData = new FormData(this);

            $.ajax({

            type:'POST',

            url: $(this).attr('action'),

            data:formData,

            // dataType:'json',

            // beforeSend:function(){$('#submit_btn').val('Please wait..').attr('disabled',true);},

            cache:false,

            contentType: false,

            processData: false,

            success:function(result){

                if(result==1)

                {

                    alert("Order Changed successfully");

                    location.reload();

                }

                else{

                    alert("Error in changing order");

                }

            }

            });        

    }); 





</script>
<?php 

    $db = new Database();

    $db->connect();

    include_once('includes/functions.php');

    $fn = new functions;

    include_once('includes/functions2.php');

    $fn2 = new functions2;

?>

<section class="content-header">

    <h1>Add CMS Page</h1>

    <ol class="breadcrumb">

        <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>

    </ol>

    <hr />

</section>
<?php 
if ($permissions['cmspages']['create'] == 1) {
?>

<section class="content">

    <div class="row">

        <div class="col-md-12">

        	<!-- general form elements -->

            <div class="box box-primary">

                <div class="box-header with-border">

                    <h3 class="box-title">Add CMS Page</h3>

                </div><!-- /.box-header -->

                <!-- form start -->

                <?php 

                if(isset($_POST['add-cmspage']))

                {

                    $sql = "select * from pages where title = '".$_POST['title']."'";

                    $db->sql($sql);

                    $res = $db->getResult();

                    if(count($res) != 0)

                    {

                        ?>

                        <p class="alert alert-danger">This Title already exists. Please Try Another</p>

                        <?php

                    }

                    else{

                    $order_exists = $fn2->checkOrder($_POST['footer_column']);

                    $next_order =  count($order_exists) + 1;



                   $slug = $fn2->slugifyy($_POST['title']);

                    $sql2 = "INSERT into pages (`title`,`slug`,`content`,`footer_column`,`status`,`row_order`) VALUES('".$_POST['title']."','".$slug."','".$_POST['content']."','".$_POST['footer_column']."','".$_POST['status']."','".$next_order."')";        

                    if ($db->sql($sql2)) {

                        ?>

                <p class="alert alert-success" id="successalert">CMS Page Added successfully!</p>

                        <?php

                    }

                    else{

                        ?>

                <p class="alert alert-danger">Something went Wrong! Please Try Again Later</p>

                        <?php

                    }                      

                    }

                }

                ?>

                <form action="" method="post" id="cms_form">

                    <input type="hidden" name="add-cmspage" value="1">

                <div class="box-body">

                	<div class="row">

                		<div class="col-md-6">

                			<div class="form-group">

                				<label for="title">Title</label>

                				<input type="text" class="form-control" name="title">

                			</div>

                		</div>

                	</div>

                	<div class="row">

                		<div class="col-md-12">

                			<label for="content">Page Content</label>

                			<textarea name="content" id="content" cols="5" rows="5" class="form-control"></textarea>

                		</div>

                	</div>

                    <br>

                	<div class="row">

                		<div class="col-md-3">

                			<label for="footer_column">Footer Column</label>

                            <select name="footer_column" class="form-control">

                                <?php for($i=1;$i<=4;$i++){ ?>

                                <option value="<?php echo $i ?>"><?php echo $i ?></option>

                            <?php } ?>

                            </select>

                		</div>

                        <div class="col-md-3">

                            <label for="status">Status</label>

                            <select name="status" class="form-control">

                                <option value="1">Visible</option>

                                <option value="0">Hidden</option>

                            </select>

                        </div>

                	</div>

                	<br>

                	<button class="btn btn-primary">Submit</button>

			</div>

        </form>

			</div>

			</div>

			</div>

</section>
<?php } else { ?>

    <div class="alert alert-danger topmargin-sm" style="margin-top: 20px;">You have no permission to create Cms pages.</div>

<?php } ?>

<script src="css/js/ckeditor/ckeditor.js"></script>

<script>

    CKEDITOR.replace('content');

</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>

        <script>

        $("#cms_form").validate({

            rules:{

                title : 'required',

                content : 'required',

                footer_column:'required',

                status:'required'

            },

            messages :{

                title : "Title is required",

                content : "Content is required",

                footer_column : "Footer Column is required",

                status : "Status is required",

            },

            submitHandler : function(form){

                form.submit();

            }

        });

            

        </script>
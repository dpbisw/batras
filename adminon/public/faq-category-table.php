<?php
include_once('includes/functions.php');
include_once('includes/functions2.php');

?>
<section class="content-header">
    <h1>FAQ Category /<small><a href="home.php"><i class="fa fa-home"></i> Home</a></small></h1>
</section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-xs-12">
                <div class="box container">
                    <div class="row">
                        <div class="col-md-5" style="border-right: 1px solid #F1F1F1">
                    <div>
                        <h3 class="box-title">Add FAQ Category</h3>
                    </div>
                    <p id="status_cate" style="display: none"></p>
                            <form id="add_form" action="public/db-operation2.php" method="POST" enctype="multipart/form-data">
                            <input type="hidden" id="add_faq_category" name="add_faq_category" required="" value="1" aria-required="true">
                                <div class="box-body">
                                    <p id="add_group_status"></p>
                                    <div class="form-group" id="col1">
                                        <label for="faq_category_name">FAQ Category Name</label>
                                        <input type="text" class="form-control" id="faq_category_name" name="faq_category_name">
                                        <p id="error_category" style="color:red"></p>
                                    </div>
                                    <div class="form-group" id="col1">
                                        <label for="is_visible">Select Status</label>
                                        <select id="is_visible" name="is_visible" class="form-control" required>
                                            <option value="1">Visible</option>
                                            <option value="0">Hidden</option>
                                        </select>
                                    </div>
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary" id="submit_btn" name="btnAdd">Add</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        	<div class="col-md-7 container">
                    <div style="padding:5px 0px">
                        <h3 class="box-title">FAQ Category Table</h3>
                    </div>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Category Name</th>
                                    <th>Slug</th>
                                    <th>Status</th>
									<th>Action</th>
                                </tr>
                            </thead>
                        <?php 
                        $fn2 = new functions2();
                        $faq_categories = $fn2->getFAQCategories();
                        ?>
                            <tbody>
                                <?php foreach($faq_categories as $fc){ ?>
                                <tr>
                                    <td><?php echo ($fc['name']); ?></td>
                                    <td><?php echo $fc['slug']; ?></td>
                                    <td><?php echo ($fc['is_visible'] == 1) ? 'Visible' : 'Hidden'; ?></td>
									<td style="display: flex">
										<button class="btn btn-primary" onclick="showEditModal('<?php echo $fc['faq_category_id'] ?>')"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>&nbsp;
										<button class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>                        		
                        	</div>                        
                    </div>
                </div>
            </div>
            <div class="separator"> </div>
        </div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit FAQ Category</h4>
        <p id="edit_status"></p>
      </div>
        <form id="edit_form" action="public/db-operation2.php" method="POST" enctype="multipart/form-data">
        <input type="hidden" id="edit_faq_category" name="edit_faq_category" required="" value="1" aria-required="true">
      <div class="modal-body">
            <input type="hidden" name="edit_faq_category_id" id="edit_faq_category_id">
            <div class="form-group">
                <label for="edit_category_name">Category Name</label>
                <input type="text" class="form-control" id="edit_category_name" name="edit_category_name"  required>
            </div>            
            <div class="form-group">
                <label for="edit_is_visible">Status</label>
                <select id="edit_is_visible" name="edit_is_visible" class="form-control" required>
                  <option value="1">Visible</option>
                  <option value="0">Hidden</option>
                </select>
            </div>
      </div>
      <div class="modal-footer">
        <button type="submit" id="edit_btn" class="btn btn-success">Update</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
        </form>
    </div>

  </div>
</div>

    </section>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>

<script>
        $("#add_form").validate({
            rules:{
                faq_category_name : 'required',
                is_visible : 'required',
            },
            messages :{
                faq_category_name : "FAQ Category Name is required",
                is_visible : "Visiblity is required",
            }
        });
        $("#edit_form").validate({
            rules:{
                edit_category_name : 'required',
                edit_is_visible : 'required',
            },
            messages :{
                edit_category_name : "FAQ Category Name is required",
                edit_is_visible : "Visiblity is required",
            }
        });
    $("#edit_form").on('submit',function(e){
        e.preventDefault();
        var formData = new FormData(this);
        $('#edit_form').validate();
        if($("#edit_form").valid()){
        $.ajax({
            method : 'POST',
            url: $(this).attr('action'),
            data: formData,
            beforeSend: function() {
                $('#edit_btn').html('Please wait..');
            },
            cache: false,
            contentType: false,
            processData: false,
            success: function(result) {
                if(result == 1)
                {
                    $("#edit_status").removeClass("alert-danger");                    
                    $("#edit_status").addClass("alert alert-success");
                    $("#edit_status").html("Record Updated Successfully!");
                    location.reload();
                }

            	if(result == 0 || result == 2){
                    $("#edit_status").removeClass("alert-success");                    
                    $("#edit_status").addClass("alert alert-danger");

            		if(result == 0) msg = 'Something Went Wrong. Please Try Again!';
            		if(result == 2) msg = 'This Category name already exists. Please try Again!';
            		$("#edit_status").html(`<i class="fa fa-exclamation-circle" aria-hidden="true"></i> ${msg}`);
            		$('#edit_btn').html('Edit');
                }
            }
        });
    }
    });
    
    $('#add_form').on('submit', function(e) {
        e.preventDefault();
        $('#add_form').validate();
        if($("#add_form").valid()){
        var formData = new FormData(this);
        $.ajax({
            method: 'POST',
            url: $(this).attr('action'),
            data: formData,
            beforeSend: function() {
                $('#submit_btn').html('Please wait..');
            },
            cache: false,
            contentType: false,
            processData: false,
            success: function(result) {
            	if(result == 1)
            	{
            		$("#status_cate").css("display",'block');
            		$("#status_cate").removeClass("alert-danger");
            		$("#status_cate").addClass("alert alert-success");
            		$("#status_cate").html(`<i class="fa fa-check-circle" aria-hidden="true"></i> Record Added Successfully!`);
            		location.reload();
            	}
            	if(result == 0 || result == 2){
            		$("#status_cate").css("display",'block');
            		$("#status_cate").removeClass("alert-success");
            		$("#status_cate").addClass("alert alert-danger");
            		var msg = '';
            		if(result == 0) msg = 'Something Went Wrong. Please Try Again!';
            		if(result == 2) msg = 'This Category name already exists. Please try Again!';
            		$("#status_cate").html(`<i class="fa fa-exclamation-circle" aria-hidden="true"></i> ${msg}`);
            		$('#submit_btn').html('Add');
            	}
            }
        });

        }
    });

    function deleteGroup(group_id,elem) {
        var c = confirm('Do you want to delete this Group? All the Related Data will be deleted');
        if(c)
        {
        $.ajax({
            type: 'POST',
            url: "public/db-operation2.php",
            data: { group_delete_id : group_id },
            beforeSend : function(){
                elem.innerHTML="Deleting";
            },
            success: function(result) {
                if(result == 1)
                {
                    $("#group_delete_status").removeClass('alert-danger');
                    $("#group_delete_status").addClass('alert alert-success');
                    $("#group_delete_status").html("Record Deleted Successfully!");
                    elem.innerHTML = 'Deleted';

                    location.reload();
                }
                if(result == 0){
                    $("#group_delete_status").removeClass('alert-success');
                    $("#group_delete_status").addClass('alert alert-danger');    
                    $("#group_delete_status").html(`Something went wrong..Please try again!`);  
                    elem.innerHTML = 'Delete';
                }

            }
        });            
        }
    }

    function showEditModal(faq_category_id) {
        $.ajax({
            type: 'POST',
            url: "public/db-operation2.php",
            data: { faq_cat_edit_id : faq_category_id },
            success: function(result) {
                var jObj = JSON.parse(result);
                jObj.forEach(function(item,index){
                	$("#edit_faq_category_id").val(item.faq_category_id);
                	$("#edit_category_name").val(item.name);
                	$("#edit_is_visible").val(item.is_visible);
                });
            }
        });            
        $("#myModal").modal('show');
    }
</script>
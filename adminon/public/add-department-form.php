<?php

include_once('includes/functions.php');

$function = new functions;

include_once('includes/custom-functions.php');

$fn = new custom_functions;



?>

<?php

if (isset($_POST['btnAdd'])) {

	if (ALLOW_MODIFICATION == 0 && !defined(ALLOW_MODIFICATION)) {

		echo '<label class="alert alert-danger">This operation is not allowed in demo panel!.</label>';

		return false;

	}

	if ($permissions['departments']['create'] == 1) {



		$department_name = $db->escapeString($fn->xss_clean($_POST['department_name']));

		$slug = $function->slugify($db->escapeString($fn->xss_clean($_POST['department_name'])));

		$department_subtitle = $db->escapeString($fn->xss_clean($_POST['department_subtitle']));

		$department_status = $db->escapeString($fn->xss_clean($_POST['department_status']));

		$is_home = $_POST['is_home'];



		// get image info

		$menu_image = $db->escapeString($fn->xss_clean($_FILES['department_image']['name']));

		$image_error = $db->escapeString($fn->xss_clean($_FILES['department_image']['error']));

		$image_type = $db->escapeString($fn->xss_clean($_FILES['department_image']['type']));



        // get image info

        $menu_image2 = $db->escapeString($fn->xss_clean($_FILES['department_image2']['name']));

        $image_error2 = $db->escapeString($fn->xss_clean($_FILES['department_image2']['error']));

        $image_type2 = $db->escapeString($fn->xss_clean($_FILES['department_image2']['type']));



		// create array variable to handle error

		$error = array();



		if (empty($department_name)) {

			$error['department_name'] = " <span class='label label-danger'>Required!</span>";

		}

		if (empty($department_subtitle)) {

			$error['department_subtitle'] = " <span class='label label-danger'>Required!</span>";

		}

		

		if (empty($department_status)) {

			$error['department_status'] = " <span class='label label-danger'>Required!</span>";

		}



		// common image file extensions

		$allowedExts = array("gif", "jpeg", "jpg", "png");



		// get image file extension

		error_reporting(E_ERROR | E_PARSE);

		$extension = end(explode(".", $_FILES["department_image"]["name"]));



        $extension2 = end(explode(".", $_FILES["department_image2"]["name"]));



		if ($image_error > 0) {

			$error['department_image'] = " <span class='label label-danger'>Not Uploaded!!</span>";

		} else {

			$mimetype = mime_content_type($_FILES["department_image"]["tmp_name"]);

			if (!in_array($mimetype, array('image/jpg', 'image/jpeg', 'image/gif', 'image/png'))) {

				$error['department_image'] = " <span class='label label-danger'>Image type must jpg, jpeg, gif, or png!</span>";

			}

		}



        if ($image_error2 > 0) {

            $error['department_image2'] = " <span class='label label-danger'>Not Uploaded!!</span>";

        } else {

            $mimetype2 = mime_content_type($_FILES["department_image2"]["tmp_name"]);

            if (!in_array($mimetype2, array('image/jpg', 'image/jpeg', 'image/gif', 'image/png'))) {

                $error['department_image2'] = " <span class='label label-danger'>Image type must jpg, jpeg, gif, or png!</span>";

            }

        }



		if (!empty($department_name) && !empty($department_subtitle) && empty($error['department_image']) && empty($error['department_image2'])) {



			// create random image file name

			$string = '0123456789';

			$file = preg_replace("/\s+/", "_", $_FILES['department_image']['name']);

			$menu_image = $function->get_random_string($string, 4) . "-" . date("Y-m-d") . "1." . $extension;

			$menu_image_new = $menu_image;



			// upload new image

			$upload = move_uploaded_file($_FILES['department_image']['tmp_name'], 'upload/images/' . $menu_image_new);



            // create random image file name

            $file2 = preg_replace("/\s+/", "_", $_FILES['department_image2']['name']);

            $menu_image2 = $function->get_random_string($string, 4) . "-" . date("Y-m-d") . "2." . $extension2;

            $menu_image_new2 = $menu_image2;



            // upload new image

            $upload2 = move_uploaded_file($_FILES['department_image2']['tmp_name'], 'upload/images/' . $menu_image_new2);



			// insert new data to menu table

			$upload_image = 'upload/images/' . $menu_image_new;

            $upload_image2 = 'upload/images/' . $menu_image_new2;



			$sql_query = "INSERT INTO departments (name, slug, subtitle, image,image2, status,is_home)VALUES('$department_name', '$slug', '$department_subtitle', '$upload_image','$upload_image2','$department_status','$is_home')";

			$db->sql($sql_query);

			$result = $db->getResult();

			if (!empty($result)) {

				$result = 0;

			} else {

				$result = 1;

			}



			if ($result == 1) {

				$error['add_department'] = " <section class='content-header'><span class='label label-success'>Department Added Successfully</span></section>";

			} else {

				$error['add_department'] = " <span class='label label-danger'>Failed add department</span>";

			}

		}

	} else {

		$error['check_permission'] = " <section class='content-header'><span class='label label-danger'>You have no permission to create department</span></section>";

	}

}

?>

<section class="content-header">

    <h1>Add Department <small><a href='departments.php'> <i class='fa fa-angle-double-left'></i>&nbsp;&nbsp;&nbsp;Back to Departments</a></small></h1>



    <?php echo isset($error['add_department']) ? $error['add_department'] : ''; ?>

    <ol class="breadcrumb">

        <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>

    </ol>

    <hr />

</section>

<section class="content">

    <div class="row">

        <div class="col-md-6">

            <?php if ($permissions['categories']['create'] == 0) { ?>

                <div class="alert alert-danger">You have no permission to create departments.</div>

            <?php } ?>

            <!-- general form elements -->

            <div class="box box-primary">

                <div class="box-header with-border">

                    <h3 class="box-title">Add Department</h3>



                </div><!-- /.box-header -->

                <!-- form start -->

                <form method="post" enctype="multipart/form-data">

                    <div class="box-body">

                        <div class="form-group">

                            <label for="department_name">Department Name</label><?php echo isset($error['department_name']) ? $error['department_name'] : ''; ?>

                            <input type="text" class="form-control" name="department_name" required id="department_name">

                        </div>

                        <div class="form-group">

                            <label for="department_subtitle">Department Subtitle</label><?php echo isset($error['department_subtitle']) ? $error['department_subtitle'] : ''; ?>

                            <input type="text" class="form-control" name="department_subtitle" required id="department_subtitle">

                        </div>

                        

                        



                        <div class="form-group">

                            <label for="department_image">Image&nbsp;&nbsp;&nbsp;*Please choose square image of larger than 350px*350px & smaller than 550px*550px.</label><?php echo isset($error['department_image']) ? $error['department_image'] : ''; ?>

                            <input type="file" name="department_image" id="department_image" required />

                            <p style="color:red" id="err_dept_image"></p>

                        </div>



                        <div class="form-group">

                            <label for="department_image2">Image 2 &nbsp;&nbsp;*Minimal Icon.</label><?php echo isset($error['department_image2']) ? $error['department_image2'] : ''; ?>

                            <input type="file" name="department_image2" id="department_image2" required />

                            <p style="color:red" id="err_dept_image2"></p>

                        </div>

                        

                        <div class="form-group">

                            <label for="">Department Status</label><?php echo isset($error['department_status']) ? $error['department_status'] : ''; ?><br>

                            <input type="checkbox" id="department_status_button" class="js-switch" checked>

                            <input type="hidden" id="department_status" name="department_status" value="1">

                        </div>

                        <div class="form-group">

                        	<label for="">Visible in Home page</label><br>

                        	<input type="checkbox" id="is_home_button" class="js-switch" checked>

                        	<input type="hidden" name="is_home" value="1" id="is_home">

                        </div>

                        

                        

                    </div><!-- /.box-body -->



                    <div class="box-footer">

                        <button type="submit" class="btn btn-primary" id="btnAdd" name="btnAdd">Add</button>

                        <input type="reset" class="btn-warning btn" value="Clear" />

                    </div>



                </form>



            </div><!-- /.box -->

            <?php echo isset($error['check_permission']) ? $error['check_permission'] : ''; ?>

        </div>

    </div>

</section>



<div class="separator"> </div>



<script>

    var changeCheckbox = document.querySelector('#department_status_button');

    var init = new Switchery(changeCheckbox);

    changeCheckbox.onchange = function() {

        if ($(this).is(':checked')) {

            $('#department_status').val(1);

        } else {

            $('#department_status').val(0);

        }

    };



    var changeCheckbox2 = document.querySelector('#is_home_button');

    var init = new Switchery(changeCheckbox2);

    changeCheckbox2.onchange = function() {

        if ($(this).is(':checked')) {

            $('#is_home').val(1);

        } else {

            $('#is_home').val(0);

        }

    };    

        //binds to onchange event of your input field

$('#department_image').bind('change', function() {



  //this.files[0].size gets the size of your file.

  if(this.files[0].size > 50000)

  {

    $("#err_dept_image").html("Please select a file less than 50kb size");

    $("#btnAdd").attr('disabled',true);

 

  }

  else{

    $("#err_dept_image").html("");

    $("#btnAdd").attr('disabled',false);

  }



});





        //binds to onchange event of your input field

$('#department_image2').bind('change', function() {



  //this.files[0].size gets the size of your file.

  if(this.files[0].size > 50000)

  {

    $("#err_dept_image2").html("Please select a file less than 50kb size");

    $("#btnAdd").attr('disabled',true);

 

  }

  else{

    $("#err_dept_image2").html("");

    $("#btnAdd").attr('disabled',false);

  }



});



</script>



<?php $db->disconnect(); ?>
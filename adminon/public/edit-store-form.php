<?php
include_once('includes/functions.php');
$function = new functions;
include_once('includes/custom-functions.php');
$fn = new custom_functions;
?>
<?php
$ID = (isset($_GET['id'])) ? $db->escapeString($fn->xss_clean($_GET['id'])) : "";
$store_data = array();

$sql_query = "SELECT image FROM store WHERE id =" . $ID;
$db->sql($sql_query);
$res = $db->getResult();
if (isset($_POST['btnEdit'])) {
	if (ALLOW_MODIFICATION == 0 && !defined(ALLOW_MODIFICATION)) {
		echo '<label class="alert alert-danger">This operation is not allowed in demo panel!.</label>';
		return false;
	}
	if ($permissions['categories']['update'] == 1) {

        
		$name = $db->escapeString($fn->xss_clean($_POST['name']));
		$slug = $db->escapeString($function->slugify($fn->xss_clean($_POST['name'])));
		$address = $db->escapeString($fn->xss_clean($_POST['address']));
		$city = $db->escapeString($fn->xss_clean($_POST['city']));
		$state = $db->escapeString($fn->xss_clean($_POST['state']));
		$phone = $db->escapeString($fn->xss_clean($_POST['phone']));
		$timings = $db->escapeString($fn->xss_clean($_POST['timings']));
		$status = $db->escapeString($fn->xss_clean($_POST['status']));
		
		$sql = "SELECT slug FROM stores where id!=" . $ID;
		$db->sql($sql);
		$resc = $db->getResult();
		$i = 1;
		$slugRes = $slug;
		$slugnew = $slug;
		foreach ($resc as $rowc) {
		    
			if ($slugnew == $rowc['slug']) {
				$slugnew = $slugRes . '-' . $i;
				$i++;
			}
		}
		

		$menu_image = $db->escapeString($fn->xss_clean($_FILES['image']['name']));
		$image_error = $db->escapeString($fn->xss_clean($_FILES['image']['error']));
		$image_type = $db->escapeString($fn->xss_clean($_FILES['image']['type']));
		$error = array();

        
		if (empty($name)) {
			$error['name'] = " <span class='label label-danger'>Required!</span>";
		}
		

		// get image file extension
		error_reporting(E_ERROR | E_PARSE);
		$extension = end(explode(".", $_FILES["image"]["name"]));

		if (!empty($menu_image)) {
			$mimetype = mime_content_type($_FILES["image"]["tmp_name"]);
			if (!in_array($mimetype, array('image/jpg', 'image/jpeg', 'image/gif', 'image/png'))) {
				$error['image'] = " <span class='label label-danger'>Image type must jpg, jpeg, gif, or png!</span>";
			}
		}

		if (!empty($name) && empty($error['image'])) {

			if (!empty($menu_image)) {

				$string = '0123456789';
				$file = preg_replace("/\s+/", "_", $_FILES['image']['name']);
				
				$image = $function->get_random_string($string, 4) . "-" . date("Y-m-d") . "." . $extension;

				// delete previous image
				$delete = unlink($res[0]['image']);

				// upload new image
				$upload = move_uploaded_file($_FILES['image']['tmp_name'], 'upload/images/' . $image);
				$upload_image = 'upload/images/' . $image;
				$sql_query = "UPDATE stores SET name = '{$name}', slug = '{$slugnew}', address = '{$address}', city = '{$city}', state = '{$state}', phone = '{$phone}', timings = '{$timings}' , status = '{$status}'  , image = '{$upload_image}' WHERE id =  '{$ID}'";
				if ($db->sql($sql_query)) {
					$db->sql($sql_query);
					$update_result = $db->getResult();
				}
			} else {

				$sql_query = "UPDATE stores SET name = '{$name}', slug = '{$slugnew}', address = '{$address}', city = '{$city}', state = '{$state}', phone = '{$phone}', timings = '{$timings}' , status = '{$status}' WHERE id =  '{$ID}'";
				$db->sql($sql_query);
				$update_result = $db->getResult();
			}

			if (!empty($update_result)) {
				$update_result = 0;
			} else {
				$update_result = 1;
			}

			// check update result
			if ($update_result == 1) {
				$error['update_store'] = " <section class='content-header'><span class='label label-success'>Store updated Successfully</span></section>";
			} else {
				$error['update_store'] = " <span class='label label-danger'>Failed update store</span>";
			}
		}
	} else {
		$error['check_permission'] = " <section class='content-header'><span class='label label-danger'>You have no permission to update store</span></section>";
	}
}

// create array variable to store previous data
$data = array();

$sql_query = "SELECT * FROM stores WHERE id =" . $ID;
$db->sql($sql_query);
$res = $db->getResult();

if (isset($_POST['btnCancel'])) { ?>
	<script>
		window.location.href = "stores.php";
	</script>
<?php } ?>
<section class="content-header">
	<h1>
		Edit Store<small><a href='stores.php'><i class='fa fa-angle-double-left'></i>&nbsp;&nbsp;&nbsp;Back to Stores</a></small></h1>
	<small><?php echo isset($error['update_store']) ? $error['update_store'] : ''; ?></small>
	<ol class="breadcrumb">
		<li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
	</ol>
</section>
<section class="content">
	<!-- Main row -->

	<div class="row">
		<div class="col-md-6">
			<?php if ($permissions['categories']['update'] == 0) { ?>
				<div class="alert alert-danger topmargin-sm">You have no permission to update store.</div>
			<?php } ?>
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Edit Store</h3>
				</div><!-- /.box-header -->
				<!-- form start -->
				<form id="edit_store_form" method="post" enctype="multipart/form-data">
					<div class="box-body">
						<div class="form-group">
							<label for="name">Store Name</label><?php echo isset($error['name']) ? $error['name'] : ''; ?>
							<input type="text" class="form-control" name="name" value="<?php echo $res[0]['name']; ?>">
						</div>
						<div class="form-group">
							<label for="address">Store Address</label><?php echo isset($error['address']) ? $error['address'] : ''; ?>
							<input type="text" class="form-control" name="address" value="<?php echo $res[0]['address']; ?>">
						</div>
						
						<div class="form-group">
							<label for="city">Store City</label><?php echo isset($error['city']) ? $error['city'] : ''; ?>
							<input type="text" class="form-control" name="city" value="<?php echo $res[0]['city']; ?>">
						</div>
						<div class="form-group">
							<label for="state">Store State</label><?php echo isset($error['state']) ? $error['state'] : ''; ?>
							<input type="text" class="form-control" name="state" value="<?php echo $res[0]['state']; ?>">
						</div>
						<div class="form-group">
							<label for="phone">Store Phone</label><?php echo isset($error['phone']) ? $error['phone'] : ''; ?>
							<input type="text" class="form-control" name="phone" value="<?php echo $res[0]['phone']; ?>">
						</div>
						<div class="form-group">
							<label for="timings">Store Timings</label><?php echo isset($error['timings']) ? $error['timings'] : ''; ?>
							<input type="text" class="form-control" name="timings" value="<?php echo $res[0]['timings']; ?>">
						</div>
						
						<div class="form-group">
                            <label for="">Store Status</label><br>
                            <input type="checkbox" id="store_status_button" class="js-switch" <?php echo ($res[0]['status'] == 1) ? "checked" : ""; ?>>
                            <input type="hidden" id="status" name="status"  value="<?php echo $res[0]['status']; ?>">
                        </div>
                        
						
						<div class="form-group">
							<label for="image">Image&nbsp;&nbsp;&nbsp;*Please choose square image of larger than 350px*350px & smaller than 550px*550px.</label><?php echo isset($error['image']) ? $error['image'] : ''; ?>
							<input type="file" name="image" id="image" title="Please choose square image of larger than 350px*350px & smaller than 550px*550px.">
							<p class="help-block"><img src="<?php echo $res[0]['image']; ?>" width="190" height="190" /></p>
						</div>
					</div><!-- /.box-body -->

					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="btnEdit">Update</button>
						<button type="submit" class="btn btn-danger" name="btnCancel">Cancel</button>
					</div>
				</form>
			</div><!-- /.box -->
		</div>
	</div>
</section>

<div class="separator"> </div>
<?php $db->disconnect(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script>
	$('#edit_store_form').validate({
		rules: {
			name: "required",
		}
	});
</script>
<script>
    var changeCheckbox = document.querySelector('#store_status_button');
    var init = new Switchery(changeCheckbox);
    changeCheckbox.onchange = function() {
        if ($(this).is(':checked')) {
            $('#store_status').val(1);
        } else {
            $('#store_status').val(0);
        }
    };
</script>
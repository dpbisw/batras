<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once('includes/functions.php');
include_once('includes/functions2.php');
$fn2 = new functions2;
?>
<section class="content-header">
    <h1>User List Requests /<small><a href="home.php"><i class="fa fa-home"></i> Home</a></small></h1>
</section>
    <!-- Main content -->
    <section class="content">
        <p class="alert alert-success" id="successalert" style="display: none">Success</p>
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-xs-12">
                <div class="box">

                	<div id="table_request">
                    
                    <?php if(isset($_GET['id'])){ 
                        $detail = $fn2->getReqDetail($_GET['id']);
                    ?>
                	<div class="box-header with-border">
                		<h3 class="box-title"><a href="user_request_lists.php"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i></a> Request Detail</h3>
                	</div>
                	<div class="box-body">
                	    <?php 
                	        foreach($detail as $d){
                	            $name = $d['name']; $email = $d['email']; $mobile = $d['mobile']; $reqd = $d['request_list']; $subject = $d['kirana_subject'];
                	        }
                	    ?>
                		<p><b>Name : </b> <span><?php echo $name ?></span></p>
                		<p><b>Email : </b> <span><?php echo $email ?></span></p>
                		<p><b>Mobile : </b> <span><?php echo $mobile ?></span></p>
                        <h5><b>Request Subject : </b></h5>
                        <p><?php echo $subject; ?></p>                        
                		<h5><b>Request List : </b></h5>
                		<p><?php echo nl2br($reqd); ?></p>
                	</div>
                    <?php }else{ ?>
                	<div class="box-header with-border">
                		<h3 class="box-title">User List Requests</h3>
                	</div>
                	<div class="box-body">
                		<?php 
                		$urequests = $fn2->getAllUserListRequests();
                		?>	

                		<div class="table-responsive">
                			<table class="table table-bordered table-hover" id="requestTable">
                				<thead>
                					<tr>
                						<th>Sl. No.</th>
                						<th>Name</th>
                						<th>Email</th>
                						<th>Phone</th>
                						<th>Kirana List</th>
                                        <th>List File</th>
                                        <th>Audio File</th>
                						<th>Requested On</th>
                						<th>Action</th>
                					</tr>
                				</thead>
                				<tbody>
                					<?php 
                					if(count($urequests)>0){ 
                						$c=0;
                					foreach($urequests as $ur){ ?>
                					<tr>
                						<td><?php echo ++$c; ?></td>
                						<td><?php echo $ur['name']; ?></td>
                						<td><?php echo $ur['email']; ?></td>
                						<td><?php echo $ur['mobile']; ?></td>
                						<td><a href="user_request_lists.php?id=<?php echo $ur['user_request_list_id']; ?>" class="btn btn-primary">View List</a></td>
                                        <td>
                                            <?php if($ur['list_file']!=''){ ?>
                                            <a href="<?php echo DOMAIN_URL."upload/request-list-files/".$ur['list_file']; ?>" class="btn btn-success">View File</a>
                                        <?php }else{ echo "-"; } ?>
                                        </td>
                                        <td>
                                            <?php if($ur['audio_file']!=''){ ?>
    <script>
      function play() {
        var id = <?php echo $ur['user_request_list_id']; ?>;
        var audio = document.getElementById(`audio_${id}`);
        audio.play();
      }
    </script>

    <button type="button" class="btn btn-warning" onclick="play()">Play</button>
    <audio id="audio_<?php echo $ur['user_request_list_id']; ?>" src="<?php echo DOMAIN_URL."upload/request-list-files/".$ur['audio_file']; ?>"></audio>

                                        <?php }else{ echo "-"; } ?>
                                        </td>                                        
                						<td><?php echo date("d-m-Y", strtotime($ur['created_at'])); ?></td>
                						<td>
                						<button onclick="confDelete('<?php echo $ur['user_request_list_id']; ?>',this)" class="btn btn-danger">Delete</button>                							
                						</td>
                					</tr>
                				<?php }}else{
                					?>
                					<tr>
                						<td colspan="7">No Records Found</td>
                					</tr>
                					<?php
                				} ?>
                				</tbody>
                			</table>
                		</div>
                	</div>
                	<?php } ?>
                </div>
                <div id="request_detail" style="display: none">
                	<div class="box-header with-border">
                		<h3 class="box-title"><a href=""><i class="fa fa-chevron-circle-left" aria-hidden="true"></i></a> Request Detail</h3>
                	</div>
                	<div class="box-body">
                		<p><b>Name : </b> <span id="request_detail_name"></span></p>
                		<p><b>Email : </b> <span id="request_detail_email"></span></p>
                		<p><b>Mobile : </b> <span id="request_detail_mobile"></span></p>
                		<h5><b>Request List : </b></h5>
                		<p id="request_detail_list"></p>
					</div>                	
                </div>
                </div>
            </div>
        </div>
    </section>
    <script>

    $(function(){
        $("#requestTable").DataTable();
    });


        function confDelete(product_request_id,elem) {
            var c = confirm("Do you want to delete?");
            if(c){
            $.ajax({
                url : 'public/db-operation2.php',
                type: "post",
                dataType : 'json',
                data: 'user_request_delete_id='+product_request_id,
                beforeSend : function(){
                	elem.innerHTML = "Deleting";
                },
                success: function(result){
                    if(result == 1)
                    {
                    elem.innerHTML = "Delete";
                    location.reload();                        
                    }
				//   window.location.href="user_request_lists.php";
                }
            });   
            }                 
        }

        function showDetail(user_request_id,elem)
        {
            $.ajax({
                url : 'public/db-operation2.php',
                type: "post",
                dataType : 'json',
                data: 'user_request_id='+user_request_id,
                beforeSend : function(){
                	elem.innerHTML = 'Showing';
                },
                success: function(result){
                	result.sql_data.forEach( function(element, index) {
                		$("#request_detail_name").html(element.name);
                		$("#request_detail_email").html(element.email);
                		$("#request_detail_mobile").html(element.mobile);
                		$("#request_detail_list").html(element.request_list);
                	});
                	$("#table_request").hide();
                	$("#request_detail").show();
                       elem.innerHTML = 'View List';
                }
            });
        }
    </script>
<?php
include_once('includes/functions.php');
include_once('includes/functions2.php');

$sql_query = "SELECT id, name FROM area where name!='Choose Your City'ORDER BY id ASC";
$db->sql($sql_query);
$res_area = $db->getResult();

?>
<section class="content-header">
    <h1>State /<small><a href="home.php"><i class="fa fa-home"></i> Home</a></small></h1>
</section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-xs-12">
                <div class="box container">
                    <div class="row">
                        <div class="col-md-5" style="border-right: 1px solid #F1F1F1">
                    <div>
                        <h3 class="box-title">Add State</h3>
                    </div>
                    <p id="status_cate" style="display: none"></p>
                            <form id="add_form" action="public/db-operation2.php" method="POST" enctype="multipart/form-data">
                            <input type="hidden" id="add_state" name="add_state" required="" value="1" aria-required="true">
                                <div class="box-body">
                                    <p id="add_group_status"></p>
                                                                        
                                    <div class="form-group" id="col1">
                                        <label for="attribute_name">State</label>
                                        <input type="text" class="form-control" id="state_name" name="state_name">
                                        <p id="error_pincode" style="color:red"></p>
                                    </div>
                                    <div class="form-group" id="col1">
                                        <label for="is_visible">Select Status</label>
                                        <select id="is_visible" name="is_visible" class="form-control" required>
                                            <option value="1">Visible</option>
                                            <option value="0">Hidden</option>
                                        </select>
                                    </div>
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary" id="submit_btn" name="btnAdd">Add</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        	<div class="col-md-7 container">
                    <div style="padding:5px 0px">
                        <h3 class="box-title">State Table</h3>
                    </div>
                        <table class="table table-hover" id="attribute_table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>State</th>
                                    <th>Status</th>
									<th>Action</th>
                                </tr>
                            </thead>
                        <?php 
                        $fn2 = new functions2();
                        $states = $fn2->getStates();
                        ?>
                            <tbody>
                                <?php foreach($states as $a){ ?>
                                <tr>
                                    <td><?php echo $a['id']; ?></td>
                                    <td><?php echo ($a['state_name']); ?></td>
                                    <td><?php echo ($a['is_visible'] == 1) ? 'Visible' : 'Hidden'; ?></td>                                    
									<td style="display: flex">
										<button  class="btn btn-primary" onclick="showEditModal('<?php echo $a['id'] ?>')"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>&nbsp;
										<button onclick="deleteConfirm('<?php echo $a['id']; ?>')" data-confirm="Do you really want to do this?" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>                        		
                        	</div>                        
                    </div>
                </div>
            </div>
            <div class="separator"> </div>
        </div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit State</h4>
        <p id="edit_status"></p>
      </div>
        <form id="edit_form" action="public/db-operation2.php" method="POST" enctype="multipart/form-data">
        <input type="hidden" id="update_state" name="update_state" required="" value="1" aria-required="true">
      <div class="modal-body">
            <input type="hidden" name="edit_state_id" id="edit_state_id">
            <div class="form-group">
                <label for="edit_pincode">State</label>
                <input type="text" class="form-control" id="edit_state_name" name="edit_state_name"  required>
            </div>            
            <div class="form-group">
                <label for="edit_is_visible">Status</label>
                <select id="edit_is_visible" name="edit_is_visible" class="form-control" required>
                  <option value="1">Visible</option>
                  <option value="0">Hidden</option>
                </select>
            </div>
      </div>
      <div class="modal-footer">
        <button type="submit" id="edit_btn" class="btn btn-success">Update</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
        </form>
    </div>
  </div>
</div>
    </section>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script>
	$(function(){
		$("#attribute_table").DataTable({
            "order": [[0,'desc']]
        });
	});
        $("#add_form").validate({
            rules:{
            	area_ID : 'required',
                pincode : 'required',
                is_visible : 'required',
            },
            messages :{
            	area_ID : "area is required",
                pincode : "Pincode is required",
                is_visible : "Visiblity is required",
            }
        });
        $("#edit_form").validate({
            rules:{
                edit_pincode_id : 'required',
                edit_pincode:'required',
                edit_is_visible : 'required'
            },
            messages:{
                edit_pincode_id : "Pincode ID is required",
                edit_pincode : "Pincode is required",
                edit_is_visible : "Visiblity is required",              
            }
        });
    $("#edit_form").on('submit',function(e){
        e.preventDefault();
        var formData = new FormData(this);
        $('#edit_form').validate();
        if($("#edit_form").valid()){        
        $.ajax({
            method : 'POST',
            url: $(this).attr('action'),
            data: formData,
            beforeSend: function() {
                $('#edit_btn').html('Please wait..');
            },
            cache: false,
            contentType: false,
            processData: false,
            success: function(result) {
                if(result == 1)
                {
                    $("#edit_status").removeClass("alert-danger");                    
                    $("#edit_status").addClass("alert alert-success");
                    $("#edit_status").html("Record Updated Successfully!");
                    location.reload();
                }
            	if(result == 0 || result == 2){
                    $("#edit_status").removeClass("alert-success");                    
                    $("#edit_status").addClass("alert alert-danger");
            		if(result == 0) msg = 'Something Went Wrong. Please Try Again!';
            		if(result == 2) msg = 'This Category name already exists. Please try Again!';
            		$("#edit_status").html(`<i class="fa fa-exclamation-circle" aria-hidden="true"></i> ${msg}`);
            		$('#edit_btn').html('Edit');
                }
            }
        });
    }
    });
    
    $('#add_form').on('submit', function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        $('#add_form').validate();
        if($("#add_form").valid()){        
        $.ajax({
            method: 'POST',
            url: $(this).attr('action'),
            data: formData,
            beforeSend: function() {
                $('#submit_btn').html('Please wait..');
                $('#submit_btn').attr('disabled',true);
            },
            cache: false,
            contentType: false,
            processData: false,
            success: function(result) {
            	if(result == 1)
            	{
            		$("#status_cate").css("display",'block');
            		$("#status_cate").removeClass("alert-danger");
            		$("#status_cate").addClass("alert alert-success");
            		$("#status_cate").html(`<i class="fa fa-check-circle" aria-hidden="true"></i> Record Added Successfully!`);
            		location.reload();
            	}
            	if(result == 0 || result == 2){
            		$("#status_cate").css("display",'block');
            		$("#status_cate").removeClass("alert-success");
            		$("#status_cate").addClass("alert alert-danger");
            		var msg = '';
            		if(result == 0) msg = 'Something Went Wrong. Please Try Again!';
            		if(result == 2) msg = 'This Pincode already exists. Please try Again!';
            		$("#status_cate").html(`<i class="fa fa-exclamation-circle" aria-hidden="true"></i> ${msg}`);
            		$('#submit_btn').html('Add');
            	}
                $('#submit_btn').attr('disabled',false);
            }
        });
        }
    });
    function showEditModal(state_edit_id) {
        $.ajax({
            type: 'POST',
            url: "public/db-operation2.php",
            data: { state_edit_id : state_edit_id },
            success: function(result) {
                var jObj = JSON.parse(result);
                jObj.forEach(function(item,index){
                	$("#edit_state_id").val(item.id);
                	$("#edit_state_name").val(item.state_name);
                	$("#edit_is_visible").val(item.is_visible);
                });
            }
        });            
        $("#myModal").modal('show');
    }
    function deleteConfirm(state_id)
    {
		var c = confirm("Do you want to Delete this State?");
		if(c)
		{
        $.ajax({
            type: 'POST',
            url: "public/db-operation2.php",
            data: { state_delete_id : state_id },
            success: function(result) {
            	if(result == 1)
            	{
            		alert("Deleted");
            		location.reload();
            	}
            	else{
            		alert("Error");
            	}
            }
        });            			
		}    	
    }
</script>
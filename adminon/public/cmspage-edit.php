<?php
include_once('includes/functions.php');
include_once('includes/functions2.php');
$function = new functions;
$fn2 = new functions2;
include_once('includes/custom-functions.php');
$fn = new custom_functions;
    $cid = base64_decode($_GET['cid']);

$data = array();
$sql_query = "SELECT * FROM pages WHERE page_id = '$cid' ";
$db->sql($sql_query);
$res_query = $db->getResult();
?>
<section class="content-header">
    <h1>Edit CMS Page</h1>
    <ol class="breadcrumb">
        <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
    </ol>
    <hr />
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        	<!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit CMS Page</h3>
                </div><!-- /.box-header -->
                <?php 
                if(isset($_POST['edit-cmspage']))
                {
                    $slug = $fn2->slugifyy($_POST['title']);
                    
                    if($_POST['footer_column'] != $_POST['ori_footer_column'])
                    {
                        $order_exists = $fn2->checkOrder($_POST['footer_column']);
                        $next_order =  count($order_exists) + 1;

                    $sql2 = "UPDATE pages SET title = '".$_POST['title']."',slug = '".$slug."',content = '".$_POST['content']."', footer_column = '".$_POST['footer_column']."',status = '".$_POST['status']."',row_order = '".$next_order."' where page_id = '".$_POST['page_id']."'";        
                    if ($db->sql($sql2)) {
                        ?>
                <p class="alert alert-success" id="successalert">CMS Page Updated successfully!</p>
                <script>window.location.href='cmspage-list.php';</script>
                        <?php
                    }
                    else{
                        ?>
                <p class="alert alert-danger">Something went Wrong! Please Try Again Later</p>
                        <?php
                    }    

                    }
                    else{


                    $sql2 = "UPDATE pages SET title = '".$_POST['title']."',slug = '".$slug."',content = '".$_POST['content']."', footer_column = '".$_POST['footer_column']."',status = '".$_POST['status']."' where page_id = '".$_POST['page_id']."'";        
                    if ($db->sql($sql2)) {
                        ?>
                <p class="alert alert-success" id="successalert">CMS Page Updated successfully!</p>
                <script>window.location.href='cmspage-list.php';</script>
                        <?php
                    }
                    else{
                        ?>
                <p class="alert alert-danger">Something went Wrong! Please Try Again Later</p>
                        <?php
                    }    


                    }
                }


                ?>
                <form action="" method="post" id="cms_form">
                    <input type="hidden" name="edit-cmspage" value="1">
                    <input type="hidden" name="page_id" value="<?php echo $res_query[0]['page_id']; ?>">
                <div class="box-body">
                	<div class="row">
                		<div class="col-md-6">
                			<div class="form-group">
                				<label for="title">Title</label>
                				<input type="text" class="form-control" name="title" value="<?php echo $res_query[0]['title']; ?>">
                			</div>
                		</div>
                	</div>
                	<div class="row">
                		<div class="col-md-12">
                			<label for="content">Page Content</label>
                			<textarea name="content" id="content" cols="5" rows="5" class="form-control"><?php echo $res_query[0]['content']; ?></textarea>
                		</div>
                	</div>
                    <br>
                	<div class="row">
                		<div class="col-md-3">
                			<label for="footer_column">Footer Column</label>
                            <select name="footer_column" class="form-control">
                                <?php for($i=1;$i<=4;$i++){ ?>
                                <option value="<?php echo $i ?>" <?php echo ($res_query[0]['footer_column'] == $i) ? 'selected' : ''; ?>><?php echo $i ?></option>
                            <?php } ?>
                            </select>
                            <input type="hidden" name="ori_footer_column" value="<?php echo $res_query[0]['footer_column']; ?>">
                		</div>
                        <div class="col-md-3">
                            <label for="footer_column">Status</label>
                            <select name="status" class="form-control">
                                <option value="1" <?php echo ($res_query[0]['status'] == 1) ? 'selected' : ''; ?>>Visible</option>
                                <option value="0" <?php echo ($res_query[0]['status'] == 0) ? 'selected' : ''; ?>>Hidden</option>
                            </select>
                        </div>
                	</div>
                	<br>
                	<button class="btn btn-primary" type="submit">Submit</button>
			</div>
        </form>                
            </div>
        </div>
    </div>
</section>    
<script src="css/js/ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace('content');
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
        <script>
        $("#cms_form").validate({
            rules:{
                title : 'required',
                content : 'required',
                footer_column:'required',
                status:'required'
            },
            messages :{
                title : "Title is required",
                content : "Content is required",
                footer_column : "Footer Column is required",
                status : "Status is required",
            },
            submitHandler : function(form){
                form.submit();
            }
        });
            
        </script>
<?php
include_once('includes/functions.php');
include_once('includes/functions2.php');
include_once('includes/custom-functions.php');

$fn = new custom_functions;
$functions2=new functions2;
?>
<section class="content-header">
    <h1>FAQ /<small><a href="home.php"><i class="fa fa-home"></i> Home</a></small></h1>
</section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-xs-12">
                <div class="box container">
                    <div class="row">
                        	<div class="col-md-12 container">
                    <div style="padding:5px 0px">
                        <h3 class="box-title">FAQ List</h3>
                    
                        <br>
                        <?php if(isset($_POST['query'])){

        if (ALLOW_MODIFICATION == 0 && !defined(ALLOW_MODIFICATION)) {
            echo '<label class="alert alert-danger">This operation is not allowed in demo panel!.</label>';
            return false;
        }
        if ($permissions['faqs']['create'] == 1) {
            if (empty($_POST['query'])) {
                $error1['query'] = " <span class='label label-danger'>Query is required!</span>";
            }
            if (empty($_POST['faq_category_id'])) {
                $error1['faq_category_id'] = " <span class='label label-danger'>FAQ Category is required!</span>";
            }
            if (!empty($_POST['query']) && empty($error1)) {
                $query = $db->escapeString($fn->xss_clean($_POST['query']));
                $answer = $db->escapeString($fn->xss_clean($_POST['answer']));
                $faq_category_id = $db->escapeString($fn->xss_clean($_POST['faq_category_id']));
                $error = array();
                // create random image file name
                $function = new functions;
                if($answer != "" && $faq_category_id != ""){
                    $sql_query = "INSERT INTO faq (`question`, `answer`,`status`,`faq_category_id`)VALUES('$query', '$answer',1,'$faq_category_id')";
                    
                }else{
                    $sql_query = "INSERT INTO faq (`question`, `answer`,`status`,`faq_category_id`)VALUES('$query', '$answer',2,'$faq_category_id')";
                }
                // insert new data to menu table
                $db->sql($sql_query);
                $result = $db->getResult();
                if (!empty($result)) {
                    $result = 0;
                } else {
                    $result = 1;
                }
                if ($result == 1) {
                    ?>
                        <p class="alert alert-success" id="status">FAQ Added Successfully!</p>
                    <?php
                    header("Refresh:0");
                } else {
                    ?>
                        <p class="alert alert-danger" id="status">Failed to Add FAQ!</p>
                    <?php
                }
            }
        } else {
            ?>
                        <p class="alert alert-danger" id="status">You have no permission to add FAQ</p>
            <?php
        }

                         ?>

                    <?php } ?>
                        <?php 
                        $faq_categories = $functions2->getFAQCategories2();
                        foreach($faq_categories as $fc){
                        ?>
      <div class="box box-default collapsed-box" style="margin-bottom: 0px">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo $fc['name'] ?></h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
          </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            <?php 
                $faq = $functions2->getFAQByCategory($fc['faq_category_id']);
                if(count($faq)>0){
                foreach($faq as $f){
                ?>
                <div class="row" style="border-bottom: 1px solid #ccc;">
                <div class="col-lg-10">
                <h4 style="color:blue;"><?php echo $f['question']; ?></h4>
                <p style="padding-bottom: 8px"><i class="fa fa-arrow-right" aria-hidden="true"></i> <?php echo $f['answer']; ?></p>
                </div>
                <div class="col-lg-2" style="display: flex;margin-top: 10px">
                    <a href="edit-query.php?id=<?php echo $f['id']; ?>" class="btn btn-primary btn-sm">Answer / Edit</a>
                    &nbsp;
                    <a href="delete-query.php?id=<?php echo $f['id']; ?>" class="btn btn-danger btn-sm">Delete</a>
                </div>
                </div>
                <?php
                }
                }
                else{
                    echo "No FAQ found";
                }
            ?>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
<?php } ?>


                    </div>

                        	</div>                        
                    </div>
                </div>
                <div class="box container">
                    <h3 class="box-title">Add FAQ</h3>
                    <div class="box-body">
                    
                        <form action="" method="post" id="add_form">
                        <div class="form-group">
                            <label>Select FAQ Category</label>
                            <select name="faq_category_id" class="form-control">
                                <?php foreach($faq_categories as $fc){ ?>
                                    <option value="<?php echo $fc['faq_category_id']; ?>"><?php echo $fc['name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>                        
                        <div class="form-group">
                            <label>Question</label>
                            <input type="text" name="query" class="form-control" id="query">
                        </div>
                        <div class="form-group">
                            <label>Answer</label>
                            <textarea rows="5" cols="5" name="answer" class="form-control"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </section>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
        <script>
        $("#add_form").validate({
            rules:{
                query : 'required',
                faq_category_id : 'required',
            },
            messages :{
                query : "Question is required",
                faq_category_id : "FAQ Category is required",
            },
            submitHandler : function(form){
                form.submit();
            }
        });
            
        </script>
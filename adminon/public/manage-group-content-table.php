<?php
include_once('includes/functions.php');
include_once('includes/functions2.php');
$function = new functions();
$fn2 = new functions2();

if(isset($_POST['edit_ad_id']))
{

    $upload_image = $_POST['ori_ad_image'];
    $ori_ad_image = $_POST['ori_ad_image'];
    // $ad_link = $_POST['edit_ad_link'];
    $status = $_POST['edit_status'];
    $category_id = (int)$_POST['edit_category'];
    // $cat_data = $fn2->getSlugById($category_id,1);
    // foreach ($cat_data as $c) {
    //     $category_slug = $c['slug'];        
    // }

    $subcategory_id=0;
    // $subcategory_slug = '';
    if(isset($_POST['show_scat']))
    {
    $subcategory_id = (int)$_POST['edit_subcategory'];
    //     $scat_data = $fn2->getSlugById($_POST['edit_subcategory'],0);
    //     foreach($scat_data as $sd)
    //     {
    //         $subcategory_slug = $sd['slug'];
    //     }
    }
    $image_caption = "";
    if(isset($_POST['edit_image_caption'])){ $image_caption = $_POST['edit_image_caption']; }  

    // $sql = "update ads ";
    if(!empty($_FILES["edit_ad_image"]["name"]))
    {
        // Upload new image
        $extension = end(explode(".", $_FILES["edit_ad_image"]["name"]));
            // create random image file name
            $string = '0123456789';
            $file = preg_replace("/\s+/", "_", $_FILES['edit_ad_image']['name']);

            $image = $function->get_random_string($string, 4) . "-" . date("Y-m-d") . "." . $extension;

            // upload new image
            $upload = move_uploaded_file($_FILES['edit_ad_image']['tmp_name'], 'upload/advertisements/' . $image);
            $upload_image = 'upload/advertisements/' . $image;        
        // Upload new image end
        // Delete existing image
                unlink($ori_ad_image);
        // Delete end
    }
    // echo gettype($category_id) . " " . $image_caption . " " . $subcategory_id . " " . $_POST['edit_ad_id'];
    // exit();
    $sql = "update ads set ad_image = '".$upload_image."', image_caption = '$image_caption', is_visible = '".$status."',category_id='$category_id',subcategory_id='$subcategory_id' where ad_id='".$_POST['edit_ad_id']."'";
    $sql_edit = $fn2->editGroup($sql);
    if($sql_edit == 1)
    {
        header("Refresh:1");
    }    
}

?>
<section class="content-header">
    <h1>Group Content /<small><a href="home.php"><i class="fa fa-home"></i> Home</a></small></h1>
</section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-xs-12">
                <div class="box">
                    <?php 
                        $group_content = $fn2->getGroupContent($ad_group_id,$ad_group_type); 
                    ?>
                    <div class="box-body table-responsive">
                        <?php 
                        if(!empty($group_content)){
                        if($ad_group_type == 1){
                            //Banner table
                            ?>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                <th>Image</th>
                                <th>Category Name</th>
                                <th>Sub-Category Name</th>
                                <th>Status</th>
                                <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($group_content as $gc){
                                	$category_name=''; 
                                    $category_data = $fn2->getNameById($gc['category_id'],1);
                                    foreach ($category_data as $cat) {
                                        $category_name = $cat['name'];
                                    }
                                    $subcategory_name = '-';
                                    if(!empty($gc['subcategory_id']))
                                    {
                                        $subcategory_data = $fn2->getNameById($gc['subcategory_id'],0);
                                        foreach ($subcategory_data as $scat) {
                                            $subcategory_name = $scat['name'];
                                        }
                                    }
                                	?>
                                <tr>
                                    <td>
                                        <a data-lightbox="ad" href="<?php echo $gc['ad_image']; ?>" data-caption="<?php echo $gc['link']; ?>">
                                        <img src="<?php echo $gc['ad_image']; ?>" style="width:300px">
                                        </a>
                                    </td>
                                    <td><?php echo $category_name; ?></td>
                                    <td><?php echo $subcategory_name; ?></td>
                                    <td><?php echo ($gc['is_visible'] == 1) ? 'Visible' : 'Hidden'; ?></td>
                                    <td>
                                        <button class="btn btn-primary" onclick="showEditModal('<?php echo $gc['ad_id']; ?>')">Edit</button>
                                        <button class="btn btn-danger" onclick="confirmDelete('<?php echo $gc['ad_id']; ?>')">Delete</button>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                            <?php
                        }
                        else{
                            //Offer table
                            ?>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                <th>Image</th>
                                <th>Image Text</th>
                                <th>Category</th>
                                <th>Subcategory</th>	
                                <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                // echo "<pre>";
                                // print_r ($group_content);
                                // echo "</pre>";
                                foreach($group_content as $gc){ 
                                    $category_data = $fn2->getNameById($gc['category_id'],1);
                                    foreach ($category_data as $cat) {
                                        $category_name = $cat['name'];
                                    }
                                    $subcategory_name = '-';
                                    if(!empty($gc['subcategory_id']))
                                    {
                                        $subcategory_data = $fn2->getNameById($gc['subcategory_id'],0);
                                        foreach ($subcategory_data as $scat) {
                                            $subcategory_name = $scat['name'];
                                        }
                                    }
                                    ?>
                                <tr>
                                    <td>
                                        <a data-lightbox="ad" href="<?php echo $gc['ad_image']; ?>" data-caption="<?php echo $gc['image_caption']; ?>">
                                            <img src="<?php echo $gc['ad_image']; ?>" style="width:300px">
                                        </a>
                                    </td>
                                    <td><?php echo $gc['image_caption']; ?></td>
                                    <td><?php echo $category_name; ?></td>
                                    <td><?php echo $subcategory_name; ?></td>
                                    <td>
                                        <button class="btn btn-primary" onclick="showEditModal('<?php echo $gc['ad_id']; ?>')">Edit</button>
                                        <button class="btn btn-danger" onclick="confirmDelete('<?php echo $gc['ad_id']; ?>')">Delete</button>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                            <?php
                        } 
                        }
                        else{
                            //Empty group content
                        ?>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                <th>Image</th>
                                <th>Link</th>
                                <th>Status</th>
                                <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="4" style="text-center">Sorry No Content Available! Please Add Some and Try Again!</td>
                                </tr>
                            </tbody>
                        </table>    
                        <?php                            
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
<div id="contentEditModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Ad</h4>
      </div>
        <form action="" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
            <input type="hidden" name="edit_ad_id" id="edit_ad_id">
            <div class="form-group">
                <label for="edit_ad_image">Change Image (Maximum size: 100 Kb)</label>
                <input type="file" class="form-control" id="edit_ad_image" name="edit_ad_image">
                <input type="hidden" name="ori_ad_image" id="ori_ad_image">
                <p style="color:red" id="err_edit_ad_image"></p>
            </div>
<!--             <div class="form-group">
                <label for="edit_ad_link">Edit Ad Link</label>
                <input type="text" class="form-control" id="edit_ad_link" name="edit_ad_link">
            </div>
 -->            <div id="spl_offer_div" style="display: none">
            <div class="form-group">
                <label for="edit_image_caption">Edit Image Caption</label>
                <input type="text" id="edit_image_caption" class="form-control" value="">
            </div>
            </div>
            <div class="form-group">
                <label for="edit_category">Category</label>
                <select name="edit_category" id="edit_category" class="form-control" onchange="showSubcats(this.value)">
                <?php 
                $cats = $fn2->getAllCategories();
                foreach ($cats as $c) {
                    ?>
                    <option value="<?php echo $c['id']; ?>"><?php echo $c['name']; ?></option>
                    <?php
                }
                ?>
                </select>
                <br>
            <input type="checkbox" name="show_scat" id="show_scat" onclick="toggleSubcats(this)"> Show Sub-category
            </div>
            <div class="form-group" id="edit_subcategory_div">
                <label for="edit_subcategory">Sub-Category</label>
                <select name="edit_subcategory" id="edit_subcategory" class="form-control">
                    
                </select>
            </div>
            <div class="form-group">
                <label for="edit_status">Edit Status</label>
                <select name="edit_status" id="edit_status" class="form-control">
                    <option value="1">Visible</option>
                    <option value="0">Hidden</option>
                </select>
            </div>            
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success" id="btnUpdate">Update</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
        </form>
    </div>

  </div>
</div>

    </section>

    <script>
        function showEditModal(ad_id) {
        $.ajax({
            type: 'POST',
            url: "public/db-operation2.php",
            data: { ad_edit_id : ad_id },
            success: function(result) {
                var jObj = JSON.parse(result);
                jObj.group_info.forEach(editEach);

                function editEach(item,index)
                {
                    $("#edit_ad_id").val(item.ad_id);
                    // $("#edit_ad_link").val(item.ad_link);
                    $("#ori_ad_image").val(item.ad_image);
                    $("#edit_status").val(item.avisible);
                    if(item.subcategory_id!=0)
                    {
                        $("#show_scat").attr('checked',true);
                        $("#edit_subcategory_div").css('display','block');
                    }
                    else{
                        $("#show_scat").attr('checked',false);
                        $("#edit_subcategory_div").css('display','none');
                    }

                    if(item.ad_group_type == 2)
                    {
                        $("#spl_offer_div").show();
                        $("#edit_image_caption").val(item.image_caption);
                        $("#edit_image_caption").attr('name','edit_image_caption');      
                    }
                }
                if(jObj.cat_data.length > 0)
                {
                    jObj.cat_data.forEach( function(element, index) {
                        $("#edit_category").val(element.id);
                    });
                }
                if(jObj.cat_subcats.length > 0)
                {
                    var html= '';
                    jObj.cat_subcats.forEach( function(element, index) {
                        html += `<option value='${element.id}'>${element.name}</option>`;
                    });
                    $("#edit_subcategory").html(html);
                }
                // jObj.cat_data.forEach()
            }
        });                        
            $("#contentEditModal").modal('show');
        }

        function confirmDelete(ad_id) {
            var c = confirm('Do you want to delete this ad? All the Related Data will be deleted');
            if(c)
            {
                $.ajax({
                    type: 'POST',
                    url: "public/db-operation2.php",
                    data: { ad_delete_id : ad_id },
                    success: function(result) {
                        if(result.includes("deleted"))
                        {
                            location.reload();
                        }
                    }
                });                            
            }
        }
    function toggleSubcats(cb)
    {
        if(cb.checked)
        {
            $(`#edit_subcategory_div`).css('display','block');
        }
        else{
            $(`#edit_subcategory_div`).css('display','none');            
        }
    }        
    function showSubcats(cat_id)
    {
            $.ajax({
                url : 'get-subcategorybyid.php',
                type: "post",
                dataType : 'json',
                data: 'accesskey=90336&category_id='+cat_id,
                success: function(result){
                    var html = '';
                    result.forEach(function(element){
                        html += `<option value='${element.id}'>${element.name}</option>`;
                    });
                    $("#edit_subcategory").html(html);
                }
            });        
    }

            //binds to onchange event of your input field
$('#edit_ad_image').bind('change', function() {

  //this.files[0].size gets the size of your file.
  if(this.files[0].size > 100000)
  {
    $("#err_edit_ad_image").html("Please select a file less than 100kb size");
    $("#btnUpdate").attr('disabled',true);
 
  }
  else{
    $("#err_edit_ad_image").html("");
    $("#btnUpdate").attr('disabled',false);
  }

});

    </script>
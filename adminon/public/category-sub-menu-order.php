<?php
include_once('includes/functions.php');
include_once('includes/functions2.php');
?>

<style>
	.deptcats{
		list-style-type: none;
		box-shadow: 0px 1px 1px 1px #ccc;
		padding: 9px;
		margin-bottom: 10px;
		cursor: move;
	}	
</style>
<section class="content-header">
    <h1>Category Sub Menu Order /<small><a href="home.php"><i class="fa fa-home"></i> Home</a></small></h1>
</section>
    <!-- Main content -->
    <section class="content">
<?php 
    $fn2 = new functions2();
    $depts = $fn2->getDepartments();
    foreach ($depts as $c) {
        ?>
<div class="box box-default collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo $c['name'] ?></h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
          </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
        	<ul>
        	<?php 
				$cats = $fn2->getCategoriesByDepartment($c['id']);
				if(count($cats) > 0)
				{
					$i=0;
					foreach($cats as $ct){
					?>
					<li class="deptcats ui-state-default" id="<?php echo $ct['id'] ?>"><?php echo $i . ".  " . $ct['name']; ?></li>
					<?php
					$i++;
					}
				}
        	?>        		
        	</ul>
        	<center>
        		<form action="public/db-operation2.php" class="orderForm">
        		<input type="hidden" name="order_categories" value="1">
        		<input type="hidden" name="cat_order" class="cat_order">
        		<button class="btn btn-success">Save Order</button>        			
        		</form>
        	</center>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
        <?php        
    }
?>

    </section>
<script>

    function deletePage(page_id) {
        var c = confirm('Do you want to delete this Page? All the Related Data will be deleted');
        if(c)
        {
        $.ajax({
            type: 'POST',
            url: "public/db-operation2.php",
            data: { cms_delete_id : page_id },
            success: function(result) {
                if(result.includes("deleted"))
                {
                    location.reload();
                }
            }
        });            
        }
    }


  $( function() {
    $( "ul" ).sortable({
        placeholder: "ui-state-highlight",
        update : function(event,ui){
               var cmsOrder = $(this).sortable('toArray').toString();  
               $(".cat_order").val(cmsOrder);
        }        
    });
    $( "tbody" ).disableSelection();
  } );

        $('.orderForm').on('submit',function(e){
        e.preventDefault();
        var formData = new FormData(this);
            $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:formData,
            // dataType:'json',
            // beforeSend:function(){$('#submit_btn').val('Please wait..').attr('disabled',true);},
            cache:false,
            contentType: false,
            processData: false,
            success:function(result){
                if(result==1)
                {
                    alert("Order Changed successfully");
                    location.reload();
                }
                else{
                    alert("Error in changing order");
                }
            }
            });
        
    }); 


</script>
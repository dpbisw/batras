<?php
session_start();
include('../includes/crud.php');
$db = new Database();
$db->connect();
$db->sql("SET NAMES 'utf8'");
$auth_username = $db->escapeString($_SESSION["user"]);
include_once('../includes/custom-functions.php');
$fn = new custom_functions;
include_once('../includes/functions.php');
include_once('../includes/functions2.php');
$function = new functions;
$fn2 = new functions2;
$permissions = $fn->get_permissions($_SESSION['id']);
$config = $fn->get_configurations();
$time_slot_config = $fn->time_slot_config();
if(isset($_POST['add_group']))
{
	$is_third_party = 0;
	if(!empty($_POST['is_third_party'])) $is_third_party = 1;
	$g_order = $_POST['order_no'];
    $sql = "select group_name from ad_groups where g_order='$g_order' ";
    $db->sql($sql);
    $res = $db->getResult();
	if(empty($res))
	{
            if($_POST['ad_group_type']==2){
            $extension = end(explode(".", $_FILES["icon_image"]["name"]));
            // create random image file name
            $string = '0123456789';
            $file = preg_replace("/\s+/", "_", $_FILES['icon_image']['name']);
            $icon_image = $function->get_random_string($string, 4) . "-" . date("Y-m-d") . "." . $extension;
            // upload new image
            $upload = move_uploaded_file($_FILES['icon_image']['tmp_name'], 'upload/images/' . $icon_image);
            // insert new data to menu table
            $upload_image = 'upload/images/' . $icon_image;
            $extension2 = end(explode(".", $_FILES["back_image"]["name"]));
            // create random image file name
            $string = '0123456789';
            $file = preg_replace("/\s+/", "_", $_FILES['back_image']['name']);
            $back_image = $function->get_random_string($string, 4) . "-" . date("Y-m-d") . "." . $extension2;
            // upload new image
            $upload = move_uploaded_file($_FILES['back_image']['tmp_name'], 'upload/images/' . $back_image);
            // insert new data to menu table
            $upload_image2 = 'upload/images/' . $back_image;
            $back_color=$_POST['back_color'];
        }
        if($_POST['ad_group_type']==1){
            $upload_image='';
            $upload_image2='';
            $back_color='';
        }
        $sql = "INSERT INTO ad_groups (`group_name`,`g_order`,`ad_group_type`,`image_per_row`,`is_third_party`,`is_visible`,`back_image`,`icon_image`,`back_color`) VALUES ('".$_POST['group_name']."','".$_POST['g_order']."','".$_POST['ad_group_type']."','".$_POST['image_per_row']."','".$is_third_party."','".$_POST['status']."','".$upload_image2."','".$upload_image."','".$_POST['back_color']."')";		
		if ($db->sql($sql)) {
			echo 1;
		} else {
			echo 0;
		}		
	}
	else
	{
        echo 2;		
	}
// //	echo "hi";
}
// Delete group
if(isset($_POST['group_delete_id']))
{
	//Deleting image ad
	$sql0 = "select * from `ads` where ad_group_id = ".$_POST['group_delete_id'];
    $db->sql($sql0);
	$res = $db->getResult();
	foreach($res as $r)
	{
		if(file_exists('../'.$r['ad_image']))
		{
			unlink('../'.$r['ad_image']);
		}
	}
	//Deleteing image ad end
	//Deleting group
    $sql = "DELETE FROM `ad_groups` WHERE ad_group_id=" . $_POST['group_delete_id'];
    if ($db->sql($sql)) {
    	//Deleting from ads
    	$ad_sql = "DELETE from `ads` where ad_group_id=" . $_POST['group_delete_id'];
    	if($db->sql($ad_sql))
        {
            echo 1;
        }
        else{
            echo 0;
        }
    }	
}
// Delete ad
if(isset($_POST['ad_delete_id']))
{
	//Deleting image ad
	$sql0 = "select * from `ads` where ad_id = ".$_POST['ad_delete_id'];
    $db->sql($sql0);
	$res = $db->getResult();
	foreach($res as $r)
	{
		if(file_exists('../'.$r['ad_image']))
		{
			unlink('../'.$r['ad_image']);
		}
	}
	//Deleteing image ad end
    // 	//Deleting from ads
    	$ad_sql = "DELETE from `ads` where ad_id=" . $_POST['ad_delete_id'];
    	$db->sql($ad_sql);
    	echo "deleted";
}	
if(isset($_POST['group_edit_id']))
{
	$group_edit_id = $_POST['group_edit_id'];
    $sql = "select * from ad_groups where ad_group_id='$group_edit_id' ";
    $db->sql($sql);
    $res = $db->getResult();
    echo json_encode(['group_info'=>$res]);
}
if(isset($_POST['ad_edit_id']))
{
	$ad_edit_id = $_POST['ad_edit_id'];
    $sql = "select a.*,ag.*,a.is_visible as avisible from ads as `a` inner join ad_groups as `ag` on a.ad_group_id = ag.ad_group_id where a.ad_id='$ad_edit_id' ";
    $db->sql($sql);
    $res = $db->getResult();
    $cat_data = [];
    $scat_data = [];
    $cat_subcats = [];
    foreach($res as $r)
    {
	    // //category and sub-category information
    	// $cat_slug = $r['category_id'];
    	// $scat_slug = $r['subcategory_id'];
    	// $csql = "select id from category where slug = '$cat_slug'";
    	// $db->sql($csql);
    	// $cat_data = $db->getResult();
    	// if(!empty($scat_slug))
    	// {
	    // 	$scsql = "select id from subcategory where id = '$scat_slug'";
	    // 	$db->sql($scsql);
	    // 	$scat_data = $db->getResult();    		
    	// }
    	// Getting subcategories for selection
    	// foreach ($cat_data as $c) {
    		$id = $r['category_id'];
    		$subsql = "select id,name from subcategory where category_id='$id'";
    		$db->sql($subsql);
    		$cat_subcats = $db->getResult();
    	// }
    }
    echo json_encode(['group_info'=>$res,'cat_data'=>$cat_data,'scat_data'=>$scat_data,'cat_subcats'=>$cat_subcats]);
}
if(isset($_POST['request_delete_id']))
{
    $id = $_POST['request_delete_id'];
    //  //Deleting from ads
        $ad_sql = "DELETE from `product_requests` where product_request_id=" . $_POST['request_delete_id'];
        $db->sql($ad_sql);
        echo "deleted";
}
if(isset($_POST['user_request_id']))
{
    $user_request_id = $_POST['user_request_id'];
    $sql= "SELECT * FROM `user_request_list` inner join users on user_request_list.user_id = users.id  where user_request_list_id=" . $user_request_id;
    $db->sql($sql);
    $sql_data = $db->getResult(); 
    echo json_encode(['sql_data'=>$sql_data]);
}
if(isset($_POST['user_request_delete_id']))
{
    $id = $_POST['user_request_delete_id'];
    //  //Deleting from ads
        $usql = "select list_file from user_request_list where user_request_list=" . $_POST['user_request_delete_id'];
        $db->sql($usql);
        $sql_data = $db->getResult();
        foreach($sql_data as $sd)
        {
            if($sd['list_file'] != NULL || $sd['list_file'] != '')
            {
                unlink("../".$sd['list_file']);
            }
        } 
        $ad_sql = "DELETE from `user_request_list` where user_request_list_id=" . $_POST['user_request_delete_id'];
        $db->sql($ad_sql);
        echo 1;
}
if(isset($_POST['cms_delete_id']))
{
    $id = $_POST['cms_delete_id'];
    //  //Deleting from ads
        $ad_sql = "DELETE from `pages` where page_id=" . $_POST['cms_delete_id'];
        $db->sql($ad_sql);
        echo "deleted";
}
if(isset($_POST['edit_group']))
{
    $is_third_party = 0;
    if(!empty($_POST['edit_is_third_party'])) $is_third_party = 1;
    $upload_image = $_POST['ori_back_image'];
            $extension = end(explode(".", $_FILES["edit_backgound_image"]["name"]));
            // create random image file name
            $string = '0123456789';
            $file = preg_replace("/\s+/", "_", $_FILES['edit_backgound_image']['name']);
            $back_image = $function->get_random_string($string, 4) . "-" . date("Y-m-d") . "." . $extension;
            // upload new image
            $upload = move_uploaded_file($_FILES['edit_backgound_image']['tmp_name'], '../upload/images/' . $back_image);
    if($upload){
            // insert new data to menu table
            $upload_image = 'upload/images/' . $back_image;
        }
        else{
    $upload_image = $_POST['ori_back_image'];
        }
    $sql = "Update ad_groups set `group_name`='" . $_POST['edit_group_name'] . "', g_order='".$_POST['edit_group_order']."', image_per_row='".$_POST['edit_image_per_row']."', is_third_party='".$_POST['edit_is_third_party']."', is_visible = '".$_POST['edit_status']."', back_color = '".$_POST['edit_backgound_color']."',back_image='".$upload_image."'  where `ad_group_id`=" . $_POST['edit_group_id'];
    $fn2 = new functions2();
    $sql_edit = $fn2->editGroup($sql);
    echo $sql_edit;
}
if(isset($_POST['promo_code_data_id']))
{
    $sql = "select * from promo_codes where id = '".$_POST['promo_code_data_id']."'";
    $db->sql($sql);
    $res = $db->getResult();
    echo json_encode($res);    
}
if(isset($_POST['edit_promo_id']))
{
    $is_home = 0;
    if(isset($_POST['edit_is_home'])){ $is_home = 1; }
    if($_FILES['edit_home_image']['name']!=""){ 
        unlink($_POST['ori_image']);        
        // Upload new image
        $extension = end(explode(".", $_FILES["edit_home_image"]["name"]));
        // create random image file name
        $string = '0123456789';
        $file = preg_replace("/\s+/", "_", $_FILES['edit_home_image']['name']);
        $image = $function->get_random_string($string, 4) . "-" . date("Y-m-d") . "." . $extension;
        // upload new image
        $upload = move_uploaded_file($_FILES['edit_home_image']['tmp_name'], '../upload/offers/' . $image);
        $upload_image = 'upload/offers/' . $image;        
    }
    if($_FILES['edit_home_image']['name']=="")
    {
        $upload_image = $_POST['ori_image'];
    }
    $sql = "Update promo_codes set `is_home`='" . $is_home . "', `home_image`='". $upload_image ."' where `id`=" . $_POST['edit_promo_id'];
    $fn2 = new functions2();
    $sql_edit = $fn2->editGroup($sql);
    echo $sql_edit;
}
if(isset($_POST['add_faq_category']))
{
    $faq_category_name = $_POST['faq_category_name'];
    $is_visible = $_POST['is_visible'];
    $sql = "select name from faq_categories where name='$faq_category_name' ";
    $db->sql($sql);
    $res = $db->getResult();
    if(empty($res))
    {
        $slug = $fn2->slugifyy($faq_category_name);
        $sql = "INSERT INTO faq_categories (`name`,`slug`,`is_visible`) VALUES ('$faq_category_name','$slug','$is_visible')";     
        if ($db->sql($sql)) {
            echo 1;
        } else {
            echo 0;
        }       
    }
    else
    {
        echo 2;     
    }    
}
if(isset($_POST['faq_cat_edit_id']))
{
    $faq_category_id = $_POST['faq_cat_edit_id'];
    $sql = "select * from faq_categories where faq_category_id='$faq_category_id' ";
    $db->sql($sql);
    $res = $db->getResult();
    echo json_encode($res);
}
if(isset($_POST['edit_faq_category']))
{
    $faq_category_name = $_POST['edit_category_name'];
    $is_visible = $_POST['edit_is_visible'];
    $sql = "select name from faq_categories where name='$faq_category_name' and is_visible='$is_visible'";
    $db->sql($sql);
    $res = $db->getResult();
    if(empty($res))
    {
        $slug = $fn2->slugifyy($faq_category_name);
        $sql = "UPDATE faq_categories set `name`='".$faq_category_name."',`slug`='".$slug."',`is_visible`='".$_POST['edit_is_visible']."' where `faq_category_id`=".$_POST['edit_faq_category_id'];     
        if ($db->sql($sql)) {
            echo 1;
        } else {
            echo 0;
        }               
    }
    else{
        echo 2;
    }
}
// Brand operations---------------------------------
if(isset($_POST['add_brand']))
{
    $brand_name = $_POST['brand_name'];
    $is_visible = $_POST['is_visible'];
    $sql = "select brand_name from brands where brand_name='$brand_name' ";
    $db->sql($sql);
    $res = $db->getResult();
    if(empty($res))
    {
        $slug = $fn2->slugifyy($brand_name);
        $sql = "INSERT INTO brands (`brand_name`,`brand_slug`,`is_visible`) VALUES ('$brand_name','$slug','$is_visible')";     
        if ($db->sql($sql)) {
            echo 1;
        } else {
            echo 0;
        }       
    }
    else
    {
        echo 2;     
    }    
}
if(isset($_POST['brand_edit_id'])) //Populate data into modal
{
    $brand_edit_id = $_POST['brand_edit_id'];
    $sql = "select * from brands where brand_id='$brand_edit_id' ";
    $db->sql($sql);
    $res = $db->getResult();
    echo json_encode($res);
}
if(isset($_POST['update_brand'])) //Update Brand
{
    $brand_name = $_POST['edit_brand_name'];
    $is_visible = $_POST['edit_is_visible'];
    $sql = "select brand_name from brands where brand_name='$brand_name' and is_visible='$is_visible'";
    $db->sql($sql);
    $res = $db->getResult();
    if(empty($res))
    {
        $slug = $fn2->slugifyy($brand_name);
        $sql = "UPDATE brands set `brand_name`='".$brand_name."',`brand_slug`='".$slug."',`is_visible`='".$_POST['edit_is_visible']."' where `brand_id`=".$_POST['edit_brand_id'];     
        if ($db->sql($sql)) {
            echo 1;
        } else {
            echo 0;
        }               
    }
    else{
        echo 2;
    }
}
if(isset($_POST['brand_delete_id']))
{
    $id = $_POST['brand_delete_id'];
    //  //Deleting from brands
        $ad_sql = "DELETE from `brands` where brand_id=" . $_POST['brand_delete_id'];
        if($db->sql($ad_sql)) echo 1;
        else echo 0;
}
// Brand Operations end ----------------------------------------------
//Attribute operations start
if(isset($_POST['add_attribute']))
{
    $attribute_name = $_POST['attribute_name'];
    $is_visible = $_POST['is_visible'];
    $sql = "select attribute_name from attributes where attribute_name='$attribute_name' ";
    $db->sql($sql);
    $res = $db->getResult();
    if(empty($res))
    {
        $sql = "INSERT INTO attributes (`attribute_name`,`is_visible`) VALUES ('$attribute_name','$is_visible')";     
        if ($db->sql($sql)) {
            echo 1;
        } else {
            echo 0;
        }       
    }
    else
    {
        echo 2;     
    }    
}
if(isset($_POST['attribute_edit_id'])) //Populate data into modal
{
    $attribute_edit_id = $_POST['attribute_edit_id'];
    $sql = "select * from attributes where attribute_id='$attribute_edit_id' ";
    $db->sql($sql);
    $res = $db->getResult();
    echo json_encode($res);
}
if(isset($_POST['update_attribute'])) //Update Attribute
{
    $attribute_name = $_POST['edit_attribute_name'];
    $is_visible = $_POST['edit_is_visible'];
    $sql = "select attribute_name from attributes where attribute_name='$attribute_name' and is_visible='$is_visible'";
    $db->sql($sql);
    $res = $db->getResult();
    if(empty($res))
    {
        $sql = "UPDATE attributes set `attribute_name`='".$attribute_name."',`is_visible`='".$_POST['edit_is_visible']."' where `attribute_id`=".$_POST['edit_attribute_id'];     
        if ($db->sql($sql)) {
            echo 1;
        } else {
            echo 0;
        }               
    }
    else{
        echo 2;
    }
}
if(isset($_POST['pincode_edit_id'])) //Populate data into modal
{
    $pincode_edit_id = $_POST['pincode_edit_id'];
    $sql = "select * from pincodes as p inner join area as a on p.area_id = a.id where p.pincode_id='$pincode_edit_id' ";
    $db->sql($sql);
    $res = $db->getResult();
    echo json_encode($res);
}
if(isset($_POST['update_pincode'])) //Update Attribute
{
    $pincode = $_POST['edit_pincode'];
    $is_visible = $_POST['edit_is_visible'];
    $sql = "select pincode from pincodes where pincode='$pincode' and is_visible='$is_visible'";
    $db->sql($sql);
    $res = $db->getResult();
    if(empty($res))
    {
        $sql = "UPDATE pincodes set `pincode`='".$pincode."',`is_visible`='".$_POST['edit_is_visible']."' where `pincode_id`=".$_POST['edit_pincode_id'];     
        if ($db->sql($sql)) {
            echo 1;
        } else {
            echo 0;
        }               
    }
    else{
        echo 2;
    }
}
if(isset($_POST['add_pin']))
{
	$area_id = $_POST['area_ID'];
    $pincode = $_POST['pincode'];
    $is_visible = $_POST['is_visible'];
    // $sql = "select pincode from pincodes where pincode='$pincode' ";
    // $db->sql($sql);
    // $res = $db->getResult();
    // if(empty($res))
    // {
        $sql = "INSERT INTO pincodes (`area_id`, `pincode`,`is_visible`) VALUES ('$area_id' ,'$pincode','$is_visible')";     
        if ($db->sql($sql)) {
            echo 1;
        } else {
            echo 0;
        }       
    // }
    // else
    // {
    //     echo 2;     
    // }    
}
if(isset($_POST['pincode_delete_id']))
{
    $id = $_POST['pincode_delete_id'];
    //  //Deleting from brands
        $ad_sql = "DELETE from `pincodes` where pincode_id=" . $_POST['pincode_delete_id'];
        if($db->sql($ad_sql)) echo 1;
        else echo 0;
}
if(isset($_POST['add_state']))
{
    $state_name = $_POST['state_name'];
    $is_visible = $_POST['is_visible'];
    $sql = "select state_name from states where state_name='$state_name' ";
    $db->sql($sql);
    $res = $db->getResult();
    if(empty($res))
    {
        $sql = "INSERT INTO states (`state_name`,`is_visible`) VALUES ('$state_name','$is_visible')";     
        if ($db->sql($sql)) {
            echo 1;
        } else {
            echo 0;
        }       
    }
    else
    {
        echo 2;     
    }    
}
if(isset($_POST['state_edit_id'])) //Populate data into modal
{
    $state_edit_id = $_POST['state_edit_id'];
    $sql = "select * from states where id='$state_edit_id' ";
    $db->sql($sql);
    $res = $db->getResult();
    echo json_encode($res);
}
if(isset($_POST['update_state'])) //Update Attribute
{
    $state_name = $_POST['edit_state_name'];
    $is_visible = $_POST['edit_is_visible'];
    $sql = "select state_name from states where state_name='$state_name' and is_visible='$is_visible'";
    $db->sql($sql);
    $res = $db->getResult();
    if(empty($res))
    {
        $sql = "UPDATE states set `state_name`='".$state_name."',`is_visible`='".$_POST['edit_is_visible']."' where `id`=".$_POST['edit_state_id'];     
        if ($db->sql($sql)) {
            echo 1;
        } else {
            echo 0;
        }               
    }
    else{
        echo 2;
    }
}
if(isset($_POST['state_delete_id']))
{
    $id = $_POST['state_delete_id'];
    //  //Deleting from brands
        $ad_sql = "DELETE from `states` where id=" . $_POST['state_delete_id'];
        if($db->sql($ad_sql)) echo 1;
        else echo 0;
}
if(isset($_POST['attribute_delete_id']))
{
    $id = $_POST['attribute_delete_id'];
    //  //Deleting from brands
        $ad_sql = "DELETE from `attributes` where attribute_id=" . $_POST['attribute_delete_id'];
        if($db->sql($ad_sql)) echo 1;
        else echo 0;
}
//Attribute operations end
//Attribute value start
if(isset($_POST['add_attribute_value']))
{
    $attribute_id = $_POST['attribute_id'];
    $attribute_value = $_POST['attribute_value'];
    $is_visible = $_POST['is_visible'];
    $sql = "select attribute_value from attribute_values where attribute_id='$attribute_id' and attribute_value='$attribute_value'";
    $db->sql($sql);
    $res = $db->getResult();
    if(empty($res))
    {
        $sql = "INSERT INTO attribute_values (`attribute_id`,`is_visible`,`attribute_value`) VALUES ('$attribute_id','$is_visible','$attribute_value')";     
        if ($db->sql($sql)) {
            echo 1;
        } else {
            echo 0;
        }       
    }
    else
    {
        echo 2;     
    }    
}
if(isset($_POST['attr_value_edit_id'])) //Populate data into modal
{
    $attr_value_edit_id = $_POST['attr_value_edit_id'];
    $sql = "select * from attribute_values as v inner join attributes as a on a.attribute_id=v.attribute_id  where v.attr_value_id='$attr_value_edit_id' ";
    $db->sql($sql);
    $res = $db->getResult();
    echo json_encode($res);
}
if(isset($_POST['update_attr_value'])) //Update Attribute
{
    $attribute_id = $_POST['edit_attribute_id'];
    $attribute_value = $_POST['edit_attribute_value'];    
    $is_visible = $_POST['edit_is_visible'];
    $sql = "select attribute_value from attribute_values where attribute_id='$attribute_name' and attribute_value='$attribute_value'";
    $db->sql($sql);
    $res = $db->getResult();
    if(empty($res))
    {
        $sql = "UPDATE attribute_values set `attribute_id`='".$attribute_id."',`is_visible`='".$_POST['edit_is_visible']."', `attribute_value`='".$attribute_value."' where `attr_value_id`=".$_POST['edit_attr_value_id'];     
        if ($db->sql($sql)) {
            echo 1;
        } else {
            echo 0;
        }               
    }
    else{
        echo 2;
    }
}
//Attribute value end
//Product page operation
if(isset($_POST['change_attribute']))
{
    $sql = "select * from attribute_values where attribute_id = '".$_POST['attribute_id']."'";
    $db->sql($sql);
    $res = $db->getResult();
    echo json_encode($res);        
}
if(isset($_POST['offer_order']))
{
    $offord = explode(',', $_POST['offer_order']);
    $stat = 0;
    for($i=0;$i<count($offord);$i++)
    {
        $row_order = $i+1;
        $sql = "UPDATE offers set `row_order`='".$row_order."' where `id`=".$offord[$i];     
        if ($db->sql($sql)) {
        $stat=1;
        }
    }
    echo json_encode(['status'=>$stat]);               
}
if(isset($_POST['order_pages']))
{
    $footer_column = $_POST['footer_column'];
    $pageOrder = $_POST['pageOrder'];
    if(!empty($pageOrder))
    {
        $stat=0;
        $order_arr = explode(',', $pageOrder);
        for($i=0;$i<count($order_arr);$i++)
        {
            $row_order = $i+1;
            $sql = "UPDATE pages set `row_order`='".$row_order."' where `page_id`=".$order_arr[$i];
            if ($db->sql($sql)) {
                $stat=1;
            }          
        }        
        echo $stat;
    }
}
if(isset($_POST['order_categories']))
{
    $cat_order = $_POST['cat_order'];
    if(!empty($cat_order))
    {
        $stat=0;
        $order_arr = explode(',', $cat_order);
        for($i=0;$i<count($order_arr);$i++)
        {
            $sub_menu_order = $i+1;
            $sql = "UPDATE category set `sub_menu_order`='".$sub_menu_order."' where `id`=".$order_arr[$i];
            if ($db->sql($sql)) {
                $stat=1;
            }          
            // echo $sub_menu_order . " " . $order_arr[$i] . "<br>";
        }
        echo $stat;
    }
}
if(isset($_POST['order_home_slider']))
{
    $ms_order = $_POST['ms_order'];
    if(!empty($ms_order))
    {
        $stat=0;
        $order_arr = explode(',', $ms_order);
        for($i=0;$i<count($order_arr);$i++)
        {
            $row_order = $i+1;
            $sql = "UPDATE slider set `row_order`='".$row_order."' where `id`=".$order_arr[$i];
            if ($db->sql($sql)) {
                $stat=1;
            }          
            // echo $sub_menu_order . " " . $order_arr[$i] . "<br>";
        }
        echo $stat;
    }
}
if(isset($_POST['get_attr_values']))
{
    $attrs = $_POST['attrs'];
    $sql = "select * from attribute_values as v inner join attributes as t on v.attribute_id=t.attribute_id where v.attribute_id in (".$_POST['attrs'].")";
    $db->sql($sql);
    $res = $db->getResult();
    echo json_encode($res);        
}
if(isset($_POST['update_marg_orders_operation'])){
    echo 'string';
}
if(isset($_POST['updateCustomAttr'])){
    // echo $_POST['attrib'];
    $product_variant_id = $_POST['product_variant_id'];
    $field = $_POST['field'];
    $value = $_POST['value'];
    $sql = "UPDATE product_variant set `".$field."`='".$value."' where `id`=".$product_variant_id;
    $db->sql($sql);
}
if(isset($_POST['removeCustomAttr'])){
    $product_variant_id = $_POST['product_variant_id'];
    $sql = "delete from product_variant where id='$product_variant_id'";
    $db->sql($sql);
}
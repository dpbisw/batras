<?php
include_once('includes/functions.php');
include_once('includes/functions2.php');

?>
<section class="content-header">
    <h1>Ad Groups /<small><a href="home.php"><i class="fa fa-home"></i> Home</a></small></h1>
</section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <form method="POST" id="filter_form" name="filter_form">
                            <div class="form-group col-md-3">
                            </div>
                        </form>
                    </div>
                    <div class="box-header">
                        <h3 class="box-title">Ad Groups</h3>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
<?php 
if ($permissions['ads']['create'] == 1) {
?>
                            <form id="add_form" action="public/db-operation2.php" method="POST" enctype="multipart/form-data">
                            <input type="hidden" id="add_group" name="add_group" required="" value="1" aria-required="true">
                                <div class="box-body">
                                    <p id="add_group_status"></p>
                                    <div class="row">
                                        <div class="col-lg-3">


                                    <div class="form-group" id="col1">
                                        <label for="ad_group_type">Select Group Type</label>
                                        <select id="ad_group_type" name="ad_group_type" class="form-control">
                                            <option value="1">Banner Images</option>
                                            <option value="2">Offer Images</option>
                                        </select>
                                    </div>
                                    <div class="form-group" id="col1">
                                        <label for="group_name">Group Name</label>
                                        <input type="text" class="form-control" id="group_name" name="group_name">
                                    </div>
                                    <div class="form-group" id="col2">
                                        <label for="image_per_row">Number of image per line : <br/>(<small> Recommended even number. Maximum 6 </small>)</label>
                                        <input type="number" class="form-control" name="image_per_row" id="image_per_row" max="6" min="2">
                                        <span style="color:red" id="error_per_row"></span>
                                    </div>

                                            
                                        </div>
                                        <div class="col-lg-3" id="offer_images_row" style="display: none">

                                    <!-- <div class="form-group" id="col2" style="display: none"> -->
                                        <!-- <label for="icon_image">Group Image</label> -->
                                        <!-- <input type="file" class="form-control" name="icon_image" id="icon_image"> -->
                                    <!-- </div> -->
                                    <div class="form-group" id="col2">
                                        <label for="back_image">Background Image</label>
                                        <input type="file" class="form-control" name="back_image" id="back_image">
                                    </div>
                                    <div class="form-group" id="col2">
                                        <div>
                                            <br>
                                        <label for="back_color">Background Color</label>
                                        <input type="color" class="form-control" name="back_color" id="back_color">                                            
                                        </div>
                                    </div>

                                            
                                        </div>
                                        <div class="col-lg-3">
                                    <div class="form-group" id="col2">
                                        <label for="g_order">Order</label>
                                        <input type="number" class="form-control" name="g_order" id="g_order">
                                    </div>
                                    <div class="form-group" id="col1">
                                        <label for="status">Select Status</label>
                                        <select id="status" name="status" class="form-control">
                                            <option value="1">Visible</option>
                                            <option value="0">Hidden</option>
                                        </select>
                                    </div>
                                    <br><br>
                                    <input type="checkbox" name="is_third_party"> <b>Opens in new tab</b>
                                            
                                        </div>
                                    </div>
                                    <div class="mt-3">
                                        <button type="submit" class="btn btn-primary" id="submit_btn" name="btnAdd">Add</button>
                                    </div>
                                    <div class="form-group">
                                        <div id="result" style="display: none;"></div>
                                    </div>
                                </div>
                            </form>
<?php } else { ?>

    <div class="alert alert-danger topmargin-sm" style="margin-top: 20px;">You have no permission to create Ads.</div>

<?php } ?>
                            <br>
                        </div>
                        <div class="col-md-12">
<?php 
if ($permissions['ads']['read'] == 1) {
?>
                    <div class="box-body table-responsive">
                        <p id="group_delete_status"></p>
                        <?php 
                        $fn2 = new functions2();
                        $ad_groups = $fn2->getAdGroups();
                        ?>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Group Order</th>
                                    <th>Group Name</th>
                                    <th>Group Type</th>
                                    <!-- <th>Background Image</th> -->
                                    <!-- <th>Background Color</th> -->
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($ad_groups as $ag){ ?>
                                <tr>
                                    <td><?php echo ($ag['g_order']); ?></td>
                                    <td><?php echo ($ag['group_name']); ?></td>
                                    <td><?php echo ($ag['ad_group_type'] == 1) ? "Banner Images" : "Offer Images"; ?></td>
                                    <td style="display: none;">
                                        <?php 
                                        if($ag['ad_group_type'] == 2){ ?>
                                        <div>
                                            <img src="<?php echo DOMAIN_URL . $ag['back_image']; ?>" style="width:50px;height: 25px" alt="">
                                        </div>
                                    <?php }
                                        if($ag['ad_group_type'] == 1){ echo "N.A."; } 
                                        ?>                                        
                                    </td>
                                    <td style="display: none;">
                                        <?php 
                                        if($ag['ad_group_type'] == 2){ ?>
                                        <div style="background-color: <?php echo $ag['back_color']; ?>;width:50px;height: 25px"></div>
                                    <?php }
                                        if($ag['ad_group_type'] == 1){ echo "N.A."; } 
                                        ?>
                                    </td>
                                    <td><?php echo ($ag['is_visible'] == 1) ? 'Visible' : 'Hidden'; ?></td>
                                    <td>
                                        <a href="manage-group-content.php?id=<?php echo base64_encode($ag['ad_group_id']); ?>&type=<?php echo base64_encode($ag['ad_group_type']); ?>" class="btn btn-primary">Show content</a>
                                        <button onclick="showEditModal('<?php echo $ag['ad_group_id']; ?>')" class="btn btn-warning">Edit</button>
                                        <button onclick="deleteGroup('<?php echo $ag['ad_group_id']; ?>',this)" class="btn btn-danger">Delete</button>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        
                    </div>
                            
                        </div>
<?php } else { ?>

    <div class="alert alert-danger topmargin-sm" style="margin-top: 20px;">You have no permission to view Ads.</div>

<?php } ?>
                    </div>
                </div>
            </div>
            <div class="separator"> </div>
        </div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Group</h4>
        <p id="edit_status"></p>
      </div>
        <form id="edit_form" action="public/db-operation2.php" method="POST" enctype="multipart/form-data">
        <input type="hidden" id="edit_group" name="edit_group" required="" value="1" aria-required="true">
      <div class="modal-body">
            <input type="hidden" name="edit_group_id" id="edit_group_id">
            <div class="form-group">
                <label for="edit_group_name">Group Name</label>
                <input type="text" class="form-control" id="edit_group_name" name="edit_group_name" >
            </div>
            <div class="form-group">
                <label for="edit_group_order">Group Order</label>
                <input type="text" class="form-control" id="edit_group_order" name="edit_group_order">
            </div>

<!--             <div class="form-group">
                <label for="edit_ad_group_type">Select Group Type</label>
                <select id="edit_ad_group_type" name="edit_ad_group_type" class="form-control">
                  <option value="1">Banner Images</option>
                  <option value="2">Offer Images</option>
                </select>
            </div>
 -->            
            <div class="form-group" id="col2">
                <label for="image_per_row">Number of image per line : (<small> Recommended even number. Maximum 6 </small>)</label>
                <input type="number" class="form-control" name="edit_image_per_row" id="edit_image_per_row" max="6" min="2">
                <span style="color:red" id="error_per_row2"></span>
            </div>
            <div class="form-group" id="o_back_image">
                <label for="edit_backgound_image">Backgound Image</label>
                <input type="file" class="form-control" id="edit_backgound_image" name="edit_backgound_image">
                <input type="hidden" name="ori_back_image" value="" id="ori_back_image">
            </div>
            <div class="form-group" id="o_back_color">
                <label for="edit_group_order">Background color</label>
                <input type="color" class="form-control" id="edit_backgound_color" name="edit_backgound_color" value="#CEFAFE">
            </div>

            <div class="form-group">
                <label for="edit_status">Status</label>
                <select id="edit_status" name="edit_status" class="form-control">
                  <option value="1">Visible</option>
                  <option value="0">Hidden</option>
                </select>
            </div>

            <input type="checkbox" name="edit_is_third_party" id="edit_is_third_party"> <b>Opens in new tab</b>
      </div>
      <div class="modal-footer">
        <button type="submit" id="edit_btn" class="btn btn-success">Update</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
        </form>
    </div>

  </div>
</div>

    </section>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
        <script>
        $("#add_form").validate({
            rules:{
                ad_group_type : 'required',
                group_name : 'required',
                image_per_row:'required',
                g_order:'required',
                status:'required'
            },
            messages :{
                ad_group_type : "Group Type is required",
                group_name : "Group Name is required",
                image_per_row : "Image Per Row is required",
                g_order : "Order is required",
                status : "Status is required",
            }
        });

        $("#edit_form").validate({
            rules:{
                // edit_ad_group_type : 'required',
                edit_group_name : 'required',
                edit_image_per_row:'required',
                edit_g_order:'required',
                edit_status:'required'
            },
            messages :{
                // edit_ad_group_type : "Group Type is required",
                edit_group_name : "Group Name is required",
                edit_image_per_row : "Image Per Row is required",
                edit_g_order : "Order is required",
                edit_status : "Status is required",
            }
        });
            
        </script>

<script>
    function queryParams_1(p) {
        return {
            limit: p.limit,
            sort: p.sort,
            order: p.order,
            offset: p.offset,
            search: p.search
        };
    }
    $("#cat_style").change(function() {
        var style = $(this).val();
        if (style == "style_1") {
            $("#col1, #col2").show();
        } else {
            $(" #col2").hide();
        }
    });

    $("#edit_image_per_row").on('blur',function(){
       var image_per_row2 = parseInt($("#edit_image_per_row").val());
       if(image_per_row2 >1 &&image_per_row2 < 6)
       {
         if(image_per_row2 !=5 )
         {
            // $("#edit_form").submit();
            $("#error_per_row2").html("");
            $("#edit_btn").attr('disabled',false);
         }
         else{
            $("#error_per_row2").html("Number not allowed");
            $("#edit_btn").attr('disabled',true);
         }
       }
       else
       {
        $("#error_per_row2").html("Please Enter a Even Number Between 2 And 6");
        $("#edit_btn").attr('disabled',true);
       }        
    });

    $("#edit_form").on('submit',function(e){
        e.preventDefault();
        var formData = new FormData(this);
        $('#edit_form').validate();
        if($("#edit_form").valid()){
        $.ajax({
            method : 'POST',
            url: $(this).attr('action'),
            data: formData,
            beforeSend: function() {
                $('#edit_btn').html('Please wait..');
            },
            cache: false,
            contentType: false,
            processData: false,
            success: function(result) {
                if(result == 1)
                {
                    $("#edit_status").removeClass("alert-danger");                    
                    $("#edit_status").addClass("alert alert-success");
                    $("#edit_status").html("Record Updated Successfully!");
                    location.reload();
                }
                else{
                    $("#edit_status").removeClass("alert-success");                    
                    $("#edit_status").addClass("alert alert-danger");
                    $("#edit_status").html("Failed To Update! Please try again!");

                }
            }
        });
    }
    });
    
    $('#add_form').on('submit', function(e) {
        e.preventDefault();
        $('#add_form').validate();
        if($("#add_form").valid()){
       var image_per_row = parseInt($("#image_per_row").val());
       if(image_per_row >=2 &&image_per_row < 6)
       {
         if((image_per_row % 2) == 0)
         {
        var formData = new FormData(this);
        $.ajax({
            method: 'POST',
            url: $(this).attr('action'),
            data: formData,
            beforeSend: function() {
                $('#submit_btn').html('Please wait..');
            },
            cache: false,
            contentType: false,
            processData: false,
            success: function(result) {
                if(result == 1)
                {
                    $("#add_group_status").removeClass('alert-danger');
                    $("#add_group_status").addClass('alert alert-success');
                    $("#add_group_status").html("Record Saved Successfully!");
                    location.reload();
                }
                if(result == 0 || result == 2)
                {
                    var str = '';
                    if(result == 0){ str = "Something went wrong please try again!"; }
                    if(result == 2){ str = "Order already exists. Please try with another order!"; }
                    
                    $("#add_group_status").removeClass('alert-success');
                    $("#add_group_status").addClass('alert alert-danger');    
                    $("#add_group_status").html(`${str}`);  
                    $('#submit_btn').html('Add');              
                }
            }
        });
            $("#error_per_row").html("");
         }
         else{
            $("#error_per_row").html("Please Enter a Even Number!");
         }
       }
       else{
        $("#error_per_row").html("Please Enter a Even Number Between 2 And 6");
       }
   }
    });

    function deleteGroup(group_id,elem) {
        var c = confirm('Do you want to delete this Group? All the Related Data will be deleted');
        if(c)
        {
        $.ajax({
            type: 'POST',
            url: "public/db-operation2.php",
            data: { group_delete_id : group_id },
            beforeSend : function(){
                elem.innerHTML="Deleting";
            },
            success: function(result) {
                if(result == 1)
                {
                    $("#group_delete_status").removeClass('alert-danger');
                    $("#group_delete_status").addClass('alert alert-success');
                    $("#group_delete_status").html("Record Deleted Successfully!");
                    elem.innerHTML = 'Deleted';

                    location.reload();
                }
                if(result == 0){
                    $("#group_delete_status").removeClass('alert-success');
                    $("#group_delete_status").addClass('alert alert-danger');    
                    $("#group_delete_status").html(`Something went wrong..Please try again!`);  
                    elem.innerHTML = 'Delete';
                }

            }
        });            
        }
    }

    function showEditModal(group_id) {
        $.ajax({
            type: 'POST',
            url: "public/db-operation2.php",
            data: { group_edit_id : group_id },
            success: function(result) {
                var jObj = JSON.parse(result);
                jObj.group_info.forEach(editEach);

                function editEach(item,index)
                {
                    $("#edit_group_id").val(item.ad_group_id);
                    $("#edit_group_name").val(item.group_name);
                    $("#edit_group_order").val(item.g_order);
                    $("#edit_ad_group_type").val(item.ad_group_type);
                    $("#edit_image_per_row").val(item.image_per_row);
                    $("#edit_status").val(item.is_visible);
                    if(item.is_third_party == 1)
                    {
                        $("#edit_is_third_party").prop('checked',true);
                    }
                    else $("#edit_is_third_party").prop('checked',false);
                    // alert(item.ad_group_id);
                    if(item.ad_group_type == 2){
                        $("#o_back_image").show();
                        $("#o_back_color").show();
                        $("#ori_back_image").val(item.back_image);
                        $("#edit_backgound_color").val(item.back_color);
                    }
                    if(item.ad_group_type == 1){
                        $("#o_back_image").hide();
                        $("#o_back_color").hide();
                    }
                }
            }
        });            
        $("#myModal").modal('show');
    }

    $("#ad_group_type").on("change",function(){
        if($(this).val() == 1){
            $("#offer_images_row").hide();
        }
        if($(this).val() == 2){
            $("#offer_images_row").show();
        }
    });
</script>
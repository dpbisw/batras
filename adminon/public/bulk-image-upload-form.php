<section class="content-header">
  <h1>Bulk Image Upload /<small><a href="products.php"><i class="fa fa-cubes"></i> Products</a></small></h1>
</section>
<!-- Main content -->
<section class="content">
  <!-- Main row -->
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">

        <div class="box-header with-border">

        </div><!-- /.box-header -->
        <!-- form start -->
         <div class="box-body">
         	<div id="div_form">
            <form class="dropzone" id="file_upload"></form>
		<br>
		  <button class="btn btn-primary" id="uploadBtn">Upload Images</button>		
		  </div>
		  <div id="success_msg" style="display: none"><br><br><p class="alert alert-success">Images Uploaded</p></div>
		</div>
	</div>
    </div>
</div>
</section>
<script>
        Dropzone.autoDiscover =false;
        var myDropzone = new Dropzone("#file_upload", { 
            url: "public/bulk-image-operation.php",
            parallelUploads: 30,
            uploadMultiple : true,
            acceptedFiles : '.png,.jpg,.jpeg',
            autoProcessQueue: false,
            success : function(file,response){
            	if(response==1)
            	{
            		$("#success_msg").css("display","block");
            		setTimeout(function(){
            		location.reload();            			
            	}, 3000);
            	}
            }
        });

        $("#uploadBtn").click(function(){
            myDropzone.processQueue();
        });	
</script>
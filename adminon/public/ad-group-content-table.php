<section class="content-header">
    <h1>Ad Group Content</h1>
    <ol class="breadcrumb">
        <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
    </ol>	
</section>
<section class="content">
	    <div class="row">
	        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box">
                <div class="box-body">
                    <?php 
                        $fn = new functions();
                        $ad_groups = $fn->getAdGroups();
                        foreach($ad_groups as $ag)
                        {
                        	?>
                        	<div class="box box-primary">
                        		<div class="box-header">
                        			<h3 class="box-title">
                        				<?php echo $ag['group_name']; ?>
                        			</h3>
                        		</div>
                        	</div>
                        	<?php
                        }
                    ?>                	
                </div>
            </div>
	        </div>
	    </div>
</section>
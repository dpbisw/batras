<?php
include_once('includes/functions.php');
$function = new functions;
include_once('includes/custom-functions.php');
$fn = new custom_functions;

?>
<?php
if (isset($_POST['btnAdd'])) {
	if (ALLOW_MODIFICATION == 0 && !defined(ALLOW_MODIFICATION)) {
		echo '<label class="alert alert-danger">This operation is not allowed in demo panel!.</label>';
		return false;
	}
	if ($permissions['stores']['create'] == 1) {

		$store_name = $db->escapeString($fn->xss_clean($_POST['store_name']));
		$store_address = $db->escapeString($fn->xss_clean($_POST['store_address']));
		$store_city = $db->escapeString($fn->xss_clean($_POST['store_city']));
		$store_state = $db->escapeString($fn->xss_clean($_POST['store_state']));
		$store_phone = $db->escapeString($fn->xss_clean($_POST['store_phone']));
		$store_timings = $db->escapeString($fn->xss_clean($_POST['store_timings']));
		$store_status = $db->escapeString($fn->xss_clean($_POST['store_status']));
		
		//Slug
		$slug = $db->escapeString($function->slugify($fn->xss_clean($_POST['store_name'])));
		$sql = "SELECT slug FROM stores";
		$db->sql($sql);
		$res = $db->getResult();
		$i = 1;
		foreach ($res as $row) {
			if ($slug == $row['slug']) {
				$slug = $slug . '-' . $i;
				$i++;
			}
		}

		// get image info
		$store_image = $db->escapeString($fn->xss_clean($_FILES['store_image']['name']));
		$image_error = $db->escapeString($fn->xss_clean($_FILES['store_image']['error']));
		$image_type = $db->escapeString($fn->xss_clean($_FILES['store_image']['type']));

		// create array variable to handle error
		$error = array();

		if (empty($store_name)) {
			$error['store_name'] = " <span class='label label-danger'>Required!</span>";
		}
		if (empty($store_address)) {
			$error['store_address'] = " <span class='label label-danger'>Required!</span>";
		}
		if (empty($store_city)) {
			$error['store_city'] = " <span class='label label-danger'>Required!</span>";
		}
		if (empty($store_state)) {
			$error['store_state'] = " <span class='label label-danger'>Required!</span>";
		}
		if (empty($store_phone)) {
			$error['store_phone'] = " <span class='label label-danger'>Required!</span>";
		}
		if (empty($store_timings)) {
			$error['store_timings'] = " <span class='label label-danger'>Required!</span>";
		}
		if (empty($store_status)) {
			$error['store_status'] = " <span class='label label-danger'>Required!</span>";
		}

		// common image file extensions
		$allowedExts = array("gif", "jpeg", "jpg", "png");

		// get image file extension
		error_reporting(E_ERROR | E_PARSE);
		$extension = end(explode(".", $_FILES["store_image"]["name"]));

		if ($image_error > 0) {
			$error['store_image'] = " <span class='label label-danger'>Not Uploaded!!</span>";
		} else {
			$mimetype = mime_content_type($_FILES["store_image"]["tmp_name"]);
			if (!in_array($mimetype, array('image/jpg', 'image/jpeg', 'image/gif', 'image/png'))) {
				$error['store_image'] = " <span class='label label-danger'>Image type must jpg, jpeg, gif, or png!</span>";
			}
		}

		if (!empty($store_name) && !empty($store_address) && empty($error['store_image'])) {

			// create random image file name
			$string = '0123456789';
			$file = preg_replace("/\s+/", "_", $_FILES['store_image']['name']);
			$store_image = $function->get_random_string($string, 4) . "-" . date("Y-m-d") . "." . $extension;
            $store_image_new = $store_image;
			// upload new image
			$upload = move_uploaded_file($_FILES['store_image']['tmp_name'], 'upload/images/' . $store_image_new);

			// insert new data to menu table
			$upload_image = 'upload/images/' . $store_image_new;
			$sql_query = "INSERT INTO stores (name, slug, address, city, state, phone, timings, status, image)VALUES('$store_name', '$slug', '$store_address', '$store_city', '$store_state', '$store_phone', '$store_timings', '$store_status', '$upload_image')";
			$db->sql($sql_query);
			$result = $db->getResult();
			if (!empty($result)) {
				$result = 0;
			} else {
				$result = 1;
			}

			if ($result == 1) {
				$error['add_store'] = " <section class='content-header'><span class='label label-success'>Store Added Successfully</span></section>";
			} else {
				$error['add_store'] = " <span class='label label-danger'>Failed add store</span>";
			}
		}
	} else {
		$error['check_permission'] = " <section class='content-header'><span class='label label-danger'>You have no permission to create store</span></section>";
	}
}
?>
<section class="content-header">
	<h1>Add Store <small><a href='stores.php'> <i class='fa fa-angle-double-left'></i>&nbsp;&nbsp;&nbsp;Back to Store</a></small></h1>

	<?php echo isset($error['add_store']) ? $error['add_store'] : ''; ?>
	<ol class="breadcrumb">
		<li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
	</ol>
	<hr />
</section>
<section class="content">
	<div class="row">
		<div class="col-md-6">
			<?php if ($permissions['categories']['create'] == 0) { ?>
				<div class="alert alert-danger">You have no permission to create store.</div>
			<?php } ?>
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Add Store</h3>

				</div><!-- /.box-header -->
				<!-- form start -->
				<form method="post" enctype="multipart/form-data">
					<div class="box-body">
						<div class="form-group">
							<label for="store_name">Store Name</label><?php echo isset($error['store_name']) ? $error['store_name'] : ''; ?>
							<input type="text" class="form-control" name="store_name" required>
						</div>
						<div class="form-group">
							<label for="store_address">Store Address</label><?php echo isset($error['store_address']) ? $error['store_address'] : ''; ?>
							<input type="text" class="form-control" name="store_address" required>
						</div>
						<div class="form-group">
							<label for="store_city">Store City</label><?php echo isset($error['store_city']) ? $error['store_city'] : ''; ?>
							<input type="text" class="form-control" name="store_city" required>
						</div>
						<div class="form-group">
							<label for="store_state">Store State</label><?php echo isset($error['store_state']) ? $error['store_state'] : ''; ?>
							<input type="text" class="form-control" name="store_state" required>
						</div>
						<div class="form-group">
							<label for="store_phone">Store Phone</label><?php echo isset($error['store_phone']) ? $error['store_phone'] : ''; ?>
							<input type="text" class="form-control" name="store_phone" required>
						</div>
						<div class="form-group">
							<label for="store_timings">Store Timings</label><?php echo isset($error['store_timings']) ? $error['store_timings'] : ''; ?>
							<input type="text" class="form-control" name="store_timings" required>
						</div>
						
						<div class="form-group">
                            <label for="">Store Status</label><br>
                            <input type="checkbox" id="store_status_button" class="js-switch">
                            <input type="hidden" id="store_status" name="store_status">
                        </div>
                        
						

						<div class="form-group">
							<label for="exampleInputFile">Image&nbsp;&nbsp;&nbsp;*Please choose square image of larger than 350px*350px & smaller than 550px*550px.</label><?php echo isset($error['store_image']) ? $error['store_image'] : ''; ?>
							<input type="file" name="store_image" id="store_image" required />
						</div>
					</div><!-- /.box-body -->

					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="btnAdd">Add</button>
						<input type="reset" class="btn-warning btn" value="Clear" />

					</div>

				</form>

			</div><!-- /.box -->
			<?php echo isset($error['check_permission']) ? $error['check_permission'] : ''; ?>
		</div>
	</div>
</section>

<div class="separator"> </div>

<script>
    var changeCheckbox = document.querySelector('#store_status_button');
    var init = new Switchery(changeCheckbox);
    changeCheckbox.onchange = function() {
        if ($(this).is(':checked')) {
            $('#store_status').val(1);
        } else {
            $('#store_status').val(0);
        }
    };
</script>

<?php $db->disconnect(); ?>
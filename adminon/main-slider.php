<?php
    // start session
    session_start();
    
    // set time for session timeout
    $currentTime = time() + 25200;
    $expired = 3600;
    
    // if session not set go to login page
    if (!isset($_SESSION['user'])) {
        header("location:index.php");
    }
    
    // if current time is more than session timeout back to login page
    if ($currentTime > $_SESSION['timeout']) {
        session_destroy();
        header("location:index.php");
    }
    
    // destroy previous session timeout and create new one
    unset($_SESSION['timeout']);
    $_SESSION['timeout'] = $currentTime + $expired;
    ?>
<?php 
include"header.php";
$allowed = ALLOW_MODIFICATION;


?>
<html>
    <head>
        <title>Main Slider Images | <?=$settings['app_name']?> - Dashboard</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" crossorigin="anonymous"></script>
        <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> -->
        <script type="text/javascript">
            $(document).ready(function () {
            
            });
            function sendPushNotification(id) {
                var data = $('form#' + id).serialize();
                $('form#' + id).unbind('submit');
                $.ajax({
                    url: "send-message.php",
                    type: 'GET',
                    data: data,
                    beforeSend: function () {
            
                    },
                    success: function (data, textStatus, xhr) {
                        $('.txt_message').val("");
                    },
                    error: function (xhr, textStatus, errorThrown) {
            
                    }
                });
            
                return false;
            }
        </script>
        <style type="text/css">
            .container{
            width: 950px;
            margin: 0 auto;
            padding: 0;
            }
            h1{
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 24px;
            color: #777;
            }
            h1 .send_btn
            {
            background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#0096FF), to(#005DFF));
            background: -webkit-linear-gradient(0% 0%, 0% 100%, from(#0096FF), to(#005DFF));
            background: -moz-linear-gradient(center top, #0096FF, #005DFF);
            background: linear-gradient(#0096FF, #005DFF);
            text-shadow: 0 1px 0 rgba(0, 0, 0, 0.3);
            border-radius: 3px;
            color: #fff;
            padding: 3px;
            }
            div.clear{
            clear: both;
            }
            ul.devices{
            margin: 0;
            padding: 0;
            }
            ul.devices li{
            float: left;
            list-style: none;
            border: 1px solid #dedede;
            padding: 10px;
            margin: 0 15px 25px 0;
            border-radius: 3px;
            -webkit-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.35);
            -moz-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.35);
            box-shadow: 0 1px 5px rgba(0, 0, 0, 0.35);
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            color: #555;
            width:100%;
            height:150px;
            background-color:#ffffff;
            }
            ul.devices li label, ul.devices li span{
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 12px;
            font-style: normal;
            font-variant: normal;
            font-weight: bold;
            color: #393939;
            display: block;
            float: left;
            }
            ul.devices li label{
            height: 25px;
            width: 50px;                
            }
            ul.devices li textarea{
            float: left;
            resize: none;
            }
            ul.devices li .send_btn{
            background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#0096FF), to(#005DFF));
            background: -webkit-linear-gradient(0% 0%, 0% 100%, from(#0096FF), to(#005DFF));
            background: -moz-linear-gradient(center top, #0096FF, #005DFF);
            background: linear-gradient(#0096FF, #005DFF);
            text-shadow: 0 1px 0 rgba(0, 0, 0, 0.3);
            border-radius: 7px;
            color: #fff;
            padding: 4px 24px;
            }
            a{text-decoration:none;color:rgb(245,134,52);}

            .msi_li{
    list-style-type: none;
    box-shadow: 0px 1px 1px 1px #ccc;
    padding: 11px;
    margin-right: 46px;
    margin-bottom: 11px;        
    cursor: move;        
            }
            #msi_order{
                padding-top: 10px;
                padding-bottom: 10px
            }
        </style>
    </head>
    <body>
        <div class="content-wrapper">
        <section class="content-header">
            <h1>Main Slider Image for Extra Offers and Benefits for Customers</h1>
            <ol class="breadcrumb">
                <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
            </ol>
            <hr/>
        </section>
        <?php
            include_once('includes/functions.php');
            
            include_once('includes/functions2.php');
            $fn2 = new functions2;
          
            ?>
        <section class="content">
            <div class="row">
                <div class="col-md-5">
                    <?php if($permissions['home_sliders']['create']==0) { ?>
                        <div class="alert alert-danger">You have no permission to create home slider</div>
                    <?php } ?>
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add / Update Exciting Offers Images here</h3>
                        </div>
                        <form id="slider_form" method="post" action="api-firebase/slider-images.php" enctype="multipart/form-data">
                            <div class="box-body">
                                <input type='hidden' name='accesskey' id='accesskey' value='90336'/>
                                <input type='hidden' name='add-image' id='add-image' value='1'/>
                                <div class="form-group">
                                    <label for="type">Type :</label>
                                    <select name="type" id="type" class="form-control" required>
                                        <option value="default">Default</option>
                                        <option value="category">Category</option>
                                        <option value="sub-category">Sub-Category</option>
                                        <option value="product">Product</option>
                                    </select>
                                </div>
                                <div class="form-group" id="categories" style="display:none;">
                                    <label for="category">Categories :</label>
                                    <select name="category" id="category" class="form-control" onchange="showSubcategory(this.value)">
                                        <?php
                                            $sql = "SELECT * FROM `category` order by id DESC";
                                            $db->sql($sql);
                                            $categories_result = $db->getResult();
                                        ?>
                                        <?php if($permissions['categories']['read']==1){?>
                                        <option value="">Select Category</option>
                                        <?php foreach($categories_result as $value){?>
                                            <option value="<?=$value['id']?>"><?=$value['name']?></option>
                                            <?php
                                        } }else {
                                        ?>
                                        <option value="">Select Category</option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group" id="sub-categories" style="display: none;">
                                    <label for="sub-category">Sub-Category:</label>
                                    <select name="sub-category" id="sub-category" class="form-control">
                <option value="">Select Sub-category</option>                                       
                                    </select>
                                </div>
            
                                <div class="form-group" id="products" style="display:none;">
                                    <label for="product">Products :</label>
                                    <select name="product" id="product" class="form-control">
                                         <?php
                                            $sql = "SELECT * FROM `products` order by id DESC";
                                            $db->sql($sql);
                                            $products_result = $db->getResult();
                                            
                                        ?>
                                        <?php if($permissions['products']['read']==1){?>
                                        <option value="">Select Product</option>
                                        <?php foreach($products_result as $value){?>
                                            <option value="<?=$value['id']?>"><?=$value['name']?></option>
                                            <?php
                                        } } else {
                                        ?>
                                        <option value="">Select Product</option>
                                    <?php } ?>    
                                </div>
                                <div class="form-group">
                                    <label for="def_link">Link</label>
                                    <input type="text" name="def_link" id="def_link" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="image">Slider Image : <small> ( Recommended Size : 1680 x 360 pixels )</small></label>
                                    <input type='file' name="image" id="image" required/> 
                                    <p style="color:red" id="err_image"></p> 

                                </div>
                                <div class="form-group">
                                    <label for="mob_image">Mobile Image : <small> ( Recommended Size : 756 x 376 pixels )</small></label>
                                    <input type='file' name="mob_image" id="mob_image" required/>
                                    <p style="color:red" id="err_mob_image"></p> 
                                </div>
                                <div class="form-group" id="image_link_div">
                                    <label for="image_link">Link</label>
                                    <input type="text" class="form-control" id="image_link" name="image_link">
                                </div>
                                <div class="form-group">
                                    <label for="slider_delay">Slider Delay <small>( in seconds.)</small></label>
                                    <input type="number" class="form-control" id="slider_delay" name="slider_delay">
                                </div>
                            </div>
                            <div class="box-footer">
                                <input type="submit" id="submit_btn" class="btn-primary btn" value="Upload"/>
                            </div>
                        </form>
                        <div id="result"></div>
                    </div>
                </div>
                <div class="col-md-7">
                    <?php if($permissions['home_sliders']['read']==1){?>
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Main Slider Image</h3>
                    <p class="alert alert-success" id="message" style="display: none;"></p>
                        </div>
                        <table id="notifications_table" class="table table-hover" data-toggle="table" 
                            data-url="api-firebase/get-bootstrap-table-data.php?table=slider"
                            data-page-list="[5, 10, 20, 50, 100, 200]"
                            data-show-refresh="true" data-show-columns="true"
                            data-side-pagination="server" data-pagination="true"
                            data-search="true" data-trim-on-search="false"
                            data-sort-name="row_order" data-sort-order="asc">
                            <thead>
                            <tr>
                                <th data-field="id" data-sortable="true">ID</th>
                                <th data-field="image">Web Image</th>
                                <th data-field="mobile_image">Mobile Image</th>
                                <th data-field="type">Type</th>
                                <th data-field="type_id">ID</th>
                                <th data-field="slider_delay">Delay <small>(in secs)</small></th>
                                <th data-field="operate">Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                     <?php } else { ?>
                    <div class="alert alert-danger">You have no permission to view home slider images.</div>
                    <?php } ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Main Slider Image Order</h3>
                        </div>
                        <div>
                            <?php 
                                $msi = $fn2->getMainSliderImages();
                                if(count($msi)>0)
                                { $i=0;
                                    ?><ul id="msi_order"><?php
                                    foreach ($msi as $s) {
                                        ?>
                                        <li class="msi_li ui-state-default" id="<?php echo $s['id'] ?>"><?php echo ++$i; ?>. &nbsp;&nbsp;<img src="<?php echo $s['image'] ?>" style="width:100px"></li>
                                        <?php
                                    }
                                    ?></ul><?php
                                }
                            ?>
                            <form action="public/db-operation2.php" method="post" id="orderForm">
                        <input type="hidden" name="order_home_slider" value="1">
                                <input type="hidden" name="ms_order" id="ms_order">
                                <center><button type="submit" class="btn btn-primary">Submit</button></center>
                                <br>
                            </form>
                        </div>
                    </div>
                </div>                                    
            </div>
        </section>
    </div>
    <script>

          $( function() {
    $( "#msi_order" ).sortable({
        placeholder: "ui-state-highlight",
        update : function(event,ui){
               var cmsOrder = $(this).sortable('toArray').toString();
               $("#ms_order").val(cmsOrder);  
               // $(".pageOrder").val(cmsOrder);
        }        
    });
    $( "#msi_order" ).disableSelection();
  } );

        $('#orderForm').on('submit',function(e){
        e.preventDefault();
        var formData = new FormData(this);
            $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:formData,
            // dataType:'json',
            // beforeSend:function(){$('#submit_btn').val('Please wait..').attr('disabled',true);},
            cache:false,
            contentType: false,
            processData: false,
            success:function(result){
                if(result==1)
                {
                    alert("Order Changed successfully");
                    location.reload();
                }
                else{
                    alert("Error in changing order");
                }
            }
            });        
    }); 


         var allowed  = '<?= $allowed; ?>';
    $("#include_image").change(function() {
        if(this.checked) {
            $('#image').show('fast');
        }else{
            $('#image').val('');
            $('#image').hide('fast');
        }
    });
    $("#type").change(function() {
        //alert('changed');
        type = $("#type").val();
        if(type == "default"){
            $("#categories").hide();
            $("#products").hide();
            $("#sub-categories").hide();          
            $("#image_link_div").show();
        }
        if(type == "category"){
            $("#categories").show();
            $("#products").hide();
            $("#sub-categories").hide();
            $("#image_link_div").hide();          
        }
        if(type == "product"){
            $("#categories").hide();
            $("#products").show();
            $("#sub-categories").hide();          
            $("#image_link_div").hide();          
        }
        if(type == 'sub-category')
        {
            $("#categories").show();
            $("#products").hide();  
            $("#sub-categories").show();        
            $("#image_link_div").hide();            
        }
    });
      $('#slider_form').on('submit',function(e){
        e.preventDefault();
        if (allowed == 0) {
            alert('Sorry! This operation is not allowed in demo panel!.');
            window.location.reload();
            return false;
        }
        var formData = new FormData(this);
            $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:formData,
            dataType:'json',
            beforeSend:function(){$('#submit_btn').val('Please wait..').attr('disabled',true);},
            cache:false,
            contentType: false,
            processData: false,
            success:function(result){
                $('#result').html(result.message);
                $('#result').show().delay(2000).fadeOut();
                $('#submit_btn').val('Upload').attr('disabled',false);
                // $('#notifications_table').bootstrapTable('refresh');
                setTimeout(function() {
                    location.reload();
                }, 2000);
            }
            });
        
    }); 

    $(document).on('click','.delete-slider',function(){
        if(confirm('Are you sure?')){
            id = $(this).data("id");
            image = $(this).data("image");
            $.ajax({
                url : 'api-firebase/slider-images.php',
                type: "get",
                data: 'accesskey=90336&id='+id+'&image='+image+'&type=delete-slider',
                success: function(result){
                    if(result==1){
                        $('#notifications_table').bootstrapTable('refresh');
                    }
                    if(result==2){
                        alert('You have no permission to delete home slider');
                    }
                    if(result==0){
                        alert('Error! slider could not be deleted');
                    }
                        
                }
            });
        }
    });

    $(document).on('click','.edit-slider',function(){
            id = $(this).data("id");
            $.ajax({
                url : 'api-firebase/slider-images.php',
                type: "get",
                dataType : 'json',
                data: 'accesskey=90336&id='+id+'&type=edit-slider',
                success: function(result){
                    result.slider_info.forEach( function(element, index) {
                        $("#edit_type").val(element.type);
                        $("#edit_ori_image").val(element.image);
                        $("#edit_ori_mob_image").val(element.mobile_image);
                        $("#edit_link").val(element.link);

                        var slider_delay=element.slider_delay;
                        if(element.slider_delay!=0)
                        {
                            slider_delay=parseInt(element.slider_delay) / 1000 ;
                        }
                        $("#edit_slider_delay").val(slider_delay);

                        if(element.type != 'default')
                        {
                            if(element.type == 'product'){
                            $("#edit_product").val(element.type_id);    
                            $("#edit_products").css('display','block');
                            $("#edit_categories").css('display','none');  
                            $("#edit_sub_category").css('display','none');
                            $("#edit_link_div").css('display','none');
                            }
                            if(element.type == 'category'){
                            $("#edit_category").val(element.type_id);    
                            $("#edit_categories").css('display','block');
                            $("#edit_products").css('display','none');
                            $("#edit_sub_category").css('display','none');
                            $("#edit_link_div").css('display','none');                            
                            }
                            if(element.type == 'sub-category')
                            {
                            
                            $.ajax({
                                url : 'get-subcategorybyid.php',
                                type : 'post',
                                dataType : 'json',
                                data : { 'sub_category_id':element.type_id },
                                success : function(result){
                                    result.sub_cat.forEach( function(el) {
                                        $("#edit_category").val(el.category_id);
                                    });
                                    var html = '';
                                    result.all_cat.forEach( function(el) {
                                        var selected = '';
                                        if(el.id == element.type_id){ selected = 'selected'; }
                                        html += `
                                        <option 
                                            value='${el.id}'
                                             ${ selected }  
                                            >${el.name}
                                        </option>
                                        `;
                                    });
                                    $("#edit-sub-category").html(html);
                                }
                            });

                            $("#edit_sub_category").css('display','block');
                            $("#edit_categories").css('display','block');
                            $("#edit_products").css('display','none');                                
                            $("#edit_link_div").css('display','none');                            
                            }
                        }
                        else{
                            $("#edit_products").css('display','none');
                            $("#edit_categories").css('display','none');                            
                            $("#edit_sub_category").css('display','none');
                            $("#edit_link_div").css('display','block');
                        }
                        $("#edit_id").val(element.id);
                    });
            $("#editModal").modal('show');
                }
            });
    });    


    </script>
    <!-- Modal -->
<div id="editModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Slider</h4>
      </div>
      <div class="modal-body">
        <form id="edit-form" action="api-firebase/slider-images.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="edit_id" id="edit_id" value="">
        <input type="hidden" name="accesskey" value="90336">
        <div class="form-group">
            <label for="type">Type :</label>
            <select name="type" id="edit_type" class="form-control" required onchange="displayType(this.value)">
                <option value="default">Default</option>
                <option value="category">Category</option>
                <option value="product">Product</option>
                <option value="sub-category">Sub Category</option>
            </select>
        </div>
        <div class="form-group" id="edit_categories" style="display:none;">
            <label for="category">Categories :</label>
            <select name="category" id="edit_category" class="form-control" onchange="showEditSubcategory(this.value)">
                <?php
                    $sql = "SELECT * FROM `category` order by id DESC";
                    $db->sql($sql);
                    $categories_result = $db->getResult();
                ?>
                <?php if($permissions['categories']['read']==1){?>
                <option value="">Select Category</option>
                <?php foreach($categories_result as $value){?>
                <option value="<?=$value['id']?>"><?=$value['name']?></option>
                <?php
                } }else {
                ?>
                <option value="">Select Category</option>
                <?php } ?>
            </select>
            <span style="color:red" id="error_edit_cat_select"></span>
        </div>
        <div class="form-group" id="edit_sub_category" style="display: none">
            <label for="edit-sub-category">Sub-Category</label>
            <select name="sub-category" id="edit-sub-category" class="form-control">
                <option value="">Select Sub-category</option>
            </select>
            <span style="color:red" id="error_edit_subcat_select"></span>
        </div>
        <div class="form-group" id="edit_products" style="display:none;">
            <label for="edit_product">Products :</label>
            <select name="product" id="edit_product" class="form-control">
                 <?php
                    $sql = "SELECT * FROM `products` order by id DESC";
                    $db->sql($sql);
                    $products_result = $db->getResult();
                    
                ?>
                <?php if($permissions['products']['read']==1){?>
                <option value="">Select Product</option>
                <?php foreach($products_result as $value){?>
                <option value="<?=$value['id']?>"><?=$value['name']?></option>
                <?php
                } } else {
                ?>
                <option value="">Select Product</option>
            <?php } ?>
            </select>
            <span style="color:red" id="error_edit_prod_select"></span>
        </div>
        <div class="form-group" id="edit_link_div">
            <label for="edit_link">Link</label>
            <input type="text" id="edit_link" value="" name="image_link" class="form-control">
        </div>
        <div class="form-group" id="edit_slider_delay_div">
            <label for="edit_slider_delay">Slider Delay</label>
            <input type="number" id="edit_slider_delay" value="" name="slider_delay" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Update Image : <small> ( Recommended Size : 1680 x 360 pixels )</small></label>
            <input type="hidden" name="ori_image" id="edit_ori_image">
            <input type="file" name="image" id="edit_image" class="form-control">
            <p style="color: red" id="err_edit_image"></p>
        </div>
        <div class="form-group">
            <label for="">Update Mobile Image : <small> ( Recommended Size : 756 x 376 pixels )</small></label>
            <input type="hidden" name="ori_mob_image" id="edit_ori_mob_image">
            <input type="file" name="mob_image" id="edit_mob_image" class="form-control">
            <p style="color: red" id="err_edit_mob_image"></p>
        </div>        
        <button class="btn btn-primary" type="submit" id="updateBtn">Update</button>
        </form>
      </div>
    </div>

  </div>
</div>

<script>
          $('#edit-form').on('submit',function(e){
        e.preventDefault();
        if (allowed == 0) {
            alert('Sorry! This operation is not allowed in demo panel!.');
            window.location.reload();
            return false;
        }
        var type_select = $("#edit_type option:selected").val();
        var flag = true;
        if(type_select == 'category' && $("#edit_category option:selected").val() == '')
        {
            $("#error_edit_cat_select").html("You Must Select a Category");
            flag=false;
        }
        if(type_select == 'product' && $("#edit_product option:selected").val() == '')
        {
            $("#error_edit_prod_select").html("You Must Select a Product");
            flag = false;
        }
        if(type_select == 'sub-category' && $("#edit_sub_category option:selected").val()==''){
            $("#error_edit_subcat_select").html("You Must Select a Sub-Category");
            flag = false;
        }

        if(flag){
        var formData = new FormData(this);
        // alert(formData);
            $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:formData,
            dataType:'json',
            // beforeSend:function(){$('#submit_btn').val('Please wait..').attr('disabled',true);},
            cache:false,
            contentType: false,
            processData: false,
            success:function(result){
                if(result == 1)
                {
                    $("#editModal").modal('hide');
                    $('#notifications_table').bootstrapTable('refresh');
                    $("#message").css('display','block');
                    $("#message").html("Record Updated Successfully");
                setTimeout(function() {
                    $("#message").css('display','none');
                }, 2000);

                }
            }
            });
    }
        
    }); 

    function displayType(type_name){
        if(type_name != 'default')
        {
            if(type_name == 'category')
            {
                $("#edit_categories").css('display','block');
                $("#edit_products").css('display','none');
                $("#edit_sub_category").css('display','none');
                $("#edit_link_div").css('display','none');
            }
            if(type_name == 'product')
            {
                $("#edit_categories").css('display','none');
                $("#edit_products").css('display','block');
                $("#edit_sub_category").css('display','none');
                $("#edit_link_div").css('display','none');
            }
            if(type_name == 'sub-category')
            {
                $("#edit_categories").css('display','block');
                $("#edit_products").css('display','none');
                $("#edit_sub_category").css('display','block');
                $("#edit_link_div").css('display','none');
            }            
        }
        else{
                $("#edit_categories").css('display','none');
                $("#edit_products").css('display','none');     
                $("#edit_sub_category").css('display','none');
                $("#edit_link_div").css('display','block');
        }
    }
    function showSubcategory(category)
    {
        if($("#type").val() == 'sub-category')
        {
            var category_id = $("#category").val();
            $.ajax({
                url : 'get-subcategorybyid.php',
                type: "post",
                dataType : 'json',
                data: 'accesskey=90336&category_id='+category_id,
                success: function(result){
                    var html = '';
                    result.forEach(function(element){
                        html += `<option value='${element.id}'>${element.name}</option>`;
                    });
                    $("#sub-category").html(html);
                }
            });
        }
    }
    function showEditSubcategory(category)
    {
        if($("#edit_type").val() == 'sub-category')
        {
            var category_id = $("#edit_category").val();
            $.ajax({
                url : 'get-subcategorybyid.php',
                type: "post",
                dataType : 'json',
                data: 'accesskey=90336&category_id='+category_id,
                success: function(result){
                    var html = '';
                    result.forEach(function(element){
                        html += `<option value='${element.id}'>${element.name}</option>`;
                    });
                    $("#edit-sub-category").html(html);
                    // alert(html);
                }
            });
        }
    }    

        //binds to onchange event of your input field
$('#image').bind('change', function() {
  //this.files[0].size gets the size of your file.
  if(this.files[0].size > 100000)
  {
    $("#err_image").html("Please select a file less than 100kb size");
    $("#submit_btn").attr('disabled',true);
  }
  else{
    $("#err_image").html("");
    $("#submit_btn").attr('disabled',false);
  }
});

        //binds to onchange event of your input field
$('#mob_image').bind('change', function() {
  //this.files[0].size gets the size of your file.
  if(this.files[0].size > 100000)
  {
    $("#err_mob_image").html("Please select a file less than 100kb size");
    $("#submit_btn").attr('disabled',true);
  }
  else{
    $("#err_mob_image").html("");
    $("#submit_btn").attr('disabled',false);
  }

});


        //binds to onchange event of your input field
$('#edit_image').bind('change', function() {
  //this.files[0].size gets the size of your file.
  if(this.files[0].size > 100000)
  {
    $("#err_edit_image").html("Please select a file less than 100kb size");
    $("#updateBtn").attr('disabled',true);
  }
  else{
    $("#err_edit_image").html("");
    $("#updateBtn").attr('disabled',false);
  }
});

        //binds to onchange event of your input field
$('#edit_mob_image').bind('change', function() {

  //this.files[0].size gets the size of your file.
  if(this.files[0].size > 100000)
  {
    $("#err_edit_mob_image").html("Please select a file less than 100kb size");
    $("#updateBtn").attr('disabled',true);
 
  }
  else{
    $("#err_edit_mob_image").html("");
    $("#updateBtn").attr('disabled',false);
  }

});

</script>

</body>
</html>
<?php include"footer.php"; ?>
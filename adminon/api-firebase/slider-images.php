<?php
session_start();
include '../includes/crud.php';
include_once('../includes/variables.php');
include_once('../includes/custom-functions.php');


header("Content-Type: application/json");
header("Expires: 0");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Access-Control-Allow-Origin: *');
// date_default_timezone_set('Asia/Kolkata');
$fn = new custom_functions;

include_once('verify-token.php');
$db = new Database();
$db->connect();
$response = array();

$config = $fn->get_configurations();
$time_slot_config = $fn->time_slot_config();
if (isset($config['system_timezone']) && isset($config['system_timezone_gmt'])) {
    date_default_timezone_set($config['system_timezone']);
    $db->sql("SET `time_zone` = '" . $config['system_timezone_gmt'] . "'");
} else {
    date_default_timezone_set('Asia/Kolkata');
    $db->sql("SET `time_zone` = '+05:30'");
}



if (!isset($_POST['accesskey'])) {
    if (!isset($_GET['accesskey'])) {
        $response['error'] = true;
        $response['message'] = "Access key is invalid or not passed!";
        print_r(json_encode($response));
        return false;
    }
}

if (isset($_POST['accesskey'])) {
    $accesskey = $db->escapeString($fn->xss_clean($_POST['accesskey']));
} else {
    $accesskey = $db->escapeString($fn->xss_clean($_GET['accesskey']));
}

if ($access_key != $accesskey) {
    $response['error'] = true;
    $response['message'] = "invalid accesskey!";
    print_r(json_encode($response));
    return false;
}

if ((isset($_POST['add-image'])) && ($_POST['add-image'] == 1)) {
    if (ALLOW_MODIFICATION == 0 && !defined(ALLOW_MODIFICATION)) {
        echo '<label class="alert alert-danger">This operation is not allowed in demo panel!.</label>';
        return false;
    }
    $permissions = $fn->get_permissions($_SESSION['id']);
    if ($permissions['home_sliders']['create'] == 0) {
        $response["message"] = "<p class='alert alert-danger'>You have no permission to create home slider.</p>";
        echo json_encode($response);
        return false;
    }
    $slider_delay = $db->escapeString($fn->xss_clean($_POST['slider_delay']));

    $slider_delay = (int)$slider_delay * 1000;

    $image = $db->escapeString($fn->xss_clean($_FILES['image']['name']));
    $image_error = $db->escapeString($fn->xss_clean($_FILES['image']['error']));
    $image_type = $db->escapeString($fn->xss_clean($_FILES['image']['type']));

    $mob_image = $db->escapeString($fn->xss_clean($_FILES['mob_image']['name']));
    $mob_image_error = $db->escapeString($fn->xss_clean($_FILES['mob_image']['error']));
    $mob_image_type = $db->escapeString($fn->xss_clean($_FILES['mob_image']['type']));

    $type = $db->escapeString($fn->xss_clean($_POST['type']));
    $id = ($type != 'default') ? $db->escapeString($fn->xss_clean($_POST[$type])) : "0";

    $cate_id = 0;
    $link = '';
    if(isset($_POST['sub-category']))
    {
        $cate_id = $_POST['category'];
    }
    if(isset($_POST['image_link'])){ $link = $_POST['image_link']; }
    // create array variable to handle error
    $error = array();
    // common image file extensions
    $allowedExts = array("gif", "jpeg", "jpg", "png");

    // get image file extension
    error_reporting(E_ERROR | E_PARSE);
    $extension = end(explode(".", $_FILES["image"]["name"]));
    $mob_extension = end(explode(".", $_FILES["mob_image"]["name"]));

    if ($mob_image_error > 0) {
        $error['mob_image'] = " <span class='label label-danger'>Not uploaded!</span>";
    } else {
        $mimetype = mime_content_type($_FILES["mob_image"]["tmp_name"]);
        if (!in_array($mimetype, array('image/jpg', 'image/jpeg', 'image/gif', 'image/png'))) {
            $response["message"] = "<span class='label label-danger'>Image type must jpg, jpeg, gif, or png!</span>";
            echo json_encode($response);
            $error['mob_image'] = " <span class='label label-danger'>Image type must jpg, jpeg, gif, or png!</span>";
            return false;
        }
    }

    if ($image_error > 0) {
        $error['image'] = " <span class='label label-danger'>Not uploaded!</span>";
    } else {
        $mimetype = mime_content_type($_FILES["image"]["tmp_name"]);
        if (!in_array($mimetype, array('image/jpg', 'image/jpeg', 'image/gif', 'image/png'))) {
            $response["message"] = "<span class='label label-danger'>Image type must jpg, jpeg, gif, or png!</span>";
            echo json_encode($response);
            $error['image'] = " <span class='label label-danger'>Image type must jpg, jpeg, gif, or png!</span>";
            return false;
        }
    }

    if (empty($error['image']) && empty($error['mob_image'])) {
        // create random image file name
        $mt = explode(' ', microtime());
        $microtime = ((int)$mt[1]) * 1000 + ((int)round($mt[0] * 1000));
        $file = preg_replace("/\s+/", "_", $_FILES['image']['name']);
        $mob_file = preg_replace("/\s+/", "_", $_FILES['mob_image']['name']);

        $image = $microtime . "." . $extension;
        $mob_image = $microtime . "mob." . $mob_extension;

        // upload new image
        $upload = move_uploaded_file($_FILES['image']['tmp_name'], '../upload/slider/' . $image);
        $mob_upload = move_uploaded_file($_FILES['mob_image']['tmp_name'], '../upload/slider/' . $mob_image);

        // insert new data to menu table
        $upload_image = 'upload/slider/' . $image;
        $upload_mob_image = 'upload/slider/' . $mob_image;

        $sql = "INSERT INTO `slider`(`image`,`type`, `type_id`,`cate_id`,`link`,`mobile_image`,`slider_delay`) VALUES ('$upload_image','" . $type . "','" . $id . "','".$cate_id."','".$link."','$upload_mob_image','$slider_delay')";
        $db->sql($sql);
        $res = $db->getResult();
        $sql = "SELECT id FROM `slider` ORDER BY id DESC";
        $db->sql($sql);
        $res = $db->getResult();
        $response["message"] = "<span class='label label-success'>Image Uploaded Successfully!</span>";
        $response["id"] = $res[0]['id'];
    } else {
        $response["message"] = "<span class='label label-daner'>Image could not be Uploaded!Try Again!</span>";
    }
    echo json_encode($response);
}
if (isset($_GET['type']) && $_GET['type'] != '' && $_GET['type'] == 'delete-slider') {
    if (ALLOW_MODIFICATION == 0 && !defined(ALLOW_MODIFICATION)) {
        echo '<label class="alert alert-danger">This operation is not allowed in demo panel!.</label>';
        return false;
    }
    $permissions = $fn->get_permissions($_SESSION['id']);
    if ($permissions['home_sliders']['delete'] == 0) {
        echo 2;
        return false;
    }

    $id        = $_GET['id'];
    $image     = $_GET['image'];
    $mobile_image     = $_GET['mobile_image'];


    if (!empty($image))
        unlink('../' . $image);

    if (!empty($mobile_image))
        unlink('../' . $mobile_image);

    $sql = 'DELETE FROM `slider` WHERE `id`=' . $id;
    if ($db->sql($sql)) {
        echo 1;
    } else {
        echo 0;
    }
}
if (isset($_GET['type']) && $_GET['type'] != '' && $_GET['type'] == 'edit-slider') {
    if (ALLOW_MODIFICATION == 0 && !defined(ALLOW_MODIFICATION)) {
        echo '<label class="alert alert-danger">This operation is not allowed in demo panel!.</label>';
        return false;
    }
    $permissions = $fn->get_permissions($_SESSION['id']);
    if ($permissions['home_sliders']['delete'] == 0) {
        echo 2;
        return false;
    }

    $id        = $_GET['id'];

    $sql = 'select * from slider where id ='.$id;
    $db->sql($sql);
    $result = $db->getResult();
    echo json_encode(['slider_info'=>$result]);
    // if (!empty($image))
    //     unlink('../' . $image);

    // $sql = 'DELETE FROM `slider` WHERE `id`=' . $id;
    // if ($db->sql($sql)) {
    //     echo 1;
    // } else {
    //     echo 0;
    // }
}

if (isset($_POST['get-slider-images'])) {
    if (!verify_token()) {
        return false;
    }
    $sql = 'select * from slider order by row_order asc';
    $db->sql($sql);
    $result = $db->getResult();
    $response = $temp = $temp1 = array();
    if (!empty($result)) {
        $response['error'] = false;
        foreach ($result as $row) {
            $name = "";
            $cate_name = '';
            $slug1 = '';
            $slug2 = '';
            if ($row['type'] == 'category') {
                $sql = 'select `name` from category where id = ' . $row['type_id'] . ' order by id desc';
                $db->sql($sql);
                $result1 = $db->getResult();
                $name = (!empty($result1[0]['name'])) ? $result1[0]['name'] : "";
            }
            if ($row['type'] == 'product') {
                $sql = 'select `name` from products where id = ' . $row['type_id'] . ' order by id desc';
                $db->sql($sql);
                $result1 = $db->getResult();
                $name = (!empty($result1[0]['name'])) ? $result1[0]['name'] : "";
            }
            if ($row['type'] == 'sub-category') {
                $sql = 'select name,slug from subcategory where id = ' . $row['type_id'] . ' order by id desc';
                $db->sql($sql);
                $result1 = $db->getResult();
                $name = (!empty($result1[0]['name'])) ? $result1[0]['name'] : "";
                $slug2 = (!empty($result1[0]['slug'])) ? $result1[0]['slug'] : "";

                $sqlc = 'select name,slug from category where id = ' . $row['cate_id'] . ' order by id desc';
                $db->sql($sqlc);
                $result2 = $db->getResult();
                $cate_name = (!empty($result2[0]['name'])) ? $result2[0]['name'] : "";
                $slug1 = (!empty($result2[0]['slug'])) ? $result2[0]['slug'] : "";
            }

            $temp['type'] = $row['type'];
            $temp['type_id'] = $row['type_id'];
            $temp['name'] = $name;
            $temp['cate_name'] = $cate_name;
            $temp['slider_delay'] = $row['slider_delay'];
            $temp['slug1'] = $slug1;
            $temp['slug2'] = $slug2;
            $temp['image'] = DOMAIN_URL . $row['image'];
            $temp['mobile_image'] = DOMAIN_URL . $row['mobile_image'];
            $temp['link'] = $row['link'];
            $temp1[] = $temp;
        }
        $response['data'] = $temp1;
    } else {
        $response['error'] = true;
        $response['message'] = "No slider images uploaded yet!";
    }
    print_r(json_encode($response));
}

//Editing the slider from admin panel
if(isset($_POST['edit_id']))
{
    $id = $_POST['edit_id'];
    $type= $_POST['type'];
    $link = $_POST['image_link'];

    $slider_delay = $_POST['slider_delay'];
    $slider_delay = (int)$slider_delay * 1000;

    $cate_id = 0;
    if($type == 'category')
    {
        $type_id = $_POST['category'];
    }
    else if($type == 'product')
    {
        $type_id = $_POST['product'];
    }
    else if($type == 'sub-category'){
        $type_id = $_POST['sub-category'];
        $cate_id = $_POST['category'];
    }
    else{
        $type_id = 0;
    }

    $image = $_POST['ori_image'];
    $mob_image = $_POST['ori_mob_image'];
    if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){
        // echo "there";
    $image = $db->escapeString($fn->xss_clean($_FILES['image']['name']));
    $image_error = $db->escapeString($fn->xss_clean($_FILES['image']['error']));
    $image_type = $db->escapeString($fn->xss_clean($_FILES['image']['type']));
    // create array variable to handle error
    $error = array();
    // common image file extensions
    $allowedExts = array("gif", "jpeg", "jpg", "png");
        // create random image file name
        $mt = explode(' ', microtime());
        $microtime = ((int)$mt[1]) * 1000 + ((int)round($mt[0] * 1000));
        $file = preg_replace("/\s+/", "_", $_FILES['image']['name']);
        $extension = end(explode(".", $_FILES["image"]["name"]));
        $image = $microtime . "." . $extension;
        // upload new image
        $upload = move_uploaded_file($_FILES['image']['tmp_name'], '../upload/slider/' . $image);
        $delete = unlink($_POST['ori_image']);
        $image = "upload/slider/".$image;
    }

    if(isset($_FILES['mob_image']['name']) && !empty($_FILES['mob_image']['name'])){
        // echo "there";
    $mob_image = $db->escapeString($fn->xss_clean($_FILES['mob_image']['name']));
    $mob_image_error = $db->escapeString($fn->xss_clean($_FILES['mob_image']['error']));
    $mob_image_type = $db->escapeString($fn->xss_clean($_FILES['mob_image']['type']));
    // create array variable to handle error
    $mob_error = array();
    // common image file extensions
    $allowedExts = array("gif", "jpeg", "jpg", "png");
        // create random image file name
        $mt = explode(' ', microtime());
        $microtime = ((int)$mt[1]) * 1000 + ((int)round($mt[0] * 1000));
        $mob_file = preg_replace("/\s+/", "_", $_FILES['mob_image']['name']);
        $extension = end(explode(".", $_FILES["mob_image"]["name"]));
        $mob_image = $microtime . "." . $extension;
        // upload new image
        $mob_upload = move_uploaded_file($_FILES['mob_image']['tmp_name'], '../upload/slider/' . $mob_image);
        $mob_delete = unlink($_POST['ori_mob_image']);
        $mob_image = "upload/slider/".$mob_image;
    }

    $sql = 'UPDATE `slider` set `link`="'.$link.'",`type`="'.$type.'", `type_id`="'.$type_id.'",`image`="'.$image.'",`cate_id`="'.$cate_id.'",`mobile_image`="'.$mob_image.'",`slider_delay`="'.$slider_delay.'" WHERE `id`=' . $id;
    if ($db->sql($sql)) {
        echo 1;
    } else {
        echo 0;
    }

}
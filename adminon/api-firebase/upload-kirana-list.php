<?php
header('Access-Control-Allow-Origin: *');
// header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
include_once('../includes/crud.php');
$db = new Database();
$db->connect();
include_once('../includes/variables.php');
include_once('verify-token.php');
include_once('../includes/custom-functions.php');
include_once('../includes/functions.php');
$fn = new custom_functions;
$fun=new functions;
$config = $fn->get_configurations();
$time_slot_config = $fn->time_slot_config();
if (isset($config['system_timezone']) && isset($config['system_timezone_gmt'])) {
    date_default_timezone_set($config['system_timezone']);
    $db->sql("SET `time_zone` = '" . $config['system_timezone_gmt'] . "'");
} else {
    date_default_timezone_set('Asia/Kolkata');
    $db->sql("SET `time_zone` = '+05:30'");
}

        if ($_FILES['attachment']['name'] != '') {
            
            $filename = $_FILES['attachment']['name'];
            
            $extension = end(explode(".", $_FILES["attachment"]["name"]));
            
            $string = '0123456789';
            $file = preg_replace("/\s+/", "_", $_FILES['attachment']['name']);
            $filename = $fun->get_random_string($string, 4) . "-" . date("Y-m-d") . "." . $extension;

            $path = "../upload/request-list-files/" . $filename;

            $stat = move_uploaded_file($_FILES['attachment']['tmp_name'], $path);
            if($stat)
            {
                $response['error'] = "false";
                $response['data'] = $filename;
                $response['message']='Upload Successful';
            }
            else{
                $response['error'] = "true";
                $response['data'] = '';
                $response['message']='Upload failed';
            }
            echo json_encode($response);
            // echo "upload/request-list-files/" . $filename;
        }

//Output the output.
$db->disconnect();
//to check if the string is json or not
function isJSON($string)
{
    return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
}

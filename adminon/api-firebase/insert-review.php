<?php
header('Access-Control-Allow-Origin: *');
include_once('../includes/crud.php');
$db = new Database();
$db->connect();
include_once('../includes/variables.php');
include_once('verify-token.php');
include_once('../includes/custom-functions.php');
$fn = new custom_functions;
$config = $fn->get_configurations();
$time_slot_config = $fn->time_slot_config();
if (isset($config['system_timezone']) && isset($config['system_timezone_gmt'])) {
    date_default_timezone_set($config['system_timezone']);
    $db->sql("SET `time_zone` = '" . $config['system_timezone_gmt'] . "'");
} else {
    date_default_timezone_set('Asia/Kolkata');
    $db->sql("SET `time_zone` = '+05:30'");
}

/*
get-products-by-category-id.php
    accesskey:90336
    category_id:32
    user_id:369 {optional}
    limit:10 // {optional}
    offset:0 // {optional}
    sort:new / old / high / low // {optional}
*/
if (!verify_token()) {
    return false;
}

if (isset($_POST['accesskey']) && isset($_POST['user_id'])) {
    $access_key_received = $db->escapeString($fn->xss_clean($_POST['accesskey']));
    $user_id = (isset($_POST['user_id'])) ? $db->escapeString($fn->xss_clean($_POST['user_id'])) : "";
    $product_id = (isset($_POST['product_id'])) ? $db->escapeString($fn->xss_clean($_POST['product_id'])) : "";
    $title = (isset($_POST['title'])) ? $db->escapeString($fn->xss_clean($_POST['title'])) : "";
    $review = (isset($_POST['review'])) ? $db->escapeString($fn->xss_clean($_POST['review'])) : "";
    $rating = (isset($_POST['rating'])) ? $db->escapeString($fn->xss_clean($_POST['rating'])) : "";


    if ($access_key_received == $access_key) {
        $setarray = [];
            $sql = "insert into product_reviews(`product_id`,`user_id`,`title`,`review`,`rating`,`status`) values('$product_id','$user_id','$title','$review','$rating',1)";        
            if($db->customInsert($sql)!=0)
            {
                $response['status'] = "Data Inserted";
                $response['error'] = false;
            }
            else{
                $response['status'] = "Data Not Inserted";
                $response['error'] = true;                
            }

        $output = json_encode($response);
    }
}

//Output the output.
echo $output;
$db->disconnect();
//to check if the string is json or not
function isJSON($string)
{
    return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
}

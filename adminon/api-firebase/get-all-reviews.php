<?php
header('Access-Control-Allow-Origin: *');
include_once('../includes/crud.php');
$db = new Database();
$db->connect();
include_once('../includes/variables.php');
include_once('verify-token.php');
include_once('../includes/custom-functions.php');
$fn = new custom_functions;
$config = $fn->get_configurations();
$time_slot_config = $fn->time_slot_config();
if (isset($config['system_timezone']) && isset($config['system_timezone_gmt'])) {
    date_default_timezone_set($config['system_timezone']);
    $db->sql("SET `time_zone` = '" . $config['system_timezone_gmt'] . "'");
} else {
    date_default_timezone_set('Asia/Kolkata');
    $db->sql("SET `time_zone` = '+05:30'");
}

/*
get-products-by-category-id.php
    accesskey:90336
    category_id:32
    user_id:369 {optional}
    limit:10 // {optional}
    offset:0 // {optional}
    sort:new / old / high / low // {optional}
*/
if (!verify_token()) {
    return false;
}

if (isset($_POST['accesskey'])) {
    $access_key_received = $db->escapeString($fn->xss_clean($_POST['accesskey']));

    // $user_id = $db->escapeString($fn->xss_clean($_POST['user_id']));
    $product_id = $db->escapeString($fn->xss_clean($_POST['product_id']));


    // $pincode = (isset($_POST['pincode']) && is_numeric($_POST['pincode'])) ? $db->escapeString($fn->xss_clean($_POST['pincode'])) : "";

    if ($access_key_received == $access_key) {
        
        // $sql = "select title,review,rating,created_at from product_reviews where user_id='$user_id' and product_id='$product_id'";        
        $sql = "select title,review,rating,created_at from product_reviews where product_id='$product_id'";        
        $db->sql($sql);
        $res = $db->getResult();
        $response = [];
        if(!empty($res)){ 
            $response['reviews'] = $res; 
            $response['error']  = false;
        }
        else{
            $response['status'] = 'No reviews available'; 
            $response['error']  = true;            
        }
        // Getting product variants
        $output = json_encode($response);
    }
}

//Output the output.
echo $output;
$db->disconnect();
//to check if the string is json or not
function isJSON($string)
{
    return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
}

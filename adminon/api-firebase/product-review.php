<?php
header('Access-Control-Allow-Origin: *');
include_once('../includes/crud.php');
$db = new Database();
$db->connect();
include_once('../includes/variables.php');
include_once('verify-token.php');
include_once('../includes/custom-functions.php');
$fn = new custom_functions;
$config = $fn->get_configurations();
$time_slot_config = $fn->time_slot_config();
if (isset($config['system_timezone']) && isset($config['system_timezone_gmt'])) {
    date_default_timezone_set($config['system_timezone']);
    $db->sql("SET `time_zone` = '" . $config['system_timezone_gmt'] . "'");
} else {
    date_default_timezone_set('Asia/Kolkata');
    $db->sql("SET `time_zone` = '+05:30'");
}

/*
get-products-by-category-id.php
    accesskey:90336
    category_id:32
    user_id:369 {optional}
    limit:10 // {optional}
    offset:0 // {optional}
    sort:new / old / high / low // {optional}
*/
if (!verify_token()) {
    return false;
}

if (isset($_POST['accesskey'])) {
    $access_key_received = $db->escapeString($fn->xss_clean($_POST['accesskey']));
    $product_id = (isset($_POST['product_id']) && is_numeric($_POST['product_id'])) ? $db->escapeString($fn->xss_clean($_POST['product_id'])) : "";
    $user_id = (isset($_POST['user_id']) && is_numeric($_POST['user_id'])) ? $db->escapeString($fn->xss_clean($_POST['user_id'])) : "";

    if ($access_key_received == $access_key) {
        $sql = "select * from orders as o inner join order_items as i on o.id = i.order_id inner join product_variant as v on i.product_variant_id=v.id where i.user_id='$user_id' and v.product_id='$product_id'";        
        $db->sql($sql);
        $res = $db->getResult();
        if(count($res)>0)
        {
            $status = 1;
        }
        else{
            $status=0;
        }
        // Getting product variants
        $output = json_encode(['review_status'=>$status]);
    }
}



//Output the output.
echo $output;
$db->disconnect();
//to check if the string is json or not
// function isJSON($string)
// {
//     return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
// }

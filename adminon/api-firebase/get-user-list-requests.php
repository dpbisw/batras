<?php
header('Access-Control-Allow-Origin: *');
session_start();
include '../includes/crud.php';
include_once '../includes/variables.php';
include_once '../includes/custom-functions.php';
$fn = new custom_functions;
include_once('verify-token.php');
$db = new Database();
$db->connect();
$response = array();
$config = $fn->get_configurations();
$time_slot_config = $fn->time_slot_config();
if (isset($config['system_timezone']) && isset($config['system_timezone_gmt'])) {
    date_default_timezone_set($config['system_timezone']);
    $db->sql("SET `time_zone` = '" . $config['system_timezone_gmt'] . "'");
} else {
    date_default_timezone_set('Asia/Kolkata');
    $db->sql("SET `time_zone` = '+05:30'");
}
/*
offer-images.php
    accesskey:90336
*/
if (!isset($_POST['accesskey'])) {
    if (!isset($_GET['accesskey'])) {
        $response['error'] = true;
        $response['message'] = "Access key is invalid or not passed!";
        print_r(json_encode($response));
        return false;
    }
}
if (isset($_POST['accesskey'])) {
    $accesskey = $db->escapeString($fn->xss_clean($_POST['accesskey']));
} else {
    $accesskey = $db->escapeString($fn->xss_clean($_GET['accesskey']));
}
if ($access_key != $accesskey) {
    $response['error'] = true;
    $response['message'] = "invalid accesskey!";
    print_r(json_encode($response));
    return false;
}
if ((!isset($_REQUEST['ajax_call']))) {
    if (!verify_token()) {
        return false;
    }
}
if (isset($_POST['user_id']) && !empty($_POST['user_id'])) {
    if (!verify_token()) {
        return false;
    }
    $user_id = $_POST['user_id'];

    if(!isset($_POST['request_list_id'])){
	    $sql = "select * from user_request_list where user_id='$user_id' and user_id!=0 order by created_at desc";
	    $db->sql($sql);
	    $result = $db->getResult();
        if(!empty($result))
        {
            for ($i = 0; $i < count($result); $i++) {
                
                if (strpos($result[$i]['list_file'], 'upload/request-list-files') !== false) {
                $result[$i]['list_file'] = (!empty($result[$i]['list_file'])) ? DOMAIN_URL . '' . $result[$i]['list_file'] : '';
                }                
            }
        }
    }
    if(isset($_POST['request_list_id'])){
    	$request_list_id = $_POST['request_list_id'];
	    $sql = "select * from user_request_list where user_request_list_id = '$request_list_id'";
	    $db->sql($sql);
	    $result = $db->getResult();
    }
    $response = $temp = $temp1 = array();
    if (!empty($result)) {
        $response['error'] = false;
        $response['data'] = $result;
    } else {
        $response['error'] = true;
        $response['message'] = "No offer images uploaded yet!";
    }
    print_r(json_encode($response));
}

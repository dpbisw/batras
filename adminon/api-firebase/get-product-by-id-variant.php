<?php
header('Access-Control-Allow-Origin: *');
include_once('../includes/variables.php');
include_once('../includes/crud.php');
include_once('verify-token.php');
$db = new Database();
$db->connect();
// date_default_timezone_set('Asia/Kolkata');
include_once('../includes/custom-functions.php');
$fn = new custom_functions;

$config = $fn->get_configurations();
$time_slot_config = $fn->time_slot_config();
if (isset($config['system_timezone']) && isset($config['system_timezone_gmt'])) {
    date_default_timezone_set($config['system_timezone']);
    $db->sql("SET `time_zone` = '" . $config['system_timezone_gmt'] . "'");
} else {
    date_default_timezone_set('Asia/Kolkata');
    $db->sql("SET `time_zone` = '+05:30'");
}

/* 
get-product-by-id.php
	accesskey:90336
	product_id:230
	user_id:369 {optional}
*/
if (!verify_token()) {
	return false;
}
if(isset($_POST['slug']) && trim($_POST['slug']) != "" && !isset($_POST['product_id']) || isset($_POST['attributes'])){
	$slug = $db->escapeString($fn->xss_clean($_POST['slug']));

	$attbs=$_POST['attributes'];
	$where='';
	foreach ($attbs as $v) {
		$where .= " and attributes like '%".$v."%' ";
	}
	// if(isset($_POST['attributes'])){

	// // $where="and attributes like '".$_POST['attributes']."'";
	// }

	$sql = "SELECT * FROM products WHERE slug = '$slug'";
	$db->sql($sql);
	$res = $db->getResult();
	if(isset($res[0]['id']) && intval($res[0]['id'])){
		$_POST['product_id'] = $res[0]['id'];
	}
}
if (isset($_POST['accesskey']) && isset($_POST['product_id'])) {
	$access_key_received = $db->escapeString($fn->xss_clean($_POST['accesskey']));
	$product_id = $db->escapeString($fn->xss_clean($_POST['product_id']));
	$user_id = (isset($_POST['user_id']) && is_numeric($_POST['user_id'])) ? $db->escapeString($fn->xss_clean($_POST['user_id'])) : "";

	if ($access_key_received == $access_key) {


		$sql = "SELECT *,attributes as attrs FROM products WHERE id = '" . $product_id . "' ";

		$db->sql($sql);
		$res = $db->getResult();
		$product = array();
		$i = 0;
		foreach ($res as $row) {
			$sql = "SELECT *,(SELECT short_code FROM unit u WHERE u.id=pv.measurement_unit_id) as measurement_unit_name,(SELECT short_code FROM unit u WHERE u.id=pv.stock_unit_id) as stock_unit_name FROM product_variant pv WHERE pv.product_id=" . $row['id'] ." ".$where." ORDER BY serve_for ASC";
			$db->sql($sql);
			$variants = $db->getResult();

			$row['other_images'] = json_decode($row['other_images'], 1);
			$row['other_images'] = (empty($row['other_images'])) ? array() : $row['other_images'];
			for ($j = 0; $j < count($row['other_images']); $j++) {
				$row['other_images'][$j] = DOMAIN_URL . $row['other_images'][$j];
			}
			if($row['tax_id'] == 0){
                $row['tax_title'] = "";
                $row['tax_percentage'] = "0";
            }else{
                $t_id = $row['tax_id'];
                $sql_tax = "SELECT * from taxes where id= $t_id";
                $db->sql($sql_tax);
                $res_tax = $db->getResult(); 
                foreach($res_tax as $tax){
                    $row['tax_title'] = $tax['title'];
                    $row['tax_percentage'] = (!empty($tax['percentage'])) ? $tax['percentage'] : "0";
                }
            }
			// for ($k = 0; $k < count($variants); $k++) {

			// 	if (!empty($user_id)) {
			// 		$sql = "SELECT qty as cart_count FROM cart where product_variant_id= " . $variants[$k]['id'] . " AND user_id=" . $user_id;
			// 		$db->sql($sql);
			// 		$res = $db->getResult();
			// 		if (!empty($res)) {
			// 			foreach ($res as $row1) {
			// 				$variants[$k]['cart_count'] = $row1['cart_count'];
			// 			}
			// 		} else {
			// 			$variants[$k]['cart_count'] = "0";
			// 		}
			// 	} else {
			// 		$variants[$k]['cart_count'] = "0";
			// 	}
            // }
            for ($k = 0; $k < count($variants); $k++) {
				if ($variants[$k]['stock'] <= 0) {
					$variants[$k]['serve_for'] = 'Sold Out';
				} else {
					$variants[$k]['serve_for'] = 'Available';
				}
				if (!empty($user_id)) {
					$sql = "SELECT qty as cart_count FROM cart where product_variant_id= " . $variants[$k]['id'] . " AND user_id=" . $user_id;
					$db->sql($sql);
					$res = $db->getResult();
					if (!empty($res)) {
						foreach ($res as $row1) {
							$variants[$k]['cart_count'] = $row1['cart_count'];
						}
					} else {
						$variants[$k]['cart_count'] = "0";
					}
				} else {
					$variants[$k]['cart_count'] = "0";
				}
			}
			if (!empty($user_id)) {
                $sql = "SELECT id from favorites where product_id = " . $row['id'] . " AND user_id = " . $user_id;
                $db->sql($sql);
                $result = $db->getResult();
                if (!empty($result)) {
                    $row['is_favorite'] = true;
                } else {
                    $row['is_favorite'] = false;
                }
            } else {
                $row['is_favorite'] = false;
            }

            //Category Information
                $sqlc = "SELECT name,slug,department_id from category where id = " . $row['category_id'];
                $db->sql($sqlc);
                $resultc = $db->getResult();
                if (!empty($resultc)) {
                	foreach($resultc as $res)
                	{
                		$row['category_name'] = $res['name'];
                		$row['category_slug'] = $res['slug'];
                		$row['dept_id'] = $res['department_id'];
                	}
                }
                else{
                	$row['category_name'] = '';
              		$row['category_slug'] = '';
                } 
            // Sub-category
                $sqlsc = "SELECT name,slug from subcategory where id = " . $row['subcategory_id'];
                $db->sql($sqlsc);
                $resultsc = $db->getResult();
                if (!empty($resultsc)) {
                	foreach($resultsc as $res)
                	{
                		$row['subcategory_name'] = $res['name'];
                		$row['subcategory_slug'] = $res['slug'];
                	}
                }
                else{
                		$row['subcategory_name'] = '';
                		$row['subcategory_slug'] = '';                	
                } 

            // Sub-category
                $sqld = "SELECT name,slug,id from departments where id = " . $row['dept_id'];
                $db->sql($sqld);
                $resultd = $db->getResult();
                if (!empty($resultd)) {
                	foreach($resultd as $res)
                	{
                		$row['dept_name'] = $res['name'];
                		$row['dept_slug'] = $res['slug'];
                		$row['dept_slug2'] = $res['id'];
                	}
                }
                else{
                		$row['dept_name'] = '';
                		$row['dept_slug'] = '';         
                		$row['dept_slug2'] = '';

                }

                //Custom attribute
                $cattr = unserialize($row['attrs']);
                $ctr=[];
                $ctv=[];
                $attributes = $cattr['attribute'];
                foreach ($attributes as $att) {
	                $sqld = "SELECT attribute_name from attributes where attribute_id = " . $att;
	                $db->sql($sqld);
	                $resultd = $db->getResult();                	
					foreach($resultd as $res)
					{
						$data = [
							'attribute_name' => $res['attribute_name'],
							'attribute_id' => $att
						];
						array_push($ctr, $data);
					}                
                }
                $row['attrs']=$ctr;
                $attribute_values = $cattr['attribute_value'];
                foreach ($attribute_values as $att) {
	                $sqld = "SELECT attribute_value,attribute_id from attribute_values where attr_value_id = " . $att;
	                $db->sql($sqld);
	                $resultd = $db->getResult();                	
					foreach($resultd as $res)
					{
						$data = [
							'attribute_value' => $res['attribute_value'],
							'attribute_id' => $res['attribute_id']
						];
						array_push($ctv, $data);
					}                
                }
                $row['attr_values'] = $ctv;

                $row['where'] = $where;

			$row['image'] = DOMAIN_URL . $row['image'];
			$product[$i] = $row;
			$product[$i]['variants'] = $variants;
			$i++;
		}
		if (!empty($product)) {
			$output = json_encode(array(
				'error' => false,
				'data' => $product
			));
		} else {
			$output = json_encode(array(
				'error' => true,
				'data' => 'No products available'
			));
		}
	} else {
		die('accesskey is incorrect.');
	}
} else {
	die('accesskey and product id are required.');
}

//Output the output.
echo $output;

$db->disconnect();
//to check if the string is json or not
function isJSON($string)
{
	return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
}

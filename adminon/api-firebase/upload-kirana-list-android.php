<?php
header('Access-Control-Allow-Origin: *');
// header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
include_once('../includes/crud.php');
$db = new Database();
$db->connect();
include_once('../includes/variables.php');
include_once('verify-token.php');
include_once('../includes/custom-functions.php');
include_once('../includes/functions.php');
$fn = new custom_functions;
$fun=new functions;
$config = $fn->get_configurations();
$time_slot_config = $fn->time_slot_config();
if (isset($config['system_timezone']) && isset($config['system_timezone_gmt'])) {
    date_default_timezone_set($config['system_timezone']);
    $db->sql("SET `time_zone` = '" . $config['system_timezone_gmt'] . "'");
} else {
    date_default_timezone_set('Asia/Kolkata');
    $db->sql("SET `time_zone` = '+05:30'");
}

        
        $response = [];
        if(isset($_POST['request-lists']))
        {
    $request_list = (isset($_POST['request-lists'])) ? $db->escapeString($fn->xss_clean($_POST['request-lists'])) : "";
    $user_id= (isset($_POST['user_id'])) ? $db->escapeString($fn->xss_clean($_POST['user_id'])) : "";
    // $fileName= (isset($_POST['fileName'])) ? $db->escapeString($fn->xss_clean($_POST['fileName'])) : "";
    $kirana_subject = (isset($_POST['kirana_subject'])) ? $db->escapeString($fn->xss_clean($_POST['kirana_subject'])) : "";

    	$list_filename = '';

        if ($_FILES['attachment1']['name'] != '') {
            
            $list_filename = $_FILES['attachment1']['name'];
            
            $extension = end(explode(".", $_FILES["attachment1"]["name"]));
            $string = '0123456789';
            $file = preg_replace("/\s+/", "_", $_FILES['attachment1']['name']);
            $list_filename = $fun->get_random_string($string, 4) . "-" . date("Y-m-d") . "." . $list_filename;
            $path = "../upload/request-list-files/" . $list_filename;
            move_uploaded_file($_FILES['attachment1']['tmp_name'], $path);
        }

    	$audio_filename = '';

        if ($_FILES['attachment2']['name'] != '') {
            
            $audio_filename = $_FILES['attachment2']['name'];
            
            $extension = end(explode(".", $_FILES["attachment2"]["name"]));
            $string = '0123456789';
            $file = preg_replace("/\s+/", "_", $_FILES['attachment2']['name']);
            $audio_filename = $fun->get_random_string($string, 4) . "-" . date("Y-m-d") . "." . $audio_filename;
            $path = "../upload/request-list-files/" . $audio_filename;
            move_uploaded_file($_FILES['attachment2']['tmp_name'], $path);
        }


    	$sql = "insert into `user_request_list`(`user_id`,`request_list`,`list_file`,`kirana_subject`,`audio_file`) values('$user_id','$request_list','$list_filename','$kirana_subject','$audio_filename')";
            $res = $db->customInsert($sql);
            if($res>0)
            {
                $response['error'] = "false";
                $response['message']='Upload Successful';
            }
            else{
                $response['error'] = "true";
                $response['message']='Upload failed';
            }
        // // Getting product variants
        echo json_encode($response);
        }
//     }
// }

//Output the output.
$db->disconnect();
//to check if the string is json or not
function isJSON($string)
{
    return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
}

<?php
session_start();
include '../includes/crud.php';
include_once('../includes/variables.php');
include_once('../includes/custom-functions.php');


header("Content-Type: application/json");
header("Expires: 0");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Access-Control-Allow-Origin: *');
// date_default_timezone_set('Asia/Kolkata');
$fn = new custom_functions;

include_once('verify-token.php');
$db = new Database();
$db->connect();
$response = array();

$config = $fn->get_configurations();
$time_slot_config = $fn->time_slot_config();
if (isset($config['system_timezone']) && isset($config['system_timezone_gmt'])) {
    date_default_timezone_set($config['system_timezone']);
    $db->sql("SET `time_zone` = '" . $config['system_timezone_gmt'] . "'");
} else {
    date_default_timezone_set('Asia/Kolkata');
    $db->sql("SET `time_zone` = '+05:30'");
}



if (!isset($_POST['accesskey'])) {
    if (!isset($_GET['accesskey'])) {
        $response['error'] = true;
        $response['message'] = "Access key is invalid or not passed!";
        print_r(json_encode($response));
        return false;
    }
}

if (isset($_POST['accesskey'])) {
    $accesskey = $db->escapeString($fn->xss_clean($_POST['accesskey']));
} else {
    $accesskey = $db->escapeString($fn->xss_clean($_GET['accesskey']));
}

if ($access_key != $accesskey) {
    $response['error'] = true;
    $response['message'] = "invalid accesskey!";
    print_r(json_encode($response));
    return false;
}

if (isset($_POST['get-all-ads']) && !empty($_POST['get-all-ads'])) {
    if (!verify_token()) {
        return false;
    }
    
    $sql = 'select * from ad_groups where is_visible = 1 order by g_order asc';
    $db->sql($sql);
    $result = $db->getResult();
    $response = $temp = $temp1 = array();
    if (!empty($result)) {
        $response['error'] = false;
        foreach ($result as $row) {
            $temp['ad_group_id'] = $row['ad_group_id'];
            $temp['ad_group_type'] = $row['ad_group_type'];
            $temp['group_name'] = $row['group_name'];
            $temp['image_per_row'] = $row['image_per_row'];
            $temp['is_third_party'] = $row['is_third_party'];
    		$temp['background_image'] = $row['back_image'];
    		$temp['background_color'] = $row['back_color'];
               $ad_group_id = $row['ad_group_id'];
            $sql2 = "select count(*) as count from ads where ad_group_id='$ad_group_id'";
            $db->sql($sql2);
            $result2 = $db->getResult();
            foreach ($result2 as $res) {
                $temp['no_of_ads'] = $res['count'];
            }

            $temp['inner-contents'] = [];
            $db->sql("select a.*, c.name as cat_name, c.slug as category_slug, s.name as scat_name,s.slug as subcategory_slug from ads a LEFT JOIN category c ON a.category_id = c.id LEFT JOIN subcategory s ON a.subcategory_id = s.id where a.is_visible = 1 AND a.ad_group_id = '{$row['ad_group_id']}'");
            $childs = $db->getResult();
                $r['childs'] = [];            
            if(!empty($childs)) {
                for($i=0; $i< count($childs);$i++){
               $childs[$i]['ad_image'] =  DOMAIN_URL . '' . $childs[$i]['ad_image'];                    
                }
                $temp['inner-contents'] = $childs;
            }
            
            $temp1[] = $temp;
        }
        $response['data'] = $temp1;
    } else {
        $response['error'] = true;
        $response['message'] = "No offer images uploaded yet!";
    }
    print_r(json_encode($response));
}else{
    $response['error'] = true;
    $response['message'] = "Invalid Parameter";
    print_r(json_encode($response));
    return false;
}
<?php
header('Access-Control-Allow-Origin: *');
session_start();
include '../includes/crud.php';
include_once '../includes/variables.php';
include_once '../includes/custom-functions.php';
$fn = new custom_functions;
include_once('verify-token.php');
$db = new Database();
$db->connect();
$response = array();
$config = $fn->get_configurations();
$time_slot_config = $fn->time_slot_config();
if (isset($config['system_timezone']) && isset($config['system_timezone_gmt'])) {
    date_default_timezone_set($config['system_timezone']);
    $db->sql("SET `time_zone` = '" . $config['system_timezone_gmt'] . "'");
} else {
    date_default_timezone_set('Asia/Kolkata');
    $db->sql("SET `time_zone` = '+05:30'");
}
/*
offer-images.php
    accesskey:90336
*/
if (!isset($_POST['accesskey'])) {
    if (!isset($_GET['accesskey'])) {
        $response['error'] = true;
        $response['message'] = "Access key is invalid or not passed!";
        print_r(json_encode($response));
        return false;
    }
}
if (isset($_POST['accesskey'])) {
    $accesskey = $db->escapeString($fn->xss_clean($_POST['accesskey']));
} else {
    $accesskey = $db->escapeString($fn->xss_clean($_GET['accesskey']));
}
if ($access_key != $accesskey) {
    $response['error'] = true;
    $response['message'] = "invalid accesskey!";
    print_r(json_encode($response));
    return false;
}
// if ((!isset($_REQUEST['ajax_call']))) {
//     if (!verify_token()) {
//         return false;
//     }
// }
    if (!verify_token()) {
        return false;
    }

    $fcm_id = $_POST['fcm_id'];
    $sql = "select id,name from notification_categories where is_visible=1";
    $db->sql($sql);
    $notif_result = $db->getResult();

    // $user_sql = "select notification_preferences from users where id = '$user_id'";
    $user_sql = "select notification_preferences from devices where fcm_id = '$fcm_id'";    
    $db->sql($user_sql);
    $uresult = $db->getResult();
    // $response['user_result'] = $uresult;
    $notif_pref = '';
    foreach ($uresult as $value) {
        $notif_pref = $value['notification_preferences'];
    }


    // for($i=0;$i<count($notif_result);$i++){
    //     $status = 1;
    //     if (strpos($notif_result[$i], '1') !== false) {
    //         $status = 1;
    //     }
    //     else{
    //         $status = 0;
    //     }        
    //     $notif_result['status'][$i] = $status;
    // }

    for($i=0;$i<count($notif_result);$i++){

        $status = 1;
        // $notif_result[$i]['id'] = $notif_pref;

        if (strpos($notif_pref,$notif_result[$i]['id']) !== false) {
            $status = 1;
        }
        else{
            $status = 0;
        }        
        $notif_result[$i]['status'] = $status;

    }

    $response['error'] = false;
    $response['notifi_categories'] = $notif_result;                
    // $response['type_u'] = gettype($notif_result);

    // $fcm_id = $_POST['fcm_id'];
    // $curdate = date('Y-m-d');

    // > Lattest
    // $user_array = [];
    // $user_sql = "select fcm_id,name,id as user_id from users where fcm_id!=''";
    // $db->sql($user_sql);
    // $user_result = $db->getResult();

    // $user_result;
    // $response['users'] = $user_array;

    // $user_devices = [];
    // $count=0;
    // if(!empty($user_result)){
    // foreach($user_result as $ua){
    //     $device_sql = "select is_user_active from devices where fcm_id='".$ua['fcm_id']."'";
    //     $db->sql($device_sql);
    //     $device_result = $db->getResult();
    //     $ua['is_user_active'] = 1;
    //     if(!empty($device_result)){
    //     array_push($user_devices, $ua);
    //     }
    // }
    //     $response['error'] = false;
    //     $response['devices'] = $user_devices;                
    // }
    // else{
    //     $response['error'] = true;
    //     $response['msg'] = "No devices found";
    // }

    // $sql = "select * from device_notifications as d inner join notifications as n on d.notification_id=n.id";
    // $db->sql($sql);
    // $result = $db->getResult();
    // $response = $temp = $temp1 = array();
    // if (!empty($result)) {
    //     $response['error'] = false;
    //     foreach ($result as $row) {
    //         $temp['notification_id'] = $row['notification_id'];
    //         $temp1[] = $temp;
    //     }
    //     $response['data'] = $temp1;
    //     // $response['curdate'] = $curdate;
    // } 
    print_r(json_encode($response));

<?php
header('Access-Control-Allow-Origin: *');
include_once('../includes/crud.php');
$db = new Database();
$db->connect();
include_once('../includes/variables.php');
include_once('verify-token.php');
include_once('../includes/custom-functions.php');
$fn = new custom_functions;
$config = $fn->get_configurations();
$time_slot_config = $fn->time_slot_config();
if (isset($config['system_timezone']) && isset($config['system_timezone_gmt'])) {
    date_default_timezone_set($config['system_timezone']);
    $db->sql("SET `time_zone` = '" . $config['system_timezone_gmt'] . "'");
} else {
    date_default_timezone_set('Asia/Kolkata');
    $db->sql("SET `time_zone` = '+05:30'");
}

/*
get-products-by-category-id.php
    accesskey:90336
    category_id:32
    user_id:369 {optional}
    limit:10 // {optional}
    offset:0 // {optional}
    sort:new / old / high / low // {optional}
*/
if (!verify_token()) {
    return false;
}

if (isset($_POST['accesskey']) && isset($_POST['user_id'])) {
    $access_key_received = $db->escapeString($fn->xss_clean($_POST['accesskey']));
    $user_id = (isset($_POST['user_id']) && is_numeric($_POST['user_id'])) ? $db->escapeString($fn->xss_clean($_POST['user_id'])) : "";
    $name = (isset($_POST['name'])) ? $db->escapeString($fn->xss_clean($_POST['name'])) : "";
    $gender = (isset($_POST['gender'])) ? $db->escapeString($fn->xss_clean($_POST['gender'])) : "";
    $gst_no = (isset($_POST['gst_no'])) ? $db->escapeString($fn->xss_clean($_POST['gst_no'])) : "";
    $business_name = (isset($_POST['business_name'])) ? $db->escapeString($fn->xss_clean($_POST['business_name'])) : "";
    $aadhar_no = (isset($_POST['aadhar_no'])) ? $db->escapeString($fn->xss_clean($_POST['aadhar_no'])) : "";


    if ($access_key_received == $access_key) {
        $setarray = [];
        if(!empty($name)){
            array_push($setarray, " `name`='$name'");
        }
        if(!empty($gender)){
            array_push($setarray, " `gender`='$gender'");
        }
        if(!empty($gst_no)){
            array_push($setarray, " `gst_no`='$gst_no'");
        }
        if(!empty($business_name)){
            array_push($setarray, " `business_name`='$business_name'");
        }
        if(!empty($aadhar_no)){
            array_push($setarray, " `aadhar_no`='$aadhar_no'");
        }

        if(!empty($name) || !empty($gender) || !empty($gst_no) || !empty($business_name) || !empty($aadhar_no))
        {
            $str = "set ".implode(',', $setarray);
            $sql = "update users ".$str." where id='".$user_id."' ";        
            if($db->customUpdate($sql)!=0)
            {
                $response['status'] = "Data Updated";
                $response['error'] = false;
            }
            else{
                $response['status'] = "Data Not Updated";
                $response['error'] = true;                
            }

        }
        else{
                $response['status'] = "Error";
                $response['error'] = true;
        }
        // $sql = "select * from pincodes where pincode='".$pincode."' ";        
        // $db->sql($sql);
        // $res = $db->getResult();
        // $response = [];
        // if(!empty($res)){ 
        //     $response['status'] = 'Available'; 
        //     $response['error']  = false;
        // }
        // else{
        //     $response['status'] = 'Not Available'; 
        //     $response['error']  = true;            
        // }
        // Getting product variants
        $output = json_encode($response);
    }
}

//Output the output.
echo $output;
$db->disconnect();
//to check if the string is json or not
function isJSON($string)
{
    return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
}

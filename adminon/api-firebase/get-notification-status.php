<?php
header('Access-Control-Allow-Origin: *');
session_start();
include '../includes/crud.php';
include_once '../includes/variables.php';
include_once '../includes/custom-functions.php';
$fn = new custom_functions;
include_once('verify-token.php');
$db = new Database();
$db->connect();
$response = array();
$config = $fn->get_configurations();
$time_slot_config = $fn->time_slot_config();
if (isset($config['system_timezone']) && isset($config['system_timezone_gmt'])) {
    date_default_timezone_set($config['system_timezone']);
    $db->sql("SET `time_zone` = '" . $config['system_timezone_gmt'] . "'");
} else {
    date_default_timezone_set('Asia/Kolkata');
    $db->sql("SET `time_zone` = '+05:30'");
}
/*
offer-images.php
    accesskey:90336
*/
if (!isset($_POST['accesskey'])) {
    if (!isset($_GET['accesskey'])) {
        $response['error'] = true;
        $response['message'] = "Access key is invalid or not passed!";
        print_r(json_encode($response));
        return false;
    }
}
if (isset($_POST['accesskey'])) {
    $accesskey = $db->escapeString($fn->xss_clean($_POST['accesskey']));
} else {
    $accesskey = $db->escapeString($fn->xss_clean($_GET['accesskey']));
}
if ($access_key != $accesskey) {
    $response['error'] = true;
    $response['message'] = "invalid accesskey!";
    print_r(json_encode($response));
    return false;
}
// if ((!isset($_REQUEST['ajax_call']))) {
//     if (!verify_token()) {
//         return false;
//     }
// }
if (isset($_POST['get-notif-status-by-fcmid']) && !empty($_POST['get-notif-status-by-fcmid'])) {
    if (!verify_token()) {
        return false;
    }
    $fcm_id = $_POST['fcm_id'];
    $curdate = date('Y-m-d');
    $sql = "select d.*,n.*,nc.name as category_name,nc.slug as slug from device_notifications as d inner join notifications as n on d.notification_id=n.id inner join notification_categories as nc where d.fcm_id='$fcm_id' and n.expiry_date>'$curdate'";
    $db->sql($sql);
    $result = $db->getResult();
    $response = $temp = $temp1 = array();
    if (!empty($result)) {
        $response['error'] = false;
        foreach ($result as $row) {
            $temp['notification_id'] = $row['notification_id'];
            $temp['title'] = $row['title'];
            $temp['message'] = $row['message'];
            $temp['type'] = $row['type'];
            $temp['type_id'] = $row['type_id'];
            $temp['notification_category_id'] = $row['notification_category_id'];
            $temp['category'] = $row['category_name'];
            $temp['category_slug'] = $row['slug'];
            $temp['image']=$row['image'];
            $temp['expiry_date']=$row['expiry_date'];
            $temp['status'] = $row['status'];
            $temp1[] = $temp;
        }
        $response['data'] = $temp1;
        // $response['curdate'] = $curdate;
    } else {
        $response['error'] = true;
        $response['message'] = "No notifications uploaded yet!";
    }
    print_r(json_encode($response));
}
<?php
header('Access-Control-Allow-Origin: *');
header("Content-Type: application/json");
header("Expires: 0");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
include('../includes/crud.php');
include('../includes/variables.php');
include_once('verify-token.php');
include_once('../includes/custom-functions.php');
$fn = new custom_functions;
$db = new Database();
$db->connect();
$response = array();
$config = $fn->get_configurations();
$time_slot_config = $fn->time_slot_config();
if (isset($config['system_timezone']) && isset($config['system_timezone_gmt'])) {
    date_default_timezone_set($config['system_timezone']);
    $db->sql("SET `time_zone` = '" . $config['system_timezone_gmt'] . "'");
} else {
    date_default_timezone_set('Asia/Kolkata');
    $db->sql("SET `time_zone` = '+05:30'");
}
$accesskey = $db->escapeString($fn->xss_clean($_POST['accesskey']));
if (!isset($_POST['accesskey']) || $access_key != $accesskey) {
    $response['error'] = true;
    $response['message'] = "invalid accesskey";
    print_r(json_encode($response));
    return false;
}
if (!verify_token()) {
    return false;
}
$settings = $setting = array();
    $sql = "select variable, value from `settings` where 1";
    $db->sql($sql);
    $res = $db->getResult();
    if (!empty($res)) {
        $settings['error'] = false;
        $settings['data'] = array();
        foreach ($res as $k => $v) {
            if ($v['variable'] == "system_timezone") {
                $system_timezone = (array)json_decode($v['value']);
                foreach ($system_timezone as $k => $v) {
                    if($k=='store_address'){
                    $settings['data'][$k] = $v;                        
                    }
                    if($k=='support_number'){
                    $settings['data'][$k] = $v;                        
                    }
                    if($k=='support_email'){
                    $settings['data'][$k] = $v;                        
                    }
                }
            }
        }
        print_r(json_encode($settings));
    } else {
        $settings['error'] = true;
        $settings['settings'] = "No settings found!";
        $settings['message'] = "Something went wrong!";
        print_r(json_encode($settings));
    }

$db->disconnect();

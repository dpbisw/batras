<?php
header('Access-Control-Allow-Origin: *');
session_start();
include '../includes/crud.php';
include_once '../includes/variables.php';
include_once '../includes/custom-functions.php';
$fn = new custom_functions;
include_once('verify-token.php');
$db = new Database();
$db->connect();
$response = array();
$config = $fn->get_configurations();
$time_slot_config = $fn->time_slot_config();
if (isset($config['system_timezone']) && isset($config['system_timezone_gmt'])) {
    date_default_timezone_set($config['system_timezone']);
    $db->sql("SET `time_zone` = '" . $config['system_timezone_gmt'] . "'");
} else {
    date_default_timezone_set('Asia/Kolkata');
    $db->sql("SET `time_zone` = '+05:30'");
}
/*
offer-images.php
    accesskey:90336
*/
if (!isset($_POST['accesskey'])) {
    if (!isset($_GET['accesskey'])) {
        $response['error'] = true;
        $response['message'] = "Access key is invalid or not passed!";
        print_r(json_encode($response));
        return false;
    }
}
if (isset($_POST['accesskey'])) {
    $accesskey = $db->escapeString($fn->xss_clean($_POST['accesskey']));
} else {
    $accesskey = $db->escapeString($fn->xss_clean($_GET['accesskey']));
}
if ($access_key != $accesskey) {
    $response['error'] = true;
    $response['message'] = "invalid accesskey!";
    print_r(json_encode($response));
    return false;
}
// if ((!isset($_REQUEST['ajax_call']))) {
//     if (!verify_token()) {
//         return false;
//     }
// }
if (isset($_POST['change-notif-status']) && !empty($_POST['change-notif-status'])) {
    if (!verify_token()) {
        return false;
    }
    $fcm_id = $_POST['fcm_id'];
    $notification_id = $_POST['notification_id'];
    $sql = "update device_notifications set status='1' where fcm_id='$fcm_id' and notification_id='$notification_id'";
    $rows = $db->customUpdate($sql);
    if ($rows == 1) {
        $response['error'] = false;
        $response['data'] = 'updated';
        // $response['curdate'] = $curdate;
    } else {
        $response['error'] = true;
        $response['message'] = "fail";
    }
    print_r(json_encode($response));
}
<?php
header('Access-Control-Allow-Origin: *');
session_start();
include '../includes/crud.php';
include_once '../includes/variables.php';
include_once '../includes/custom-functions.php';
$fn = new custom_functions;
include_once('verify-token.php');
$db = new Database();
$db->connect();
$response = array();
$config = $fn->get_configurations();
$time_slot_config = $fn->time_slot_config();
if (isset($config['system_timezone']) && isset($config['system_timezone_gmt'])) {
    date_default_timezone_set($config['system_timezone']);
    $db->sql("SET `time_zone` = '" . $config['system_timezone_gmt'] . "'");
} else {
    date_default_timezone_set('Asia/Kolkata');
    $db->sql("SET `time_zone` = '+05:30'");
}
/*
offer-images.php
    accesskey:90336
*/
if (!isset($_POST['accesskey'])) {
    if (!isset($_GET['accesskey'])) {
        $response['error'] = true;
        $response['message'] = "Access key is invalid or not passed!";
        print_r(json_encode($response));
        return false;
    }
}
if (isset($_POST['accesskey'])) {
    $accesskey = $db->escapeString($fn->xss_clean($_POST['accesskey']));
} else {
    $accesskey = $db->escapeString($fn->xss_clean($_GET['accesskey']));
}
if ($access_key != $accesskey) {
    $response['error'] = true;
    $response['message'] = "invalid accesskey!";
    print_r(json_encode($response));
    return false;
}
if ((!isset($_REQUEST['ajax_call']))) {
    if (!verify_token()) {
        return false;
    }
}
if (isset($_POST['top-deal-products']) && !empty($_POST['top-deal-products'])) {
    if (!verify_token()) {
        return false;
    }
    $sql = "SELECT count(v.product_id) as pc, p.id as product_id FROM order_items as o inner join product_variant as v on o.product_variant_id=v.id inner join products as p on v.product_id=p.id group by v.product_id order by pc desc limit 8";
    $db->sql($sql);
    $result = $db->getResult();
    $response = $temp = $temp1 = array();
    $pid_arr = [];
    if (!empty($result)) {
    	$pids='';
        $response['error'] = false;
        foreach ($result as $row) {
            $temp1[] = $temp;
            array_push($pid_arr, $row['product_id']);
        }
        
        $pids = implode(',', $pid_arr);
        $usq = "update section_products set product_ids='$pids' where section_name='Top Deals'";
	    $db->sql($usq);
	    $uresult = $db->getResult();
	    $response['data']="updated";
    } else {
        $usq = "update section_products set product_ids='' where section_name='Top Deals'";
        $db->sql($usq);
        $uresult = $db->getResult();
        $response['error'] = true;
        $response['message'] = "No offer images uploaded yet!";
    }
}
if(isset($_POST['get-topdeal-products']))
{
    echo "hi thwre";
}
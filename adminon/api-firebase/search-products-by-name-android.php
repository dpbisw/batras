<?php
header('Access-Control-Allow-Origin: *');
include_once('../includes/crud.php');
$db = new Database();
$db->connect();
include_once('../includes/variables.php');
include_once('verify-token.php');
include_once('../includes/custom-functions.php');
$fn = new custom_functions;
$config = $fn->get_configurations();
$time_slot_config = $fn->time_slot_config();
if (isset($config['system_timezone']) && isset($config['system_timezone_gmt'])) {
    date_default_timezone_set($config['system_timezone']);
    $db->sql("SET `time_zone` = '" . $config['system_timezone_gmt'] . "'");
} else {
    date_default_timezone_set('Asia/Kolkata');
    $db->sql("SET `time_zone` = '+05:30'");
}

/*
get-products-by-category-id.php
    accesskey:90336
    category_id:32
    user_id:369 {optional}
    limit:10 // {optional}
    offset:0 // {optional}
    sort:new / old / high / low // {optional}
*/
if (!verify_token()) {
    return false;
}

if (isset($_POST['accesskey']) && isset($_POST['keyword'])) {
    $access_key_received = $db->escapeString($fn->xss_clean($_POST['accesskey']));
    $keyword = (isset($_POST['keyword'])) ? $db->escapeString($fn->xss_clean($_POST['keyword'])) : "";
    $limit = $_POST['limit'];
    if ($access_key_received == $access_key) {
        if(strlen($keyword) >= 3){
        $sql = "select p.name as p_name,c.name as c_cname,p.image as image,p.slug as slug,c.slug as cslug from products as p join category as c on p.category_id=c.id";        
        $db->sql($sql);
        $res = $db->getResult();
        // Getting product variants

        $prods = [];
        $filprods=[];
        $fils=[];

        if(count($res) > 0){


            foreach ($res as $value) {
                $ip = [
                    'p_name'=>$value['p_name'],
                    'cname'=>$value['c_name'],
                    'image'=>$value['image'],
                    'slug'=>$value['slug'],
                    'cslug'=>$value['cslug'],
                    'pnamestr'=>explode(' ', strtolower($value['p_name']))
                ];
                array_push($prods, $ip);
            }

            foreach ($prods as $prd) {
                foreach ($prd['pnamestr'] as $strv) {
                    // array_push($fils,$prd);
                    if(substr( $strv, 0, strlen($keyword) ) === $keyword){
                    array_push($filprods,$prd);                        
                    }

                    // if(strpos($strv,$keyword)!==false){
                    // array_push($filprods,$prd);                        
                    // }
                }
            }



            for ($i = 0; $i < count($res); $i++) {
                $res[$i]['image'] = (!empty($res[$i]['image'])) ? DOMAIN_URL . '' . $res[$i]['image'] : '';
                // $res[$i]['image2'] = (!empty($res[$i]['image2'])) ? DOMAIN_URL . '' . $res[$i]['image2'] : '';
                // $res[$i]['web_image'] = (!empty($res[$i]['web_image'])) ? DOMAIN_URL . '' . $res[$i]['web_image'] : '';
            }


            $response['error'] = false;
            $response['data']  = $filprods;            
        }
        else{
            $response['error'] = true;
            $response['message']  = 'No products found';                        
        }            
        }
        else{
        $response['error'] = true;
        $response['message'] = 'please enter at least 3 characters';            
        }
    }
    else{
        $response['error'] = true;
        $response['message'] = 'invalid access key';
    }
}
else{
    $response['error'] = true;
    $response['message'] = 'no access key';
}

$output = json_encode($response);
//Output the output.
echo $output;
$db->disconnect();
//to check if the string is json or not
function isJSON($string)
{
    return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
}

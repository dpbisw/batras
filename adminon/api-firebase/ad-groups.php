<?php
header('Access-Control-Allow-Origin: *');
session_start();
include '../includes/crud.php';
include_once '../includes/variables.php';
include_once '../includes/custom-functions.php';
$fn = new custom_functions;
include_once('verify-token.php');
$db = new Database();
$db->connect();
$response = array();
$config = $fn->get_configurations();
$time_slot_config = $fn->time_slot_config();
if (isset($config['system_timezone']) && isset($config['system_timezone_gmt'])) {
    date_default_timezone_set($config['system_timezone']);
    $db->sql("SET `time_zone` = '" . $config['system_timezone_gmt'] . "'");
} else {
    date_default_timezone_set('Asia/Kolkata');
    $db->sql("SET `time_zone` = '+05:30'");
}
/*
offer-images.php
    accesskey:90336
*/
if (!isset($_POST['accesskey'])) {
    if (!isset($_GET['accesskey'])) {
        $response['error'] = true;
        $response['message'] = "Access key is invalid or not passed!";
        print_r(json_encode($response));
        return false;
    }
}
if (isset($_POST['accesskey'])) {
    $accesskey = $db->escapeString($fn->xss_clean($_POST['accesskey']));
} else {
    $accesskey = $db->escapeString($fn->xss_clean($_GET['accesskey']));
}
if ($access_key != $accesskey) {
    $response['error'] = true;
    $response['message'] = "invalid accesskey!";
    print_r(json_encode($response));
    return false;
}
if ((!isset($_REQUEST['ajax_call']))) {
    if (!verify_token()) {
        return false;
    }
}
if (isset($_POST['get-ad-groups']) && !empty($_POST['get-ad-groups'])) {
    if (!verify_token()) {
        return false;
    }
    $sql = 'select * from ad_groups where is_visible = 1 order by g_order asc';
    $db->sql($sql);
    $result = $db->getResult();
    $response = $temp = $temp1 = array();
    if (!empty($result)) {
        $response['error'] = false;
        foreach ($result as $row) {
            $temp['ad_group_id'] = $row['ad_group_id'];
            $temp['ad_group_type'] = $row['ad_group_type'];
            $temp['group_name'] = $row['group_name'];
            $temp['back_color'] = $row['back_color'];
            $temp['back_image'] = $row['back_image'];            
            $temp['image_per_row'] = $row['image_per_row'];
            $temp['is_third_party'] = $row['is_third_party'];

            $ad_group_id = $row['ad_group_id'];
            $sql2 = "select count(*) as count from ads where ad_group_id='$ad_group_id'";
            $db->sql($sql2);
            $result2 = $db->getResult();
            foreach ($result2 as $res) {
                $temp['no_of_ads'] = $res['count'];
            }
            $temp1[] = $temp;
        }
        $response['data'] = $temp1;
    } else {
        $response['error'] = true;
        $response['message'] = "No offer images uploaded yet!";
    }
    print_r(json_encode($response));
}

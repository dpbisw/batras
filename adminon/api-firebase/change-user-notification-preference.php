<?php
header('Access-Control-Allow-Origin: *');
session_start();
include '../includes/crud.php';
include_once '../includes/variables.php';
include_once '../includes/custom-functions.php';
$fn = new custom_functions;
include_once('verify-token.php');
$db = new Database();
$db->connect();
$response = array();
$config = $fn->get_configurations();
$time_slot_config = $fn->time_slot_config();
if (isset($config['system_timezone']) && isset($config['system_timezone_gmt'])) {
    date_default_timezone_set($config['system_timezone']);
    $db->sql("SET `time_zone` = '" . $config['system_timezone_gmt'] . "'");
} else {
    date_default_timezone_set('Asia/Kolkata');
    $db->sql("SET `time_zone` = '+05:30'");
}
/*
offer-images.php
    accesskey:90336
*/
if (!isset($_POST['accesskey'])) {
    if (!isset($_GET['accesskey'])) {
        $response['error'] = true;
        $response['message'] = "Access key is invalid or not passed!";
        print_r(json_encode($response));
        return false;
    }
}
if (isset($_POST['accesskey'])) {
    $accesskey = $db->escapeString($fn->xss_clean($_POST['accesskey']));
} else {
    $accesskey = $db->escapeString($fn->xss_clean($_GET['accesskey']));
}
if ($access_key != $accesskey) {
    $response['error'] = true;
    $response['message'] = "invalid accesskey!";
    print_r(json_encode($response));
    return false;
}
// if ((!isset($_REQUEST['ajax_call']))) {
//     if (!verify_token()) {
//         return false;
//     }
// }
if (isset($_POST['fcm_id'])) {
    if (!verify_token()) {
        return false;
    }
    $fcm_id = $_POST['fcm_id'];
    // $user_id = $_POST['user_id'];
    $notif_pref_id = $_POST['notif_pref_id'];
    $status = $_POST['status'];

    $notif_pref = '';
    $sql = "select notification_preferences,id from devices where fcm_id='$fcm_id'";
    $db->sql($sql);
    $user_result = $db->getResult();
    $id=0;
    foreach ($user_result as $value) {
        $notif_pref = $value['notification_preferences'];
        $id = $value['id'];
    }

    if($status == 0){
        $notif_pref = str_replace($notif_pref_id,'',$notif_pref);        
    }
    if($status == 1){
        if (strpos($notif_pref, $notif_pref_id) !== false) {
            
        }
        else{
        $notif_pref = $notif_pref.$notif_pref_id;            
        }
    }

    $sql = "update devices set notification_preferences='$notif_pref' where id='$id'";
    $rows = $db->customUpdate($sql);

    if ($rows == 1) {
        $response['error'] = false;
        $response['data'] = 'updated';
        // $response['curdate'] = $curdate;
    } else {
        $response['error'] = true;
        $response['message'] = "fail";
    }


    // $where_arr = [];
    // if(isset($fcm_id) && !empty($fcm_id)) array_push($where_arr, "fcm_id = '$fcm_id'");
    // if(isset($user_id) && !empty($user_id)) array_push($where_arr, "id = '$user_id'");
    // if(count($where_arr)>1) $where = " where " . implode(' and ', $where_arr);
    // else {
    //     $where = " where ".$where_arr[0];        
    // }
        
    // // if(!empty($user_id)){
    //     $$fcm_update_id = '';
    //     $user_sql = "select fcm_id from users $where";
    //     $db->sql($user_sql);
    //     $user_result = $db->getResult();
    //     foreach ($user_result as $value) {
    //         $fcm_update_id = $value['fcm_id'];
    //     }
    //     $u_sql = "select is_user_active,id from devices where fcm_id = '$fcm_update_id'";
    //     $db->sql($u_sql);
    //     $u_result = $db->getResult();
    //     foreach ($u_result as $value) {
    //         $status = 1;
    //         if($value['is_user_active'] == '1')
    //         {
    //             $status = 0;
    //         }
    //         if($value['is_user_active'] == '0')
    //         {
    //             $status = 1;
    //         }
    //         $id = $value['id'];
    //         $sql = "update devices set is_user_active='$status' where id='$id'";
    //         $rows = $db->customUpdate($sql);
    //         if ($rows == 1) {
    //             $response['status'] = 'updated';
    //             $response['error'] = false;
    //         }
    //         else{
    //             $response['status'] = 'not updated';
    //             $response['error'] = true;
    //         }
    //     }


    // $sql = "update device_notifications set status='1' where fcm_id='$fcm_id' and notification_id='$notification_id'";
    // $rows = $db->customUpdate($sql);
    // if ($rows == 1) {
    //     $response['error'] = false;
    //     $response['data'] = 'updated';
    //     // $response['curdate'] = $curdate;
    // } else {
    //     $response['error'] = true;
    //     $response['message'] = "fail";
    // }
    print_r(json_encode($response));
}
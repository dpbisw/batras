<?php
header('Access-Control-Allow-Origin: *');
session_start();
include '../includes/crud.php';
include_once '../includes/variables.php';
include_once '../includes/custom-functions.php';
$fn = new custom_functions;
include_once('verify-token.php');
$db = new Database();
$db->connect();
$response = array();
$config = $fn->get_configurations();
$time_slot_config = $fn->time_slot_config();
if (isset($config['system_timezone']) && isset($config['system_timezone_gmt'])) {
    date_default_timezone_set($config['system_timezone']);
    $db->sql("SET `time_zone` = '" . $config['system_timezone_gmt'] . "'");
} else {
    date_default_timezone_set('Asia/Kolkata');
    $db->sql("SET `time_zone` = '+05:30'");
}
/*
offer-images.php
    accesskey:90336
*/
if (!isset($_POST['accesskey'])) {
    if (!isset($_GET['accesskey'])) {
        $response['error'] = true;
        $response['message'] = "Access key is invalid or not passed!";
        print_r(json_encode($response));
        return false;
    }
}
if (isset($_POST['accesskey'])) {
    $accesskey = $db->escapeString($fn->xss_clean($_POST['accesskey']));
} else {
    $accesskey = $db->escapeString($fn->xss_clean($_GET['accesskey']));
}
if ($access_key != $accesskey) {
    $response['error'] = true;
    $response['message'] = "invalid accesskey!";
    print_r(json_encode($response));
    return false;
}
if ((!isset($_REQUEST['ajax_call']))) {
    if (!verify_token()) {
        return false;
    }
}
if (isset($_POST['user_id']) && !empty($_POST['user_id'])) {
    if (!verify_token()) {
        return false;
    }
    $user_id = $_POST['user_id'];
    $sql = "select u.*,c.name as city_name,a.name as area_name,a.minimum_free_delivery_order_amount as minimum_free_delivery_order_amount,a.delivery_charges as delivery_charges from user_addresses u LEFT JOIN city c ON c.id=u.city_id LEFT JOIN area a ON a.id=u.area_id where u.user_id=" . $user_id . " and u.is_default=1";
    $db->sql($sql);
    $result = $db->getResult();
    $response = $temp = $temp1 = array();
    if (!empty($result)) {
        $response['error'] = false;
        foreach ($result as $r) {	
	        $response['city_name'] = $r['city_name']; 
	        $response['pincode'] = $r['pincode'];        	
	        $response['area_name'] = $r['area_name'];        	
	        $response['minimum_free_delivery_order_amount'] = $r['minimum_free_delivery_order_amount'];
	        $response['delivery_charges'] = $r['delivery_charges'];        	

        }
    } else {
        $response['error'] = true;
        $response['message'] = "No offer images uploaded yet!";
    }
    print_r(json_encode($response));
}

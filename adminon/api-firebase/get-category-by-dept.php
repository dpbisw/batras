<?php
header('Access-Control-Allow-Origin: *');
session_start();
include '../includes/crud.php';
include_once '../includes/variables.php';
include_once '../includes/custom-functions.php';
$fn = new custom_functions;
include_once('verify-token.php');
$db = new Database();
$db->connect();
$response = array();
$config = $fn->get_configurations();
$time_slot_config = $fn->time_slot_config();
if (isset($config['system_timezone']) && isset($config['system_timezone_gmt'])) {
    date_default_timezone_set($config['system_timezone']);
    $db->sql("SET `time_zone` = '" . $config['system_timezone_gmt'] . "'");
} else {
    date_default_timezone_set('Asia/Kolkata');
    $db->sql("SET `time_zone` = '+05:30'");
}
/*
offer-images.php
    accesskey:90336
*/
if (!isset($_POST['accesskey'])) {
    if (!isset($_GET['accesskey'])) {
        $response['error'] = true;
        $response['message'] = "Access key is invalid or not passed!";
        print_r(json_encode($response));
        return false;
    }
}
if (isset($_POST['accesskey'])) {
    $accesskey = $db->escapeString($fn->xss_clean($_POST['accesskey']));
} else {
    $accesskey = $db->escapeString($fn->xss_clean($_GET['accesskey']));
}
if ($access_key != $accesskey) {
    $response['error'] = true;
    $response['message'] = "invalid accesskey!";
    print_r(json_encode($response));
    return false;
}
if ((!isset($_REQUEST['ajax_call']))) {
    if (!verify_token()) {
        return false;
    }
}
if (isset($_POST['department_id']) && !empty($_POST['department_id'])) {
    if (!verify_token()) {
        return false;
    }
    $department_id = (isset($_POST['department_id']) && !empty($_POST['department_id'])) ? $db->escapeString($fn->xss_clean($_POST['department_id'])) : 'id';

    $sql = "select * from category where department_id='$department_id'";
    $db->sql($sql);
    $result = $db->getResult();
    $response = $temp = $temp1 = array();
    if (!empty($result)) {

        for($i=0;$i < count($result);$i++)
        {
            // array_push($images, $result[$i]['image']);
            $result[$i]['image'] = DOMAIN_URL . '' . $result[$i]['image'];
        }


            $tmp = [];
            foreach ($result as $r) {
                $r['childs'] = [];

                $db->sql("SELECT * FROM subcategory WHERE category_id = '" . $r['id'] . "' ORDER BY id DESC");
                $childs = $db->getResult();
                if (!empty($childs)) {
                    for ($i = 0; $i < count($childs); $i++) {
                        $childs[$i]['image'] = (!empty($childs[$i]['image'])) ? DOMAIN_URL . '' . $childs[$i]['image'] : '';
                        $r['childs'][$childs[$i]['slug']] = (array)$childs[$i];
                    }
                }
                $tmp[] = $r;
            }
            $result = $tmp;
        $response['error'] = false;
        $response['data'] = $result;
    } else {
        $response['error'] = true;
        $response['message'] = "No category found";
    }
    print_r(json_encode($response));
}

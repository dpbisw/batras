<?php
include_once('crud.php');
/*
-----------------
Functions Additional By Deep Biswas 
-----------------
*/
class functions2
{
    protected $db;
    function __construct()
    {
        $this->db = new Database();
        $this->db->connect();
        date_default_timezone_set('Asia/Kolkata');
    }
    function editGroup($sql)
    {
        if ($this->db->sql($sql)) {
            return 1;
        } else {
            return 0;
        }       
    }
    public function getAdGroups()
    {
        $sql = "SELECT * FROM ad_groups order by g_order asc";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;
    }    
    public function getSelectedAdGroups($id)
    {
        $sql = "SELECT * FROM ad_groups where ad_group_type='$id'";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;
    }    
    public function getGroupContent($group_id,$ad_group_type)
    {
        if($ad_group_type == 1)
        {
            $sql = "SELECT * FROM ads where ad_group_id='$group_id'";
        }
        if($ad_group_type == 2)
        {
            $sql = "SELECT * FROM ads where ad_group_id='$group_id'";
        }
            $this->db->sql($sql);
            $res = $this->db->getResult();
            return $res;
    }
    public function getAllCategories()
    {
        $sql = "SELECT slug,name,id FROM category";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;        
    }
    public function getSubCategories()
    {
        $sql = "SELECT slug,name,id FROM subcategory";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;        
    }
    public function getFirstSubcategory()
    {
        $sql = "SELECT id FROM `category` order by id asc limit 1";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        $result = [];
        foreach($res as $rs)
        {
            $category_id = $rs['id'];
            $sql = "SELECT id,name FROM `subcategory` where category_id = '$category_id'";
            $this->db->sql($sql);
            $result = $this->db->getResult();                
        }
        return $result;                
    }
    public function getSlugById($id,$is_category)
    {
        if($is_category  == 1)
        {
            //Category slug
            $sql = "select slug from `category` where id='$id'";
            $this->db->sql($sql);
            $res = $this->db->getResult();
            return $res;
        }
        else{
            //Subcat slug
            $sql = "select slug from `subcategory` where id='$id'";
            $this->db->sql($sql);
            $res = $this->db->getResult();
            return $res;
        }
    }
    public function getNameBySlug($slug,$is_category)
    {
        if($is_category  == 1)
        {
            //Category slug
            $sql = "select name from `category` where slug='$slug'";
            $this->db->sql($sql);
            $res = $this->db->getResult();
            return $res;
        }
        else{
            //Subcat slug
            $sql = "select name from `subcategory` where slug='$slug'";
            $this->db->sql($sql);
            $res = $this->db->getResult();
            return $res;
        }
    }
    public function getNameById($id,$is_category)
    {
        if($is_category  == 1)
        {
            //Category slug
            $sql = "select name from `category` where id='$id'";
            $this->db->sql($sql);
            $res = $this->db->getResult();
            return $res;
        }
        else{
            //Subcat slug
            $sql = "select name from `subcategory` where id='$id'";
            $this->db->sql($sql);
            $res = $this->db->getResult();
            return $res;
        }
    }
    public function getAllProductRequests()
    {
            $sql = "select * from `product_requests`";
            $this->db->sql($sql);
            $res = $this->db->getResult();
            return $res;        
    }
    public function getAllUserListRequests()
    {
            $sql = "select u.name as name,u.email as email,u.mobile as mobile,ur.user_request_list_id as user_request_list_id,ur.created_at as created_at,ur.list_file as list_file,ur.kirana_subject as kirana_subject,ur.audio_file as audio_file from user_request_list as ur inner join users as u on ur.user_id = u.id order by ur.user_request_list_id desc";
            $this->db->sql($sql);
            $res = $this->db->getResult();
            return $res;        
    }    
   public function slugifyy($text){
    // replace non letter || digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);
    // trim
    $text = trim($text, '-');
    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);
    // lowercase
    $text = strtolower($text);
    if (empty($text)) {
        return 'n-a';
    }
    return $text;
}
    public function getCmspages()
    {
        $sql = "SELECT * FROM pages order by footer_column,c_order";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;
    }
    public function getCmsFooterColumns()
    {
        $sql = "SELECT DISTINCT footer_column FROM `pages` order by footer_column asc";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;
    }
    public function getCmspagesByFooter($footer_column)
    {
        $sql = "SELECT * FROM `pages` where footer_column = '$footer_column' order by row_order";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;
    }
    public function checkOrder($footer_column)
    {
        $sql = "SELECT * FROM pages where footer_column='$footer_column'";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;        
    }
    public function getFAQCategories()
    {
        $sql = "SELECT * FROM faq_categories order by faq_category_id desc";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;
    }
    public function getFAQCategories2()
    {
        $sql = "SELECT * FROM faq_categories order by faq_category_id asc";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;
    }
    public function getFAQByCategory($category_id)
    {
        $sql = "SELECT * FROM faq where faq_category_id ='$category_id'";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;
    }
    public function getBrands()
    {
        $sql = "SELECT * FROM brands order by brand_id desc";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;
    }
    public function getBrandProducts($id)
    {
        $sql = "SELECT * FROM products as p inner join product_variant as v on p.id=v.product_id where p.brand_id='$id'";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;
    }
    public function getAttributes()
    {
        $sql = "SELECT * FROM attributes order by attribute_id desc";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;        
    }
    public function getPins()
    {
        $sql = "SELECT * FROM pincodes as p inner join area as a on p.area_id=a.id";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;        
    }
    public function getStates()
    {
        $sql = "SELECT * FROM states order by id desc";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;        
    }    
    public function getStateNameById($id)
    {
        $sql = "SELECT state_name FROM states where id='$id'";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        $state_name='';
        if($res){
            foreach($res as $r){
                $state_name = $r['state_name'];
            }
        }
        return $state_name;
    }    

    public function getAttributeValues()
    {
        $sql = "SELECT * FROM attribute_values as v inner join attributes as a on a.attribute_id = v.attribute_id order by v.attr_value_id desc";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;        
    }
    public function getReqDetail($detail_id){
            $sql = "select * from user_request_list inner join users on user_request_list.user_id = users.id where user_request_list.user_request_list_id='$detail_id'";
            $this->db->sql($sql);
            $res = $this->db->getResult();
            return $res;        
    }
    public function getDepartments()
    {
        $sql = "SELECT * FROM departments order by row_order asc";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;                
    }
    public function getCategoriesByDepartment($dept_id)
    {
        $sql = "SELECT * FROM category where department_id='$dept_id' order by sub_menu_order asc";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;                
    }
    public function getMainSliderImages()
    {
        $sql = "SELECT * FROM slider order by row_order asc";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;                        
    }
    public function getVariantAttribute($variant_id)
    {
        $sql = "SELECT attributes FROM product_variant where id='$variant_id'";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;                        
    }
    function decrypt($key,$encrypted)
    {
        $mcrypt_cipher = MCRYPT_RIJNDAEL_128;
        $mcrypt_mode = MCRYPT_MODE_CBC;
        $iv=$key . "\0\0\0\0";
        $key=$key . "\0\0\0\0";
                    
        $encrypted = base64_decode($encrypted);     
        $decrypted = rtrim(mcrypt_decrypt($mcrypt_cipher, $key, $encrypted, $mcrypt_mode, $iv), "\0");
        if($decrypted != '')
        {
            $dec_s2 = strlen($decrypted);
            $padding = ord($decrypted[$dec_s2-1]);
            $decrypted = substr($decrypted, 0, -$padding);
        }
        return $decrypted;
    }
    public function getMargUpdateLogs()
    {
        $sql = "SELECT m.*,a.username as name FROM marg_update_record as m inner join admin as a on m.admin_id=a.id order by m.updated_at desc";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;                        
    }
    public function getTypeFromProduct($product_id)
    {
        $sql = "select v.type as type from products as p inner join product_variant as v on p.id=v.product_id where p.id='$product_id' limit 1";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        return $res;                                
    }
    public function getAttributeName($attribute_id)
    {
        $sql = "SELECT attribute_name FROM attributes where attribute_id='$attribute_id'";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        $attr_name = '';
        foreach ($res as $r) {
            $attr_name = $r['attribute_name'];
        }
        return $attr_name;
        // return $attribute_id;
    }
    public function getAttributeValueName($attribute_value_id)
    {
        $sql = "SELECT attribute_name FROM attributes where attr_value_id='$attribute_value_id'";
        $this->db->sql($sql);
        $res = $this->db->getResult();
        $attr_name = '';
        foreach ($res as $r) {
            $attr_name = $r['attribute_value'];
        }
        return $attr_name;
    }
}

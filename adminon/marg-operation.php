<?php 
session_start();
include('includes/crud.php');
$db = new Database();
$db->connect();
$db->sql("SET NAMES 'utf8'");
$auth_username = $db->escapeString($_SESSION["user"]);
include_once('includes/custom-functions.php');
$fn = new custom_functions;
include_once('includes/functions.php');
include_once('includes/functions2.php');
$function = new functions;
$fn2 = new functions2;
$permissions = $fn->get_permissions($_SESSION['id']);
$config = $fn->get_configurations();
$time_slot_config = $fn->time_slot_config();

if(isset($_GET['insert-marg']) && !empty($_GET['insert-marg'])){
?>
<div style="display: flex;justify-content: center;">
<div>
<script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
<lottie-player src="https://assets3.lottiefiles.com/packages/lf20_jvkRrK.json"  background="transparent"  speed="1"  style="width: 300px; height: 300px;"  loop  autoplay></lottie-player>	
<h3>Uploading MARG Data</h3>
<p>Please Wait..</p>	
</div>
</div>


 <!-- header('Content-Type: application/json'); -->
<?php


function decrypt($key,$encrypted)
{
	$mcrypt_cipher = MCRYPT_RIJNDAEL_128;
	$mcrypt_mode = MCRYPT_MODE_CBC;
	$iv=$key . "\0\0\0\0";
	$key=$key . "\0\0\0\0";
				
	$encrypted = base64_decode($encrypted);		
	$decrypted = rtrim(mcrypt_decrypt($mcrypt_cipher, $key, $encrypted, $mcrypt_mode, $iv), "\0");
	if($decrypted != '')
	{
		$dec_s2 = strlen($decrypted);
		$padding = ord($decrypted[$dec_s2-1]);
		$decrypted = substr($decrypted, 0, -$padding);
	}
	return $decrypted;
}

$ch = curl_init( "https://wservices.margcompusoft.com/api/eOnlineData/MargMST2017");
$payload = json_encode( array("CompanyCode" => "BATRAWHOLESALE2","MargID" => 226752,"Datetime" => "", "index" => 0));



curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );

$response = curl_exec($ch);
$err = curl_error($ch);
curl_close($ch);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
}

$key = '61YLTIABE63O';
// echo '<br>';
// echo "Decrypted Data ";   
// echo '<br>';

$result = decrypt($key,$response);
// print_r($result);

$data = gzinflate(base64_decode($result));

// $je = mb_convert_encoding($data, 'UTF-8', 'UTF-8') ;


function correct_encoding($text) {
    $current_encoding = mb_detect_encoding($text, 'auto');
    $text = iconv($current_encoding, 'UTF-8', $text);
    return $text;
}


$json_string = correct_encoding($data);



$newd = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $json_string), true );

// foreach($newd as $k=> $v){
//     echo $k;
// }

$pro_N = $newd['Details']['pro_N'];
$pro_U = $newd['Details']['pro_U'];
$pro_S = $newd['Details']['pro_S'];
$pro_R = $newd['Details']['pro_R'];

// if(!empty($pro_N)){
// 	  $sqltr = "TRUNCATE TABLE marg_update_products";
//       $db->sql($sqltr);
// foreach($pro_N as $nd){
//         $code = $nd['code'];
//         $mrp  = $nd['MRP'];
//         $rate = $nd['Rate'];
//         $stock = (int)$nd['stock'];
//         $date = date("Y-m-d h:i:s");
//              $sql = "insert into marg_update_products (`code`,`stock`,`discounted_price`,`price`,`created_at`) values ('$code','$stock','$rate','$mrp','$date')";
//             $db->sql($sql);
// }	
// }
if(!empty($pro_N) || !empty($pro_U) || !empty($pro_S || !empty($pro_R))){
	  $sqltr = "TRUNCATE TABLE marg_update_products";
      $db->sql($sqltr);

foreach($pro_N as $nd){
        $code = $nd['code'];
        $mrp  = $nd['MRP'];
        $rate = $nd['Rate'];
        $stock = (int)$nd['stock'];
        $date = date("Y-m-d h:i:s");
             $sql = "insert into marg_update_products (`code`,`stock`,`discounted_price`,`price`,`created_at`) values ('$code','$stock','$rate','$mrp','$date')";
            $db->sql($sql);
}

if(!empty($pro_U)){
	foreach($pro_U as $nd){
	        $code = $nd['code'];
	        $mrp  = $nd['MRP'];
	        $rate = $nd['Rate'];
	        $stock = (int)$nd['stock'];
	        $date = date("Y-m-d h:i:s");
	             $sql = "insert into marg_update_products (`code`,`stock`,`discounted_price`,`price`,`created_at`) values ('$code','$stock','$rate','$mrp','$date')";
	            $db->sql($sql);
	}	
}

if(!empty($pro_S)){
	foreach($pro_S as $nd){
	        $code = $nd['code'];
	        $mrp  = $nd['MRP'];
	        $rate = $nd['Rate'];
	        $stock = (int)$nd['stock'];
	        $date = date("Y-m-d h:i:s");
	             $sql = "insert into marg_update_products (`code`,`stock`,`discounted_price`,`price`,`created_at`) values ('$code','$stock','$rate','$mrp','$date')";
	            $db->sql($sql);
	}	
}

if(!empty($pro_R)){
	foreach($pro_R as $nd){
	        $code = $nd['code'];
	        $mrp  = $nd['MRP'];
	        $rate = $nd['Rate'];
	        $stock = (int)$nd['stock'];
	        $date = date("Y-m-d h:i:s");
	             $sql = "insert into marg_update_products (`code`,`stock`,`discounted_price`,`price`,`created_at`) values ('$code','$stock','$rate','$mrp','$date')";
	            $db->sql($sql);
	}	
}

}    
    $date = date("Y-m-d h:i:s");			
		$admin_id = $_SESSION['id'];
      $sql = "insert into marg_update_record (`admin_id`,`updated_at`,`status`) values ('$admin_id','$date','2')";
      $db->sql($sql);
      ?>
      <script>
      	window.location.href="marg-insert.php";
      </script>
      <?php
}




// NEW PRODUCT UPLOAD


if(isset($_GET['insert-marg-new']) && !empty($_GET['insert-marg-new'])){
?>
<div style="display: flex;justify-content: center;">
<div>
<script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
<lottie-player src="https://assets3.lottiefiles.com/packages/lf20_jvkRrK.json"  background="transparent"  speed="1"  style="width: 300px; height: 300px;"  loop  autoplay></lottie-player>	
<h3>Uploading MARG Data</h3>
<p>Please Wait..</p>	
</div>
</div>


 <!-- header('Content-Type: application/json'); -->
<?php


function decrypt($key,$encrypted)
{
	$mcrypt_cipher = MCRYPT_RIJNDAEL_128;
	$mcrypt_mode = MCRYPT_MODE_CBC;
	$iv=$key . "\0\0\0\0";
	$key=$key . "\0\0\0\0";
				
	$encrypted = base64_decode($encrypted);		
	$decrypted = rtrim(mcrypt_decrypt($mcrypt_cipher, $key, $encrypted, $mcrypt_mode, $iv), "\0");
	if($decrypted != '')
	{
		$dec_s2 = strlen($decrypted);
		$padding = ord($decrypted[$dec_s2-1]);
		$decrypted = substr($decrypted, 0, -$padding);
	}
	return $decrypted;
}

$ch = curl_init( "https://wservices.margcompusoft.com/api/eOnlineData/MargMST2017");
$payload = json_encode( array("CompanyCode" => "BATRAWHOLESALE2","MargID" => 226752,"Datetime" => "", "index" => 0));



curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );

$response = curl_exec($ch);
$err = curl_error($ch);
curl_close($ch);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
}

$key = '61YLTIABE63O';
// echo '<br>';
// echo "Decrypted Data ";   
// echo '<br>';

$result = decrypt($key,$response);
// print_r($result);

$data = gzinflate(base64_decode($result));

// $je = mb_convert_encoding($data, 'UTF-8', 'UTF-8') ;


function correct_encoding($text) {
    $current_encoding = mb_detect_encoding($text, 'auto');
    $text = iconv($current_encoding, 'UTF-8', $text);
    return $text;
}


$json_string = correct_encoding($data);



$newd = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $json_string), true );

// foreach($newd as $k=> $v){
//     echo $k;
// }

$pro_N = $newd['Details']['pro_N'];
if(!empty($pro_N)){
	  $sqltr = "TRUNCATE TABLE marg_update_products";
      $db->sql($sqltr);

foreach($pro_N as $nd){
        $code = $nd['code'];
        $mrp  = $nd['MRP'];
        $rate = $nd['Rate'];
        $stock = (int)$nd['stock'];
        $date = date("Y-m-d h:i:s");
             $sql = "insert into marg_update_products (`code`,`stock`,`discounted_price`,`price`,`created_at`) values ('$code','$stock','$rate','$mrp','$date')";
            $db->sql($sql);
}

}    
    $date = date("Y-m-d h:i:s");			
		$admin_id = $_SESSION['id'];
      $sql = "insert into marg_update_record (`admin_id`,`updated_at`,`status`) values ('$admin_id','$date','2')";
      $db->sql($sql);
      header('Location: marg-insert.php');
}


if(isset($_GET['update-marg']) && !empty($_GET['update-marg'])){
	    $sql = "select * from marg_update_products";
      $db->sql($sql);
			$up_products = $db->getResult();

			$sqlv = "select id,product_unique_id from product_variant";
			$db->sql($sqlv);
			$v_products = $db->getResult();			
			foreach ($v_products as $vp) {
				if($vp['product_unique_id']!=''){

					foreach ($up_products as $value) {
						if($value['code'] == $vp['product_unique_id'])
						{
							$stock = $value['stock'];
							$price = $value['price'];
							$vid = $vp['id'];
							$discounted_price = $value['discounted_price'];
	            $sql = "UPDATE product_variant set `stock`='".$stock."',`price`='".$price."',`discounted_price`='".$discounted_price."' where `id`=".$vid;
	            $db->sql($sql);
						}
					}					 

				}
			}

			$sqltr = "TRUNCATE TABLE marg_update_products";
      $db->sql($sqltr);

		$admin_id = $_SESSION['id'];
      $sql = "update marg_update_record set `status`='1' where `admin_id`='$admin_id'";
      $db->sql($sql);
      header('Location: marg-insert.php');

}

?>
<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Kreait\Firebase\Factory;
class HomeController extends Controller{
    public function index(){
        
        $title = "Ecommerce | Full Store Website";
        $this->html('home', ['title'=> $title], true);
        
    }
    public function page($slug){
        $page = array('privacy-policy' => 'privacy_policy', 'tnc' => 'terms_conditions', 'about' => 'about_us');
        $title = array('privacy-policy' => 'Privacy Policy', 'tnc' => 'Terms & Conditions', 'about' => 'About Us');
        if(isset($page[$slug]) && Cache::has($page[$slug])){
            $this->html('page', ['title'=> $title[$slug], 'content' => Cache::get($page[$slug])]);
        }else{
            abort(404);
        }
    }
    public function faq(Request $request){
        $page = 0;
        if($request->has('page') && intval($request->page)){
            $page = intval($request->page);
        }
        $limit = 50;
        $result = $this->post('faq', ['data' => [api_param('search') => $request->search ?? '', api_param('limit') => $limit, api_param('page') => $page+1], 'data_param' => '']);
        if(isset($result['error']) && $result['error'] === false){
            $breadcrumb = array();
            $breadcrumb[] = array('title' => 'Home', 'link' => route('home'));
    
            $breadcrumb[] = array('title' => 'Faq', 'link' => '#');
    
            $this->html('faq', ['title' => __('msg.faq'), 'breadcrumb'=> $breadcrumb, 'faq' => $result['data'], 'page' => $page, 'limit' => $limit, 'total' => $result['total']]);
        }else{
            
            abort(404);
        }
    }
    
    public function contact_page(Request $request){
       $this->html('contact', ['title' => __('msg.contact_page')]);
        
    }
    public function product(Request $request,$slug){
        // $data = ['slug' => $slug];
        // if(isLoggedIn()){
        //     $data[api_param('user-id')] = session()->get('user')['user_id'];
        // }
        // $product = $this->post('get-product', ['data' => $data]);
        // if(count($product) && !(isset($product['error']) && $product['error']) && isset($product[0])){
        //     $product = $product[0];
        //     $similarProducts = array();
        //     $data = [api_param('category-id') => $product->category_id, api_param('product-id') => $product->id, api_param('get_similar_products') => 1];
            
        //     $result = $this->post('get-similar-products', ['data' => $data]);
            
        //     if(!isset($result['error'])){
        //         foreach($result as $r){
        //             if($r->slug != $slug){
        //                 $similarProducts[] = $r;
        //             }
        //         }
        //     }
        //     $dont_know_prods = [];
        //     if($product->dont_forget_at_add_products!=0){
        //     $dont_know_prod_data = explode(',',$product->dont_forget_at_add_products);
        //         foreach ($dont_know_prod_data as $dp) {
        //             $data2 = ['product_id'=>$dp];
        //             $product2 = $this->post('get-product', ['data' => $data2]);
        //             array_push($dont_know_prods, $product2);                    
        //         }
        //     }
        //     $this->html('product', ['title' => $product->name, 'product' => $product, 'similarProducts' => $similarProducts,'dont_know_prods' => $dont_know_prods]);
        // }else{
        //     abort(404);
        // } 

        if(empty($request->input())){
        $data = ['slug' => $slug];
        if(isLoggedIn()){
            $data[api_param('user-id')] = session()->get('user')['user_id'];
        }
        $product = $this->post('get-product', ['data' => $data]);
        if(count($product) && !(isset($product['error']) && $product['error']) && isset($product[0])){
            $product = $product[0];
            $similarProducts = array();
            $data = [api_param('category-id') => $product->category_id, api_param('product-id') => $product->id, api_param('get_similar_products') => 1];
            
            $result = $this->post('get-similar-products', ['data' => $data]);
            
            if(!isset($result['error'])){
                foreach($result as $r){
                    if($r->slug != $slug){
                        $similarProducts[] = $r;
                    }
                }
            }
            $dont_know_prods = [];
            if($product->dont_forget_at_add_products!=0){
            $dont_know_prod_data = explode(',',$product->dont_forget_at_add_products);
                foreach ($dont_know_prod_data as $dp) {
                    $data2 = ['product_id'=>$dp];
                    $product2 = $this->post('get-product', ['data' => $data2]);
                    array_push($dont_know_prods, $product2);                    
                }
            }

            $svariant = '';
            $av=[];
            $attrb_values = '';
            $attrbs = '';
            if(!empty($product->variants) && !empty($product->variants[0]->attributes)){
            $svariant = $product->variants[0];
            $svarattrs = $svariant->attributes;
            if(strpos($svarattrs, ' ') !== false)
            {
                $ar=explode(" ", $svarattrs);
                foreach ($ar as $a) {
                    $r=explode(":", $a);
                    array_push($av, $r[1]);
                }
            }
            else{
                $ar=explode(":", $svarattrs);
                array_push($av, $ar[1]);
            }
            $attrb_values = $product->attr_values;
            $attrbs = $product->attrs;
        }

            // dd(['title' => $product->name, 'product' => $product, 'similarProducts' => $similarProducts,'dont_know_prods' => $dont_know_prods]);

             $this->html('product', ['title' => $product->name, 'product' => $product, 'similarProducts' => $similarProducts,'dont_know_prods' => $dont_know_prods,'attributes'=>$attrbs,'attribute_values'=>$attrb_values,'svariant'=>$svariant,'attrvars'=>$av]);

        }else{
            abort(404);
        } 
    }
        else{
            $strarr=[];
            $valuearr = [];
            if(count($request->input())==1){
            foreach ($request->input() as $key => $value) {
                array_push($strarr, $key.":".$value);
                array_push($valuearr, $value);
            }
            }
            else{
                $strarr = [];
                foreach ($request->input() as $key => $value) {
                array_push($strarr, $key.":".$value);
                array_push($valuearr, $value);

                }
            }
        $data = ['slug' => $slug,'attributes'=>$strarr];
        if(isLoggedIn()){
            $data[api_param('user-id')] = session()->get('user')['user_id'];
        }
        $product = $this->post('get-product', ['data' => $data]);

        // $product = $this->post('get-product-variant', ['data' => $data]);
        $product=$product[0];

            $similarProducts = array();
            $data = [api_param('category-id') => $product->category_id, api_param('product-id') => $product->id, api_param('get_similar_products') => 1];
            
            $result = $this->post('get-similar-products', ['data' => $data]);
            if(!isset($result['error'])){
                foreach($result as $r){
                    if($r->slug != $slug){
                        $similarProducts[] = $r;
                    }
                }
            }
            $dont_know_prods = [];
            if($product->dont_forget_at_add_products!=0){
            $dont_know_prod_data = explode(',',$product->dont_forget_at_add_products);
                foreach ($dont_know_prod_data as $dp) {
                    $data2 = ['product_id'=>$dp];
                    $product2 = $this->post('get-product', ['data' => $data2]);
                    array_push($dont_know_prods, $product2);                    
                }
            }
            $svariant = $product->selected_variant;

            $this->html('product', ['title' => $product->name, 'product' => $product, 'similarProducts' => $similarProducts,'dont_know_prods' => $dont_know_prods,'attributes'=>$product->attrs,'attribute_values'=>$product->attr_values,'svariant'=>$svariant,'valuearr'=>implode(',',$valuearr)]);
        }
        
        
    }
    public function get_shop_params_category(Request $request){
        $selectedCategoryIds = [];
        $selectedSubCategoryIds = [];
        $selectedSubCategory = explode(",", $request->get('sub-category', ""));
        $selectedCategory = explode(",", $request->get('category', ""));
        $categories = Cache::get('categories');
        foreach($selectedCategory as $cat){
            if(isset($categories[$cat]->childs)){
                $selectedCategoryIds[intval($categories[$cat]->id ?? 0)] = intval($categories[$cat]->id ?? 0);
                $childs = (array)$categories[$cat]->childs;
                foreach($selectedSubCategory as $sub){
                    if(isset($childs[$sub])){
                        unset($selectedCategoryIds[intval($categories[$cat]->id ?? 0)]);
                    
                        $selectedSubCategoryIds[] = intval($childs[$sub]->id ?? 0);
        
                    }
        
                }
            }
        }
        return ['selectedCategory' => $selectedCategory, 'selectedCategoryIds' => $selectedCategoryIds, 'selectedSubCategory' => $selectedSubCategory, 'selectedSubCategoryIds' => $selectedSubCategoryIds];
    }
    public function get_shop_params(Request $request){
    
        $limit = 40;
        $page = $request->page ?? 0;
        $data = ['limit' => $limit, 'offset' => ($page * $limit)];
        
        $param = [];
        if(isLoggedIn()){
            $data[api_param('user-id')] = session()->get('user')['user_id'];
        }
        $data['s'] = trim($request->s ?? "");
        $param['s'] = trim($request->s ?? "");
        if($request->has('section') && trim($request->section) != ""){
            $sections = Cache::get('sections');
            if(isset($sections[$request->section])){
                $data['section'] = intval($sections[$request->section]->id ?? 0);
                $param['section'] = trim($request->section);
            }
        }
        extract($this->get_shop_params_category($request));
        $param['sub-category'] = trim($request->{'sub-category'});
        $param['category'] = trim($request->category);
        $data['category'] = implode(",", $selectedCategoryIds);
        $data['sub-category'] = implode(",", $selectedSubCategoryIds);
        $data[api_param('sort')] = trim($request->sort ?? "");
        $param[api_param('sort')] = trim($request->sort ?? "");
        
        $param['min_price'] = trim($request->{'min_price'});
        $param['max_price'] = trim($request->{'max_price'});
        $data['min_price'] = $request->min_price ?? 0;
        $data['max_price'] = $request->max_price ?? 0;
        return ['data' => $data, 'param' => $param, 'page' => $page, 'limit' => $limit, 'selectedCategory' => $selectedCategory, 'selectedSubCategory' => $selectedSubCategory];
    }
    public function shop(Request $request){

        $pr = [];
        $prods_data = [];
        if($request->has('section') && trim($request->section) != ""){
            $sections = Cache::get('sections');
            if(isset($sections[$request->section])){
                $prods_data = $sections[$request->section]->product_ids;

                foreach ($prods_data as $prod) {
                    $data = ['product_id'=>$prod];
                    $product = $this->post('get-product', ['data' => $data]);
                    array_push($pr, $product);
                }
            }
        }

        extract($this->get_shop_params($request));
        $list = $this->post('shop', ['data' => $data, 'data_param' => '']);
        $total = $list['total'] ?? 0;
        $min_price = $list['min_price'] ?? 0;
        $max_price = $list['max_price'] ?? 0;
        
        $selectedMaxPrice = ($request->max_price ?? 0) < $max_price ? $max_price : ($request->max_price ?? 0);
        $selectedMinPrice = ($request->min_price ?? 0) < $min_price ? $min_price : ($request->min_price ?? 0);
        if(isset($list['category']) && is_array($list['category']) && count($list['category'])){
            
            $this->update_categories($list['category']);
        }
        $lastPage = "";
        if(intval($page - 1) > -1){
            if(intval($page - 1) == 0){
                $lastPage = route('shop', $param);
            }else{
                $param['page'] = $page - 1;
                $lastPage = route('shop', $param);
            }
        }
        $nextPage = "";
        if(intval($total / $limit) > $page){
            $param['page'] = $page + 1;
            $nextPage = route('shop', $param);
        }
        $number_of_pages= $total / $limit;
        $categories = Cache::get('categories', []);
        $this->html('shop', ['title' => __('msg.shop'), 'list' => $list, 'limit' => $limit, 'total' => $total, 'min_price' => $min_price, 'max_price' => $max_price, 'selectedMinPrice' => $selectedMinPrice, 'selectedMaxPrice' => $selectedMaxPrice, 'next' => $nextPage, 'last' => $lastPage, 'categories' => $categories, 'selectedCategory' => $selectedCategory, 'selectedSubCategory' => $selectedSubCategory, 'number_of_pages' => $number_of_pages,'section-prods'=>$prods_data]);
    }

    public function store(Request $request)
    {
        $stores = Cache::get('stores');
        $this->html('store',['title'=>'Store','store'=>$stores]);
    }

    public function category(Request $request, $slug){
        $breadcrumb = array();
        $breadcrumb[] = array('title' => 'Home', 'link' => route('home'));
        $breadcrumb[] = array('title' => 'Shop', 'link' => route('shop'));
    	if($slug != 'all'){
        if(Cache::has('categories') && is_array(Cache::get('categories')) && isset(Cache::get('categories')[$slug])){
            $category = Cache::get('categories')[$slug];
            $subCategory = $this->post('get-sub-categories', ['data' => [api_param('category-id') => $category->id]]);
            if(isset($subCategory['error']) && $subCategory['error']){
                return redirect()->route('shop')->with('err', 'No Sub Category Found.');
            }
            $s = Cache::get('sub-categories') ?? [];
            foreach($subCategory as $t){
                $s[$t->slug] = $t;
            }
            Cache::put('sub-categories', $s);
            $breadcrumb[] = array('title' => $category->name, 'link' => route('category', $category->slug));
            
            $this->html('sub-categories', ['title' => $category->name, 'breadcrumb' => $breadcrumb, 'category' => $category,'sub-categories' => $subCategory]);
        }else{
            return redirect()->route('shop');
        }
    }
    else if($slug == 'all'){
    	if(Cache::has('categories') && is_array(Cache::get('categories')))
    	{
    		$categories = Cache::get('categories');
            $this->html('category', ['title' => 'All Category', 'breadcrumb' => $breadcrumb, 'categories' => $categories]);
    	}
    }
    }
    public function sub_category(Request $request, $categorySlug = "", $subCategorySlug = "", $offset = 0){
        $subTitle = '';
        $products = [];
        $total = 0;
        $breadcrumb = array();
        $breadcrumb[] = array('title' => 'Home', 'link' => route('home'));
        $breadcrumb[] = array('title' => 'Shop', 'link' => route('shop'));
        if(Cache::has('categories') && is_array(Cache::get('categories')) && isset(Cache::get('categories')[$categorySlug])){
            $category = Cache::get('categories')[$categorySlug];
            $breadcrumb[] = array('title' => $category->name, 'link' => route('category', $category->slug));
            $more = false;
            
            if(Cache::has('sub-categories') && isset(Cache::get('sub-categories')[$subCategorySlug])){
                $subCategory = Cache::get('sub-categories')[$subCategorySlug];
                $title = $subCategory->name;
                $breadcrumb[] = array('title' => $title, 'link' => route('sub-category', [$category->slug, $subCategory->slug]));
                $response = $this->post('get-products-by-subcategory', ['data_param' => '', 'data' => [api_param('limit') => get('load-item-limit'), api_param('offset') => intval($offset * get('load-item-limit')), api_param('sub-category-id') => $subCategory->id]]);
                if(isset($response['data']) && is_array($response['data']) && count($response['data'])){
                    $products = $response['data'];
    
                    $total = $response['total'];
    
                    if($total > ($response['limit'] ?? get('load-item-limit'))){
        
                        $more = true;
        
                    }
    
                }
                $this->html('shop', ['title' => $title, 'subTitle' => $subTitle, 'products' => $products, 'more' => $more, 'breadcrumb' => $breadcrumb, 'total' => $total]);
            }else{
                return redirect()->route('category', $categorySlug);
            }
        }else{
            return redirect()->route('shop');
        }
    }
    public function login_page(){
        
        if(isLoggedIn()){
            return redirect()->route('home');
        }
        $this->html('login', ['title' => __('msg.login')]);
    }
    public function login(Request $request){
        $error = msg('error');
        $validator = Validator::make($request->all(), [
            'mobile' => 'required',
            'password' => 'required',
        ]);
		if ($validator->fails()) {
            $errors = $validator->messages()->all();
            $response['message'] = $errors[0];
		}else{
            
            $login = $this->post('login', ['data' => [api_param('mobile') => $request->mobile, api_param('password') => $request->password]]);
            if(isset($login['user_id']) && intval($login['user_id'])){
                $msg = $login['message'];
                unset($login['message']);
                unset($login['error']);
                $request->session()->put('user', $login);
                $lastUrl = $request->get('last_url', '');
                if(trim($lastUrl) != ''){
                    return redirect()->to($lastUrl);
                }else{
                    return redirect()->route('home')->with('suc', $msg);
                }
            }elseif(isset($login['message']) && trim($login['message']) != ""){
                $error = $login['message'];
            }
            return back()->with("err", $error);
        }
    }
    public function already_registered(Request $request){
        $response = ['error' => false, 'message' => 'Enter Valid Mobile Number'];
        if(strlen(trim($request->get('mobile', ""))) > 0){
            $response = $this->post('user-registration', ['data' => [api_param('mobile') => $request->mobile, api_param('type') => api_param('verify-user')]]);
            if($response['error']){
                session()->put('temp_user', $response);
            }
        }
        echo json_encode($response);
    }
    public function register(Request $request){
        $factory = (new Factory)->withServiceAccount(base_path('config/firebase.json'));
        $auth = $factory->createAuth();
        $user = $auth->getUser($request->auth_uid);
        if($user->uid == $request->auth_uid){
            if($request->has('action') && $request->action == 'save'){
                $response = array('error' => true, 'message' => 'Something Went Wrong');
                $validator = Validator::make($request->all(), [
                    'password' => 'required|min:5|confirmed',
                ]);
                if ($validator->fails()) {
                    $errors = $validator->messages()->all();
                    $response['message'] = $errors[0];
                }else{
                    $param = array();
                    $session = session()->get('registeration');
                    $mobile = substr($user->phoneNumber, strlen($session['country'] ?? $request->country));
                    $param[api_param('type')] = api_param('register'); 
                    $param[api_param('name')] = $request->display_name;
                    $param[api_param('friends-code')] = $request->friends_code ?? session()->get('friends_code', '');
                    $param[api_param('email')] = $request->email;
                    $param[api_param('mobile')] = $mobile ?? $session['mobile'] ?? $request->mobile;
                    $param[api_param('password')] = $request->password;
                    $param[api_param('pincode')] = $request->pincode ?? '';
                    $param[api_param('city-id')] = $request->city ?? '';
                    $param[api_param('area-id')] = $request->area ?? '';
                    $param[api_param('street')] = $request->address ?? '';
                    $param[api_param('latitude')] = 0;
                    $param[api_param('longitude')] = 0;
                    $param[api_param('country-code')] = $session['country'] ?? $request->country;
                    $registration = $this->post('user-registration', ['data' => $param]);
                    if($registration['error'] === false){
                        $response['error'] = false;
                        $response['message'] = $registration['message'];
                        unset($registration['friends_code']);
                        unset($registration['message']);
                        unset($registration['error']);
                        session()->put('user', $registration);
                    }else{
                        $response = $registration;
                    }
                }
                echo json_encode($response);
            }else{
                $data = array();
                $data['email'] = $user->email;
                $data['display_name'] = $user->displayName;
                $data['country'] = $request->country_code;
                $data['mobile'] = substr($request->mobile, strlen($request->country_code));
                $data['auth_uid'] = $request->auth_uid;
                $data['friends_code'] = $request->friends_code ?? session()->get('friends_code', '');
                session()->put('registeration', $data);
                $data['title'] = __('msg.register');
                $this->html('register', $data);
                
            }
        }else{
            return back()->with("err", 'Auth Token Not Matched');
        }
    }
    public function city(Request $request){
        echo json_encode($this->post('get-cities'));
    }
    public function area(Request $request, $cityId = 0){
        echo json_encode($this->post('get-areas', ['data' => [api_param('city-id') => $cityId]]));
    }
    public function refer($code){
        session()->put('friends_code', $code);
        $this->html('login', ['title' => __('msg.refer_and_earn'), 'code' => $code]);
    }
    public function newsletter(Request $request){
        $rules = [
    
            'email' => 'required|email',
        ];
    
        $validator = Validator::make($request->all(), $rules);
    
        if($validator->fails()){
    
            $messages = $validator->messages();
    
            return res(false, $messages->all());
    
        }else{
            $result = $this->post('newsletter', ['data' => ['email' => $request->email]]);
            return res(!$result['error'], $result['message']);
        }
    }
    
    //SM
    public function test(){
        // dd(Cache::get('departments', 0));
        // $response = ($this->post('get-departments', []));
        // dd(count($response));
    }

    public function department(Request $request, $slug)
    {
        $dept_id = 0;
        $dept_name = "";
        $deps = Cache::get('departments');
        $dept_prods = [];
        foreach($deps as $d){
            if($slug == $d->slug)
            {
            $dept_name = $d->name;
            $dept_prods = $this->post('get-products-by-department', ['data' => [api_param('department-id') => $d->id]]);
            }
        }
            $this->html('dept_products', ['title' => $dept_name, 'dept_prods'=>$dept_prods ]);
        }
    public function pincheck(Request $request){
        // dd($request->all());
        $pincode = $request->input('pincode_input');
        $check = $this->post('check-pincode',['data' =>[api_param('pincode') => $pincode]]);
        foreach ($check as $c) {
            echo count($c);
        }
    }
    public function productRequest(Request $request)
    {
        $params[api_param('name')] = $request->input('cname');
        $params[api_param('email')] = $request->input('cemail');
        $params[api_param('mobile')] = $request->input('cphone');
        $params[api_param('product_name')] = $request->input('product_name');
        $insert = $this->post('product-request',['data' =>[api_param('name') => $request->input('cname'),api_param('email') => $request->input('cemail'),api_param('mobile') => $request->input('cphone'),api_param('product_name') => $request->input('product_name')]]);
        
        if(isset($insert['error']) && $insert['error'] ===  false){
            echo 1;
        }else{
            echo 0;
        }
    }

    public function sectionShop(Request $request,$slug=''){
                $pr = [];
        $prods_data = [];
        $pids = '';
        if($slug != ""){
            $sections = Cache::get('sections');
            if(isset($sections[$slug])){
                $prods_data = $sections[$slug]->product_ids;
                $pd = [];
                foreach ($prods_data as $value) {
                    $p = "'".$value."'";
                    array_push($pd, $p);
                }
            }
        }

        $limit = 40;
        $page = $request->page ?? 0;
        $data = ['limit' => $limit, 'offset' => ($page * $limit)];
        
        $param = [];
        if(isLoggedIn()){
            $data[api_param('user-id')] = session()->get('user')['user_id'];
        }
        $data['s'] = trim($request->s ?? "");
        $param['s'] = trim($request->s ?? "");

        $selectedCategoryIds = [];
        $selectedSubCategoryIds = [];
        $selectedCategory = '';
        $selectedSubCategory = '';
        if($request->get('sub-category') != null  || $request->get('category')!=null){
        $selectedSubCategory = explode(",", $request->get('sub-category', ""));
        $selectedCategory = explode(",", $request->get('category', ""));
        $categories = Cache::get('categories');
        foreach($selectedCategory as $cat){
            if(isset($categories[$cat]->childs)){
                $selectedCategoryIds[intval($categories[$cat]->id ?? 0)] = intval($categories[$cat]->id ?? 0);
                $childs = (array)$categories[$cat]->childs;
                foreach($selectedSubCategory as $sub){
                    if(isset($childs[$sub])){
                        unset($selectedCategoryIds[intval($categories[$cat]->id ?? 0)]);
                    
                        $selectedSubCategoryIds[] = intval($childs[$sub]->id ?? 0);
        
                    }
        
                }
            }
        }

        $param['sub-category'] = trim($request->{'sub-category'});
        $param['category'] = trim($request->category);
        $data['category'] = implode(",", $selectedCategoryIds);
        $data['sub-category'] = implode(",", $selectedSubCategoryIds);
        }

        $data[api_param('sort')] = trim($request->sort ?? "");
        $param[api_param('sort')] = trim($request->sort ?? "");
        
        $param['min_price'] = trim($request->{'min_price'});
        $param['max_price'] = trim($request->{'max_price'});
        $data['min_price'] = $request->min_price ?? 0;
        $data['max_price'] = $request->max_price ?? 0;
        $data['prods_data'] = $prods_data;
        $list = $this->post('section-shop', ['data' => $data, 'data_param' => '']);
        $total = $list['total'] ?? 0;
        $min_price = $list['min_price'] ?? 0;
        $max_price = $list['max_price'] ?? 0;
        
        $selectedMaxPrice = ($request->max_price ?? 0) < $max_price ? $max_price : ($request->max_price ?? 0);
        $selectedMinPrice = ($request->min_price ?? 0) < $min_price ? $min_price : ($request->min_price ?? 0);
        // if(isset($list['category']) && is_array($list['category']) && count($list['category'])){
            
        //     $this->update_categories($list['category']);
        // }
        $lastPage = "";
        if(intval($page - 1) > -1){
            if(intval($page - 1) == 0){
                $lastPage = route('shop', $param);
            }else{
                $param['page'] = $page - 1;
                $lastPage = route('shop', $param);
            }
        }
        $nextPage = "";
        if(intval($total / $limit) > $page){
            $param['page'] = $page + 1;
            $nextPage = route('shop', $param);
        }
        $number_of_pages= $total / $limit;
        $cat_arr = [];

        $categoriess = $this->post('get-categories', []);
        // dd($list['cat']);
        foreach ($categoriess as $cats) {
            foreach($list['cat'] as $c)
            {

                if($cats->id == $c)
                {
                    array_push($cat_arr, $cats);
                }
            }
        }
         $this->html('section-shop', ['title' => __('msg.shop'), 'list' => $list, 'limit' => $limit, 'total' => $total, 'min_price' => $min_price, 'max_price' => $max_price, 'selectedMinPrice' => $selectedMinPrice, 'selectedMaxPrice' => $selectedMaxPrice, 'next' => $nextPage, 'last' => $lastPage, 'categories' => $cat_arr, 'selectedCategory' => $selectedCategory, 'selectedSubCategory' => $selectedSubCategory, 'number_of_pages' => $number_of_pages,'section-prods'=>$prods_data]);
    }

    public function searchProduct(Request $request)
    {
        $keyword = $request->input('keyword');
        $search = $this->post('search-products-by-name',['data' =>[api_param('keyword') => $keyword,api_param('limit')=>10]]);
        $data['status'] = (count($search['search_result'])==0) ? '0':'1';
        $data['search'] = $search;
        echo json_encode($data);
    }

    public function searchPage(Request $request)
    {
        $pr = [];
        $prods_data = [];
        if($request->has('section') && trim($request->section) != ""){
            $sections = Cache::get('sections');
            if(isset($sections[$request->section])){
                $prods_data = $sections[$request->section]->product_ids;

                foreach ($prods_data as $prod) {
                    $data = ['product_id'=>$prod];
                    $product = $this->post('get-product', ['data' => $data]);
                    array_push($pr, $product);
                }
            }
        }

        $limit = 40;
        $page = $request->page ?? 0;
        $data = ['limit' => $limit, 'offset' => ($page * $limit)];
        
        $param = [];
        if(isLoggedIn()){
            $data[api_param('user-id')] = session()->get('user')['user_id'];
        }
        $data['s'] = trim($request->s ?? "");
        $param['s'] = trim($request->s ?? "");
        if($request->has('section') && trim($request->section) != ""){
            $sections = Cache::get('sections');
            if(isset($sections[$request->section])){
                $data['section'] = intval($sections[$request->section]->id ?? 0);
                $param['section'] = trim($request->section);
            }
        }
        $selectedCategoryIds = [];
        $selectedSubCategoryIds = [];
        $selectedSubCategory = explode(",", $request->get('sub-category', ""));
        $selectedCategory = explode(",", $request->get('category', ""));
        $categories = Cache::get('categories');
        foreach($selectedCategory as $cat){
            if(isset($categories[$cat]->childs)){
                $selectedCategoryIds[intval($categories[$cat]->id ?? 0)] = intval($categories[$cat]->id ?? 0);
                $childs = (array)$categories[$cat]->childs;
                foreach($selectedSubCategory as $sub){
                    if(isset($childs[$sub])){
                        unset($selectedCategoryIds[intval($categories[$cat]->id ?? 0)]);
                    
                        $selectedSubCategoryIds[] = intval($childs[$sub]->id ?? 0);
        
                    }
        
                }
            }
        }
        $param['sub-category'] = trim($request->{'sub-category'});
        $param['category'] = trim($request->category);
        $data['category'] = implode(",", $selectedCategoryIds);
        $data['sub-category'] = implode(",", $selectedSubCategoryIds);
        $data[api_param('sort')] = trim($request->sort ?? "");
        $param[api_param('sort')] = trim($request->sort ?? "");
        
        $param['min_price'] = trim($request->{'min_price'});
        $param['max_price'] = trim($request->{'max_price'});
        $data['min_price'] = $request->min_price ?? 0;
        $data['max_price'] = $request->max_price ?? 0;

        $list = $this->post('shop-search', ['data' => $data, 'data_param' => '']);
        $total = $list['total'] ?? 0;
        $min_price = $list['min_price'] ?? 0;
        $max_price = $list['max_price'] ?? 0;
        
        $selectedMaxPrice = ($request->max_price ?? 0) < $max_price ? $max_price : ($request->max_price ?? 0);
        $selectedMinPrice = ($request->min_price ?? 0) < $min_price ? $min_price : ($request->min_price ?? 0);
        if(isset($list['category']) && is_array($list['category']) && count($list['category'])){
            
            $this->update_categories($list['category']);
        }
        $lastPage = "";
        if(intval($page - 1) > -1){
            if(intval($page - 1) == 0){
                $lastPage = route('shop', $param);
            }else{
                $param['page'] = $page - 1;
                $lastPage = route('shop', $param);
            }
        }
        $nextPage = "";
        if(intval($total / $limit) > $page){
            $param['page'] = $page + 1;
            $nextPage = route('shop', $param);
        }
        $number_of_pages= $total / $limit;
        $categories = Cache::get('categories', []);
        $this->html('search-product', ['title' => __('msg.shop'), 'list' => $list, 'limit' => $limit, 'total' => $total, 'min_price' => $min_price, 'max_price' => $max_price, 'selectedMinPrice' => $selectedMinPrice, 'selectedMaxPrice' => $selectedMaxPrice, 'next' => $nextPage, 'last' => $lastPage, 'categories' => $categories, 'selectedCategory' => $selectedCategory, 'selectedSubCategory' => $selectedSubCategory, 'number_of_pages' => $number_of_pages,'section-prods'=>$prods_data]);
    }

    public function productvariant(Request $request,$slug){
        if(empty($request->input())){
        $data = ['slug' => $slug];
        if(isLoggedIn()){
            $data[api_param('user-id')] = session()->get('user')['user_id'];
        }
        $product = $this->post('get-product', ['data' => $data]);
        if(count($product) && !(isset($product['error']) && $product['error']) && isset($product[0])){
            $product = $product[0];
            $similarProducts = array();
            $data = [api_param('category-id') => $product->category_id, api_param('product-id') => $product->id, api_param('get_similar_products') => 1];
            
            $result = $this->post('get-similar-products', ['data' => $data]);
            
            if(!isset($result['error'])){
                foreach($result as $r){
                    if($r->slug != $slug){
                        $similarProducts[] = $r;
                    }
                }
            }
            $dont_know_prods = [];
            if($product->dont_forget_at_add_products!=0){
            $dont_know_prod_data = explode(',',$product->dont_forget_at_add_products);
                foreach ($dont_know_prod_data as $dp) {
                    $data2 = ['product_id'=>$dp];
                    $product2 = $this->post('get-product', ['data' => $data2]);
                    array_push($dont_know_prods, $product2);                    
                }
            }

            $svariant = '';
            $av=[];
            $attrb_values = '';
            $attrbs = '';
            if(!empty($product->variants) && !empty($product->variants[0]->attributes)){
            $svariant = $product->variants[0];
            $svarattrs = $svariant->attributes;
            if(strpos($svarattrs, ' ') !== false)
            {
                $ar=explode(" ", $svarattrs);
                foreach ($ar as $a) {
                    $r=explode(":", $a);
                    array_push($av, $r[1]);
                }
            }
            else{
                $ar=explode(":", $svarattrs);
                array_push($av, $ar[1]);
            }
            $attrb_values = $product->attr_values;
            $attrbs = $product->attrs;
        }

            // dd(['title' => $product->name, 'product' => $product, 'similarProducts' => $similarProducts,'dont_know_prods' => $dont_know_prods]);

             $this->html('product', ['title' => $product->name, 'product' => $product, 'similarProducts' => $similarProducts,'dont_know_prods' => $dont_know_prods,'attributes'=>$attrbs,'attribute_values'=>$attrb_values,'svariant'=>$svariant,'attrvars'=>$av]);

        }else{
            abort(404);
        } 
    }
        else{
            $strarr=[];
            $valuearr = [];
            if(count($request->input())==1){
            foreach ($request->input() as $key => $value) {
                array_push($strarr, $key.":".$value);
                array_push($valuearr, $value);
            }
            }
            else{
                $strarr = [];
                foreach ($request->input() as $key => $value) {
                array_push($strarr, $key.":".$value);
                array_push($valuearr, $value);

                }
            }
        $data = ['slug' => $slug,'attributes'=>$strarr];
        if(isLoggedIn()){
            $data[api_param('user-id')] = session()->get('user')['user_id'];
        }
        $product = $this->post('get-product', ['data' => $data]);

        // $product = $this->post('get-product-variant', ['data' => $data]);
        $product=$product[0];

            $similarProducts = array();
            $data = [api_param('category-id') => $product->category_id, api_param('product-id') => $product->id, api_param('get_similar_products') => 1];
            
            $result = $this->post('get-similar-products', ['data' => $data]);
            if(!isset($result['error'])){
                foreach($result as $r){
                    if($r->slug != $slug){
                        $similarProducts[] = $r;
                    }
                }
            }
            $dont_know_prods = [];
            if($product->dont_forget_at_add_products!=0){
            $dont_know_prod_data = explode(',',$product->dont_forget_at_add_products);
                foreach ($dont_know_prod_data as $dp) {
                    $data2 = ['product_id'=>$dp];
                    $product2 = $this->post('get-product', ['data' => $data2]);
                    array_push($dont_know_prods, $product2);                    
                }
            }
            $svariant = $product->selected_variant;

            $this->html('product', ['title' => $product->name, 'product' => $product, 'similarProducts' => $similarProducts,'dont_know_prods' => $dont_know_prods,'attributes'=>$product->attrs,'attribute_values'=>$product->attr_values,'svariant'=>$svariant,'valuearr'=>implode(',',$valuearr)]);
        }
        
    }


    public function productvariant1(Request $request,$slug='')
    {
        if(empty($request->input())){

        $data = ['slug' => $slug];
        if(isLoggedIn()){
            $data[api_param('user-id')] = session()->get('user')['user_id'];
        }
        $product = $this->post('get-product-variant', ['data' => $data]);
        if(count($product) && !(isset($product['error']) && $product['error']) && isset($product[0]))
        {
            $product = $product[0];
            $similarProducts = array();
            $data = [api_param('category-id') => $product->category_id, api_param('product-id') => $product->id, api_param('get_similar_products') => 1];
            
            $result = $this->post('get-similar-products', ['data' => $data]);
            

            if(!isset($result['error'])){
                foreach($result as $r){
                    if($r->slug != $slug){
                        $similarProducts[] = $r;
                    }
                }
            }
            $dont_know_prods = [];
            if($product->dont_forget_at_add_products!=0){
            $dont_know_prod_data = explode(',',$product->dont_forget_at_add_products);
                foreach ($dont_know_prod_data as $dp) {
                    $data2 = ['product_id'=>$dp];
                    $product2 = $this->post('get-product', ['data' => $data2]);
                    array_push($dont_know_prods, $product2);                    
                }
            }

            $svariant = $product->variants[0];
            $svarattrs = $svariant->attributes;
            $av=[];
            if(strpos($svarattrs, ' ') !== false)
            {
                $ar=explode(" ", $svarattrs);
                foreach ($ar as $a) {
                    $r=explode(":", $a);
                    array_push($av, $r[1]);
                }
            }
            else{
                $ar=explode(":", $svarattrs);
                array_push($av, $ar[1]);
            }
            // dd(['title' => $product->name, 'product' => $product, 'similarProducts' => $similarProducts,'dont_know_prods' => $dont_know_prods,'attributes'=>$product->attrs,'attribute_values'=>$product->attr_values,'svariant'=>$svariant,'attrvars'=>$av]);

            $this->html('product-variant', ['title' => $product->name, 'product' => $product, 'similarProducts' => $similarProducts,'dont_know_prods' => $dont_know_prods,'attributes'=>$product->attrs,'attribute_values'=>$product->attr_values,'svariant'=>$svariant,'attrvars'=>$av]);
        }
        else{
            abort(404);
        } 
        }
        else{
            $strarr=[];
            $valuearr = [];
            if(count($request->input())==1){
            foreach ($request->input() as $key => $value) {
                array_push($strarr, $key.":".$value);
                array_push($valuearr, $value);
            }
            }
            else{
                $strarr = [];
                foreach ($request->input() as $key => $value) {
                array_push($strarr, $key.":".$value);
                array_push($valuearr, $value);

                }
            }
        $data = ['slug' => $slug,'attributes'=>$strarr];
        if(isLoggedIn()){
            $data[api_param('user-id')] = session()->get('user')['user_id'];
        }
        $product = $this->post('get-product-variant', ['data' => $data]);
        $product=$product[0];

            $similarProducts = array();
            $data = [api_param('category-id') => $product->category_id, api_param('product-id') => $product->id, api_param('get_similar_products') => 1];
            
            $result = $this->post('get-similar-products', ['data' => $data]);
            if(!isset($result['error'])){
                foreach($result as $r){
                    if($r->slug != $slug){
                        $similarProducts[] = $r;
                    }
                }
            }
            $dont_know_prods = [];
            if($product->dont_forget_at_add_products!=0){
            $dont_know_prod_data = explode(',',$product->dont_forget_at_add_products);
                foreach ($dont_know_prod_data as $dp) {
                    $data2 = ['product_id'=>$dp];
                    $product2 = $this->post('get-product', ['data' => $data2]);
                    array_push($dont_know_prods, $product2);                    
                }
            }
            $svariant = $product->variants[0];

            $this->html('product-variant', ['title' => $product->name, 'product' => $product, 'similarProducts' => $similarProducts,'dont_know_prods' => $dont_know_prods,'attributes'=>$product->attrs,'attribute_values'=>$product->attr_values,'svariant'=>$svariant,'valuearr'=>implode(',',$valuearr)]);

        }
    }

    public function sectionShopTest(Request $request,$slug='')
    {
                $pr = [];
        $prods_data = [];
        $pids = '';
        if($slug != ""){
            $sections = Cache::get('sections');
            if(isset($sections[$slug])){
                $prods_data = $sections[$slug]->product_ids;
                $pd = [];
                foreach ($prods_data as $value) {
                    $p = "'".$value."'";
                    array_push($pd, $p);
                }
            }
        }

        $limit = 40;
        $page = $request->page ?? 0;
        $data = ['limit' => $limit, 'offset' => ($page * $limit)];
        
        $param = [];
        if(isLoggedIn()){
            $data[api_param('user-id')] = session()->get('user')['user_id'];
        }
        $data['s'] = trim($request->s ?? "");
        $param['s'] = trim($request->s ?? "");

        $selectedCategoryIds = [];
        $selectedSubCategoryIds = [];
        $selectedCategory = '';
        $selectedSubCategory = '';
        if($request->get('sub-category') != null  || $request->get('category')!=null){
        $selectedSubCategory = explode(",", $request->get('sub-category', ""));
        $selectedCategory = explode(",", $request->get('category', ""));
        $categories = Cache::get('categories');
        foreach($selectedCategory as $cat){
            if(isset($categories[$cat]->childs)){
                $selectedCategoryIds[intval($categories[$cat]->id ?? 0)] = intval($categories[$cat]->id ?? 0);
                $childs = (array)$categories[$cat]->childs;
                foreach($selectedSubCategory as $sub){
                    if(isset($childs[$sub])){
                        unset($selectedCategoryIds[intval($categories[$cat]->id ?? 0)]);
                    
                        $selectedSubCategoryIds[] = intval($childs[$sub]->id ?? 0);
        
                    }
        
                }
            }
        }

        $param['sub-category'] = trim($request->{'sub-category'});
        $param['category'] = trim($request->category);
        $data['category'] = implode(",", $selectedCategoryIds);
        $data['sub-category'] = implode(",", $selectedSubCategoryIds);
        }

        $data[api_param('sort')] = trim($request->sort ?? "");
        $param[api_param('sort')] = trim($request->sort ?? "");
        
        $param['min_price'] = trim($request->{'min_price'});
        $param['max_price'] = trim($request->{'max_price'});
        $data['min_price'] = $request->min_price ?? 0;
        $data['max_price'] = $request->max_price ?? 0;
        $data['prods_data'] = $prods_data;
        $list = $this->post('section-test', ['data' => $data, 'data_param' => '']);
        $total = $list['total'] ?? 0;
        $min_price = $list['min_price'] ?? 0;
        $max_price = $list['max_price'] ?? 0;
        
        $selectedMaxPrice = ($request->max_price ?? 0) < $max_price ? $max_price : ($request->max_price ?? 0);
        $selectedMinPrice = ($request->min_price ?? 0) < $min_price ? $min_price : ($request->min_price ?? 0);
        // if(isset($list['category']) && is_array($list['category']) && count($list['category'])){
            
        //     $this->update_categories($list['category']);
        // }
        $lastPage = "";
        if(intval($page - 1) > -1){
            if(intval($page - 1) == 0){
                $lastPage = route('shop', $param);
            }else{
                $param['page'] = $page - 1;
                $lastPage = route('shop', $param);
            }
        }
        $nextPage = "";
        if(intval($total / $limit) > $page){
            $param['page'] = $page + 1;
            $nextPage = route('shop', $param);
        }
        $number_of_pages= $total / $limit;
        $cat_arr = [];

        $categoriess = $this->post('get-categories', []);
        dd($list['data']);
        // dd($list['cat']);
        // foreach ($categoriess as $cats) {
        //     foreach($list['cat'] as $c)
        //     {

        //         if($cats->id == $c)
        //         {
        //             array_push($cat_arr, $cats);
        //         }
        //     }
        // }
         // dd(['title' => __('msg.shop'), 'list' => $list, 'limit' => $limit, 'total' => $total, 'min_price' => $min_price, 'max_price' => $max_price, 'selectedMinPrice' => $selectedMinPrice, 'selectedMaxPrice' => $selectedMaxPrice, 'next' => $nextPage, 'last' => $lastPage, 'categories' => $cat_arr, 'selectedCategory' => $selectedCategory, 'selectedSubCategory' => $selectedSubCategory, 'number_of_pages' => $number_of_pages,'section-prods'=>$prods_data]);    	
    }
}

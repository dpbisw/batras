<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Kreait\Firebase\Factory;
class PageController extends Controller{
	public function cms_page($slug = '')
	{
       $page = $this->post('get-page-by-slug', ['data' => [api_param('slug') => $slug]]);	
       if(isset($page['error'])){
   	   	echo "No Page Found";
       }
       else{
       foreach ($page as $p) {
       	  $title = $p->title;
       }
       if($slug=='about-us'){
        $title="About us";
       $this->html('aboutpage', ['title' => $title]);
       }
       else{
       $this->html('cpage', ['title' => $title, 'page_data'=>$page ]);        
       }
   	   }
	}
  public function noIPage(Request $request)
  {
    return view('themes.ekart.no_internet');
  }
  public function aboutPage()
  {
       $title="About us";
       $this->html('aboutpage', ['title' => $title]);
  }
}
?>
<?php
namespace App\Http\Controllers;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Firebase\JWT\JWT;
use Prophecy\Call\Call;
use function GuzzleHttp\json_decode;
class Controller extends BaseController{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public $client;
    public function __construct(){
        $this->client = new \GuzzleHttp\Client(['base_uri' => config('ekart.api_url')]);
    }
    public function init($reload = false){
        $lmt = Cache::get('lmt', 0);
        if($reload || $lmt + config('ekart.reload_settings') < time()){
            
            $this->update_settings();
            $this->update_offers();
            $this->update_social_media();
            $this->update_departments();
            $this->update_categories();
            $this->update_sliders();
            $this->update_ad_groups();
            $this->update_stores();
            $this->update_ads();
            $this->update_pages();
            $this->update_promo_codes();
            $this->update_faq_categories();
            $this->update_faqs();
            $this->update_brands();
            $this->update_topdeals();
            $this->update_home_departments();

            $this->update_departments_submenu();
        }else{
            if(!Cache::has('logo')){
                $this->update_settings();
            }
            if(!Cache::has('departments')){
                $this->update_departments();
            }
            if(!Cache::has('categories')){
                $this->update_categories();
            }
            if(!Cache::has('social_media')){
                $this->update_social_media();
            }
            if(!Cache::has('sliders')){
                $this->update_sliders();
            }
            if(!Cache::has('offers')){
                $this->update_offers();
            }

            if(!Cache::has('ad_groups')){
                $this->update_ad_groups();
            }

            if(!Cache::has('stores')){
                $this->update_stores();
            }

            if(!Cache::has('stores')){
                $this->update_stores();
            }
            if(!Cache::has('ads')){
                $this->update_ads();
            }
            if(!Cache::has('pages')){
                $this->update_pages();
            }
            if(!Cache::has('get-promo-codes')){
                $this->update_promo_codes();
            }
            if(!Cache::has('faq_categories')){
                $this->update_faq_categories();
            }
            if(!Cache::has('faqs')){
                $this->update_faqs();
            }
            if(!Cache::has('brands')){
                $this->update_brands();
            }
            if(!Cache::has('top-deal-products')){
                $this->update_topdeals();
            }
            if(!Cache::has('home_depts')){
                $this->update_home_departments();
            }
            if(!Cache::has('sub_menu_depts')){
                $this->update_departments_submenu();
            }

        }
    }
    function post($api, $params = array(), $die = false){
        try {
            $return = [];
            $data = $params['data'] ?? [];
            $token = $this->generate_token();
            $data[config('ekart.access_key')] = config('ekart.access_key_val');
            $response = $this->client->post(config("ekart.apis.$api"), [
                'headers' => ["Authorization" => "Bearer $token"],
                'form_params' => $data
            ]);
            if($response->getStatusCode() === 200){
                if($die){
                    echo "Bearer $token<br><br>";
                    echo "API URL : ".get("apis.$api")."<br><br>";
                    echo "Params : <pre>";
                    var_dump($data);
                    echo "</pre><br><br>";
                    echo "API Response : ".$response->getBody(); die();
                }
                $return = $this->response($response->getBody(), $params);
            }
            return $return;
        } catch(\GuzzleHttp\Exception\GuzzleException $e) {
            if(isset($_GET['debug']) && $_GET['debug'] == true){
                echo $e->getMessage();
            }else{
                $theme = get('theme');
                echo view("themes.$theme.error");
            }
        }
    }
    
    function postwithfile($api, $params = array(), $die = false, $file=null){
        try {
            $return = [];
            $data = $params['data'] ?? [];
            $token = $this->generate_token();
            $data[config('ekart.access_key')] = config('ekart.access_key_val');
            
            if($file != null){
                
                $file_new_name = time() . mt_rand(1111, 99999) . '.' . $file->getClientOriginalExtension();
                
                $path = $file->storeAs('/public', $file_new_name);
                
                $response = Http::attach('attachment', file_get_contents($file), 'newfile'.$file->getClientOriginalExtension())->post(config("ekart.apis.$api"), [
                    'headers' => ["Authorization" => "Bearer $token"],
                    'form_params' => $data
                ]);
                
                
            }else{
                $response = $this->client->post(config("ekart.apis.$api"), [
                    'headers' => ["Authorization" => "Bearer $token"],
                    'form_params' => $data
                ]);
            }
        
            
            
            
            if($response->getStatusCode() === 200){
                if($die){
                    echo "Bearer $token<br><br>";
                    echo "API URL : ".get("apis.$api")."<br><br>";
                    echo "Params : <pre>";
                    var_dump($data);
                    echo "</pre><br><br>";
                    echo "API Response : ".$response->getBody(); die();
                }
                $return = $this->response($response->getBody(), $params);
            }
            return $return;
        } catch(\GuzzleHttp\Exception\GuzzleException $e) {
            if(isset($_GET['debug']) && $_GET['debug'] == true){
                echo $e->getMessage();
            }else{
                $theme = get('theme');
                echo view("themes.$theme.error");
            }
        }
    } 
    
    function response($return = [], $params = []){
        $return = (array)getJSON($return);
        $error =  true;
        if(isset($return['error'])){
            if(is_bool($return['error'])){
                if($return['error'] === false){
                    $error = false;
                }
            }elseif(is_string($return['error'])){
                if($return['error'] === "false"){
                    $error = false;
                }
            }
        }
        if(!$error){
            $data_param = 'data';
            if(isset($params['data_param'])){
                $data_param = $params['data_param'];
            }
            if(isset($return[$data_param])){
               $return = (array)$return[$data_param];
            }
        }
        return $return;
    }
    function generate_token(){
        return jwt::encode(config('ekart.jwt_payload'), config('ekart.jwt_secret_key'), config('ekart.jwt_alg'));
    }
    public function html($view, $data = array(), $reload = false){
        $this->init($reload);
        $theme = get('theme');
        if(isLoggedIn() && !isset($data['cart'])){
            $data['cart'] = $this->getCart();
        }
        echo view("themes.$theme.common.header", compact('view', 'data'));
        echo view("themes.$theme.common.mega-menu");
        echo view("themes.$theme.common.mobile_more");
        echo view("themes.$theme.common.search-result");
        echo view("themes.$theme.$view", compact('theme', 'data'));
        if($view == "home"){
            echo view("themes.$theme.parts.offers");
            echo view("themes.$theme.parts.icon_categories");
            echo view("themes.$theme.parts.departments");
            
            $this->update_sections();
            if(Cache::has('sections') && is_array(Cache::get('sections')) && count(Cache::get('sections'))){
                foreach(Cache::get('sections') as $s){
                    echo view("themes.$theme.parts.".$s->style, compact('theme','s', 'view'));
                }
            }
            echo view("themes.$theme.parts.categories");
            
            echo view("themes.$theme.parts.ads");
        }
        echo view("themes.$theme.common.footer");
        return true;
    }
    public function update_settings(){
        $settings = $this->post('settings', ['data' => ['all' => true]]);
        foreach($settings as $k => $v){
            if($k == "payment_methods"){
                Cache::put('payment_methods', json_decode($v));
            }elseif($k == "front_end_settings"){
                $frontEndSettings = \json_decode($v);
                foreach($frontEndSettings as $k1 => $v1){
                    Cache::put($k1, $v1);
                }
            }elseif($k == "time_slot_config"){
                $v = \json_decode($v);
                if(($v->is_time_slots_enabled ?? 0) == 1){
                    if(($v->time_slot_config ?? 0) == 1){
                        $getTimeSlot = $this->post('settings', ['data' => ['get_time_slots' => 1, 'setting' => 1]]);
                        if(($getTimeSlot['error'] ?? true) == false){
                            $v->slots = $getTimeSlot['time_slots'];
                            Cache::put('timeslot', $v);
                        }
                    }
                }
            }else{
                Cache::put($k, $v);
            }
        }
        Cache::put('lmt', time());
    }
    public function update_social_media($social_media = []){
        if(empty($social_media)){
            $social_media = $this->post('get-social-media', []);
        }
        if(isset($social_media['error']) && $social_media['error']){
            die('No Data Found. Please Add Data On Admin Panel');
        }
       if(count($social_media)){
            $socialmedia = [];
            for($i = 0; $i < count($social_media); $i++){
                $slug = getSlug($social_media[$i]->icon, $socialmedia);
                $social_media[$i]->icon = $slug;
                $socialmedia[$slug] = $social_media[$i];
            }
            Cache::put('social_media', $socialmedia);
            return true;
        }else{
            return false;
        }
    }
    
    public function update_departments($departments = []){
        
        $departments = $this->post('get-departments', []);
        if(isset($departments['error']) && $departments['error']){
            die('No Data Found. Please Add Data On Admin Panel');
        }
        if(count($departments)){
            $department = [];
            
            Cache::put('departments', $departments);
            return true;
        }else{
            return false;
        }
    }

    public function update_home_departments(){
        return Cache::put('home_depts', $this->post('get-departments-android', ['data' => [api_param('get-departments-android') => api_param('get-val')]]));
    }        

    public function update_departments_submenu(){
        return Cache::put('sub_menu_depts', $this->post('get-departments-submenu', ['data' => [api_param('get-departments-submenu') => api_param('get-val')]]));
    }        
    
    public function update_categories($categories = []){
        if(empty($categories)){
            $categories = $this->post('get-categories', []);
        }
        if(isset($categories['error']) && $categories['error']){
            die('No Data Found. Please Add Data On Admin Panel');
        }
        if(count($categories)){
            $category = [];
            for($i = 0; $i < count($categories); $i++){
                $slug = getSlug($categories[$i]->name, $category);
                $categories[$i]->slug = $slug;
                $category[$slug] = $categories[$i];
            }
            Cache::put('categories', $category);
            return true;
        }else{
            return false;
        }
    }
    public function update_sliders(){
        $sliders = $this->post('slider-images', ['data' => [api_param('get-slider-images') => api_param('get-val')]]);
        if(count($sliders)){
            if(isset($sliders['error']) && $sliders['error']){
                return false;
            }else{
                Cache::put('sliders', $sliders);
            }
            return true;
        }else{
            return false;
        }
    }
    public function update_offers(){
        return Cache::put('offers', $this->post('offers', ['data' => [api_param('get-offer-images') => api_param('get-val')]]));
    }
    public function update_ads(){
        return Cache::put('ads', $this->post('ads', ['data' => [api_param('get-ad-images') => api_param('get-val')]]));
    }
    public function update_pages(){
        return Cache::put('pages', $this->post('pages', ['data' => [api_param('pages') => api_param('get-val')]]));
    }
    public function update_ad_groups(){
        return Cache::put('ad_groups', $this->post('ad_groups', ['data' => [api_param('get-ad-groups') => api_param('get-val')]]));
    }
    public function update_promo_codes(){
        return Cache::put('get-promo-codes', $this->post('get-promo-codes', ['data' => [api_param('get-promo-codes') => api_param('get-val')]]));
    }
    public function update_faqs(){
        return Cache::put('faqs', $this->post('get-all-faqs', ['data' => [api_param('get-all-faqs') => api_param('get-val')]]));
    }    
    public function update_brands(){
        return Cache::put('brands', $this->post('get-brands', ['data' => [api_param('get-brands') => api_param('get-val')]]));
    }    
    public function update_topdeals(){
        $this->post('top-deal-products', ['data' => [api_param('top-deal-products') => api_param('get-val')]]);
    }    
    public function update_faq_categories(){
        return Cache::put('faq_categories', $this->post('get-faq-categories', ['data' => [api_param('get-faq-categories') => api_param('get-val')]]));
    }
    public function update_stores(){
        return Cache::put('stores', $this->post('get-stores', ['data' => [api_param('get-stores') => api_param('get-val')]]));
    }
    public function update_sections(){
        $data = [api_param('get-all-sections') => api_param('get-val')];
        if(isLoggedIn()){
            $data[api_param('user-id')] = session()->get('user')['user_id'];
        }
        $response = $this->post('sections', ['data' => $data, 'data_param' => 'sections']);
        if(is_array($response) && count($response)){
            if(isset($response['error']) && $response['error']){
                return false;
            }else{
                $sections = [];
                foreach($response as $r){
                    if(isset($r->title)){
                        $r->slug = getSlug($r->title, $sections);
                        $sections[$r->slug] = $r;
                    }
                }
                Cache::put('sections', $sections);
                return true;
            }
        }else{
            return false;
        }
    }
    public function getCart(){
        $cart = $this->post('cart', ['data' => ['get_user_cart' => 1, 'user_id' => session()->get('user')['user_id']]]);
        $data['subtotal'] = 0;
        $data['mrptotal'] = 0;
        $data['cart'] = [];
        if(is_array($cart) && count($cart) && !($cart['error'] ?? false)){
            $total_tax = 0;
            foreach($cart as $c){
                if(isset($c->item[0]) && intval($c->qty ?? 0)){
                    $item_mrpprice = get_price_mrp($c->item[0], 0) * ($c->qty ?? 1);
                    $data['mrptotal'] += $item_mrpprice;
                    $item_price = get_price_varients($c->item[0], 0) * ($c->qty ?? 1);
                    $data['subtotal'] += $item_price;
                    $total_tax += ($item_price * ($c->item[0]->tax_percentage ?? 0) / 100);
                }
            }
            $data['saved_price'] = $data['mrptotal'] - $data['subtotal'];
            $data['cart'] = $cart;
            $data['total'] = $data['subtotal'];
            if(floatval(Cache::get('tax', 0)) > 0){
                $data['tax'] = (floatval(Cache::get('tax', 0)) > 0) ? number_format(Cache::get('tax'), 2) : 0;
                $data['tax_amount'] = (floatval(Cache::get('tax', 0)) > 0) ? floatval(floatval(Cache::get('tax')) * $data['subtotal'] / 100) : 0;
            }else{
                $data['tax'] = '';
                $data['tax_amount'] = $total_tax;
            }
            $data['total'] += $data['tax_amount'];
            $data['shipping'] = (intval($data['subtotal']) < Cache::get('min_amount', 0) && Cache::has('delivery_charge')) ? floatval(Cache::get('delivery_charge')) : 0;
            $data['total'] += $data['shipping'];
            $coupon = session()->get('discount', []);
            $data['coupon'] = floatval($coupon['discount'] ?? 0);
            $data['total'] -= $data['coupon'];
        }
        $data['address'] = session()->get('checkout-address');
        return $data;
    }
    public function pagination($list, $routeName = "", $page = 0,  $limit = 10){
        $total = $list['total'] ?? 0;
        $lastPage = "";
        if(intval($page - 1) > -1){
            if(intval($page - 1) == 0){
                $lastPage = route($routeName);
            }else{
                $lastPage = route($routeName, ['page' => $page-1]);
            }
        }
        $nextPage = "";
        if(intval($total / $limit) > $page){
            $nextPage = route($routeName, ['page' => $page+1]);
        }
        return ['list' => $list, 'limit' => $limit, 'total' => $total, 'page' => $page, 'lastPage' => $lastPage, 'nextPage' => $nextPage];
    }
    public function ekart(){
        echo env("API_URL");
    }
    
    public function test(){
        print_r($token = $this->generate_token());
        // dd(Cache::get('departments', 0));
        // $response = ($this->post('get-departments', []));
        // dd(count($response));
    }
    
    
}

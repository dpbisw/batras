<?php
/* api list and theme change */
return [
    /*
    |--------------------------------------------------------------------------
    | Application Name : Batras  Mart
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */
    'name' => env('APP_NAME'),
    'theme' => 'ekart',
    'home_url' => env("APP_URL", "https://batrasmart.com/BATRAS/"),
    'api_url' => env("API_URL", "https://batrasmart.com/adminon/api-firebase/"),
    'asset_url' => env("ASSET_URL1", "https://batrasmart.com/adminon/dist/img/"),
    'asset_url_upload' => env("ASSET_URL1", "https://batrasmart.com/adminon/upload/"),
    'access_key' => env("ACCESS_KEY", "accesskey"),
    'access_key_val' => env("ACCESS_KEY_VAL", "90336"),
    'reload_settings' => (30 * 60 * 60 * 24),
    'reload_settings' => 0,
    'jwt_secret_key' => env("JWT_SECRET_KEY", "bA!@#as854R@kI10oU54"),
    'jwt_payload' => [
        'iat' => time(), /* issued at time */
        'iss' => 'eKart',
        'exp' => time() + (30*60), /* expires after 1 minute */
        'sub' => 'eKart Authentication'
    ],
    'jwt_alg' => 'HS256',
    'apis' => [
        'get-promo-codes'=>'get-promo-codes.php',
        'get-departments' => 'get-departments.php',
        'get-departments-android' => 'get-departments-android.php',
        'get-categories' => 'get-categories.php',
        'get-departments-submenu' => 'get-departments-submenu.php',
        'get-social-media' => 'get-social-media.php',
        'get-sub-categories' => 'get-subcategories-by-category-id.php',
        'get-cities' => 'get-cities.php',
        'get-areas' => 'get-areas-by-city-id.php',
        'get-products-by-category' => 'get-products-by-category-id.php',
        'get-products-by-department' => 'get-products-by-department-id.php',
        'get-products-by-subcategory' => 'get-products-by-subcategory-id.php',
        'search-products-by-name' => 'search-products-by-name.php',
        'get-page-by-slug' => 'get-page-by-slug.php',
        'get-user-list-requests' => 'get-user-list-requests.php',
        'get-product' => 'get-product-by-id.php',
        'get-product-variant' => 'get-product-by-id-variant.php',
        'order-process' => 'order-process.php',
        'set-device' => 'set-device.php',
        'user-registration' => 'user-registration.php',
        'login' => 'login.php',
        'offers' => 'offer-images.php',
        'ads' => 'ad-images.php',
        'ad_groups' => 'ad-groups.php',
        'check-pincode' => 'check-pincode.php',
        'product-request' => 'product-request.php',
        'request-lists' => 'request-lists.php',
        'change-avatar-image' => 'change-avatar-image.php',
        'get-stores'=> 'get-stores.php',
        'products-search' => 'products-search.php',
        'get-similar-products' => 'get-similar-products.php',
        'get-dont-know-products' => 'get-dont-know-products.php',
        'sections' => 'sections.php',
        'settings' => 'settings.php',
        'slider-images' => 'slider-images.php',
        'test' => 'test.php',
        'validate-promo-code' => 'validate-promo-code.php',
        'cart' => 'cart.php',
        'favorites' => 'favorites.php',
        'products' => 'get-all-products.php',
        'top-deal-products' => 'get-topdeal-products.php',
        'shop' => 'shop.php',
        'section-shop' => 'section-shop.php',
        'section-test' => 'section-test.php',
        'shop-new' => 'shop-new.php',
        'shop-search' => 'shop-search.php',
        'faq' => 'faq.php',
        'addresses' => 'user-addresses.php',
        'get-user' => 'get-user-data.php',
        'wallet-history' => 'get-user-transactions.php',
        'razorpay-order' => 'create-razorpay-order.php',
        'paypal-ipn' => '../paypal/ipn.php',
        'newsletter' => 'newsletter.php',
        'midtrans-order' => '../midtrans/create-payment.php',
        'category' => 'category.php',
        'pages' => 'pages.php',
        'get-faq-categories'=>'faq_category.php',
        'get-all-faqs'=>'all_faqs.php',
        'get-brands'=>'brands.php',
        'product-review'=>'product-review.php'        
    ],
    'api-params' => [
        'kirana_subject'=>'kirana_subject',
        'request-list-id'=>'request_list_id',
        'top-deal-products'=>'top-deal-products',
        'check_review_id'=>'check_review_id',
        'add_review'=>'add_review',
        'title'=>'title',        
        'review'=>'review',
        'rating'=>'rating',                
        'fileName' => 'fileName',
        'pages' => 'pages',
        'mobile' => 'mobile',
        'password' => 'password',
        'register' => 'register',
        'name' => 'name',
        'email' => 'email',
        'password' => 'password',
        'pincode' => 'pincode',
        'product_name'=>'product_name',
        'street' => 'street',
        'latitude' => 'latitude',
        'longitude' => 'longitude',
        'user-id' => 'user_id',
        'edit-profile' => 'edit-profile',
        'address' => 'address',
        'wallet-used' => 'wallet_used',
        'wallet-balance' => 'wallet_balance',
        'change-password' => 'change-password',
        'verify-user' => 'verify-user',
        'promo-code' => 'promo_code',
        'promo-discount' => 'promo_discount',
        'city-id' => 'city_id',
        'area-id' => 'area_id',
        'country-code' => 'country_code',
        'product-id' => 'product_id',
        'sub-category' => 'subcat',
        'category' => 'category',
        'category-id' => 'category_id',
        'sub-category-id' => 'subcategory_id',
        'department-id' => 'department_id',
        'check-pincode' => 'check-pincode',
        'product-request'=>'product-request',
        'request-lists'=>'request-lists',
        'avatar-image'=>'avatar-image',
        'cat-id' => 'cat_id',
        'search' => 'search',
        'products-search' => 'products-search',
        'search-products-by-name'=>'search-products-by-name',
        'keyword'=>'keyword',
        'get-orders' => 'get_orders',
        'get-promo-codes'=>'get-promo-codes',
        'place-order' => 'place_order',
        'tax-percentage' => 'tax_percentage',
        'tax-amount' => 'tax_amount',
        'quantity' => 'quantity',
        'get-slider-images' => 'get-slider-images',
        'get-offer-images' => 'get-offer-images',
        'get-ad-images' => 'get-ad-images',
        'get-ad-groups' => 'get-ad-groups',
        'get-stores'=> 'get-stores',
        'get-all-sections' => 'get-all-sections',
        'get-page-by-slug' => 'get-page-by-slug',
        'get-faq-categories'=>'get-faq-categories',
        'get-brands'=>'get-brands',
        'get-all-faqs'=>'get-all-faqs',
        'slug'=>'slug',
        'attributes'=>'attributes',
        'get-val' => 1,
        'type' => 'type',
        'id' => 'id',
        'limit' => 'limit',
        'offset' => 'offset',
        'total' => 'total',
        'final-total' => 'final_total',
        'product-variant-id' => 'product_variant_id',
        'get_similar_products' => 'get_similar_products',
        'get-dont-know-products' => 'get-dont-know-products',
        'delivery-charge' => 'delivery_charge',
        'delivery-time' => 'delivery_time',
        'payment-method' => 'payment_method',
        'sort' => 'sort',
        'page' => 'page',
        'referral-code' => 'referral_code',
        'friends-code' => 'friends_code',
        'status' => 'status',
        'order-status' => [
            'awaiting-payment' => 'awaiting_payment',
        ]
    ],
    'payment_method' => [
        'cod' => 'C.O.D',
        'paypal' => 'PayPal',
        'payumoney' => 'PayUMoney',
        'razorpay' => 'RazorPay',
    ],
    'load-item-limit' => 12,
    'similar-product-load-limit' => 5,
    'show_price_range_in_discounted_price' => false,
    'show_price_range_in_price' => true,
    'style_1' => [
        'max_product_on_homne_page' => 6
    ],
    'style_2' => [
        'max_product_on_homne_page' => 6
    ],
    'style_3' => [
        'max_product_on_homne_page' => 4
    ]
];

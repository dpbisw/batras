"use strict";

$( document ).ready(function() {

$(".ratyli").ratyli({
      full:"<i class='fas fa-star fa-2x'></i>",
      empty:"<i class='far fa-star fa-2x'></i>",
        onRated:function(value,init){
            // rating callback
            if(!init) $("#rating").val(value);  // prevent run at init
        },    
});

$("#avatar_change_btn").click(function(){
    $("#avatar_image").click();
});

//binds to onchange event of your input field
$('#avatar_image').bind('change', function() {

  //this.files[0].size gets the size of your file.
  if(this.files[0].size > 150000)
  {
    $("#err_avatar_image").html('Picture must be less than 150kb size'); 
  }
  else{
    $("#err_avatar_image").html(''); 
    $("#avatar_image_form").submit();
  }

});

// $("#avatar_image").change(function(){
//     // $("#avatar_image_form").submit();
// });

    $("#search_result").css('display','none');
    

    $("#searchInput").keydown(function(e){
        var aItems = $("#ul_results a");
        var key = e.keyCode;
        var current;
        var selected = aItems.filter('.selected');
        aItems.removeClass('selected');

        if ( key != 40 && key != 38 ){
        var keyword = $("#searchInput").val();
        if(keyword.length >=3)
        {
            $("#searchFormNew").submit();           
        }
        if(keyword.length == 0)
        {
            $("#ul_results").html('');
            $("#searchresult").css('display','none');
        }
    }

    if(key == 40)
    {
        if ( ! selected.length || selected.is(':last-child') ) {
            current = aItems.eq(0);
        }
        else {
            current = selected.next();
        }
    current.addClass('selected');    

    }
    else if(key == 38)
    {

        if ( ! selected.length || selected.is(':first-child') ) {
            current = listItems.last();
        }
        else {
            current = selected.prev();
        }
    current.addClass('selected');    

    }

    if(key == 13)
    {
        window.location.href=`${home}productsearch?s=${keyword}`;
    }
    });
    
    $("#searchFormNew").on("submit",function(e){
        e.preventDefault();

        var cat_select = $("#cat_select option:selected").val();

        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serialize(),
            beforeSend: function(){
                var rhtml='';
                rhtml='<li>Getting your Search Results</li>';                
                $("#searchresult").css("display","block");
                $("#ul_results").html(rhtml);
            },
            success: function(response){
                var rhtml = '';
                var jObj = JSON.parse(response);
                if(jObj.status==1)
                {
                var searchQ = $("#searchInput").val();
                jObj.search.search_result.forEach(function(element,index){
                    // rhtml += `
                    //         <a href="${home}product/${element.slug}"><i class="fa fa-search" aria-hidden="true"></i> &nbsp;&nbsp;${element.p_name}</a>
                    // `;
                    if(cat_select!='')
                    {
                        if(cat_select == element.cslug)
                        {
                            rhtml += `
                                <a class='res' href="${home}productsearch?s=${searchQ}&category=${element.cslug}"><i class="fa fa-search" aria-hidden="true"></i> &nbsp;&nbsp;${element.p_name}</a>
                            `;                                                
                        }
                        else{
                            rhtml='<li>No Search Results</li>';                            
                        }
                    }
                    else{
                    rhtml += `
                        <a class='res' href="${home}productsearch?s=${searchQ}"><i class="fa fa-search" aria-hidden="true"></i> &nbsp;&nbsp;${element.p_name}</a>
                    `;                                                                        
                    }
                });                    
                }
                else{
                    rhtml='<li>No Search Results</li>';
                }
                $("#searchresult").css("display","block");
                $("#ul_results").html(rhtml);
            }
        });

    });
    
    // For Mobile start
    
    $("#search_resultMobile").css('display','none');
    
    $("#searchInputMobile").keydown(function(e){

        var aItems = $("#ul_resultsMobile a");
        var key = e.keyCode;
        var current;
        var selected = aItems.filter('.selected');
        aItems.removeClass('selected');

        if ( key != 40 && key != 38 ){        
        var keyword = $("#searchInputMobile").val();
        if(keyword.length >=3)
        {
            $("#searchFormNewMobile").submit();           
        }
        if(keyword.length == 0)
        {
            $("#ul_resultsMobile").html('');
            $("#searchresultMobile").css('display','none');
        }
        }


    if(key == 40)
    {
        if ( ! selected.length || selected.is(':last-child') ) {
            current = aItems.eq(0);
        }
        else {
            current = selected.next();
        }
    current.addClass('selected');    

    }
    else if(key == 38)
    {

        if ( ! selected.length || selected.is(':first-child') ) {
            current = listItems.last();
        }
        else {
            current = selected.prev();
        }
    current.addClass('selected');    

    }

    if(key == 13)
    {
        window.location.href=`${home}productsearch?s=${keyword}`;
    }
        
    });
    
    $("#searchFormNewMobile").on("submit",function(e){
        e.preventDefault();
        var cat_select = $("#cat_select_mobile option:selected").val();
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serialize(),
            beforeSend: function(){
                var rhtml='';
                rhtml='<li>Getting your Search Results</li>';                
                $("#searchresult").css("display","block");
                $("#ul_results").html(rhtml);
            },
            success: function(response){
                var rhtml = '';
                var jObj = JSON.parse(response);
                if(jObj.status==1)
                {
                var searchQ = $("#searchInputMobile").val();
                jObj.search.search_result.forEach(function(element,index){



                    if(cat_select!='')
                    {
                        if(cat_select == element.cslug)
                        {
                            rhtml += `
                                <a href="${home}productsearch?s=${searchQ}&category=${element.cslug}"><i class="fa fa-search" aria-hidden="true"></i> &nbsp;&nbsp;${element.p_name}</a>
                            `;                                                
                        }
                        else{
                            rhtml='<li>No Search Results</li>';                            
                        }
                    }
                    else{
                    rhtml += `
                        <a href="${home}productsearch?s=${searchQ}"><i class="fa fa-search" aria-hidden="true"></i> &nbsp;&nbsp;${element.p_name}</a>
                    `;                                                                        
                    }
                    // rhtml += `
                    //         <a href="${home}product/${element.slug}"><i class="fa fa-search" aria-hidden="true"></i> &nbsp;&nbsp;${element.p_name}</a>
                    // `;
                    // rhtml += `
                    //     <a href="${home}productsearch?s=${searchQ}"><i class="fa fa-search" aria-hidden="true"></i> &nbsp;&nbsp;${element.p_name}</a>
                    // `;
                });                    
                }
                else{
                    rhtml='<li>No Search Results</li>';
                }
                $("#searchresultMobile").css("display","block");
                $("#ul_resultsMobile").html(rhtml);
            }
        });

    });
    
    // For mobile end


    $("#pincode_input").html('');
    $("body").on("submit", ".addToCart", function(){
        if($(this).find('select[name=child_id] option:selected').val() == 0){
            $(this).find('select[name=child_id]').focus().vibrate({stopAfterTime:2});
            return false;
        }else{
            return true;
        }
    });
    $("body").on("click", ".button-plus", function(){
        var qP=$(this).parent().parent().find('.qtyPicker');
        var nP = parseInt(qP.val())+1;
        if(qP.data('max') > nP){qP.val(nP);$(this).parent().parent().find('.button-minus').removeAttr('disabled')}else{$(this).attr('disabled','disabled')}
    });
    $("body").on("click", ".button-minus", function(){
        var qP=$(this).parent().parent().find('.qtyPicker');
        var nP = parseInt(qP.val())-1;
        if(qP.data('min') <= nP){qP.val(nP);$(this).parent().parent().find('.button-plus').removeAttr('disabled')}else{$(this).attr('disabled','disabled')}
    });
    if($('[data-toggle="tooltip"]').length > 0) {
        $('[data-toggle="tooltip"]').tooltip()
    }
    $("body").on("change", ".addToCart select[name=child_id]", function(){
        var val = $(this).find('option:selected').val();
        if(val > 0){
            $(".actualPrice").html($(this).find('option:selected').data('price')).show();
            $(".rangePrice").hide()
        }else{
            $(".actualPrice").hide();
            $(".rangePrice").show()
        }
    });

    $("body").on("change", "select[name=child_id]", function(){
        var selected = $(this).find('option:selected');
        $("#productPrice" + $(this).data('id')).html(selected.data('price'));
        if(selected.data('mrp') == ""){
            $("#productMrp" + $(this).data('id')).hide();
        }else{
            $("#productMrp" + $(this).data('id')).html(selected.data('mrp')).show();
        }
        $("#productTitle" + $(this).data('id')).html(selected.text());
        if(selected.data('discount') != ""){
            $("#productDiscount" + $(this).data('id')).html(selected.data('discount')).show();
        }else{
            $("#productDiscount" + $(this).data('id')).hide();
        }
    });

    // $(".variant_btn").on('click',function(){
    //     alert("hu");
    //     var fullurl=$("#fullurl").val();
    //     var att = $(this).attr('data-att');
    //     var atv = $(this).attr('data-atv');
    //     var qry = $(this).attr('data-qry');
    //     var newurl='';

    //         if(!fullurl.includes('?'))
    //         {
    //             newurl=`${fullurl}?${att}=${atv}`;
    //             window.location.href=newurl;
    //         }
    //         else{

    //         if(qry!='' && qry!=atv)
    //         {
    //             newurl=fullurl.replace(qry, atv);
    //         }
    //         else{
    //             newurl=fullurl;
    //         }

    //         if(qry!='' && qry!=att){}
    //         else{
    //             newurl=`${fullurl}&${att}=${atv}`;
    //         }

    //         window.location.href=newurl;

    //         }
    //     // console.log('hey')
    // 	// console.log(`${fullurl}?${att}=${atv}`);
    //  //    var finalurl='';
    //  //    if(!fullurl.includes(att))
    //  //    {
    //  //        finalurl = `${fullurl}`;
    //  //    }
    // });

    $(".btnEdit").on("click", function(e){
        $(this).closest('tr').find('.cartShow').toggle();
        $(this).closest('tr').find('.cartEdit').toggle();
    });
    $("#pinForm").on('submit',function(e){
    	e.preventDefault();
        var pin = $("#pincode_input").val();
        if(pin.length == 6)
        {
            $("#pinstatus").html("");            
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serialize(),
            beforeSend : function(){
            	$("#checkPin").html(`Checking <i class="fas fa-spinner fa-spin"></i>`);
            	$("#checkPin").attr('disabled',true);
            	$("#pinstatus").html('');
            },
            success: function(response){
            	if(response == 0)
            	{
            		$("#pinstatus").html("<span style='color:red'>Product unavailable in this Pincode</span>");
            	}
            	else if(response == 1){
            		$("#pinstatus").html("<span style='color:green'>Product available in this Pincode</span>");            		
            	}
            	$("#checkPin").html(`Check`);        
            	$("#checkPin").attr('disabled',false);

            }
        });
        }
        else{
            $("#pinstatus").html("<span style='color:red'>Pincode Must be of 6 Digits</span>");            
        }    	
    });

    $("#reqForm").on('submit',function(e){
    	e.preventDefault();
        if(rFlag){
    	$.ajax({
    		url : $(this).attr('action'),
    		type : $(this).attr('method'),
    		data : $(this).serialize(),
    		beforeSend : function(){
    			$("#reqBtn").html(`Submitting <i class="fas fa-spinner fa-spin"></i>`);
    		},
    		success : function(response){
    			if(response == 1)
    			{
    				$("#success_alert").css('display','block');
    			$("#reqBtn").html(`Submit`);    				
    				$("#reqForm").trigger("reset");
                    location.reload();

    			}
    		}
    	});
    }
    });

    $("#list_item_modal_show").on('click',function(){
        var list_url = $("#list_item_url").val();
        $.ajax({
            url : list_url,
            type : 'get',
            data : { 'list':'1' },
            success : function(r){
                if(r == 1)
                {
                    $("#myModal").modal('show');
                }
                else{
                    window.location.href="login";
                }
            }
        });
    });

    $("#request_list_form").on('submit',function(e){
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            url : $(this).attr('action'),
            type : $(this).attr('method'),
            data : formData,
            contentType: false,
            processData: false,
            beforeSend : function(){
                console.log(formData);
                // $("#success_msg").css('display','none');                    
                // $("#requestbtn").attr('disabled',true);
                // $("#requestbtn").html(`Submitting Your List <i class="fas fa-spinner fa-spin"></i>`);
            },
            success : function(response){
                console.log(response);
                // console.log(response);
                // if(response == 1)
                // {
                // $("#requestbtn").attr('disabled',false);
                // $("#requestbtn").html(`Submit`);
                // $("#request_list_form").trigger('reset');
                // $("#success_msg").css('display','block');                    
                // }
            }
        });
    });

function getExtension(filename) {
  var parts = filename.split('.');
  return parts[parts.length - 1];
}

function isValidFile(filename) {
  var ext = getExtension(filename);
  switch (ext.toLowerCase()) {
    case 'jpg':
    case 'png':
    case 'pdf':
      //etc
      return true;
  }
  return false;
}


    $("#requestbtn").on("click",function(){
        var kiranaFile= $("#kiranaFile").val();
        if(kiranaFile.length > 0){
            if(isValidFile(kiranaFile) == true)
            {
                $("#kiranaform").submit();
            }
            else{
                $("#fail_msg").css("display","block");
            }
        }
    });

    $("#av_form_button").click(function(){
    	$("#avModal").modal('show');
    });

    $(".cartSave").on("click", function(e){
        var tr = $(this).closest('tr');
        if(tr.find('form.cartEdit').find('input[name=qty]').val() == tr.find('.price-wrap.cartShow').html()){
            tr.find('.cartShow').toggle();
            tr.find('.cartEdit').toggle();
        }else{
            tr.find('form.cartEdit').submit();
        }
    });
    $("#btnRegister").on("click", function(e){
        $("#cardLogin").hide();
        $("#registerError").hide();
        $("#phone").val('');
        $("#cardRegister input[type=hidden][name=action]").val('1');
        $("#cardRegister").show();
        $("#cardRegister .card-title").html('Register');
        $(".alreadyLogin").show();
        $(".backToLogin").hide();
    });
    $(".btnLogin").on("click", function(e){
        $("#cardLogin").show();
        $("#cardRegister").hide();
    });
    $("#btnForgot").on("click", function(e){
        $("#cardLogin").hide();
        $("#registerError").hide();
        $("#phone").val('');
        $("#cardRegister input[type=hidden][name=action]").val('0');
        $("#cardRegister .card-title").html('Forgot Password');
        $("#cardRegister").show();
        $(".alreadyLogin").hide();
        $(".backToLogin").show();
    });
    <!-- search function -->
    $("#searchForm").on("submit", function(e){
        e.preventDefault();
        var s = $(this).find('input[name=s][type=text]').val().trim();
        if(s != ""){
            window.location.href = $(this).attr('action') + "/" + s;
        }
    });
    <!-- end search function -->
    
    $(document).on('focus', '.select2-selection.select2-selection--single', function (e) {
        $(this).closest(".select2-container").siblings('select:enabled').select2('open');
    });
    
    <!-- reset password -->
    $("#formResetPassword").on("submit", function(e){
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serialize(),
            success: function(response){
                var data = JSON.parse(response);
                if(data.status === false){
                    $("#errorResetPassword").html(data.message).show();
                }else{
                    window.location.href = '/login';
                }
            }
        });
    });
    <!-- end reset password -->
    
    $(".ajax-form").on("submit", function(e){

        e.preventDefault();

        var formResponse = $(this).find('.formResponse');

        formResponse.html('').hide();

        var submit = $(this).find("button[type=submit]");

        submit.attr('disabled', 'disabled');

        $.ajax({

            url: $(this).attr('action'),

            type: $(this).attr('method'),

            data: $(this).serialize(),

            success: function(response){

                console.log(response);

                var data = response;

                if(IsJsonString(response) == true){

                    data = JSON.parse(response);

                }

                var msg = "Something Went Wrong";

                if(data.error === true){

                    if(data.message != ""){

                        msg = data.message;

                    }

                    formResponse.html("<div class='alert alert-danger'>" + msg + "</div>").show();

                    submit.removeAttr('disabled');

                }else{

                    msg = "Success";

                    if(data.message != ""){

                        msg = data.message;

                    }

                    formResponse.html("<div class='alert alert-success'>" + msg + "</div>").show();

                }

                if (typeof data.url !== 'undefined' && data.url != "") {

                    window.location.href = data.url;

                }else{

                    submit.removeAttr('disabled');

                }

            }

        });

    });


    $("#checkoutProceedBtn").on("click", function(e){

        e.preventDefault();

        $("#paymentMethodError").html('').hide();

        var selectedPaymentMethod = $("input[type=radio][name=paymentMethod]:checked");

        if (typeof selectedPaymentMethod.val() === 'undefined') {

            $("#paymentMethodError").show();

        }else{

            $("#checkoutProceed input[type=hidden][name=paymentMethod]").val(selectedPaymentMethod.val());

            var deliveryTime = $("input[type=radio][name=deliverTime]:checked").val();

            var deliverDay = $("input[type=radio][name=deliverDay]:checked").val();

            $("#checkoutProceed input[type=hidden][name=deliveryTime]").val(deliveryTime + " - " + deliverDay);

        }

    });


    /* Product Review check */
    $("#rmdl_toggle").on("click",(e)=>{
        e.preventDefault();
        var prdid = $("#prdid").val();
        $.ajax({
            url: home+"order/check",
            method:"post",
            data: { prdid:prdid },
            success: function(response){
                if(response == 1)
                {
                    $("#reviewModal").modal('show');
                }
                if(response == 0)
                {
                    window.location.href = home + "login";
                }
                if(response ==2)
                {
                    var dialog = bootbox.dialog({
                        title: '',
                                message: `
                                    <div class="text-center">
                                        <i class="fas fa-info-circle fa-3x" style="color:blue"></i> 
                                        <h5 style="margin-top:14px">Please Buy This product to give review</h5>
                                    </div>
                                    `, 
                        buttons: {
                            cancel: {
                                label: "Close",
                                className: 'btn-primary'
                            },
                        }
                    });        
                }
            }
        });        
    });

    $("#no_rmdl").click(function(){
var dialog = bootbox.dialog({
    title: '',
            message: `
                <div class="text-center">
                    <i class="fas fa-info-circle fa-3x" style="color:blue"></i> 
                    <h5 style="margin-top:14px">Please Login to continue</h5>
                </div>
                `, 
    buttons: {
        cancel: {
            label: "Close",
            className: 'btn-primary'
        },
    }
});        
    });

    $("#reviewForm").on("submit",function(e){
        e.preventDefault();
        $.ajax({
            url: home+"product/review/add",
            method: "post",
            data: $(this).serialize(),
            success: function(response){
                if(response==1)
                {
                    $("#status_suc").css("display",'block');
                }
            }
        });

    });
    /* End Reivew */

    <!-- favourite -->
    $('body').on('click', ".saved", function(){
        var i = $(this);
        i.removeClass('saved').addClass('save');
        $.post(home + "favourite-post/remove",{
            id: i.data('id')
        },
        function(data, status){
            if(data == "login"){
                window.location.href = home + "login";
            }else{
                try {
                    var r = JSON.parse(data);
                    console.log(data);
                    if((r.error ?? true) == true){
                        i.removeClass('saved').addClass('save');
                    }
                }catch (e) {
                    i.removeClass('saved').addClass('save');
                }
            }
        });
    });
    $('body').on('click', ".save", function(){
        var i = $(this);
        i.removeClass('save').addClass('saved');
        $.post(home + "favourite-post/add",{
            id: i.data('id')
        },
        function(data, status){
            if(data == "login"){
                window.location.href = home + "login";
            }else{
                try {
                    var r = JSON.parse(data);
                    console.log(data);
                    if((r.error ?? true) == true){
                        i.removeClass('save').addClass('saved');
                    }
                }catch (e) {
                    i.removeClass('save').addClass('saved');
                }
            }
        });
    });
    <!-- end favourite -->
    
    $('.variant .btn').on("click", function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var v = $(this).find('input[type=radio][name=options]').data();
        $("#child_" + id).val(v.id);
        $('#qtyPicker_' + id).attr('data-max', v.maxorderquantity);
        $('#qtyPicker_' + id).attr('max', v.maxorderquantity).change();
        $(`#product_qty_${id}`).html(v.vname);
        if (v.mrp != "") {
            // $('#favbtn').attr("data-id",v.id);
            $('#price_mrp_' + id).show().find('.value').html(v.mrp);
            $('#price_savings_' + id).show().find('.value').html(v.savings);
            $("#price_offer_" + id).show().find('.value').html(v.price).show();
            $('#price_regular_' + id).hide();
        } else {
            $('#price_mrp_' + id).hide();
            $('#price_savings_' + id).hide();
            $('#price_offer_' + id).hide();
            $('#price_regular_' + id).show().find('.value').html(v.price);
        }
    });
    
    $('form').each(function(index, value) {
        $(this).find('.variant .btn:first').click();
    });
    
    $(".qtyPicker").on("change", function() {
        if (parseInt($(this).val()) < 1) {
            $(this).val(1);
        }else if(parseInt($(this).val()) >= $(this).data('max')){
            $(this).val($(this).data('max'));
        }
    });

    $("select[name=varient]").on("change", function(e) {
        var id = $(this).data('id');
        var selected = $(this).find('option:selected');

        $(`#mrp_dyn_${id}`).css("display","none");
        $(`#dis_mrp_${id}`).html(`<del>${selected.data('mrp')}</del>`);

        $(`#prc_dyn_${id}`).css("display","none");
        $(`#dis_prc_${id}`).html(selected.data('price'));

        $("#price_" + id).html(selected.data('price'));
        $("#mrp_" + id).html(selected.data('mrp'));

        $(`#dyn_savings_${id}`).css("display","none");
        $(`#prc_savings_${id}`).html(selected.data('savings'));
        $("#savings_" + id).html(selected.data('savings'));
        $("#save_" + id).html(selected.data('save'));
        $("#variant_val_" + id).html( " : " + selected.text());
    });

    $('.footerfix').css('min-height',$(window).height()-525);

    <!-- home pade side menu category -->
    $("#navContainer").on("click", "li", function(){
        $(this).children("ul").toggleClass("active");
        $("#navContainer li").not(this).children("ul").removeClass("active");
    });

    <!-- sub-header -->
    $('.dropdown').on("hover", function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });

    $('#myCarouselArticle').carousel({
        interval: 10000
    });

    $("#address").addClass("address-show");

    $("#editAddress").addClass("address-hide");

    $("#addAddress").addClass("address-hide");

    $("#dateError").addClass("error-hide");

    $("#timeError").addClass("error-hide");

    $("#paymentError").addClass("error-hide");

    $("#otp-error").addClass("error-hide");

    $("#otpError").addClass("error-hide");

    $("#errorResetPassword").addClass("error-hide");

    $("#registerError").addClass("error-hide");

    $("#cardOtp").addClass("card-hide");

    $("#cardResetPassword").addClass("card-hide");

    $("#registerError").addClass("error-hide");

    $("#backToLogin").addClass("error-hide");

    $('a[data-confirm]').on("click", function(ev) {

        var href = $(this).attr('href');

        $('#orderConfirm').find('.modal-title').text($(this).attr('data-confirm'));

        $('#modal-btn-yes').attr('href', href);

        $('#orderConfirm').modal({show:true});

        return false;

    });


    $('a[data-confirm]').on("click", function(ev) {

        var href = $(this).attr('href');

        $('#modal').find('.modal-title').text($(this).attr('data-confirm'));

        $('#modal-btn-yes').attr('href', href);

        $('#modal').modal({show:true});

        return false;
    });


    $(window).on("scroll", function () {
        if($(this).scrollTop() > 200){
            $("#scroll").fadeIn();
        }else{
            $("#scroll").fadeOut();
        }
    });
    $("#scroll").on("click", function () {
        return $("html, body").animate({ scrollTop: 0 }, 600);
    });

    $('#list').click(function(event){event.preventDefault();$('#products .item1').addClass('list-group-item').removeClass('grid-group-item');});
    $('#grid').click(function(event){event.preventDefault();$('#products .item1').removeClass('list-group-item');$('#products .item1').addClass('grid-group-item');});
    $("#sort").on("change", function(e){
        $("input[type=hidden][name=sort]").val($(this).val());
        $("#filter").submit();
    });
    $(".subs").on("change", function(){
        var sub_ids = [];
        $('.subs:checked').each(function(){
            sub_ids.push($(this).val());
        });
        if(sub_ids.length > 0){
            $("#filter input[type=hidden][name=sub-category]").val(sub_ids.join(","));
            $("#filter").submit();
        }
    });
    $(".cats").on("change", function(){
        var cat_ids = [];
        $('.cats:checked').each(function(){
            cat_ids.push($(this).val());
        });
        if(cat_ids.length > 0){
            $("#filter input[type=hidden][name=category]").val(cat_ids.join(","));
            $("#filter").submit();
        }
    });
    var slider = $("#slider-range");
    slider.slider({
        range: true,
        min: slider.data('min'),
        max: slider.data('max'),
        values: [ slider.data('selected-min'), slider.data('selected-max') ],
        slide: function( event, ui ) {
            $( "input[type=number][name=min_price]" ).val(ui.values[ 0 ]);
		    $( "input[type=number][name=max_price]" ).val(ui.values[ 1 ]);
        }
    });
    $("#filter").on("submit", function(){
        $(this).find("button[type=submit]").click();
    });
});

function loadOptions(element, url, clear = false, open = false, triggerChange = false, selected = 0){
    if(clear == true){
        element.find('option').remove();
    }
    $.ajax({
        url: url,
        success: function(response){
            var data = JSON.parse(response);
            $.each(data, function(id, item){
                if(element.val() != item.id){
                    var isSelected = false;
                    if(selected == item.id){
                        isSelected = true;
                    }
                    element.append(new Option(item.name, item.id, isSelected, isSelected));
                }
            });
            element.select2('close');
            if(open == true){
                element.select2('open');
            }
            if(triggerChange == true){
                element.trigger('change');
            }
        }
    });
}
function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
function address() {
    if ($("#address").hasClass('address-show')) {
        $("#addAddress").removeClass("address-hide");
        $("#addAddress").addClass("address-show");
        $("#address").removeClass("address-show");
        $("#address").addClass("address-hide");
    } else {
        $("#editAddress").addClass("address-hide");
        $("#addAddress").addClass("address-show");
    }
}
function copycode(){
    /* Get the text field */
    var copyText = document.getElementById("referCode");

    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /*For mobile devices*/

    /* Copy the text inside the text field */
    document.execCommand("copy");

}
var rFlag=false;
function validatePRequest(field,value,text)
    {
        var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
            if(value =='' || value ==null)
            {
                rFlag=false;
                $(`#error_${field}`).html(`<i class="fas fa-exclamation-circle"></i> ${text} is Required`);
            }
            else if(field == 'cemail' && !value.match(validRegex))
            {
                rFlag=false;
                $(`#error_${field}`).html(`<i class="fas fa-exclamation-circle"></i> ${text} is Invalid`);
            }
            else if(field == 'cphone' && !Number.isInteger(parseInt(value)))
            {
                rFlag=false;
                $(`#error_${field}`).html(`<i class="fas fa-exclamation-circle"></i> ${text} Must be A Number`);                                                
            }
            else if(field == 'cphone' && value.length != 10)
            {
                rFlag=false;
                $(`#error_${field}`).html(`<i class="fas fa-exclamation-circle"></i> ${text} Must contain 10 digits`);                                                
            }
            else if(field == 'cphone' && !(value.startsWith('6') ||value.startsWith('7') || value.startsWith('8') || value.startsWith('9')))
            {
                rFlag=false;
                $(`#error_${field}`).html(`<i class="fas fa-exclamation-circle"></i> ${text} Must start with 6,7, 8 or 9.`);                                
            }
            else{
                rFlag = true;
                $(`#error_${field}`).html('');                
            }
    }
    
    
$(document).ready(function(){


	function touchCapable() {
        return (
          'ontouchstart' in window||
          (window.DocumentTouch && document instanceof window.DocumentTouch) ||
          navigator.maxTouchPoints > 0 ||
          window.navigator.msMaxTouchPoints > 0
        );
      };

    

    var i=1;
    var str1='sliderBox_'
    var str2='.sliderBox_';
    $('.swiper-container').each(function (index,value) {
        
        // console.log($(this).classList());
    
        if($(this).hasClass('swiper-home-slider')){
            const swiper = new Swiper('.swiper-home-slider', {
                // Optional parameters
                direction: 'horizontal',
                loop: true,
    
               simulateTouch : !touchCapable(),
                
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                
                autoplay: {
                   delay: 1800,
                   disableOnInteraction: false,
                 },
                 
                
            });
        }else if($(this).hasClass('newProductHigh')){
            const swiper = new Swiper('.newProductHigh', {
                // Optional parameters
                direction: 'horizontal',
                loop: true,
                slidesPerView:1,
               
                
                pagination: {
                    el: '.swiper-pagination-dark',
                    clickable: true,
                },
                
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                
                autoplay: {
                   delay: 10000,
                   disableOnInteraction: false,
                 },
                 
                
            });
        }
        
        else if($(this).hasClass('gallery-topr') || $(this).hasClass('gallery-thumbs')){
            var galleryThumbs = new Swiper('.gallery-thumbs', {
                spaceBetween: 10,
                slidesPerView: 4,
                freeMode: true,
                watchSlidesVisibility: true,
                watchSlidesProgress: true,
                
            });
            
            var galleryTop = new Swiper('.gallery-topr', {
              spaceBetween: 10,
              slidesPerView: 1,
              
                direction: 'horizontal',
                loop: false,
    
               breakpoints: {
                    480: {
                        
                    },
                    0: {
                        pagination: {
                            el: '.swiper-pagination',
                            clickable: true,
                        },
                        thumbs:false
                    }
                },
                thumbs: {
                            swiper: galleryThumbs
                          },
               
                navigation: {
                    nextEl: '.swiper-button-nxt-top',
                    prevEl: '.swiper-button-prv-top',
                },
                
                autoplay: {
                   delay: 10000,
                   disableOnInteraction: false,
                 },
                 
                
              
            });
            
        }
        
        else{
            var classAdd = str1 + i;
            var classCall = str2 + i;
            var next_slider = 'slider-next' + i;
            var prev_slider = 'slider-prev' + i;
            $(this).addClass(classAdd);
            $(this).siblings(".sites-slider__prev").addClass(prev_slider);
            $(this).siblings(".sites-slider__next").addClass(next_slider);
            new Swiper(classCall,{
                // slidesPerView: 6,
                spaceBetween: 10,
                // loop: true,
                
                breakpoints: {
                    1920: {
                        slidesPerView: 5,
                        navigation: {
                            nextEl: ('.' + next_slider),
                            prevEl: ('.' + prev_slider),
                        },
                    },
                    1400: {
                        slidesPerView: 4,
                        navigation: {
                            nextEl: ('.' + next_slider),
                            prevEl: ('.' + prev_slider),
                        },
                    },
                    1199: {
                        slidesPerView: 4,
                        navigation: {
                            nextEl: ('.' + next_slider),
                            prevEl: ('.' + prev_slider),
                        },
                    },
                    991: {
                        slidesPerView: 3,
                        navigation: {
                            nextEl: ('.' + next_slider),
                            prevEl: ('.' + prev_slider),
                        },
                    },
                    500: {
                        slidesPerView: 2,
                        navigation: {
                            nextEl: ('.' + next_slider),
                            prevEl: ('.' + prev_slider),
                        },
                    },
                    425:{
                        slidesPerView: 1,
                        navigation: {
                            nextEl: ('.' + next_slider),
                            prevEl: ('.' + prev_slider),
                        },
                    }
                }
            });
            i++;
        }
    
    
    
    });
    
   
    
    
    
    
    
    $(function () {
        $("[data-toggle='tooltip']").tooltip()
    });
    
    
    
}) ;   
    
<style>
	.cheading{
		padding: 15px 23px
	}
	p{
		padding: 0px 43px
	}
</style>
<div class="section-content footerfix">
    <a href="#" id="scroll"><span></span></a>
    <div class="container card mt-3 padding-bottom"> 
        <div class="row">
            <div class="col-12">
            	<?php 
            	foreach ($data['page_data'] as $p) {
            		?>
            		<h1 class="cheading"><?php echo $p->title; ?></h1>
            		<?php
					echo $p->content;            		
            	}
            	?>
            </div>
        </div>
    </div>
</div>    
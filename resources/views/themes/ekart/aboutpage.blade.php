<style>
    .about-img{
    background: url({{ theme('images/about1.jpg') }});
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    min-height: 630px;
    animation: 60s ease 0s normal none infinite back_ken;
    -webkit-animation: 60s ease 0s normal none infinite back_ken;        
    }
    .about-title{
        font-size: 37px;
    line-height: 34px;
    margin-top: 10px;
    color: red;
        font-family: "Poppins", sans-serif;
    letter-spacing: -0.5px;
    font-weight: 600;
    }
    .about-para{
margin-top: 35px !important;
        text-align: justify;
    font-size: 19px !important;
    font-weight: 400;
    color: #8d8d8d;
    line-height: 25px;
    margin: 0;
    padding: 0;
    border: 0;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
    }
    .about-para2{
        text-align: justify;
    font-size: 19px !important;
    font-weight: 400;
    color: #8d8d8d;
    line-height: 25px;
    margin: 0;
    padding: 0;
    border: 0;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
    }
    .about-para2 p{
        text-align: justify;
    font-size: 19px !important;
    font-weight: 400;
    color: #8d8d8d;
    line-height: 25px;
    margin: 0;
    padding: 0;
    border: 0;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
    }
    .about-title2{
        font-size: 21px;
    line-height: 34px;
    margin-top: 10px;
    color: red;
        font-family: "Poppins", sans-serif;
    letter-spacing: 1px;
    font-weight: 600;
    }
    .about-title3{
    font-size: 23px;
    line-height: 34px;
    margin-top: 10px;
    color: red;
    font-family: "Poppins", sans-serif;
    letter-spacing: 0px;
    font-weight: 600;        
    }
</style>
<section class="footerfix section-content mt-3">


    <a href="#" id="scroll"><span></span></a>

    <div class="container padding-bottom">
        <div class="row">
            <div class="col-lg-6">
                <div class="about-img"></div>
            </div>
            <div class="col-lg-6">
                <h2 class="about-title">About Us</h2>
                <p class="about-para">
Quality in a service or a product is just not what you put into it, but what customers get out of it. Batra's everyday routine stands on four powerful pillars comprising of customer's wellbeing, safety, Best pricing and fresh stock. We take every customer like a one big family envisioning for a better tomorrow for all of us.                    
                </p>

                <h4 class="about-title2 mt-4">BATRA'S MISSION AND VISION</h4>    
                <p class="about-para2">
To continually provide our members with quality goods and services at the best possible prices.
<br/>
A Personal touch with customers has always been the focus at Batra's and will remain our priority in the future.
                </p>

                <h4 class="about-title2 mt-4">CODE OF ETHICS :</h4>
                <p class="about-para2">
1. Obey the law
2. Take care of our members
3. Take care of our employees
4. Respect our suppliers
5. Give back to the society
6. Generate Employment
                </p>                    
                <h4 class="about-title2 mt-4">OUR MANTRA</h4>
                <p class="about-para2">
Only best flat rates, No misleading offers.
                </p>            
            </div>
        </div>

    </div>
                <div class="col-lg-12 mt-4" style="text-align: center; background-color: red; padding-top: 30px; padding-bottom: 30px;">
                    <h1 style="color: white;font-size: 55px;line-height: 85px;font-weight: 900;">Humari Kahani</h1>
                </div>
    <div class="container padding-bottom abt-para" style="background-color: white !important ;">
        <div class="about-para2" style="width: 84%;margin: auto;">
            <br>
Pickle and Papad have a history of ages, dating as far back as 2030 BC when cucumbers from India were pickled in the Tigris Valley. Pickle is used as staple food supplement (TBC) by most of the Indians. Batra's have such a strong History associated with it, that we are more known by "BATRA PAPAD AND ACHAAR WAALE"
<br/>
It all started in 1960 with the sole motive of survival. During the days of Independence, a highly motivated and patriotic Indian and a freedom fighter named (Late) Shri Govind Ram Batra @GB had to migrate from Pakistan to India. Unaware of any market strategies and just as a means of survival he along with his brothers started to sell handmade pickles in Indore (M.P.) and continued to do so for 5 years.  Then a wave of Separation hit the family and the brothers got separated. But GB's unending efforts and hard work did not pause here. He started a very small shop in Azad Market Bhopal and then with the revival of the economy, a pickle factory was started in Kotwali Road. With his immense hard work, dedication and a gifted sense of taste he Slowly and gradually gained expertise - to be called as the "Master of Pickles" of his time.
        </div>

        <div style="display: flex;justify-content: center;">
            <img src="{{ theme('images/about2.jpg') }}" alt="about2" class="img-fluid">
        </div>
        <div style="width:84%;margin:auto">
            <h4 class="about-title3">
1980'S KI BAAT HAI
            </h4>
            <div class="about-para2">
Like father like son, The second generation took the leap forward and expanded Batra’s to a whole new level.

Mr. Bharat Batra, s/o (Late) Shri Govind Ram Batra with his high spirits learning one step at a time under his father's guidance stole the show. Now a bigger platform was required to fulfil needs of growing customers. This led to establishment of a bigger store which not only sold handmade pickles but everyday household needs, with dominance in handmade papads and fryums. This new store is now called Batra Provision Store and is also the lime lite of the street Lalwani Press Road Bhopal M.P.
            </div>

        <div style="display: flex;justify-content: center;">
            <img src="{{ theme('images/about3.jpg') }}" alt="about2" class="img-fluid">
        </div>
            <h4 class="about-title3">
1990'S KI BAAT HAI
            </h4>
            <div class="about-para2">
The Indian Economy underwent a series of reforms at this stage where the government adopted LPG (Liberalisation, Privatisation and Globalisation) which brought many new opportunities in the business world. Organised and Packaged food items took over the market which gave Batra's new wings to fly in the retail division. The Batra's avenue tree now had two sub branches primarily wholesale and retail. Now Mr. Bharat Batra was accompanied by Mr. Prakash Punjabi to take the business forward. Batra's now became the undisputed player in the Market, be it Wholesale or retail division.
            </div>
        <div style="display: flex;justify-content: center;">
            <img src="{{ theme('images/about4.jpg') }}" alt="about2" class="img-fluid">
        </div>
            <h4 class="about-title3">
2015 HAMARA PEHELA MILESTONE
            </h4>
            <div class="about-para2">
New generation came with digitisation and modernisation to flawlessly cater needs of the growing world. This third generation Mr. Lokesh Batra the elder son took care of the wholesale avenue followed by Mr. Mohit Batra the younger one who launched himself into the retail avenue and ramped up the ongoing business. A new space for wholesale was bought at the Itwara bazaar which now supplies to approximately 2500 small and big stores.
            </div>
            <h4 class="about-title3">
2017 HAMARA DUSRA MILESTONE
            </h4>
            <div class="about-para2">
A huge "Batra’s Wholesale Mart" was inaugurated on March 27 which is a state of art for any retailer. Every single floor manages different household products, ranging from grocery to cosmetics and basic to fancy. It gives employment to more than 50 people with various backgrounds and all genders. It's a one big family which is ever growing with everybody's efforts.
<br>
Since the launch of "Batra's Wholesale Mart" has earned a loyal customer base because of its commitment to quality and taste.
<br/>
The world now needs a purely Home quintessential solution which is fast to order and safe to be delivered. This thought brings us to the introduction of a full-fledged online store like you have never seen before.
            </div>
        <div style="display: flex;justify-content: center;">
            <img src="{{ theme('images/about5.jpg') }}" alt="about2" class="img-fluid">
        </div>
            <h4 class="about-title3">
2020 HAMARA TEESRA MILESTONE
            </h4>
            <p class="about-para2">
Batra's Home - Only best flat rates, No misleading offers.
            </p>
            <h4 class="about-title3">
WHAT IS BATRA'S HOME?
            </h4>
            <p class="about-para2">
Batra's Home is the name of Batra's online services. Through this service, we hope to bring Batra's closer to you.
            </p>
            <h4 class="about-title3">
HOW?
            </h4>
            <p class="about-para2">
Now you can buy all your favourite products online from our app or website, and get them delivered to your home.
            </p>
        </div>
    </div>
</section>


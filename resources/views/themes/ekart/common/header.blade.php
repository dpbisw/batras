<!DOCTYPE HTML>
<html lang="en" dir="ltr">

<head>
    <link rel="icon" type="image/x-icon" href="{{ _asset(Cache::get('favicon', 'images/favicon.ico')) }}" />
    <title>
        {{ ((isset($data['title']) && trim($data['title']) != "") ? $data['title']." | " : ''). Cache::get('app_name', get('name')) }}
        @php
            $title = $data['title'];
        @endphp
    </title>
    <meta charset="utf-8">
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="cache-control" content="max-age=604800" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ValuePac">
    <meta name="copyright" content="">
    <meta name="keywords" content="{{ Cache::get('common_meta_keywords', '') }}">
    <meta name="description" content="{{ Cache::get('common_meta_description', '') }}">

    <link href="{{ theme('css/ui.css') }}" rel="stylesheet" type="text/css" />
    {{-- <link href="{{ theme('css/rtl_ui.css') }}" rel="stylesheet" type="text/css" /> --}}
    <link href="{{ theme('css/custom.css') }}" rel="stylesheet" type="text/css" />
    {{-- <link href="{{ theme('css/rtl_custom.css') }}" rel="stylesheet" type="text/css" /> --}}
    <link href="{{ theme('css/plugins.css') }}" rel="stylesheet" type="text/css" />
    {{-- <link href="{{ theme('css/rtl_responsive.css') }}" rel="stylesheet" type="text/css" /> --}}
    <link href="{{ theme('css/responsive.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ theme('css/stepper.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ theme('css/calender.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ theme('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ theme('css/jquery-ui.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ theme('css/intlTelInput.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ theme('css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ theme('css/swiper-bundle.min.css') }}" rel="stylesheet" type="text/css" />
    
    <link href="{{ theme('fonts/fontawesome/css/all.min.css') }}" rel="stylesheet" type="text/css" />
<link rel="preconnect" href="https://fonts.gstatic.com">
    

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"/>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css"/>

    <script src="{{ theme('js/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ theme('js/popper.js') }}"></script>
    <script src="{{ theme('js/bootstrap.min.js') }}"></script>
    
    <!-- Bootbox -->
    <script src="{{ theme('js/bootbox.min.js') }}"></script>
    <script src="{{ theme('js/bootbox.locales.min.js') }}"></script>
    <!-- Bootbox end -->

    <script src="{{ theme('js/jquery-ui.min.js') }}"></script>
    <script src="{{ theme('js/intlTelInput.js') }}"></script>
    <script src="{{ theme('js/select2.min.js') }}"></script>
    <script src="{{ theme('js/swiper-bundle.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="https://raw.githack.com/peet86/Ratyli/master/jquery.ratyli.min.js"></script>
    
    <script>
        var home = "{{ get('home_url') }}";
    </script>
    <script src="{{ asset('js/script.js') }}"></script>

    <link href="{{ theme('css/cart.css') }}" rel="stylesheet" type="text/css" />
    <script>
        window.addEventListener('offline', (e) => {
            $("#internet_exists").css('display','none');
            $("#no-internet").css('display','block');
        });

        if (navigator.onLine) { 
            $("#internet_exists").css('display','block');
                $("#no-internet").css('display','none');
        }

            function nextInList(event) {
        switch (event.which) {
            case 40:
                $(".res_0").css("background-color","green");
                break;
            default:
                break;
        }
    }

    </script>
    
    <script>

        window.addEventListener("load", function () {
          $('#status').fadeOut(); // will first fade out the loading animation 
          $('#preloader').delay(350).fadeOut(); // will fade out the white DIV that covers the website.     
           document.getElementById('internet_exists').style.display = 'block'; 
        });

        $(document).ready(function(){
            $('.dropdown').mouseenter(function(){ 
              $('.dropdown-toggle', this).trigger('click'); 
            });
            $('.dropdown').mouseleave(function(){ 
              $('.dropdown-toggle', this).trigger('click'); 
            });
        });
    </script>
    
    <style>
        .reloadBTN{
            width: 17%;
            padding: 15px 14px;
            border: none;
            background: blue;
            color: white;
            font-size: 18px;
            text-transform: uppercase;
            cursor: pointer;            
        }
        
        .header-top-right{
            display:flex;
            justify-content: flex-end;
            align-items: center;
        }
        .header_whatsapp_icon{
            margin-right:60px;
        }
        
        .header_whatsapp_icon>p{
            color:#000;
        }
        
        .header_whatsapp_icon>p>i{
            font-size:25px;
            color:#00e676;
        }

        #preloader {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: white;
            z-index: 99999;
            height: 100%;
        }
        #status {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            /*margin-top: -149px;*/
            /*margin-left: -200px;    */
            /*padding: 0;*/
        }
        .selected {
    background: white;
}
    .checkbtn{
        text-transform: uppercase;
        font-weight: 700;
        padding: 4px 10px;
        color: #ec141e;
        border: 1px solid #ced4da;
    }
    
    .mobile_more{
        position: absolute;
        right: 26%;
        top: 10%;
    }
    .mobile_more button{
        border: none;
        background: transparent;
    }
    .cart_mobile{
        position: absolute;
        right: 6%;
        top: 28%;
    }
    
    @media(min-width:576px){
        .mobile_more{
            display:none;
        }
        .cart_mobile{
            display:none;
        }
    }
    </style>

</head>

<body>
    <div id="no-internet" style="display: none">
    <center>
        <br>
        <br>
        <h1>No Internet</h1>
    <script src="{{ theme('js/lottie.js') }}"></script>
<lottie-player src="{{ theme('js/lottie-icon.json') }}"  background="transparent"  speed="1"  style="width: 300px; height: 300px;"  loop  autoplay></lottie-player>
    <h3>You're Offline</h3>
    <p style="width: 25%">No internet or Slow Connection found.
Check your connection or Try again</p>
    <button class="reloadBTN" onclick="location.reload();">Reload</button>
    </center>
    </div>

    <div id="preloader">
   <div id='status'>
      <img alt='' src="{{ theme('images/preloader3.gif') }}"/>
   </div>       
    </div>


    <div id="internet_exists" class="page-wrapper-main">


    <div class="mobile_overlay"></div>
    <div class="mobile_menu">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    
                    <div class="mobile_wrapper">
                        <div class="bar_close">
                            <a href="#"><i class="fas fa-times"></i></a>
                        </div>
                        <div class="freeshipping">
                            <p>{{__('msg.you_can_get_free_delivery_by_shopping_more_than')}} {{ get_price(Cache::get('min_amount')) }}</p>
                        </div>

                        @if(Cache::has('social_media') && is_array(Cache::get('social_media')) && count(Cache::get('social_media')))
                        <div class="header_social_icon text-center">
                            <ul>
                                @foreach(Cache::get('social_media') as $i => $c)
                                    <li class="social-icon">
                                        <a target="_blank" href="{{ $c->link }}"><em class="fab {{ $c->icon }}"></em></a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        @if(trim(Cache::get('support_number', '')) != '')
                        <div class="header_call-support">
                            <p><a href="#">{{Cache::get('support_number')}}</a> Customer Support</p>
                        </div>
                        @endif
                        <div id="menu" class="text-left ">
                            <ul class="header_main_menu">
                            @if(Cache::has('categories') && is_array(Cache::get('categories')) && count(Cache::get('categories')))
                            @foreach(Cache::get('categories') as $ca)
                            @if($ca->is_header == 1)
                                <li class="header_submenu_item"><a href="{{ route('shop', ['category' => $ca->slug]) }}">{{ $ca->name }}</a></li>
                            @endif
                            @endforeach
                            @endif

<!--                                 <li class="header_submenu_item active">
                                    <a href="{{ route('home') }}">Home </a>
                                </li>
 -->

<!--                                 <li class="header_submenu_item">
                                    <a href="{{ route('shop') }}"> Shop</a>
                                </li>
 -->
                            </ul>
                        </div>
                        <div class="offcanvas_footer">
                            <span><a href="#"><i class="fa fa-envelope"></i> {{Cache::get('support_email')}}</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--offcanvas menu area end-->
    

    <header class="shadow-sm bg-white">
        <div class="main_header">
            <div class="header_top">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-md-6">

                            <div class="freeshipping">
                                <!--<p>{{__('msg.you_can_get_free_delivery_by_shopping_more_than')}} {{ get_price(Cache::get('min_amount')) }}</p>-->
                                <p style="font-weight: 700; color: #ec1922; text-transform:capitalize;">Only best flat rates, No misleading offers.</p>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="header-top-right">
                                
                                <!--<div class="header_whatsapp_icon text-right">-->
                                <!--    <p><i class="fab fa-whatsapp"></i> Whatsapp :+91 9407322212</p>-->
                                <!--</div>-->
                                
                                
                                @if(trim(Cache::get('support_number', '')) != '')
                                    <div class="header_call-support">
                                        <p><a href="#"><span><i class="fab fa-whatsapp"></i></span>Whatsapp :+91 9407322212</a></p>
                                    </div>
                                    <div class="header_call-support">
                                        <p><a href="#"><i class="fas fa-phone-volume"></i>{{Cache::get('support_number')}}</a></p>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
            
        </div>
    </header>
    
    
    <nav class="shadow-sm bg-white top-sticky-nav" style="">
        <div class="secondheader py-1">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-2 col-md-12 col-sm-12 col-12">
                            <div class="bar_open">
                                <a href="#"><i class="fas fa-bars"></i></a>
                            </div>
                            <div class="logo">
                                <a href="{{ route('home') }}"><img src="{{ _asset(Cache::get('web_logo')) }}" alt="logo"></a>
                            </div>
                            <div class="mobile_more">
                              <button type="button" id="mobile_more_open"><i class="fas fa-caret-down fa-2x"></i></button>
                            </div>
                            <div class="cart_mobile">
                              <a href="{{ route('cart') }}"><i class="fas fa-shopping-cart fa-lg"></i></a>
                                        @if(isset($data['cart']['cart']) && is_array($data['cart']['cart']) && count($data['cart']['cart']))
                                            <span class="item_count">{{ count($data['cart']['cart']) }}</span>
                                        @endif
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-12 col-sm-12 col-12">
                            <div class="header_right">
                                <div class="header_search mobile_screen_none">
                                    <form action="{{ route('psearch') }}" method="post" id="searchFormNew">
                                        @csrf
                                        @php
                                            $categories = Cache::get('categories', []);
                                        @endphp
                                        <div class="header_hover_category">
                                            <select class="select_option" name="category" id="cat_select">
                                                <option selected value="">All Category</option>
                                                @foreach($categories as $i=>$c)
                                                    <option value="{{ $c->slug }}">{{ $c->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="header_search_box">
                                            <input type="text" class="form-control top-search" name="keyword" placeholder="Search Product..." id="searchInput" autocomplete="off">
                                            <button type="button" id="searchButton"><i class="fas fa-search"></i></button>
                                        </div>
                                    </form>
                                    
                                    <div id="searchresult">
                                        <ul id="ul_results"></ul>
                                    </div>
                                </div>
                                <div class="header_account_area">
                                    <div class="header_account_list register">

                                        <ul>
                                            @if(isLoggedIn())
                                            <li><a href="{{ route('my-account') }}"><span class="top-bar-text"><?php echo (isset(session()->get('user')['name'])) ? session()->get('user')['name'] : ''; ?>, </span><i class="far fa-user"></i></a></li>

                                            @else
                                                <li><a href="{{ route('login') }}"><span class="top-bar-text">Sign In / Register</span> <i class="far fa-user"></i></a></li>
                                            @endif
                                            
                                            <li>
                                                <div class="dropdown">
                                                      <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        More
                                                      </button>
                                                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                        
                                                        @if(isLoggedIn())
<!--                                                             <a class="dropdown-item" href="{{ route('favourite') }}"><i class="fas fa-heart fa-sm"></i> Wishlist</a>
 -->
                                                            <a class="dropdown-item" href="{{ route('favourite') }}">
<img src="{{ asset('new_icons/my_wishlist.png') }}" class="img-fluid" style="width: 20px" alt=""> 
                                                                 Wishlist</a>

                                                        @endif
                                                            <!-- <a class="dropdown-item" href="{{ route('store') }}"><i class="fas fa-store fa-sm"></i> Our Stores</a>                                                         -->
                                                            <a class="dropdown-item" href="{{ route('store') }}">
                                                               <img src="{{ asset('new_icons/store.png') }}" class="img-fluid" style="width: 20px" alt="">   Our Stores</a>                                                            
                                                      </div>
                                                </div>
                                                
                                            </li>
                                            @if(isLoggedIn())
                                            <li>

                                                        <a href="javascript:void(0)">
                                                            <button type="button" id="list_item_modal_show" class="btn btn-top-list p-0">
                                                            <img src="{{ asset('new_icons/kirana_list.png') }}" class="img-fluid" style="width: 20px" alt=""> 
                                                        </button>
                                                        <input type="hidden" id="list_item_url" value="{{ route('check-user-login') }}">
                                                        </a>

                                            </li>
                                            @endif
                                            <!--    <button type="button" id="list_item_modal_show" class="btn btn-top-list"><i class="fas fa-clipboard-list"></i></button>-->
                                            <!--    <input type="hidden" id="list_item_url" value="{{ route('check-user-login') }}">-->
                                            <!--</li>-->

                                        
                                        </ul>
                                    </div>
                                    <!--<div class="header_account_list header_wishlist">-->
                                    <!--    @if(isLoggedIn())-->
                                    <!--        <a href="{{ route('favourite') }}"><i class="fas fa-heart fa-sm"></i></a>-->
                                    <!--    @endif-->
                                    <!--</div>-->
                                    <div class="header_account_list">
                                        <a href="{{ route('cart') }}"><i class="fas fa-shopping-cart fa-sm"></i></a>
                                        @if(isset($data['cart']['cart']) && is_array($data['cart']['cart']) && count($data['cart']['cart']))
                                            <span class="item_count">{{ count($data['cart']['cart']) }}</span>
                                        @endif

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            
            <div class="header_bottom sticky-header" >
                <div class="container">
                    <div class="row align-items-center positionheader">
                        <div class="col-12 col-md-6 mobile_screen_block">
                            <div class="header_search">
                                <!--<form action="#">-->
                                    
                                    <form action="{{ route('psearch') }}" method="post" id="searchFormNewMobile">
                                        @csrf
                                        @php
                                            $categories = Cache::get('categories', []);
                                        @endphp
                                        <div class="header_hover_category">
                                            <select class="select_option" name="category" id="cat_select_mobile">
                                                <option selected value="">All Category</option>
                                                @foreach($categories as $i=>$c)
                                                    <option value="{{ $c->slug }}">{{ $c->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="header_search_box">
                                            <input type="text" class="form-control top-search" name="keyword" placeholder="Search Product..." id="searchInputMobile" autocomplete="off">
                                            <button type="button" id="searchButtonMobile"><i class="fas fa-search"></i></button>
                                        </div>
                                    </form>
                                    
                                    <div id="searchresultMobile">
                                        <ul id="ul_resultsMobile"></ul>
                                    </div>

                            </div>
                        </div>
                        <div class="col-lg-1" style="display:none">
<div style="display: flex;color: white;justify-content: space-between;width: 100%;cursor: pointer;" data-toggle="modal" data-target="#checkDeliveryModal">
                            <i class="fas fa-map-marker-alt" style="padding-top: 10px;"></i>
                            <p style="
    color: white;
    line-height: 18px;
    font-size: 14px;
">Deliver to<br>
<span style="font-weight: bold;">
@if(!isset(session()->get('user')['user_id']))
    462001
@else
    @if(isset(session()->get('user')['dpincode']))
    {{ session()->get('user')['dpincode'] }}

    @else
    462001
    @endif
@endif
</span>
</p>                                
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="header_categories_menu">
                                @php
                                    $categories = Cache::get('categories', []);
                                    $maxProductToShow = 10;
                                    $totalCategories = count($categories);
                                @endphp

<!--                                 <div class="categories_title">
                                    <h2 class="category_toggle">All Categories</h2>
                                    <i class="fas fa-chevron-down fa-xs"></i>
                                </div>

 -->                            <div class="categories_title_new">
                                    <h2 class="category_toggle"><span><i class="fas fa-list"></i></span> All Departments</h2>
                                    <i class="fas fa-chevron-down fa-xs"></i>
                                </div>


                                <div class="header_categories_toggle">
                                    <ul>
                                        @foreach($categories as $k=>$c)
                                            @if(isset($c->childs) && count((array)$c->childs))
                                                <li class="header_menu_item {{ $k >= $maxProductToShow ? 'hidden' : ''}}"><a>{{ $c->name }}<i
                                                            class="fas fa-plus fa-xs"></i></a>
                                                    <ul class="header_categories_mega_menu">
                                                            @foreach($c->childs as $child)

                                                                    <li><a href="{{ route('shop', ['category' => $c->slug, 'sub-category' => $child->slug]) }}">{{ $child->name }}</a></li>

                                                            @endforeach
                                                    </ul>
                                                </li>
                                            @else
                                            <li class="{{ $k >= $maxProductToShow ? 'hidden' : ''}}"><a href="{{ route('category', $c->slug) }}">{{ $c->name }}</a></li>
                                            @endif
                                                @if(intval(--$maxProductToShow))
                                                @else
                                                    @if($maxProductToShow == 0)
                                                        <li ><a href="#" id="more-btn"><i class="fa fa-plus" aria-hidden="true"></i> More
                                                        Categories</a>
                                                        </li>
                                                    @endif
                                                @endif
                                        @endforeach
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <!--main menu start-->
                            <div class="main_menu header_menu_position">
                                <nav>
                                    <ul>
                                        @if(Cache::has('categories') && is_array(Cache::get('categories')) && count(Cache::get('categories')))

                                        @php $maxProductShow = 6; @endphp


                                        @foreach(Cache::get('categories') as $ca)
                                        
                                        @if((--$maxProductShow) > -1)

                                        @if($ca->is_header == 1)
                                            <li><a href="{{ route('shop', ['category' => $ca->slug]) }}">{{ $ca->name }}</a></li>
                                        @endif

                                        @endif

                                        @endforeach
                                        @endif
                                        <!-- <li><a class="active" href="{{ route('home') }}">Home</a> -->
                                            <!--<ul class="sub_menu">
                                                <li><a href="#">Home shop 1</a></li>
                                                <li><a href="#">Home shop 2</a></li>
                                                <li><a href="#">Home shop 3</a></li>
                                                <li><a href="#">Home shop 4</a></li>
                                                <li><a href="#">Home shop 5</a></li>
                                            </ul>-->
<!--                                         </li>
                                        <li><a href="{{ route('shop') }}"> Shop</a></li>
                                        <li><a href="{{ route('store') }}"> Our Stores</a></li>

                                        <li><a>More <em class="fas fa-chevron-down fa-xs"></em></a>
                                            <ul class="sub_menu">
                                                <li><a href="{{ route('page', 'about') }}">About Us</a></li>
                                                <li><a href="{{ route('page', 'faq') }}">FAQ</a></li>
                                                @if(isLoggedIn())
                                                    <li><a href="{{ route('my-account') }}">My Account</a></li>
                                                @endif
                                            </ul>
                                        </li>

                                        <li><a href="{{ route('contact') }}"> Contact Us</a></li>
 -->                                    </ul>
                                </nav>
                            </div>
                            <!--main menu end-->
                        </div>
<!--                         <div class="col-lg-3">
                            @if(trim(Cache::get('support_number', '')) != '')
                            <div class="header_call-support">
                                <p><a href="#">{{Cache::get('support_number')}}</a> Customer Support</p>
                            </div>
                            @endif
                        </div>
 -->                    </div>
                </div>
            </div>

            <!-- Delivery Address Mobile -->
            <div style="text-align: center;font-size: 15px;font-weight: 700;color: #1d86d6" data-toggle="modal" data-target="#checkDeliveryModal">
                <i class="fas fa-map-marker-alt"></i> &nbsp;Deliver to <span>
@if(!isset(session()->get('user')['user_id']))
    462001
@else
    @if(isset(session()->get('user')['dpincode']))
    {{ session()->get('user')['dpincode'] }}

    @else
    462001
    @endif
@endif
                </span>
            </div>            
            </nav>
    <div>
        @include("themes.".get('theme').".parts.breadcrumb")
        @include("themes.".get('theme').".common.msg")
    </div>

<!-- The Modal -->
<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">List Your Kirana Items</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <form action="{{ route('kiranasubmit') }}" method="post" enctype="multipart/form-data" id="kiranaform">      
        @csrf
      <!-- Modal body -->
      <div class="modal-body">
        <p class="alert alert-danger" id="fail_msg" style="display: none"><i class="fas fa-times"></i> Invalid Extension. Please try again!</p>

        <div class="form-group">
            <label>Kirana Subject</label>
            <input type="text" name="kirana_subject" id="kirana_subject" class="form-control">
        </div>
        <div class="form-group">
            <input type="hidden" name="user_id" value="<?php echo (isset(session()->get('user')['user_id'])) ? session()->get('user')['user_id'] : ''; ?>">
            <label for="">Items</label>
            <textarea name="request_list" id="request_area" cols="5" rows="5" class="form-control"></textarea>
        </div>
        
        <div class="form-group">
            <label>Upload List File : <span style="color:red;font-size: 12px">( Only .jpg , .png, and .pdf extensions allowed. )</span></label>
            <input type="file" name="kiranaFile" id="kiranaFile" class="form-control" accept=".pdf,.jpg,.png"/>
        </div>
        
       </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button class="btn btn-success" type="button" id="requestbtn">Submit</button>
        <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
      </div>
    </form>
    
    </div>
  </div>
</div>

<!-- The Modal -->
<div class="modal fade" id="checkDeliveryModal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <!-- Modal body -->
      <div class="modal-body">

        @if(!isset(session()->get('user')['user_id']))
        <a href="" class="btn btn-primary btn-block">Sign in to see all addresses</a>

        @else
        <h4>User Addresses</h4>
        <p>
        @if(isset(session()->get('user')['darea']) && !empty(session()->get('user')['darea']) && isset(session()->get('user')['dcity']) && !empty(session()->get('user')['dcity']) && isset(session()->get('user')['dstate']) && !empty(session()->get('user')['dstate']) )
            {{ session()->get('user')['darea'] . " " . session()->get('user')['dcity'] . " " . session()->get('user')['dstate'] }}
         @endif
        </p>

        @if(!empty(session()->get('user')['other_add_array']))

        @foreach(session()->get('user')['other_add_array'] as $oarr)

        <p>{{ $oarr->area . " " . $oarr->city . " " . $oarr->state }}</p>

        @endforeach

        @endif

        @endif
        <hr>

                        <form action="{{ route('pincheck') }}" method="post" id="pinForm">
                        <div class="row">
                            <div class="col-md-12">
                                <span style="margin-bottom: 5px !important; margin-top: 20px; display:block;"><strong>Delivery Availablity</strong></span>
                            </div>
                            
                            <div class="col-lg-12">
                                <input type="text" class="form-control form-control-pincode" placeholder="Enter Pincode" id="pincode_input" name="pincode_input">
                                @csrf
                                <div id="pinstatus"></div>
                            </div>
                            <div class="col-lg-12" style="display: flex;justify-content: center;margin-top: 3%">
                                <button class="btn checkbtn" type="submit" id="checkPin">Check</button>
                            </div>
                            
                        </div>
                        </form>        
        
       </div>
    
    </div>
  </div>
</div>
<script src="{{ theme('js/plugins.js') }}"></script>
<script src="{{ theme('js/home.js') }}"></script>
    <style>
        .footer-new{
            padding:30px 0px;
            background:#dcdcdc;
        }
        @media (min-width:767px) {
            .footer-box-style-1{
                padding:0px 20px;
            }
        }
        .footer_link_header{
            font-size: 20px;
            font-family: 'Oswald', sans-serif;
            text-transform: uppercase;
            font-weight: 500;
            letter-spacing: 1px;
            color:#000;
        }
        .footer_link_single>a{
                color: #737373;
                line-height: 28px;
                font-size: 12px;
        }
        
        
        
        .footer_android_img{
            max-width:250px;
            margin-top:15px;
        }
      
      .facebook_color{color:#f00};
      
      
      
    </style>
    
    <footer class="footer-new">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 col-md-12">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="footer-box-style-1">
                                <div class="footer_link_header">About</div>
                                <div class="footer_links">
                                    <ul class="footer_link_list">
<!--                                 <li class="footer_link_single">
                                    <a href="{{ route('shop') }}"> Shop</a>
                                </li>                                        
 -->
                                 <!-- <li class="footer_link_single"><a href="{{ route('page', 'faq') }}">FAQ</a></li> -->
@if(Cache::has('pages') && is_array(Cache::get('pages')) && count(Cache::get('pages')))
@foreach(Cache::get('pages') as $page)
    @if($page->footer_column == 1)
        <li class="footer_link_single"><a href="{{ route('cms_page',$page->slug) }}">{{ $page->title }}</a></li>
    @endif
@endforeach
@endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="footer-box-style-1">
                                <div class="footer_link_header">Help & Support</div>
                                <div class="footer_links">
                                    <ul class="footer_link_list">
@if(Cache::has('pages') && is_array(Cache::get('pages')) && count(Cache::get('pages')))
@foreach(Cache::get('pages') as $page)
    @if($page->footer_column == 2)
        <li class="footer_link_single"><a href="{{ route('cms_page',$page->slug) }}">{{ $page->title }}</a></li>    
    @endif
@endforeach
@endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="footer-box-style-1">
                                <div class="footer_link_header">Policy</div>
                                <div class="footer_links">
                                    <ul class="footer_link_list">
                                        <!-- <li class="footer_link_single"><a href="{{ route('page', 'about') }}">About Us</a></li> -->
<!--                                 <li class="footer_link_single">
                                    <a href="{{ route('contact') }}"> Contact Us</a>
                                </li>                                        
 -->
 @if(Cache::has('pages') && is_array(Cache::get('pages')) && count(Cache::get('pages')))
@foreach(Cache::get('pages') as $page)
    @if($page->footer_column == 3)
        <li class="footer_link_single"><a href="{{ route('cms_page',$page->slug) }}">{{ $page->title }}</a></li>    
    @endif
@endforeach
@endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3" style="display:none">
                            <div class="footer-box-style-1">
                                <div class="footer_link_header">Legal</div>
                                <div class="footer_links">
                                    <ul class="footer_link_list">
@if(Cache::has('pages') && is_array(Cache::get('pages')) && count(Cache::get('pages')))
@foreach(Cache::get('pages') as $page)
    @if($page->footer_column == 4)
        <li class="footer_link_single"><a href="{{ route('cms_page',$page->slug) }}">{{ $page->title }}</a></li>    
    @endif
@endforeach
@endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                  <div class="row">
                    <div class="col-lg-4 offset-lg-4 col-md-4 offset-md-4 col-12">
                        <div class="footer_link_header">{{ __('msg.subscribe_to_our_newsletter') }}</div>                                
                        <div class="well1">
                          <form action="{{ route('newsletter') }}" method="POST" class="ajax-form">
                            @csrf
                            <div class="formResponse">
                                
                            </div>
                            <div class="input-group">                              
                              <input class="btn btn-lg border border-info" name="email" id="email" type="email" placeholder="Enter Your Email.." required>
                              <button class="btn btn-lg" type="submit" name="submit" value="submit"><em class="fas fa-paper-plane"></em></button>
                            </div>
                          </form>
                        </div>
                    </div>
                  </div>
                </div>
                
                <div class="col-lg-3 col-md-12">
                    <div class="footer-box-right">
                        <div class="footer_link_header">EXPERIENCE BATRAS APP</div>
                        <div class="footer_app_wrapper">
                          <div class="row">
                            <div class="col-sm-5">
                              @if(trim(Cache::get('android_app_url', '')) != '')
                              <div class='footer_android_img'>
                                  <a target="_blank" href="{{ Cache::get('android_app_url', 'https://play.google.com') }}">
                                      <img src="{{ _asset(Cache::get('google_play', theme('images/google1.png'))) }}" alt="Google Play Store" class="img-fluid">
                                  </a>                                                              
                              </div>
                              @endif
                            </div>   
                            <div class="col-sm-5">
                              <div class='footer_android_img'>
                                  <a target="_blank" href="#">
                                      <img src="https://batrasmart.com/adminon/dist/img/apple-apps.png" alt="Google Play Store" class="img-fluid">
                                  </a>                                                              
                              </div>
                            </div>   
                          </div>
                            
                          
                        </div>
                        <hr>
                        <div class="footer_link_header">Be Social </div>
                        <div class="footer_social_wrapper">
                            @if(Cache::has('social_media') && is_array(Cache::get('social_media')) && count(Cache::get('social_media')))
                                <div class="col-12 mt-2">
                                    <div class="row d-flex justify-content-start">
                                        <div class="social-button m-0">
                                            <ul>
                                                @foreach(Cache::get('social_media') as $i => $c)                                                    
                                                    
                                                    <li class="social-icon">
                                                        <a href="{{ $c->link }}" target="_blank" style="color:{{$c->color}} !important"><em class="fab {{ $c->icon }}"></em></a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <hr>
                        <div class="footer-widget d-none">
                            <div class="footer-menu  no-padding">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </footer>

    <footer class="footer-area">
        <div class="footer-big">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="line-footer">
                            <div class="text-center">
                                <p class="copyright-text">{{__('msg.copyright')}} &copy; {{date('Y')}} {{__('msg.made')}}
                                    <a target="_blank" href="https://smsolutions.in/" class="companyname"> {{__('msg.wteam')}}.</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

</body>
<!-- The core Firebase JS SDK is always required and must be listed first -->

<script src="https://www.gstatic.com/firebasejs/8.6.3/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/8.6.3/firebase-analytics.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.6.3/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.6.3/firebase-firestore.js"></script>
<script>
  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyBpvvf4RN2qMOTVQsQGcJw0mBCMYiMF53A",
    authDomain: "batrashome-e1d08.firebaseapp.com",
    projectId: "batrashome-e1d08",
    storageBucket: "batrashome-e1d08.appspot.com",
    messagingSenderId: "826619080864",
    appId: "1:826619080864:web:c3741c0f8e18ffe7f2d9c5",
    measurementId: "G-LZ9TT1Y76R"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();
</script>
</html>
<section class="footerfix section-content padding-bottom">
    <a href="#" id="scroll"><span></span></a>
    <div class="container">
        <nav class="row row-eq-height">
		@if(isset($data['dept_prods']) && count($data['dept_prods']['product']) > 0)				
			@foreach($data['dept_prods'] as $dp)
				@foreach($dp as $c)
					@if(!is_array($c))			
								<div class="col-md-3 mt-2">
                        <a href="{{ route('product-single', ['slug'=>$c->pslug]) }}">									
										<div class="card card-category eq-height-element">
											<div class="img-wrap">
												@php
												$url = "adminon/".$c->image;
												@endphp
			                                    <img src="{{ url($url) }}" alt="{{ $c->name ?? '' }}">
											</div>
											<div class="card-body">
			                                    <h4 class="card-title">{{ $c->name }}</h4>
			                                    {{ $c->subtitle }}
											</div>
										</div>
									</a>
								</div>
					@endif 
				@endforeach
			@endforeach
			@else
			<div class="jumbotron jumbotron-fluid" style="width: 100%">
				<h1 class="text-center">No Products Found for this Department!<br/> Please try again later!</h1>
			</div>
		@endif
        </nav>
    </div>
</section>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
    <script src="{{ theme('js/jquery-3.5.1.min.js') }}"></script>
	<script>

        window.addEventListener('offline', (e) => {
        	$("#no-internet").css('display','block');
        });

    if (navigator.onLine) { 
        	$("#no-internet").css('display','none');
    }


	</script>
	<style>
		button{
    width: 17%;
    padding: 15px 14px;
    border: none;
    background: blue;
    color: white;
    font-size: 18px;
    text-transform: uppercase;
    cursor: pointer;			
		}
	</style>
</head>
<body>
	<div id="no-internet" style="display: none">
	<center>
		<br>
		<br>
		<h1>No Internet</h1>
    <script src="{{ theme('js/lottie.js') }}"></script>
<lottie-player src="{{ theme('js/lottie-icon.json') }}"  background="transparent"  speed="1"  style="width: 300px; height: 300px;"  loop  autoplay></lottie-player>
	<h3>You're Offline</h3>
	<p style="width: 25%">No internet or Slow Connection found.
Check your connection or Try again</p>
	<button>Reload</button>
	</center>
	</div>
</body>
</html>
<style>
    .offer_container, .banner_container{
        margin:20px 0px;
    }
    
    .ad_offer_box_wrapper{
        position:relative;
        margin-bottom:20px;
        background-color:#fff;
    }
    
    .ad_offer_box_wrapper>a{
        height:240px;
        width:100%;
        display:inline-block;
    }
    
    .ad_offer_box_wrapper>a>.offer_cat_img{
        width:150px;
        margin: 30px auto;
    }
    
    .ad_offer_badge{
        position:absolute;
        top: 2%;
        left: 2%;
        z-index: 2;
        width: 70px;
    }
    
    .ad_offer_text{
position: absolute;
    top: 6%;
    left: 3%;
    line-height: 17px;
    z-index: 3;
    color: #fff;
    font-size: 16px;
    width: 62px;
    font-weight: 700;
    }
    
    .ad_offer_text_dyn{
        font-weight:600px !important;
    }
    
    .offer_cat_name{
        position: absolute;
        top: 85%;
        color: #ffffff;
        font-size: 16px;
        z-index: 5;
        background: #1d86d6;
        width: 100%;
        text-align: center;
        font-weight: 700;
        box-shadow: 0px 2px 2px 0px #00000082;
    }
</style>

<section class="section-content spacingrm container">

@if(Cache::has('ad_groups') && is_array(Cache::get('ad_groups')) && count(Cache::get('ad_groups')))
    
@foreach(Cache::get('ad_groups') as $ag)
    <div class="banner_container">
        <div class="container">
        @if(isset($ag->ad_group_type) && $ag->ad_group_type == 1)
    
            <div class="row">
        @if(Cache::has('ads') && is_array(Cache::get('ads')) && count(Cache::get('ads')))
            @foreach(Cache::get('ads') as $o)
            @if(isset($o->image) && trim($o->image) !== "" && $o->ad_group_id == $ag->ad_group_id)
                <div class="col-md-6 div_banner_style" style="padding-left: 3px !important;padding-right: 3px !important;padding-top: 7px">
                    <a href="{{ $o->ad_link }}">
                        <img src="{{ $o->image }}" alt="{{ $o->ad_link }}" class="img-fluid image_banner_style" style="display: block;margin: 0 auto;width: 100%;">
                    </a>
                </div>
            @endif
            @endforeach
    
        @endif
            </div>
        @endif
        </div>
    </div>
    
    @if(isset($ag->ad_group_type) && $ag->ad_group_type == 2)
    <br>
        <div class="offer_container">
            <div class="container">
            <h4 class="title-section title-sec font-weight-bold">{{ $ag->group_name }}</h4>
            <hr class="line">
            
                <div class="row">
                    @if(Cache::has('ads') && is_array(Cache::get('ads')) && count(Cache::get('ads')))
                        @foreach(Cache::get('ads') as $o)
                        @if(isset($o->image) && trim($o->image) !== "" && $o->ad_group_id == $ag->ad_group_id)
                            <div class="{{ getClassByCount($ag->image_per_row) }}" style="">
                                
                                <div class="ad_offer_box_wrapper" style="background-color: {{ $ag->back_color  }}">
                                @if($ag->is_third_party == 1)
                                    <a href="{{ $o->ad_link }}" target="_blank">
                                        <div class="offer_cat_img">
                                        <img src="{{ $o->image }}" alt="{{ $o->image_caption }}" class="img-fluid" style="{{ getStyleByCount($ag->image_per_row) }}">
                                        </div>
                                    </a>
                                @else
                                    @if(empty($o->subcategory_slug))
                                        <a href="{{ route('category',['slug'=>$o->category_slug2]) }}">
                                            <div class="offer_cat_img">
                                                <img src="{{ $o->image }}" alt="{{ $o->image_caption }}" class="img-fluid" style=" {{ getStyleByCount($ag->image_per_row) }}">
                                            </div>
                                            <p class="offer_cat_name">{{ $o->cat_name }}</p>
                                        </a>
                                    @else
                                        <a href="{{ route('shop',['category'=>$o->category_slug2,'sub-category'=>$o->subcategory_slug2]) }}">
                                            <div class="offer_cat_img">
                                                <img src="{{ $o->image }}" alt="{{ $o->image_caption }}" class="img-fluid" style="{{ getStyleByCount($ag->image_per_row) }}">
                                            </div>
                                            <p class="offer_cat_name">{{ $o->scat_name }}</p>
                                        </a>
                                    @endif
                                @endif
                                
                                <img class="ad_offer_badge" src="{{asset('images/offer-badge.png')}}">
                                <p class="ad_offer_text" style="text-align: center;"><small>Upto</small><br/><span class="ad_offer_text_dyn">{{ $o->discount_text }} % </span><br/><small>off</small></p>
                                </div>
                            </div>
                        @endif
                        @endforeach
                
                    @endif
                </div>
            </div>
        </div>
    @endif
    
@endforeach

@endif


</section>

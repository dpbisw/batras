<!-------------------------------->

@if(Cache::has('topdeals') && is_array(Cache::get('topdeals')) && count(Cache::get('topdeals')))
    <!--section recently added and new on ekart -->
    <section class="section-content padding-bottom mt-3 sellproc">
        <div class="container">
                <h4 class="title-section title-sec font-weight-bold">Top Deals</h4>
                <hr class="line">
            
            <div class="slider-container-box">
                <div class="swiper-container swiper-style-1-slider-container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        @foreach(Cache::get('topdeals') as $p)
                            
                            <div class="swiper-slide">
                                <figure class="card card-sm card-product-grid card-style-1">
                                    <aside class="add-to-fav add-to-fav-1">
                                        <button type="button" class="d-block btn {{ (isset($p->is_favorite) && intval($p->is_favorite)) ? 'saved' : 'save' }}" data-id='{{ $p->id }}' ></button>
                                        @if($p->indicator == 1)
                                            <img style="width:15px;" class="tooltip_new"  src="{{asset('images/vag.svg')}}" data-toggle="tooltip" data-placement="top" title="Vegetarian">
                                            
                                        @elseif($p->indicator == 2)
                                            <img style="width:15px;" class="tooltip_new"  src="{{asset('images/nonvag.svg')}}" data-toggle="tooltip" data-placement="top" title="Non Vegetarian">
                                        @endif
                                    </aside>
                                    
                                    <a href="{{ route('product-single', $p->slug) }}" class="img-wrap"> <img src="{{ $p->image }}" alt="{{ $p->name ?? 'Product Image' }}"> </a>
                                    <figcaption class="info-wrap">
                                        <div class="text-wrap p-2 text-left">
                                            <a href="{{ route('product-single', $p->slug) }}" class="title-product product-name">{{ $p->name }}<span class="product_variant_val" id="variant_val_{{ $p->id }}"> : 
                                                
                                                @if(count(getInStockVarients($p)))
                                                    @php
                                                        print_r( get_varient_name($p->variants[0]))
                                                    @endphp
                                                @endif
                                            </span></a>
                            
                                            <span class="text-muted style-desc d-none">
                                                @if(strlen(strip_tags($p->description)) > 20) {!! substr(strip_tags($p->description), 0,20)."..." !!} @else {!! substr(strip_tags($p->description), 0,20) !!} @endif
                                            </span>
                                            <div class="price mt-1 mb-2">
                                                <div class="row">
                                                    <div class="col-8">
                                                        <div class="row">
                                                            <div class="col-6 pr-1">
                                                                <p class="product-price">MRP</p>
                                                                <p class="product-price" id="mrp_{{ $p->id }}">{!! print_mrp($p) !!}</p>
                                                            </div>
                                                            
                                                            <div class="col-6 pl-1">
                                                                <p class="product-price-batras">Batras</p>
                                                                <p class="product-price-batras product-price-batras-spl" id="price_{{ $p->id }}">{!! print_price($p) !!}</p>
                                                            </div>
                                                            <div class="col-12">
                                                                <p class="product-price" style="font-size: 12px;">(Inclusive of all taxes)</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-4">
                                                        <div class="product-savings">
                                                            <p class="ps1">Save</p>
                                                            <p class="ps2" id="save_{{ $p->id }}">₹ {{$p->variants[0]->price - $p->variants[0]->discounted_price}} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </figcaption>
                                    
                                    
                                    
                                    @if(count(getInStockVarients($p)))
                                    <div class="add_cart_style_1">
                                        <form action='{{ route('cart-add-single-varient') }}' method="POST">
                                            <button type="submit" class="btn cart-1 fa fa-shopping-cart"><span>&nbsp;&nbsp;{{__('msg.add_to_cart')}}</span></button>
                                            
                                            <div class="style_1_select">
                                                <input type="hidden" name="id" value="{{ $p->id }}">
                                                <select class="form-control" name="varient" data-id="{{ $p->id }}">
                                                    @foreach(getInStockVarients($p) as $v)
                                                        <option value="{{ $v->id }}"  data-price='{{ get_price(get_price_varients($v)) }}' data-mrp='{{ get_price(get_mrp_varients($v)) }}' data-savings='{{ get_savings_varients($v) }}' data-save='{{ get_save_varients($v, false) }}'>{{ get_varient_name($v) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                    @else
                                    <div class="add_cart_style_1">
                                        <button type="button" disabled="true" class="btn cart-1 fa fa-shopping-cart"><span>&nbsp;&nbsp;{{__('msg.add_to_cart')}}</span></button>
                                    </div>
                                        <span class="sold-out">{{ __('msg.sold_out') }}</span>
                                    @endif
                                    
                                    @if(!count(getInStockVarients($p)) < 1)
                                    <div class="disc-price-tag disc-price-tag-1">
                                        <div class="tag_badge tag-badge-1">
                                            <img class="tag-img img-fluid" src="{{asset('images/offerimg.png')}}">
                                            <p class="tag-text tag-text-1" id="savings_{{ $p->id }}"> {{ get_savings_varients($p->variants[0]) }} </p>
                                        </div>
                                    </div>
                                    @endif
                                    
                                    
                                </figure>
                            </div>
                            
                        @endforeach
                    </div>
                    <!-- If we need pagination -->
                    <div class="swiper-pagination"></div>
        
                </div>
                
                <!-- If we need navigation buttons -->
                    <div class="swiper-button-prev sites-slider__prev"></div>
                    <div class="swiper-button-next sites-slider__next"></div>
            </div>
        </div>
    </section>
    <!--section end recently added and new on ekart -->
@endif
@if(Cache::has('departments') && is_array(Cache::get('departments')) && count(Cache::get('departments')))
	<!--section categories popular categories transparent image-->
    <section class="section-content padding-bottom popular-categories mb-4 mt-4">
		<div class="container">
			<h4 class="title-section font-weight-bold">{{__('msg.popular_departments')}}</h4>
			<hr class="line">
			
			<style>
    			.owl-dept-item-wrapper{
    			    background-color:#fff;
    			    border-radius:3px;
    			    box-shadow: 1px 1px 5px #e6e6e6;
                    margin: 5px 0px;
    			}
    			
    			.owl-dept-item-wrapper a{
    			    color:#363940;
    			    font-weight: 700;
                    letter-spacing: 0.1px;
    			}
			    .owl-dept-item-wrapper>.owl-dept-img>a>img{
			        padding: 0px 20px;
			    }
			    
			    .owl-carousel-departments{
			        position:relative;
			    }
			    
			    .owl-carousel-departments>.owl-nav{
			        position:relative;
			    }
			    
			    .owl-dept-text{
			        min-height:50px;
			    }
			    
			    @media(max-width:480px){
			        .owl-dept-item-wrapper a{
			            font-size:12px;
			            line-height:13px;
			        }
			    }
			</style>
			
			<div class="owl-container">
			    <div class="owl-carousel owl-carousel-departments" id="departments-slider-hm">                    
                    @foreach(Cache::get('departments') as $dept)
                    @if($dept->is_home !=0)
                        @if(isset($dept->image) && trim($dept->image) !== "")
                              <div class="owl-dept-item-wrapper">
                                  <div class="owl-dept-img">
                                      <a href="department/{{ $dept->slug }}"><img src="{{ $dept->image }}" alt="{{ $dept->name }}" ></a>
                                  </div>
                                  
                                  <div class="owl-dept-text">
                                      <p class="text-center"><a href="departments/{{ $dept->slug }}">{{ $dept->name }}</a></p>
                                  </div>
                              </div>          
                        @endif
                        @endif
                    @endforeach
                </div>
			</div>
			
			<script>
                $(document).ready(function(){
                    $("#departments-slider-hm").owlCarousel({
                        loop:true,
                        margin:20,
                        autoplay:true,
                        autoplayTimeout:1800,
                        autoplayHoverPause:true,
                        responsive:{
                            0:{
                                items:2
                            },
                            576:{
                                items:3
                            },
                            991:{
                                items:5,
                                // nav:true,
                            },
                            1199:{
                                items:6,
                                // nav:true,
                            },
                            1920:{
                                items:7,
                                // nav:true,
                            }
                        }
                    });
                });
            </script>
			
			<div class="d-none">
				<div class="row p-0">
					@foreach(Cache::get('categories') as $i => $c)
						<div class="col-lg-4 col-md-6 col-12">
                        @if($c->web_image !== '')
							<div class="item popular web">
                                <img class="category-item" src="{{ $c->web_image }}" alt="{{ $c->name ?? 'Category' }}">
                                <span class="overlay-text">
                                    <p class="text-dark title font-weight-bold name mb-0">{{ $c->name }}</p>
                                    <small class="text-muted subtitle">{{ $c->subtitle }}</small>
                                    <p class="m-0">
                                        <a href="{{ route('category', $i) }}" class="shop-now">{{__('msg.shop_now')}} <i class="fa fa-chevron-right"></i></a>
                                    </p>
                                </span>
							</div>
                        @else
                            <div class="item category-item-card rounded">
                                <img class="category-item" src="{{ $c->image }}" alt="{{ $c->name ?? 'Category' }}">
                                <span class="overlay-text">
                                    <p class="text-dark title font-weight-bold name mb-0">{{ $c->name }}</p>
                                    <small class="text-muted subtitle">{{ $c->subtitle }}</small>
                                    <p class="m-0">
                                        <a href="{{ route('category', $i) }}" class="shop-now">{{__('msg.shop_now')}} <em class="fa fa-chevron-right"></em></a>
                                    </p>
                                </span>
                            </div>
                        @endif

						</div>
					@endforeach
				</div>
			</div>
		</div>
    </section>
    <!--end section categories-->
@endif
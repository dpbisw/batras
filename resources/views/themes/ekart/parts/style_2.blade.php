<style>
    .add_cart_style_2{
        text-align: center;
    }
    .add_cart_style_2 .cart-2{
        text-align: center;
        background: #1d86d6;
        width: 80%;
        margin: 0 auto;
        color: #fff;
    }
    
    .style_1_save{
        font-size:15px;
        margin-left:5px;
    }
    
    .style_2_select{
        width: 80%;
        text-align: center;
        margin: 0 auto;
        margin-top:5px;
    }
    
    .style_2_select select{
        height:30px;
    }
    
    .disc-price-tag{
        position: absolute;
        margin-top:-8px;
    }
    
    .card-product-grid-style-2{
        overflow:visible;
    }
    
    .tag_badge{
        position:relative;
    }
    
    .tag-text {
        position: absolute;
        top: 0;
        font-size: 13px;
        font-weight: 700;
        color: #fff;
        left: 14%;
    }
    
    .cart-2 {
        font-weight: 700 !important;
        letter-spacing: 0.6px;
    }
</style>
@if(isset($s->products) && is_array($s->products) && count($s->products))
    <!--section recently added and new on ekart -->
    <section class="section-content padding-bottom mt-3 sellpro">
        <div class="container">
            @if(isset($s->title) && $s->title != "")
                <h4 class="title-section title-sec font-weight-bold">{{ $s->title }} <small class="text-muted short-desc"> {{ $s->short_description }}</h4>
                @if(isset($s->slug) && $s->slug != "")
                    <a href="{{ route('section',$s->slug) }}" class="view title-section viewall">{{__('msg.view_all')}}</a>
                @endif
                <hr class="line">
            @endif
            <div class="row ekart">

                @php   $maxProductShow = get('style_2.max_product_on_homne_page'); @endphp
                @foreach($s->products as $p)
                    @if((--$maxProductShow) > -1)
                        <div class="col-xl-2 col-lg-3 col-md-4 col-6 recent-add">
                            <figure class="card card-sm card-product-grid card-product-grid-style-2">
                                <aside class="add-to-fav">
                                    <button type="button" class="btn {{ (isset($p->is_favorite) && intval($p->is_favorite)) ? 'saved' : 'save' }}" data-id='{{ $p->id }}' />
                                </aside>
                                <a href="{{ route('product-single', $p->slug) }}" class="img-wrap"> <img src="{{ $p->image }}" alt="{{ $p->name ?? 'Product Image' }}"> </a>
                                <figcaption class="info-wrap">
                                    <div class="text-wrap p-3 text-left">
                                        <a href="{{ route('product-single', $p->slug) }}" class="title font-weight-bold product-name">{{ $p->name }}</a>

                                        <span class="text-muted style-desc d-none">
                                            @if(strlen(strip_tags($p->description)) > 20) {!! substr(strip_tags($p->description), 0,20)."..." !!} @else {!! substr(strip_tags($p->description), 0,20) !!} @endif
                                        </span>
                                        <div class="price mt-1 ">
                                            <strong id="price_{{ $p->id }}">{!! print_price($p) !!}</strong> &nbsp; <s class="text-muted" id="mrp_{{ $p->id }}">{!! print_mrp($p) !!}</s>
                                            <small class="text-success d-none" id="savings_{{ $p->id }}"> {{ get_savings_varients($p->variants[0]) }} </small>
                                        </div>
                                    </div>
                                </figcaption>
                                @if(count(getInStockVarients($p)))
                                    <div class="add_cart_style_2">
                                        <form action='{{ route('cart-add-single-varient') }}' method="POST">
                                            <button type="submit" class="btn cart-2 fa fa-shopping-cart"><span>&nbsp;&nbsp;{{__('msg.add_to_cart')}}</span></button>
                                            
                                            <div class="style_2_select">
                                                <input type="hidden" name="id" value="{{ $p->id }}">
                                                <select class="form-control" name="varient" data-id="{{ $p->id }}">
                                                    @foreach(getInStockVarients($p) as $v)
                                                        <option value="{{ $v->id }}"  data-price='{{ get_price(get_price_varients($v)) }}' data-mrp='{{ get_price(get_mrp_varients($v)) }}' data-savings='{{ get_savings_varients($v) }}'>{{ get_varient_name($v) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                @else
                                    <span class="sold-out">{{ __('msg.sold_out') }}</span>
                                @endif
                                
                                @if(!count(getInStockVarients($p)) < 1)
                                <div class="disc-price-tag">
                                    <div class="tag_badge">
                                        <img class="tag-img" src="{{asset('images/offerimg.png')}}" class="img-fluid">
                                        <p class="tag-text" id="savings_{{ $p->id }}"> {{ get_savings_varients($p->variants[0]) }} </p>
                                    </div>
                                    
                                </div>
                                @endif
                                
                            </figure>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </section>
    <!--section end recently added and new on ekart -->
@endif



<section class="section-content padding-bottom mt-5">
    <!--user address-->
    <a href="#" id="scroll"><span></span></a>
    <nav aria-label="breadcrumb"> 
        <ol class="breadcrumb">
            <li class=" item-1"></li>
            <li class="breadcrumb-item">{{__('msg.my_account')}}</li>
            <li class="breadcrumb-item active" aria-current="page">User Request List</li>
        </ol>   
    </nav>
    <div class="container">
        <div class="row">
            @include("themes.$theme.user.sidebar")
            <main class="col-md-9">
                <div class="card">
                    <div class="card-body">
                    	<?php 
                    	if($data['exists'] == 0){
                    		?>
                    		<h3 class="jumbotron text-center">No data Found</h3>
                    		<?php
                    	} 
                    	else{
                    		if(isset($data['list-requests'])){
                    		$i=0;
                    		foreach($data['list-requests'] as $rl){

                    		?>
							<div class="card mt-3">
							  <div class="card-body">
							  	<div class="row">
							  		<div class="col-lg-3">
							  			Request list <?php echo ++$i; ?>		
							  		</div>
							  		<div class="col-lg-3">
							  			<?php echo date('d-m-Y', strtotime($rl->created_at)); ?>
							  		</div>
							  		<div class="col-lg-4">
							  			<a href="{{ route('kiranadetail',$rl->user_request_list_id) }}" class="btn btn-primary">View List</a>
							  		</div>
							  	</div>
							  </div>
							</div>
                    		<?php
                    		}
                    	}
                    	if(isset($data['list-requests-detail'])){
                    		$detail = $data['list-requests-detail'][0];
                    		?>
                    		<a href="{{ route('kiranalist') }}" class="btn btn-primary">Back To List</a>
                    		<div class="card mt-3">
                    			<div class="card-body">
                    				<h4>List Items</h4>
                    				<p><?php echo $detail->request_list; ?></p>
                    			</div>
                    		</div>
                    		<?php 
                    		$file = "https://apps.smsolutions.in/BATRAS/adminon/upload/request-list-files/".$detail->list_file;

                    		// if(file_exists($file)){ 
                    			?>
                    		<div class="card mt-3">
                    			<div class="card-body">
                    				<h4>List Items</h4>
                        <iframe src="https://apps.smsolutions.in/BATRAS/adminon/upload/request-list-files/<?php echo $detail->list_file; ?>" width="100%" height="500px"></iframe>
                    			</div>
                    		</div>
                    		<?php
                    	// }
                    	}
                    	}
                    	?>
                    </div>
                </div>
            </main>   
        </div>   
    </div>
    <!--end user address-->
</section>
<aside class="col-md-3">
    <div class="card mb-3" style="display: none;">
        <div class="card-body">
            <div class="profile-header-container">
                <div class="profile-header-img">
                    <a class="navbar-brand ml-2" href="{{ route('home') }}">
                        <a href="{{ route('home') }}"><img src="{{ _asset(Cache::get('web_logo')) }}" alt="logo"></a>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="list-group">
        <a href="{{ route('my-account') }}" class="list-group-item">
            <em class="fa fa-user fa-2x" style="color: black"></em>
            <span class="side-menu">{{__('msg.my_profile')}}</span>
        </a>
        <a href="{{ route('change-password') }}" class="list-group-item">
            <!-- <em class="fa fa-asterisk"></em> -->
            <img src="{{ asset('new_icons/change_password.png') }}" class="img-fluid" style="width: 22px" alt="">
            <span class="side-menu">{{__('msg.change_password')}}</span>
        </a>
        <a href="{{ route('my-orders') }}" class="list-group-item">
            <!-- <em class="fas fa-taxi"></em> -->
            <img src="{{ asset('new_icons/my_orders.png') }}" class="img-fluid" style="width: 22px" alt="">
            <span class="side-menu">{{__('msg.my_orders')}}</span>
        </a>
        <a href="{{ route('notification') }}" class="list-group-item">
            <!-- <em class="fa fa-bell"></em> -->
            <img src="{{ asset('new_icons/notifications.png') }}" class="img-fluid" style="width: 22px" alt="">
            <span class="side-menu">{{__('msg.notifications')}}</span>
        </a>
        <a href="{{ route('favourite') }}" class="list-group-item">
            <!-- <em class="fa fa-heart fa-2x"></em> -->
            <img src="{{ asset('new_icons/my_wishlist.png') }}" class="img-fluid" style="width: 22px" alt="">
            <span class="side-menu">{{__('msg.favourite')}}</span>
        </a>
        <a href="{{ route('kiranalist') }}" class="list-group-item">
            <!-- <em class="fa fa-plus"></em> -->
            <img src="{{ asset('new_icons/kirana_list.png') }}" class="img-fluid" style="width: 22px" alt="">
            <span class="side-menu">{{__('msg.kiranalist')}}</span>
        </a>
        <a style="display:none;" href="{{ route('wallet-history') }}" class="list-group-item">
            <!-- <em class="fab fa-google-wallet"></em> -->
            <img src="{{ asset('new_icons/wallet_history.png') }}" class="img-fluid" style="width: 22px" alt="">
            <span class="side-menu">{{__('msg.wallet_history')}}</span>
        </a>
        <a  href="{{ route('transaction-history') }}" class="list-group-item">
            <!-- <em class="fa fa-outdent"></em> -->
            <img src="{{ asset('new_icons/transaction_history.png') }}" class="img-fluid" style="width: 22px" alt="">
            <span class="side-menu">{{__('msg.transaction_history')}}</span>
        </a>
        <a style="display:none;" href="{{ route('refer-earn') }}" class="list-group-item">
            <em class="fa fa-user-plus"></em>
            <span class="side-menu">{{__('msg.refer_and_earn')}}</span>
        </a>
        <a href="{{ route('addresses') }}" class="list-group-item">
            <!-- <em class="fa fa-wrench"></em> -->
            <img src="{{ asset('new_icons/manage_address.png') }}" class="img-fluid" style="width: 22px" alt="">
            <span class="side-menu">{{__('msg.manage_addresses')}}</span>
        </a>
        <a href="{{ route('logout') }}" class="list-group-item">
            <!-- <em class="fa fa-sign-out-alt"></em> -->
            <img src="{{ asset('new_icons/logout.png') }}" class="img-fluid" style="width: 22px" alt="">
            <span class="side-menu">{{__('msg.logout')}}</span>
        </a>
    </div>
</aside>
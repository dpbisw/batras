<section class="section-content padding-bottom footerfix">
    <a href="#" id="scroll"><span></span></a>
    <nav aria-label="breadcrumb" class="mt-5">
        <ol class="breadcrumb">
            <li class=" item-1"></li>
            <li class="breadcrumb-item"><a href="{{ route('my-account') }}">{{__('msg.my_account')}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{__('msg.favourites')}}</li>
        </ol>
    </nav>
    <div class="container mt-5 shopdetails">
        @if(isset($data['list']['data']) && is_array($data['list']['data']) && count($data['list']['data']))
            <div  id="products"  class="row view-group ekart spacingrm">
                @foreach($data['list']['data'] as $itm)
                    <div class="col-xl-2 col-lg-3 col-md-4 col-6 recent-add d-none">
                        <figure class="card card-sm card-product-grid">
                            <aside class="add-to-fav">
                                <a type="button" class="btn" href="{{ route('favourite-remove', $itm->product_id) }}">
                                    <em class="fas fa-heart"></em>
                                </a>
                            </aside>
                            <a href="{{ route('product-single', $itm->slug) }}" class="img-wrap"> <img src="{{ $itm->image }}" alt="{{ $tim->name ?? 'Product Image' }}"> </a>
                            <figcaption class="info-wrap">
                                <div class="text-wrap p-3 text-left">
                                    <a href="{{ route('product-single', $itm->slug) }}" class="title font-weight-bold product-name mb-2">{{ $itm->name }}</a>

                                    <div class="price mt-1 ">
                                        <strong id="price_{{ $itm->id }}">{!! print_price($itm) !!}</strong> &nbsp; <s class="text-muted" id="mrp_{{ $itm->id }}">{!! print_mrp($itm) !!}</s>
                                        <small class="text-success" id="savings_{{ $itm->id }}"> {{ get_savings_varients($itm->variants[0]) }} </small>
                                    </div>
                                </div>
                            </figcaption>
                            <span class="inner">
                                <form action='{{ route('cart-add') }}' method="POST">
                                    <input type="hidden" name='id' value='{{ $itm->product_id }}'>
                                    <input type="hidden" name="type" value='add'>
                                    <input type="hidden" name="child_id" value='{{ $itm->variants[0]->id }}' id="child_{{ $itm->id }}">
                                    <select name="varient" data-id="{{ $itm->id }}">
                                        @foreach($itm->variants as $v)
                                            @if(intval($v->stock))
                                                <option value="{{ $v->id }}"  data-price='{{ get_price(get_price_varients($v)) }}' data-mrp='{{ get_price(get_mrp_varients($v)) }}' data-savings='{{ get_savings_varients($v) }}'>{{ get_varient_name($v) }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                    <button type="submit" name="submit" class="btn fa fa-shopping-cart"><span>&nbsp;&nbsp;{{__('msg.add_to_cart')}}</span></button>
                                </form>
                            </span>
                        </figure>
                    </div>
                    
                    <div class="item1 col-sm-12 col-lg-6 col-md-4">
                        <div class="add-to-fav add-to-fav-1 text-center">
                            <button type="button" class="d-block btn {{ (isset($itm->is_favorite) && intval($itm->is_favorite)) ? 'saved' : 'save' }}" data-id='{{ $itm->id }}'></button>
                            @if($itm->indicator == 1)
                                <img style="width:15px;" class="tooltip_new"  src="{{asset('images/vag.svg')}}" data-toggle="tooltip" data-placement="top" title="Vegetarian">
                                
                            @elseif($itm->indicator == 2)
                                <img style="width:15px;" class="tooltip_new"  src="{{asset('images/nonvag.svg')}}" data-toggle="tooltip" data-placement="top" title="Non Vegetarian">
                            @endif
                        </div>
                        <a href="{{ route('product-single', $itm->slug ?? '-') }}">
                            <div class="thumbnail card-sm">
                                <a href="{{ route('product-single', $itm->slug ?? '-') }}">
                                    <div class="img-event">
                                        <img class="group list-group-image img-fluid" src="{{ $itm->image }}" alt="<?php echo $itm->id; //in_array($itm->id, $data['section-prods']); ?>">
                                    </div>
                                </a>
                                <div class="caption card-body">
                                    <div class="text-wrap text-left">
                                        <a href="{{ route('product-single', $itm->slug ?? '-') }}" class="title product-name">{{ $itm->name }}<span class="product_variant_val" id="variant_val_{{ $itm->id }}"> : 
                                        
                                        @if(count(getInStockVarients($itm)))
                                            @php
                                                print_r( get_varient_name($itm->variants[0]))
                                            @endphp
                                        @endif
                                    </span></a>

                                        <span class="d-none text-muted description1">@if(strlen(strip_tags($itm->description)) > 18) {!! substr(strip_tags($itm->description), 0,18) ."..." !!} @else {!! substr(strip_tags($itm->description), 0,18) !!} @endif</span>
                                        <div class="price mt-1 ">
                                            
                                            <div class="row">
                                                <div class="col-8">
                                                    <div class="row">
                                                        <div class="col-6 pr-1">
                                                            <p class="product-price">MRP</p>
                                                            <div id="mrp_dyn_{{ $itm->id }}">
                                                            <p class="product-price" id="mrp_{{ $itm->id }}">{!! print_mrp($itm) !!}</p>
                                                            </div>
                                                            <div>
                                                                <p class="product-price" id="dis_mrp_{{ $itm->id }}"></p>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-6 pl-1">
                                                            <p class="product-price-batras">Batras</p>
                                                            <div id="prc_dyn_{{ $itm->id }}">
                                                            <p class="product-price-batras product-price-batras-spl" id="price_{{ $itm->id }}">{!! print_price($itm) !!}</p> 
                                                            </div>
                                                            <div>
                                                                <p class="product-price-batras product-price-batras-spl" id="dis_prc_{{ $itm->id }}"></p>
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <p class="product-price" style="font-size: 12px;">(Inclusive of all taxes)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-4">
                                                    <div class="product-savings">
                                                        <p class="ps1 mb-0">Save</p>
                                                        <p class="ps2" id="save_{{ $itm->id }}">₹ {{$itm->variants[0]->price - $itm->variants[0]->discounted_price}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="add_cart_wrapper mt-2">
                                    @if(count(getInStockVarients($itm)))
                                    <div class="add_cart_style_1">
                                        <form action='{{ route('cart-add-single-varient') }}' method="POST">
                                            <button type="submit" class="btn cart-1 fa fa-shopping-cart"><span>&nbsp;&nbsp;{{__('msg.add_to_cart')}}</span></button>
                                            
                                            <div class="style_1_select">
                                                <input type="hidden" name="id" value="{{ $itm->product_id }}">
                                                <select class="form-control" name="varient" data-id="{{ $itm->id }}">
                                                    @foreach(getInStockVarients($itm) as $v)
                                                        <option value="{{ $v->id }}"  data-price='{{ get_price(get_price_varients($v)) }}' data-mrp='{{ get_price(get_mrp_varients($v)) }}' data-savings='{{ get_savings_varients($v) }}' data-save='{{ get_save_varients($v, false) }}'>{{ get_varient_name($v) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                    @else
                                        <span class="sold-out">{{ __('msg.sold_out') }}</span>
                                    @endif
                                    
                                    
                                </div>
                            </div>
                        </a>
                        
                        @if(!count(getInStockVarients($itm)) < 1)
                            <div class="disc-price-tag disc-price-tag-1">
                                <div class="tag_badge tag-badge-1">
                                    <img class="tag-img img-fluid" src="{{asset('images/offerimg.png')}}">
                                    <div id="dyn_savings_{{ $itm->id }}">
                                    <p class="tag-text tag-text-1" id="savings_{{ $itm->id }}"> {{ get_savings_varients($itm->variants[0]) }} </p>
                                    </div>
                                    <p id="prc_savings_{{ $itm->id }}" class="tag-text tag-text-1"></p>
                                </div>
                                
                            </div>
                        @endif
                    </div>
                @endforeach
            </div>
        @else
            <div class="row text-center">
                <div class="col-12">
                    <br><br>
                    <h3>{{__('msg.no_favorites_product_found')}}</h3>
                </div>
                <div class="col-12">
                    <br><br>
                    <a href="{{ route('shop') }}" class="btn btn-primary"><em class="fa fa-chevron-left mr-1"></em> {{__('msg.continue_shopping')}}</a>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col">
                @if(isset($data['last']) && $data['last'] != "")
                    <a href="{{ $data['last'] }}" class="btn btn-primary pull-left text-white"><em class="fa fa-arrow-left"></em> {{__('msg.previous')}}</a>
                @endif
            </div>
            <div class="col favnext text-right">
                @if(isset($data['next']) && $data['next'] != "")
                    <a href="{{ $data['next'] }}" class="btn btn-primary pull-right text-white">{{__('msg.next')}} <em class="fa fa-arrow-right"></em></a>
                @endif
            </div>
        </div>
    </div>
</section>
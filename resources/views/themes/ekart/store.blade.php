<section class="mt-5 padding-bottom section-contact footerfix">
	<div class="container">
		<div class="card">
            <h2 class="text-center pt-3 mb-5 title-contact">Our Stores</h2>
            <div class="container">
            <table class="table">
            	<thead>
            		<tr>
                              <th>Image</th>
            			<th>Name</th>
            			<th>Address</th>
            			<th>City</th>
            			<th>State</th>
            			<th>Phone</th>
            			<th>Timings</th>
            		</tr>
            	</thead>
            	<tbody>
            		@if(!empty($data['store']))
            			@foreach($data['store'] as $st)
            			<tr>
                                 <td><img src="{{ $st->image }}" style="width: 75px" /></td>
            			   <td>{{ $st->name }}</td>
					   <td>{{ $st->address }}</td>
					   <td>{{ $st->city }}</td>
					   <td>{{ $st->state }}</td>
					   <td>{{ $st->phone }}</td>
					   <td>{{ $st->timings }}</td>
					</tr>
            			@endforeach
            		@endif
            	</tbody>
            </table>			
            </div>
		</div>
	</div>
</section>
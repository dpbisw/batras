
<section class="section-content padding-bottom mt-4 spacingrm">
    <a href="#" id="scroll"><span></span></a>
    
    
    <!--Swiper Slider Start-->
    <style>
        .swiper-home-slider {
            width: 100%;
            height: auto;
        }
@media only screen and (max-width: 600px) {
    .web-swipe-img{
        display: none
    }
    .mobile-img {
        display: block
    }
}        
@media only screen and (min-width: 600px) {
    .web-swipe-img{
        display: block
    }
    .mobile-img {
        display: none
    }
    
    html, body { overscroll-behavior: none; }
}        

    </style>
   
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="swiper-container swiper-home-slider">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper">
                            <!-- Slides -->
                            @foreach(Cache::get('sliders') as $i => $s)
                                
                                    @if($s->type_id == 0 && $s->link == "")
                                    <div class="swiper-slide" data-swiper-autoplay="{{$s->slider_delay}}">
                                        <img src="{{ $s->image }}" alt="{{ $s->name }}" class="img-fluid web-swipe-img">
                                        <img src="{{ $s->mobile_image }}" alt="{{ $s->name }}" class="img-fluid mobile-img">
                                    </div>    
                                    @elseif($s->type_id == 0 && $s->link != "")
                                     <div class="swiper-slide" data-swiper-autoplay="{{$s->slider_delay}}">
                                        <a href="{{ $s->link }}">
                                            <img src="{{ $s->image }}" alt="{{ $s->name }}" class="img-fluid web-swipe-img">
                                            <img src="{{ $s->mobile_image }}" alt="{{ $s->name }}" class="img-fluid mobile-img">
                                        </a>
                                    </div>    
                                    @elseif($s->type == 'sub-category')
                                    <div class="swiper-slide" data-swiper-autoplay="{{$s->slider_delay}}">
                                        <a href="shop?category={{ $s->slug1 }}&sub-category={{ $s->slug2 }}" >
                                            <img src="{{ $s->image }}" alt="{{ $s->name }}" class="img-fluid web-swipe-img">
                                            <img src="{{ $s->mobile_image }}" alt="{{ $s->name }}" class="img-fluid mobile-img">
                                        </a>
                                    </div>
                                    @elseif($s->type == 'category')
                                    <div class="swiper-slide" data-swiper-autoplay="{{$s->slider_delay}}">
                                        <a href="{{ $s->type }}/{{ slugify($s->name) }}">
                                            <img src="{{ $s->image }}" alt="{{ $s->name }}" class="img-fluid web-swipe-img">
                                            <img src="{{ $s->mobile_image }}" alt="{{ $s->name }}" class="img-fluid mobile-img">
                                        </a>

                                    </div>
                                    @elseif($s->type == 'product')
                                    <div class="swiper-slide" data-swiper-autoplay="{{$s->slider_delay}}">
                                        <a href="{{ $s->type }}/{{ slugify($s->name) }}">
                                            <img src="{{ $s->image }}" alt="{{ $s->name }}" class="img-fluid web-swipe-img">
                                            <img src="{{ $s->mobile_image }}" alt="{{ $s->name }}" class="img-fluid mobile-img">

                                        </a>
                                    </div>
                                    @endif
                                    
                                    <!--<img src="{{theme('images/test-slider-1.jpg')}}" alt="{{ $s->name }}" class="img-fluid">-->
                                    
                                    <!--for link-->
                                    <!--<a href="#"><img src="{{theme('images/test-slider-1.jpg')}}" alt="{{ $s->name }}" class="img-fluid"></a>-->
                            @endforeach
                        </div>
                        <!-- If we need pagination -->
                        <div class="swiper-pagination swiper-pagination-custom"></div>
            
                        <!-- If we need navigation buttons -->
                        <div class="swiper-button-prev swiper-button-custom"></div>
                        <div class="swiper-button-next swiper-button-custom"></div>
            
                        <!-- If we need scrollbar -->
                        <!--<div class="swiper-scrollbar"></div>-->
                    </div>
                </div>
            </div>
        </div>
    
    
    
    <!--Swiper Slider END-->

</section>



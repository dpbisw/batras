
<style>
    #brdcrmb{
        padding: 10px 27px;font-size: 16px;font-weight: 500
    }
    #brdcrmb a{
        color: #424453 !important;
        margin:0px 6px;
    }
    .product_name
    {
        font-family: 'Oswald', sans-serif;
        letter-spacing: 1px;
        margin-bottom:0.2rem;
    }
    #price_flexbox
    {
        width: 60%;display: flex;align-items: center;
    }
    
    .product_details_save{
        font-size:15px;
        background-color: rgba(35,165,65,.1);
        border-radius: 2px;
        padding: 4px 8px;
        margin:10px 0px;
    }
    
    .product_details_save>small{
        font-size:15px;
        font-weight:700;
    }
    
    .product_deal_main{
        
    }
    .submit-btn
    {
        width: 100% !important;
        padding: 6px !important;
        background-color: #ffda00;
        border: none !important;
        color: black !important;
        font-family: 'Oswald', sans-serif;
        font-weight: 700;
        border-radius: 4px;
    }
    .submit-btn:hover{
        background-color: #FFDD33
    }
    .buy-now-btn{
        /*width: 19% !important;*/
        background-color: #1d86d6;
        color: #fff !important;
        padding: 7px;
        border: none !important;
        font-weight: 700;
    }
    .buy-now-btn:hover{
        background-color: #33D1FF
    }
    .checkbtn{
        text-transform: uppercase;
        font-weight: 700;
        padding: 4px 10px;
        color: #ec141e;
        border: 1px solid #ced4da;
    }
    
    .product_thumb{
        border: 1px solid #dcdcdc;
        display: inline-block;
        padding: 5px;
    }
    .product_thumb>img{
        /*max-height: 70px;*/
        /*max-width: 70px;*/
    }
    
    .form-control-pincode{
        height: calc(1.5em + .55rem + 2px);
        padding: 0.05rem .75rem;
    }
    
    .creativity>img{
        border-radius: 50%;
        width:69px;
    }
    #brand_name
    {
        font-size: 16px !important
    }
    #stock_status{
        color: #1aaf1a;font-size: 17px;font-weight: bolder;
    }
    .returnable_img{
        width:69px;
    }
    #yellow_box{
        padding: 2px 20px;   background-color: #ffda00;    border-radius: 4px;
    }
    #currency_value{
        font-family: 'Oswald', sans-serif;letter-spacing: 1px;
    }
    #mrp_box{
        margin-top: 10px;font-family: 'Oswald', sans-serif;letter-spacing: 1px;
    }
    #mrp_value{
        font-family: 'Oswald', sans-serif;letter-spacing: 1px;
    }
    #mrp_white_box{
        padding: 2px 20px
    }
    
    /* Mobile responsive */
@media only screen and (max-width: 767px) {
    .returnable_img{
        /*width: 13% !important*/
    }
    .outerdetailimg {
    height: 188px;
}
.product_name_details{
    margin-top: 17px;
}
#price_flexbox{
    height: 54px;
}
#yellow_box{
    padding: 9px 31px;
}


.currency_value2{
    width: 80px;
}
.mrp_box{
    width: 100px
}
.resp_variant{
    display: flex
}
.submit-btn{
    font-size: 11px;
}
.buy-now-btn{
    font-size: 11px;
}
}
@media only screen and (max-width: 600px) {
    .returnable_img{
        /*width: 20% !important*/
    }
.outerdetailimg {
    height: 188px;
}
.product_name_details{
    margin-top: 17px;
}

#price_flexbox{
    height: 54px;
}
#yellow_box{
    padding: 2px 8px;
    text-align: center;
}
.currency_value2{
    width: 80px;
}
.mrp_box{
    width: 100px
}
.resp_variant{
    display: flex
}
.submit-btn{
    font-size: 11px;
}
.buy-now-btn{
    font-size: 11px;
}
.checkbtn{
     font-size: 11px;   
     padding:6px;
}

}


    /*Tab responsive*/
    @media only screen and (min-width: 768px) and (max-width: 991px) {
    .returnable_img{
        /*width: 32% !important*/
    }
    }
    
    @media(max-width:500px){
        /*.submit-btn{*/
        /*    width:45% !important;*/
        /*}*/
        .buy-now-btn{
            width: 100% !important; 
        }
        
        
    }
    
    
    /**/
     .gallery-top {
      /*height: 80%;*/
      width: 100%;
    }

    .gallery-thumbs {
      /*height: 16%;*/
      box-sizing: border-box;
      padding: 5px 5px;
      margin: 0px 10px;
      width:500px;
      margin: 0 auto;
      margin-top: 40px;
    }

    .gallery-thumbs .swiper-slide {
      width: 25%;
      height: 100%;
      /*opacity: 0.8;*/
     /* padding: 37px;*/
     /*box-shadow: 0px 0px 5px #a7a7a7d9;*/
    }
    
    .gallery-thumbs .swiper-slide-inner-thumb{
        height:100% !important;
        box-shadow: 0px 0px 5px #a7a7a7d9;
        cursor:pointer;
    }
    
    .gallery-thumbs .swiper-slide .swiper-slide-inner-thumb >img{
       /*height:100% !important;*/
    }

    .gallery-thumbs .swiper-slide-thumb-active {
      opacity: 1;
    }
    
    .swiper-pagination-product{
        
    }
    
    .swiper-pagination-product .swiper-pagination-bullet {
        display: inline-block;
        margin: 0 3px;
        border-radius: 100%;
        background: #afafaf;
        opacity: .8;
        width: 10px;
        height: 10px;
        /*-webkit-box-shadow: 0 0 2px 1px #6f7284;*/
        /*box-shadow: 0 0 2px 1px #6f7284;*/
        /*border: 1px solid #6f7284;*/
        outline: 0;
    }
    
    .swiper-pagination-product .swiper-pagination-bullet-active {
        opacity: 1 !important;
        background: #313131 !important;
        /*border: 1px solid #fff !important;*/
        outline: 0 !important;
    }
    
    @media(max-width:767px){
        .gallery-thumbs {width:100%;}
    }
    
    .mobile-product-silder{
        display:none;
    }
    
    .web-product-slider{
        display:block;
    }
    
    @media(max-width:480px){
        .gallery-thumbs{
            display:none;
        }
        .mobile-product-silder{
            display:block;
        }
        
        .web-product-slider{
            display:none;
        }
    }
    
        .swiper-pagination-dark .swiper-pagination-bullet {
            display: inline-block;
            margin: 0 3px;
            border-radius: 100%;
            background: #fff;
            opacity: .8;
            width: 10px;
            height: 10px;
            -webkit-box-shadow: 0 0 2px 1px #6f7284;
            box-shadow: 0 0 2px 1px #6f7284;
            border: 1px solid #6f7284;
            outline: 0;
        }
        
        .swiper-pagination-dark .swiper-pagination-bullet-active {
            opacity: 1 !important;
            background: #000 !important;
            border: 1px solid #fff !important;
            outline: 0 !important;
        }

        .modal-footer{
            justify-content: center !important;
            border-top: none !important
        }

    .att_btn{
        padding: 5px 11px;
        border: none;
        background: rgba(0,0,0,.06);
        color: black
    }
    .att_active{
        background-color: #008ecc !important;
        color: white !important
    }    
</style>
<div class="section-content mt-2">
    <a href="#" id="scroll"><span></span></a>
    <div class="container mt-3 padding-bottom">
        <div class="card pb-4 mt-3">
            <!--Grid row-->
            @php
            $category_slug = $data['product']->category_slug;
            $subcategory_slug = $data['product']->subcategory_slug;
            $dept_slug = $data['product']->dept_slug;

            @endphp
            <span id="brdcrmb"><a href="{{ route('department',$dept_slug) }}">{{ $data['product']->dept_name }}</a> \ <a href="{{ route('category',$category_slug) }}">{{ $data['product']->category_name }}</a> \ <a href="{{ route('shop', ['category' => $category_slug, 'sub-category' => $subcategory_slug]) }}">{{ $data['product']->subcategory_name }}</a></span>
            <div class="row mt-3">
                <!--Grid column-->
                
                <div class="col-lg-6 mobile-product-silder" >
                    <div class="swiper-container newProductHigh">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img class="img-fluid" src="{{ $data['product']->image }}" alt="{{ $data['product']->name ?? 'Product Image' }}">
                            </div>
                            @foreach($data['product']->other_images as $index => $img)
                            <div class="swiper-slide">
                                <img class="img-fluid" src="{{ $img }}">
                            </div>
                            @endforeach
                        </div>
                        <div class="swiper-pagination swiper-pagination-dark"></div>
                    </div>
                </div>
                
                
                
                <div class="col-lg-6 col-md-12 col-12 web-product-slider">
                    <div class="swiper-container gallery-topr">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide swiper-slide-product">
                                <img class="img-fluid" src="{{ $data['product']->image }}" alt="{{ $data['product']->name ?? 'Product Image' }}">
                            </div>
                            @if(isset($data['product']->other_images) && is_array($data['product']->other_images) && count($data['product']->other_images))
                                    @foreach($data['product']->other_images as $index => $img)
                                    <div class="swiper-slide swiper-slide-product">
                                        <img class="img-fluid" src="{{ $img }}" alt="{{ $data['product']->name ?? 'Product Image' }}">
                                    </div>
                                    @endforeach
                            @endif
                          
                        </div>
                        <div class="swiper-pagination swiper-pagination-product"></div>
                        <!-- Add Arrows -->
                        <div class="swiper-button-next swiper-button-white swiper-button-nxt-top"></div>
                        <div class="swiper-button-prev swiper-button-white swiper-button-prv-top"></div>
                    </div>
                    
                    <div class="swiper-container gallery-thumbs">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide swiper-slide-product-thumb">
                                <div class="swiper-slide-inner-thumb">
                                    <img class="img-fluid" src="{{ $data['product']->image }}" alt="{{ $data['product']->name ?? 'Product Image' }}">
                                </div>
                            </div>
                            @if(isset($data['product']->other_images) && is_array($data['product']->other_images) && count($data['product']->other_images))
                                    @foreach($data['product']->other_images as $index => $img)
                                    <div class="swiper-slide swiper-slide-product-thumb">
                                        <div class="swiper-slide-inner-thumb">
                                            <img class="img-fluid" src="{{ $img }}" alt="{{ $data['product']->name ?? 'Product Image' }}">
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                        </div>
                        
                    </div>
                </div>
                
                
                <!--Grid column-->
                <!--Grid column-->
                <div class="col-lg-6 col-md-12 col-12 text-left productdetails2" style="">
                    <aside class="add-to-fav">
                        <button id="favbtn" type="button" class="btn {{ (isset($data['product']->is_favorite) && intval($data['product']->is_favorite)) ? 'saved' : 'save' }}" data-id='{{ $data['product']->id }}' />
                    </aside>
                    <!--Content-->
                    <div class="text-left product_name_details">
                        @if(!empty($data['product']->attributes))

                        <h4 class="product_name title-sec font-weight-bold">{{ strtoupper($data['product']->name ?? '-') }} &nbsp;&nbsp;
                            @if($data['product']->id)

                            <?php 
                            $str='';
                            if(isset($data['attrvars'])){
                            if(count($data['attrvars'])==1){
                                $str = $data['attrvars'][0];
                            }else{
                                $str = implode(',', $data['attrvars']);
                            }
                            }
                            else{
                                $str = $data['valuearr'];
                            } 
                            ?>

                            (<span><?php echo get_varient_name($data['svariant']); echo ",". $str; ?></span>)
                            @endif
                        </h4>

                        
                        @else

                        <h4 class="product_name title-sec font-weight-bold">{{ strtoupper($data['product']->name ?? '-') }} &nbsp;&nbsp;
                            @if($data['product']->id)
                            (<span id="product_qty_{{$data['product']->id}}"></span>)
                            @endif
                        </h4>

                        @endif
                        <!-- <p> {{ $data['product']->category_name }} {{ $data['product']->subcategory_name }} </p> -->
                        <!-- <hr class="line1 ml-0"> -->
<!--                         <p class="mt-2 read-more desc">@if(strlen($data['product']->description) > 200) {!! substr($data['product']->description, 0,200) ."..." !!} @else {!! substr($data['product']->description, 0,200) !!} @endif
                            @if(strlen($data['product']->description) > 200)
                                <a class="more-content" href="#desc" id="description">{{__('msg.read_more')}}</a>
                            @endif
                        </p>
 -->
                        <p id="brand_name" class="mb-1">{{ getBrandName($data['product']->brand_id) }}</p>
                        
                        <div class="mb-2"><span class="ratyli" data-rate="{{ceil($data['product']->rating_average[0]->avg_rating)}}" style="font-size:7px;"></span></div>

                        @if(count(getInStockVarients($data['product'])))
                            <!-- <hr class="line1 ml-0"> -->
                            <p id="stock_status"><i class="far fa-check-circle"></i> In Stock</p>
                        <div id="price_flexbox">
                            <div id="yellow_box">
                                <p class="font-weight-bold title-sec currency_value2 product_deal_main" id="price_offer_{{ $data['product']->id }}">{{ Cache::get('currency') }} <span class='value' id="currency_value">@if(!empty($data['product']->attributes)) {{ get_price_varients($data['svariant']) }} @endif </span>
                                </p>
                            </div>
                            <div id="mrp_white_box" >
                                <h6 id="price_mrp_{{ $data['product']->id }}" class="mt-2 mrp_box">MRP <s>{{ Cache::get('currency') }} <span class='value' id="mrp_value">@if(!empty($data['product']->attributes)) {{ get_mrp_varients($data['svariant']) }} @endif</span></s></h6>
                            </div>
                            
                        </div>
                        
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-8 col-12">
                                <div class="product_details_save">
                                <small class="text-success" id="price_savings_{{ $data['product']->id }}">{{__('msg.you_save')}}: {{ Cache::get('currency') }} <span class='value'>@if(!empty($data['product']->attributes)) {{ get_savings_varients($data['svariant'], false) }} @endif</span></small>
                            </div>
                            </div>
                        </div>
                        
                        
                        
                            <!-- <p class="text-muted" id="price_mrp_{{ $data['product']->id }}"><del>{{__('msg.price')}}: <span class='value'></span></del></p> -->
                            <!-- <h5 class="font-weight-bold title-sec" id="price_offer_{{ $data['product']->id }}">{{__('msg.offer_price')}}: {{ Cache::get('currency') }} <span class='value'></span></h5> -->
                            <!-- <h5 class="font-weight-bold" id="price_regular_{{ $data['product']->id }}">{{__('msg.price')}}: <span class='value'></span></h5> -->
                            
                            <div class="form">
                                <form action="{{ route('cart-add') }}" class="addToCart" method="POST">
                                    @csrf
                                    <input type="hidden" name='id' value='{{ $data['product']->id }}'>
                                    <input type="hidden" name="type" value='add'>
                                    <input type="hidden" name="child_id" value="@if(!empty($data['product']->attributes)) {{ $data['svariant']->id }} @endif" id="child_{{ $data['product']->id }}">
                                    <div class="row mt-4">
                                        <div class="button-container col">
                                            <button class="cart-qty-minus button-minus" type="button" id="button-minus" value="-">-</button>
                                            <input class="form-control qtyPicker" id="qtyPicker_{{ $data['product']->id }}" type="number" name="qty" data-min="0" min="1" max="1" data-max="1" value="1">
                                            <button class="cart-qty-plus button-plus" type="button" id="button-plus" value="+">+</button>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="form-group col mt-4">
                                    @if(!empty($data['attributes']) && count($data['attributes'])>0)
                                    
                                        @foreach ($data['attributes'] as $att) 
                                            
                                            <p style="margin-bottom: 0px !important;margin-top: 9px;color:black;font-weight: bold">{{ $att->attribute_name }} : </p>
                                            
                                            @foreach ($data['attribute_values'] as $atv) 
                                                @if($atv->attribute_id == $att->attribute_id)                                                
                                                    @php 
                                                        $slug=$data['product']->slug; 
                                                        $fullurl = Request::fullUrl();
                                                    @endphp

                                                    @if(strpos($fullurl, $att->attribute_name) !== false && strpos($fullurl, $atv->attribute_value) !== false)

                                                    <button type="button" data-att="{{ $att->attribute_name }}" data-atv="{{ $atv->attribute_value }}" class="att_btn att_active variant_btn" data-qry="{{ request()->query($att->attribute_name) }}">{{ $atv->attribute_value }}</button>
                                                    
                                                    @else

                                                        @php
                                                        $varstr = $att->attribute_name . ":" . $atv->attribute_value;

                                                        $attr=$data['svariant']->attributes;
                                                        $flag=0;
                                                        @endphp

                                                        @if(strpos($attr, ' ') !== false)
                                                        
                                                          @php  $exp = explode(' ', $attr); @endphp
                                                            @foreach ($exp as $ex)
                                                                @if($varstr == $ex)
                                                                
                                                                   @php $flag=1; @endphp
                                                                @endif
                                                            @endforeach
                                                        
                                                        @else
                                                            @if($varstr==$attr)
                                                                @php $flag=1; @endphp
                                                            @endif
                                                        
                                                        @endif


                                                        @if(strpos(Request::fullUrl(), '?') !== false)
                                                            @php $cls = ''; @endphp
                                                        @else
                                                            @php $cls = ($flag==1)?"att_active":""; @endphp
                                                        @endif
                                                        
                                                    <button type="button" data-att="{{ $att->attribute_name }}" data-atv="{{ $atv->attribute_value }}" class="att_btn variant_btn {{ $cls }}" data-qry="{{ request()->query($att->attribute_name) }}" >{{ $atv->attribute_value }}</button>
                                                    @endif
                                                    
                                                @endif
                                            @endforeach
                                            
                                        @endforeach
                                    @endif
                                    <input type="hidden" id="fullurl" value="{{Request::fullUrl()}}">
                                        </div>
                                    </div>

                                    @if(empty($data['attributes']))

                                    <div class="row mt-4">
                                        <div class="form-group col">
                                            <div class="btn-group-toggle variant resp_variant" data-toggle="buttons">
                                                @php $firstSelected = true; @endphp
                                                @foreach(getInStockVarients($data['product']) as $v)
                                                    <button class="btn" data-id="{{ $data['product']->id }}">
                                                        <span class="text-dark name">{{ get_varient_name($v) }}</span><br>
                                                        <small> {{__('msg.option_from')}} {{ get_price_varients($v) }}</small>
                                                        <input type="radio" name="options" id="option{{ $v->id }}" value="{{ $v->id }}" data-id='{{ $v->id }}' data-price='{{ get_price_varients($v) }}' data-mrp='{{ get_mrp_varients($v) }}' data-savings='{{ get_savings_varients($v, false) }}' data-stock='{{ intval(getMaxQty($v)) }}' autocomplete="off" data-maxorderquantity="{{ intval($v->max_order_quantity) }}" data-vname="{{ get_varient_name($v) }}">
                                                    </button>
                                                    @if($firstSelected == true)
                                                        {{ $firstSelected = false }}
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="form-group">
                                        @if(intval($data['product']->indicator) == 2)
                                            <img src="{{ asset('images/nonvag.svg') }}" alt="Not Vegetarian Product">
                                            <span class="text-left ml-1"> {{__('msg.not')}} <strong>{{__('msg.vegetarian')}}</strong> {{__('msg.v_product')}}.</span>
                                        @elseif(intval($data['product']->indicator) == 1)
                                            <img src="{{ asset('images/vag.svg') }}" alt="Vegetarian Product">
                                            <span class="text-left ml-1"> {{__('msg.this_is')}} <strong>{{__('msg.vegetarian')}}</strong> {{__('msg.v_product')}}.</span>
                                        @endif
                                    </div>
                                    <div class="form-group text-left add-to-cart1">
                                    <div class="row">
                                        
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-7">
                                                <button type="submit" name="submit" class="btn btn-block text-center submit-btn">
                                                <em class="fa fa-shopping-cart"></em> <span class="text-uppercase ml-2">{{__('msg.add_to_cart')}}</span>
                                            </button>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-5">
                                                <button class="buy-now btn text-center text-uppercase buy-now-btn" type="submit" name="submit" value="buynow"> <span class="buy-now1">{{__('msg.buy_now')}}</span></button>
                                            </div>
                                            
                                            
                                        </div>
                                    </div>
                                    
                                    
                                    
                                </form>
                            </div>
                        @else
                            <span class="sold-out">{{ __('msg.sold_out') }}</span>
                        @endif
                        <form action="{{ route('pincheck') }}" method="post" id="pinForm">
                        <div class="row">
                            <div class="col-md-12">
                                <span style="margin-bottom: 5px !important; margin-top: 20px; display:block;"><strong>Delivery Availablity</strong></span>
                            </div>
                            
                            <div class="col-lg-4 col-md-4 col-sm-6 col-7">
                                <input type="text" class="form-control form-control-pincode" placeholder="Enter Pincode" id="pincode_input" name="pincode_input">
                                @csrf
                                <div id="pinstatus"></div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-5">
                                <button class="btn checkbtn" type="submit" id="checkPin">Check</button>
                            </div>
                            
                        </div>
                        </form>
                        
                        
                        
                         <div class="row card-content text-center policycontent">
                         
                            @if(isset($data['product']->return_status))
                                <div class="card productcard p-2 col-6 col-md-4 col-lg-2 returnpolicy">
                                    @if(intval($data['product']->return_status))
                                        <div class="card-img pb-3">
                                            <span class="creativity">
                                                <img src="{{ asset('images/returnable.png') }}" class="returnable_img" alt="Returnable">
                                            </span>
                                        </div>
                                        <div class="card-box">
                                            <h6 style="font-size: 13px !important" class=" py-2 text-center">{{ Cache::get('max-product-return-days') }}  {{__('msg.days')}} {{__('msg.returnable')}}</h6>
        
                                        </div>
                                    @else
                                        <div class="card-img pb-3">
                                            <span class="creativity">
                                                <img src="{{ asset('images/not-returnable.svg') }}" alt="notReturnable">
                                            </span>
                                        </div>
                                        <div class="card-box">
                                            <h6 style="font-size: 13px !important" class=" py-2 text-center">{{__('msg.not_returnable')}}</h6>
        
                                        </div>
                                    @endif
                                </div>
                            @endif
        
                            @if(isset($data['product']->cancelable_status))
                                <div class="card productcard p-2 col-6 col-md-4 col-lg-2 returnpolicy">
                                    @if(intval($data['product']->cancelable_status))
                                        <div class="card-img pb-3">
                                            <span class="creativity">
                                                <img src="{{ asset('images/cancellable.png') }}" alt="Cancellable">
                                            </span>
                                        </div>
                                        <div class="card-box">
                                            <h6 style="font-size: 13px !important"  class=" py-2 text-center">{{__('msg.order_can_cancel_till_order')}} {{ strtoupper($data['product']->till_status ?? '') }}</h6>
        
                                        </div>
                                    @else
                                        <div class="card-img pb-3">
                                            <span class="creativity">
                                                <img src="{{ asset('images/not-cancellable.svg') }}" alt="notCancellable">
                                            </span>
                                        </div>
                                        <div class="card-box">
                                            <h6 style="font-size: 13px !important" class=" py-2 text-center">{{__('msg.not_cancellable')}}</h6>
        
                                        </div>
                                    @endif
                                </div>
                            @endif
        
                        </div>
                        
                        
                        
                   </div>
                </div>
            </div>



                {{-- return and cancelable --}}
                 <!--returnable and cancelable status-->
        <div class="features1 service-quality padding-bottom">
            <div class=" container text-center justify-content-center">
                <span class="border-line"></span>


            </div>
        </div>
        <!--end returnable and cancelable status-->
        </div>



                        <!--Grid row tab content-->
                <div class="row padding-bottom">
                    <div class="col-md-12 mt-5">
                    
                        @if($data['product']->description != '')
                        <h4 class="title-sec font-weight-bold">Product Details</h4>
                        <hr class="line">
                        <div class="card" style="padding: 3px 10px">
                            {!! $data['product']->description !!}
                        </div>
                        <br>
                        @endif


                        @if($data['product']->feature_details != '')
                        <h4 class="title-sec font-weight-bold">Features & Details</h4>
                        <hr class="line">
                        <div class="card" style="padding: 3px 10px">
                            {!! $data['product']->feature_details !!}
                        </div>
                        <br>
                        @endif
                        
                        
                        @if($data['product']->product_description != '')
                        <h4 class="title-sec font-weight-bold">Product Information</h4>
                        <hr class="line">
                        <div class="card" style="padding: 3px 10px">
                            <div>
                                {!! $data['product']->product_description !!}
                            </div>
                        </div>

                        <br>
                        @endif

                            <h5 class="text-bold text-dark">
                            @if(isset($data['product']->manufacturer) && trim($data['product']->manufacturer) != "")
                                {{__('msg.manufacturer')}} : {{ $data['product']->manufacturer }}
                            @endif
                            </h5>
                            <h5 class="text-bold text-dark">  
                            @if(isset($data['product']->made_in) && trim($data['product']->made_in) != "")
                                {{__('msg.made_in')}} : {{ $data['product']->made_in }}
                            @endif
                                
                            </h5> 
                            <br>

                        @if($data['product']->disclaimer != '')
                        <h4 class="title-sec font-weight-bold">Disclaimer</h4>
                        <hr class="line">
                        <div class="card" style="padding: 3px 10px">
                            {!! $data['product']->disclaimer !!}
                        </div>
                        @endif

                        <input type="hidden" id="prdid" value="{{ $data['product']->id }}">
                        @if(isLoggedIn())
                        <button class="btn btn-primary mt-4" id="rmdl_toggle">Write Review</button>
                        @else
                        <button class="btn btn-primary mt-4" id="no_rmdl">Write Review</button>                        
                        @endif
                        <h4 class="title-sec font-weight-bold" style="display: none">Product Rating</h4>
                        <hr class="line" style="display: none">
                        <div class="card" style="padding: 3px 10px;display: none">
                        <span class="ratyli" data-rate="3"></span>
                        </div>
                        
                        
    
                    </div>
                </div>

        <!-- Dont forget to add Start -->
        @if(isset($data['dont_know_prods']) &&  !empty($data['dont_know_prods']))
            <section class="section-content padding-bottom mt-3 sellproc similarpro">
                <h4 class="title-sec font-weight-bold">Don't Forget To Add
                    <a href="{{ route('shop') }}" class="view title-section viewall  d-none">{{__('msg.view_all')}}</a>
                </h4>
                <hr class="line">
                
                    <div class="rowd ekartd">
                @php   $maxProductShow = get('style_2.max_product_on_homne_page'); @endphp
                <div class="slider-container-box">
                <div class="swiper-container swiper-style-1-slider-container">
                <div class="swiper-wrapper">
                @foreach($data['dont_know_prods'] as $prod)
                
                    @foreach($prod as $p)
                    @if((--$maxProductShow) > -1)
                        <div class="swiper-slide">
                                <figure class="card card-sm card-product-grid card-style-1">
                                    <aside class="add-to-fav add-to-fav-1">
            
                                        <button type="button" class="d-block btn {{ (isset($p->is_favorite) && intval($p->is_favorite)) ? 'saved' : 'save' }}" data-id='{{ $p->id }}' ></button>
                                        @if($p->indicator == 1)
                                            <img style="width:15px;" class="tooltip_new"  src="{{asset('images/vag.svg')}}" data-toggle="tooltip" data-placement="top" title="Vegetarian">
                                            
                                        @elseif($p->indicator == 2)
                                            <img style="width:15px;" class="tooltip_new"  src="{{asset('images/nonvag.svg')}}" data-toggle="tooltip" data-placement="top" title="Non Vegetarian">
                                        @endif
                                    </aside>
                                    <a href="{{ route('product-single', $p->slug) }}" class="img-wrap"> <img src="{{ $p->image }}" alt="{{ $p->name ?? 'Product Image' }}"> </a>
                                    <figcaption class="info-wrap">
                                        <div class="text-wrap p-2 text-left">
                                            <a href="{{ route('product-single', $p->slug) }}" class="title-product product-name">{{ $p->name }}<span class="product_variant_val" id="variant_val_{{ $p->id }}"> : 
                                                
                                                @if(count(getInStockVarients($p)))
                                                    @php
                                                        print_r( get_varient_name($p->variants[0]))
                                                    @endphp
                                                @endif
                                            </span></a>
                            
                                            <span class="text-muted style-desc d-none">
                                                @if(strlen(strip_tags($p->description)) > 20) {!! substr(strip_tags($p->description), 0,20)."..." !!} @else {!! substr(strip_tags($p->description), 0,20) !!} @endif
                                            </span>
                                            <div class="price mt-1 mb-2">
                                                <div class="row">
                                                    <div class="col-8">
                                                        <div class="row">
                                                            <div class="col-6 pr-1">
                                                                <p class="product-price">MRP</p>
                                                                <p class="product-price" id="mrp_{{ $p->id }}">{!! print_mrp($p) !!}</p>
                                                            </div>
                                                            
                                                            <div class="col-6 pl-1">
                                                                <p class="product-price-batras">Batras</p>
                                                                <p class="product-price-batras product-price-batras-spl" id="price_{{ $p->id }}">{!! print_price($p) !!}</p>
                                                            </div>
                                                            <div class="col-12">
                                                                <p class="product-price" style="font-size: 12px;">(Inclusive of all taxes)</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-4">
                                                        <div class="product-savings">
                                                            <p class="ps1">Save</p>
                                                            <p class="ps2" id="save_{{ $p->id }}">₹ {{$p->variants[0]->price - $p->variants[0]->discounted_price}} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </figcaption>
                                    
                                    
                                    
                                    @if(count(getInStockVarients($p)))
                                    <div class="add_cart_style_1">
                                        <form action='{{ route('cart-add-single-varient') }}' method="POST">
                                            <button type="submit" class="btn cart-1 fa fa-shopping-cart"><span>&nbsp;&nbsp;{{__('msg.add_to_cart')}}</span></button>
                                            
                                            <div class="style_1_select">
                                                <input type="hidden" name="id" value="{{ $p->id }}">
                                                <select class="form-control" name="varient" data-id="{{ $p->id }}">
                                                    @foreach(getInStockVarients($p) as $v)
                                                        <option value="{{ $v->id }}"  data-price='{{ get_price(get_price_varients($v)) }}' data-mrp='{{ get_price(get_mrp_varients($v)) }}' data-savings='{{ get_savings_varients($v) }}' data-save='{{ get_save_varients($v, false) }}'>{{ get_varient_name($v) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                    @else
                                    <div class="add_cart_style_1">
                                        <button type="button" disabled="true" class="btn cart-1 fa fa-shopping-cart"><span>&nbsp;&nbsp;{{__('msg.add_to_cart')}}</span></button>
                                    </div>
                                        <span class="sold-out">{{ __('msg.sold_out') }}</span>
                                    @endif
                                    
                                    @if(!count(getInStockVarients($p)) < 1)
                                    <div class="disc-price-tag disc-price-tag-1">
                                        <div class="tag_badge tag-badge-1">
                                            <img class="tag-img img-fluid" src="{{asset('images/offerimg.png')}}">
                                            <p class="tag-text tag-text-1" id="savings_{{ $p->id }}"> {{ get_savings_varients($p->variants[0]) }} </p>
                                        </div>
                                        
                                    </div>
                                    @endif
                                    
                                    
                                </figure>
                            </div>
                    @endif
                    @endforeach
                    
                @endforeach
                </div>
                <div class="swiper-pagination"></div>
        
                </div>
                <div class="swiper-button-prev sites-slider__prev"></div>
                    <div class="swiper-button-next sites-slider__next"></div>
            </div>
                
                </div>
                
            </section>
        @endif
        <!-- Dont forget to add end -->


        <!--similar product-content-->
        @if(isset($data['similarProducts']) &&  !empty($data['similarProducts']))
            <section class="section-content padding-bottom mt-3 sellproc similarpro">

                <h4 class="title-sec font-weight-bold">{{__('msg.similar_products')}}
                    <a href="{{ route('shop', ['category' => $category_slug, 'sub-category' => $subcategory_slug]) }}" class="view title-section viewall">{{__('msg.view_all')}}</a></h4>
                <hr class="line">

                <div class="rowd ekartd">
                @php $maxProductShow = get('style_2.max_product_on_homne_page'); @endphp
                <div class="slider-container-box">
                <div class="swiper-container swiper-style-1-slider-container">
                <div class="swiper-wrapper">
                @foreach($data['similarProducts'] as $p)
                    @if((--$maxProductShow) > -1)
                    
            @foreach($p->variants as $v)
                @php $vid = $v->id; @endphp
            @endforeach
                    
                        <div class="swiper-slide">
                                <figure class="card card-sm card-product-grid card-style-1">
                                    <aside class="add-to-fav add-to-fav-1">
                                        <button type="button" class="d-block btn {{ (isset($p->is_favorite) && intval($p->is_favorite)) ? 'saved' : 'save' }}" data-id='{{ $p->id }}' ></button>
                                        @if($p->indicator == 1)
                                            <img style="width:15px;" class="tooltip_new"  src="{{asset('images/vag.svg')}}" data-toggle="tooltip" data-placement="top" title="Vegetarian">
                                            
                                        @elseif($p->indicator == 2)
                                            <img style="width:15px;" class="tooltip_new"  src="{{asset('images/nonvag.svg')}}" data-toggle="tooltip" data-placement="top" title="Non Vegetarian">
                                        @endif
                                    </aside>
                                    
                                    <a href="{{ route('product-single', $p->slug) }}" class="img-wrap"> <img src="{{ $p->image }}" alt="{{ $p->name ?? 'Product Image' }}"> </a>
                                    <figcaption class="info-wrap">
                                        <div class="text-wrap p-2 text-left">
                                            <a href="{{ route('product-single', $p->slug) }}" class="title-product product-name">{{ $p->name }}<span class="product_variant_val" id="variant_val_{{ $p->id }}"> : 
                                                
                                                @if(count(getInStockVarients($p)))
                                                    @php
                                                        print_r( get_varient_name($p->variants[0]))
                                                    @endphp
                                                @endif
                                            </span></a>
                            
                                            <span class="text-muted style-desc d-none">
                                                @if(strlen(strip_tags($p->description)) > 20) {!! substr(strip_tags($p->description), 0,20)."..." !!} @else {!! substr(strip_tags($p->description), 0,20) !!} @endif
                                            </span>
                                            <div class="price mt-1 mb-2">
                                                <div class="row">
                                                    <div class="col-8">
                                                        <div class="row">
                                                            <div class="col-6 pr-1">
                                                                <p class="product-price">MRP</p>
                                                                <p class="product-price" id="mrp_{{ $p->id }}">{!! print_mrp($p) !!}</p>
                                                            </div>
                                                            
                                                            <div class="col-6 pl-1">
                                                                <p class="product-price-batras">Batras</p>
                                                                <p class="product-price-batras product-price-batras-spl" id="price_{{ $p->id }}">{!! print_price($p) !!}</p>
                                                            </div>
                                                            <div class="col-12">
                                                                <p class="product-price" style="font-size: 12px;">(Inclusive of all taxes)</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-4">
                                                        <div class="product-savings">
                                                            <p class="ps1">Save</p>
                                                            <p class="ps2" id="save_{{ $p->id }}">₹ {{$p->variants[0]->price - $p->variants[0]->discounted_price}} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </figcaption>
                                    
                                    
                                    
                                    @if(count(getInStockVarients($p)))
                                    <div class="add_cart_style_1">
                                        <form action='{{ route('cart-add-single-varient') }}' method="POST">
                                            <button type="submit" class="btn cart-1 fa fa-shopping-cart"><span>&nbsp;&nbsp;{{__('msg.add_to_cart')}}</span></button>
                                            
                                            <div class="style_1_select">
                                                <input type="hidden" name="id" value="{{ $p->id }}">
                                                <select class="form-control" name="varient" data-id="{{ $p->id }}">
                                                    @foreach(getInStockVarients($p) as $v)
                                                        <option value="{{ $v->id }}"  data-price='{{ get_price(get_price_varients($v)) }}' data-mrp='{{ get_price(get_mrp_varients($v)) }}' data-savings='{{ get_savings_varients($v) }}' data-save='{{ get_save_varients($v, false) }}'>{{ get_varient_name($v) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                    @else
                                    <div class="add_cart_style_1">
                                        <button type="button" disabled="true" class="btn cart-1 fa fa-shopping-cart"><span>&nbsp;&nbsp;{{__('msg.add_to_cart')}}</span></button>
                                    </div>
                                        <span class="sold-out">{{ __('msg.sold_out') }}</span>
                                    @endif
                                    
                                    @if(!count(getInStockVarients($p)) < 1)
                                    <div class="disc-price-tag disc-price-tag-1">
                                        <div class="tag_badge tag-badge-1">
                                            <img class="tag-img img-fluid" src="{{asset('images/offerimg.png')}}">
                                            <p class="tag-text tag-text-1" id="savings_{{ $p->id }}"> {{ get_savings_varients($p->variants[0]) }} </p>
                                        </div>
                                        
                                    </div>
                                    @endif
                                    
                                    
                                </figure>
                            </div>
                    @endif
                @endforeach
                </div>
                <div class="swiper-pagination"></div>
        
                </div>
                <div class="swiper-button-prev sites-slider__prev"></div>
                    <div class="swiper-button-next sites-slider__next"></div>
                </div>
            </div>
            
            <hr>
            </section>
        @endif
        <!--end similar product content-->
        
        
        <!--Review Section Start-->
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div>
                            <h4 class="title-sec font-weight-bold my-4">Customer Review</h4>
                            <div class="my-3">
                                @forelse($data['product']->reviews as $review)
                                    <div class="mt-3 mb-5">
                                        <div class="mb-1"><div style="width:35px; height:35px;border-radius:35px; display: inline-block; "><img src="{{Config::get('ekart.asset_url_upload') .'avatars/'. $review->profile_img}}" class="img-fluid rounded-circle"></div><span class="mb-1" style=" color:#000;">{{$review->name}}</span></div>
                                        <div><span>Reviewed on {{ Carbon\Carbon::parse($review->created_at)->diffForHumans() }}</span></div>
                                        <div><span class="ratyli" data-rate="{{$review->rating}}" style="font-size:7px;"></span> <span style="font-size:16px;font-weight:700; color:#000;">{{$review->title}}</span></div>
                                        <p class="mb-1" style=" color:#000;">{{$review->review}}</p>
                                    </div>
                                @empty
                                <div class="mt-3 mb-5">
                                    <h5>No Product Review Available</h5>
                                </div>
                                @endforelse
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Review Section End-->
    </div>
    <!-- The Modal -->
<div class="modal fade" id="reviewModal">
    <style>
        .ratyli i{
            color:#FF990E !important;
        }
    </style>
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Give Product Review</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="alert alert-success" id="status_suc" style="display: none">Your Review has been Sent Successfully</div>
        <form action="{{ route('reviewadd') }}" method="post" id="reviewForm">
        <div class="form-group">
            <label>Title</label>
            <input type="text" name="title" class="form-control" required>
        </div>
        <div class="form-group">
            <label>Review</label>
            <textarea name="review" rows="5" cols="5" class="form-control" required></textarea>
        </div>
        <div class="form-group">
        <label>Rate Product : </label>
        <span class="ratyli" data-rate="3"></span>        
        <input type="hidden" name="rating" value="3" id="rating">            
        </div>
        <div class="form-group">
            <input type="hidden" name="product_id" value="{{ $data['product']->id }}">
            <button class="btn btn-success" type="submit">Submit</button>
        </div>
        </form>

      </div>

    </div>
  </div>
</div>
</div>
<!-- end product detail page -->
<script>
        $(".variant_btn").on('click',function(){
        var fullurl=$("#fullurl").val();
        var att = $(this).attr('data-att');
        var atv = $(this).attr('data-atv');
        var qry = $(this).attr('data-qry');
        var newurl='';

            if(!fullurl.includes('?'))
            {
                newurl=`${fullurl}?${att}=${atv}`;
                window.location.href=newurl;
            }
            else{

            if(qry!='' && qry!=atv)
            {
                newurl=fullurl.replace(qry, atv);
            }
            else{
                newurl=fullurl;
            }

            if(qry!='' && qry!=att){}
            else{
                newurl=`${fullurl}&${att}=${atv}`;
            }

            window.location.href=newurl;

            }
        // console.log('hey')
        // console.log(`${fullurl}?${att}=${atv}`);
     //    var finalurl='';
     //    if(!fullurl.includes(att))
     //    {
     //        finalurl = `${fullurl}`;
     //    }
    });

</script>